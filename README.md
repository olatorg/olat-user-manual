# OLAT User Manual

This is the user manual project for [OLAT](https://www.olat.org/).

## Usage

1. Install [Python](https://www.python.org).

    ```shell
    brew install python
    ```

2. Install [MkDocs](https://www.mkdocs.org) and all required dependencies. Make sure a [GitHub token](https://docs.github.com/en/authentication/keeping-your-account-and-data-secure/managing-your-personal-access-tokens)
   is provided to access [Material for MkDocs Insiders](https://squidfunk.github.io/mkdocs-material/insiders/getting-started/#requirements).

   ```shell
   GH_TOKEN=some-token pip install -r requirements.txt
   ```

3. Build project.

   ```shell
   mkdocs serve
   ```

4. Open a browser and visit [http://127.0.0.1:8000](http://127.0.0.1:8000).

## Troubleshoot

- Error `error: externally-managed-environment` when installing MkDocs.

   This is a [feature of newer Python releases](https://peps.python.org/pep-0668/).
   It is required to use [venv](https://docs.python.org/3/library/venv.html).

   ```shell
   python3 -m venv .venv
   source .venv/bin/activate
   GH_TOKEN=some-token python3 -m pip install -r requirements.txt
   ```

   See [https://stackoverflow.com/a/75696359](https://stackoverflow.com/a/75696359).
