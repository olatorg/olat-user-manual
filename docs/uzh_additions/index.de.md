# UZH-Erweiterungen

Die Basis von OLAT ist OpenOlat, daher finden Sie einen grossen Teil der Dokumentation zu OLAT unter dem Reiter [OpenOlat](../../de/manual_user/). Die von der Universität Zürich vorgenommenen Anpassungen und Erweiterungen sind hier aufgelistet:

## Registrierung und Login, Campuskurse

* [Registrierung, An- und Abmeldung](registration/)
[//]: # ( * [Semester Checkliste für UZH-Dozierende](semesterchecklist_for_lecturers/) )
* [Campuskurse (nur UZH)](campus_course/)
* [Delegation (nur UZH)](delegation/)

## UZH-Kursbausteine und Kurstools

* [Kaltura](kaltura/)
* [Literatur](literature/)
* [OnlyOffice](onlyoffice/)
* [Peer Review](peer_review/)
* [Programmieren in ACCESS](access/)
* [Zutrittskontrolle](entry_control/)
* [Unterstützende Technologien](supporting_technologies/)

## Kursverwaltung und Kurszugang

* [FAQs Kurse](faqs_courses/)
* [Zugriffsbeschränkungen im Expertenmodus](access_restrictions_in_expert_mode/)

## Anwendungsbeispiele

* [Anwendungsbeispiele](example_of_use)


