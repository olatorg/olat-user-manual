---
search:
    exclude: true
---

# Access restrictions in expert mode

  * [Getting started with expert rules](#getting-started-with-expert-rules)
  * [Configuration of Expert Rules](#configuration-of-expert-rules)
    * [Working with the Constants "TRUE" and "FALSE"](#working-with-the-constants-true-and-false)
  * [Use of AAI attributes](#use-of-aai-attributes)
    * [Utilization](#utilization)

* * *

!!! info ""
    The settings in the tabs "Visibility" and "Access" are only available in
    [conventional courses](../campus_course/learning_path_vs_conventional_courses.md).
    This means that the expert mode can only be used in conventional courses and
    not in "[Learning path Courses](../campus_course/learning_path_vs_conventional_courses.md)".

## Getting started with expert rules

In the tabs "Visibilty" and "Access" of conventional courses, you can
configure additional preferences for many course elements. For instance, you
can block a course element for course participants, grant access only for
certain groups or unlock it depending on the date.

In case of more complicated visibility or access rules you can use the expert
mode, thus enabling you to configure visibility and access of course elements
as required. You can e.g. limit access to a course element to specific user
names, link several types of restrictions to each other or work with relative
date values. The following example illustrates this:

!!! info "Example"
    You have activated the course element depending on the date so that you no
    longer have to worry about it during the course. In the tabs Visibility and
    Access of the course element, you can enter the exact start and end date in
    simple mode. If you want to use this course again in another semester, you
    have to change the date so that the module is activated. This is feasible if
    only one course element is subject to such a rule. If there are several such
    rules, expert rules can relieve you of this work: You simply enter a relative
    date for the respective course element that refers to the start or end date of
    the course.

    Simplified, this means that e.g. the course element will not be unlocked from
    01.12.2021 to 31.12.2021, but at the "end of course", and exactly at the "end
    of course plus 7 days" it will no longer be accessible. Thus, when using the
    course again, you only have to change the course start and end dates.

Expert rules primarily serve to save you time and work or just simplify it.
Therefore, it is worth the effort to take a closer look at them. Just like any
language, expert rules follow a syntax. OpenOlat will indicate an error should
you make a syntactic mistake. This is very helpful, especially at the
beginning if one does not have any or just little programming skills. Expert
rules verify if a certain attribute is TRUE or FALSE.

As an introduction to the syntax of expert rules, you should at first define a
rule in the simple mode. For example, you may generate a "single page“, and
click on "Blocked for learners“ in the "Access" tab.

Then click on "Display expert mode“ and see your first expert rule:
    
    (  ( isCourseCoach(0) | isCourseAdministrator(0) ) )

The whole term is enclosed in double brackets. The two outer brackets can be
omitted in this case. Just try it out. The vertical line in the center "|“ is
the Boolean operator OR and connects the course coach with the course
administrator. Both of them have exclusive access to the "single page“.

Now change the Boolen Operator into "&“:

    isCourseCoach(0) & isCourseAdministrator(0)

This rule grants access exclusively to those course coaches who are also
course administrators. This preference is only possible in the expert mode.

You can try out any number of scenarios and insert further attributes and
operators. In this chapter you will find further attributes and examples
illustrating their meaning to help you become more acqainted with expert
rules.

## Configuration of Expert Rules

Expert rules certify if there is an attribute with a specific value.

| Attribute | Description | Example Expert rule |
|---|---|---|  
| isGuest | accessible only for guests| isGuest(0) |
| isCourseCoach | available only for coach | isCourseCoach(0) |
| isUser | available only for one specific user | isUser("pmuster") |
  
### Working with the Constants "TRUE" and "FALSE"

By using the constants "true“ and "false“, the existence (“true” = “1”) or
non-existence (“false” = “0”) of an attribute can be verified. In this case,
we refer to a so-called Boolean Variable (named after George Boole, the father
of the Boolean Algebra). These variables can only take a limited number of
values or states. In our specific case, the variable can only take the two
values (“true” = “1” = existing or “false” =”0” = non-exisiting).

!!! info ""
    To give a practical example in our OLAT context, we will use a simple expert
    rule for managing the access to a course:

      *  **Case 1:** Only guest-users get access to the course. The respective user therefore only gets access if the attribute “isGuest” is true. There are three alternatives for this expert rule:  
    **isGuest(0)** oder **isGuest(0)=1** oder **isGuest(0)=true**

      *  **Case 2: **In this case we want guest-users not to have access. The respective user therefore only gets access if the attribute “isGuest” is false. There are two alternatives for this expert rule:  
    **isGuest(0)=0** oder **isGuest(0)=false**

An extensive list of all relevant components needed for applying expert rules
can be found in the following box.

!!! info "Functions, operators and other components of the expert rules"
    <table>
        <thead>
            <tr style="background-color: #F4F5F7;">
                <th>Type</th>
                <th>Syntax</th>
                <th>Meaning</th>
            </tr>
            <tr>
                <td rowspan="3" style="background-color: #F4F5F7;">
                    <strong>Constants</strong>
                </td>
                <td>
                    <em>TRUE</em>&nbsp;or&nbsp;<em>1</em>
                </td>
                <td>True</td>
            </tr>
            <tr>
                <td>
                    <em>FALSE</em>&nbsp;or&nbsp;<em>0</em>
                </td>
                <td>False</td>
            </tr>
            <tr>
                <td><em>ANY_COURSE</em></td>
                <td>Query should be applied to
                    every course (only for isCourseAdministrator(), isCourseCoach(),
                        isCourseParticipant())</td>
            </tr>
            <tr>
                <td><strong>Variable</strong>
                </td>
                <td><em>now</em></td>
                <td>Actual time of server
                    system</td>
            </tr>
            <tr>
                <td rowspan="34" style="background-color: #F4F5F7;">
                    <strong>Functions</strong>
                </td>
                <td>
                    <em>date("</em>[date]<em>")</em>
                </td>
                <td>Retrieve date</td>
            </tr>
            <tr>
                <td>
                    <em>inLearningGroup("</em>[string]<em>")</em>
                </td>
                <td>Generates TRUE for all
                    members of the learning group [string]</td>
            </tr>
            <tr>
                <td>
                    <em>inRightGroup("</em>[string]<em>")</em>
                </td>
                <td>Generates TRUE for all
                    group members with the same rights [string]</td>
            </tr>
            <tr>
                <td>
                    <em>isLearningGroupFull("</em>[string]<em>")</em>
                </td>
                <td>Generates the boolean TRUE
                    (= full) or FALSE (= vacancies) for the learning group indicated.</td>
            </tr>
            <tr>
                <td>
                    <em>isUser("</em>[string]<em>")</em>
                </td>
                <td>Results in TRUE for users
                    with the user name [string]</td>
            </tr>
            <tr>
                <td>
                    <em>inLearningArea("</em>[string]<em>")</em>
                </td>
                <td>Generates TRUE for all
                    group members in the learning area [string]</td>
            </tr>
            <tr>
                <td><em>isGlobalAuthor(0)</em>
                </td>
                <td>Generates TRUE for all
                    members with OLAT authoring rights</td>
            </tr>
            <tr>
                <td>
                    <em>isCourseAdministrator(0)</em>
                </td>
                <td>Generates TRUE for all
                    owners of a course (learning resource)</td>
            </tr>
            <tr>
                <td>
                    <em>isCourseAdministrator(<em>ANY_COURSE</em>)</em>
                </td>
                <td>Generates TRUE for all
                    users which have owner rights on at least one course on the system</td>
            </tr>
            <tr>
                <td><em>isCourseCoach(0)</em>
                </td>
                <td>Generates TRUE for all
                    users supervising a learning group or are supervising the course</td>
            </tr>
            <tr>
                <td>
                    <em>isCourseCoach(<em>ANY_COURSE</em>)</em>
                </td>
                <td>Generates TRUE for
                        all users supervising at least one learning group of a course or are supervising at
                        lease one course on the system</td>
            </tr>
            <tr>
                <td>
                    <em>isCourseParticipant(0)</em>
                </td>
                <td>
                    Generates TRUE for all participants of this course
                </td>
            </tr>
            <tr>
                <td>
                    <em>isCourseParticipant(<em>ANY_COURSE</em>)</em>
                </td>
                <td>
                    Generates TRUE for all users on the system that
                        participate in at least one course
                </td>
            </tr>
            <tr>
                <td><em>isGuest(0)</em></td>
                <td>Generates TRUE for all
                    users visiting OLAT as guests</td>
            </tr>
            <tr>
                <td>
                    <em>hasAttribute("</em>[AttrName]<em>","</em>[string]<em>")</em>
                </td>
                <td>Generates TRUE, if
                    [string] corresponds to the relevant user's value of the AAI attribute [AttrName] .</td>
            </tr>
            <tr>
                <td>
                    <em>isInAttribute("</em>[AttrName]<em>","</em>[substring]<em>")</em>
                </td>
                <td>Generates TRUE, if
                    [substring] corresponds to part of the relevant user's value of the AAI attribute
                    [AttrName].<br>[General information on AAI](https://www.switch.ch/aai/){:target="\_blank"}<br>
                        [http://www.switch.ch/aai/docs/AAI_Attr_Specs.pdf](Specification of AAI attributes (pdf file)){:target="\_blank"}</td>
            </tr>
            <tr>
                <td>
                    <em>getUserProperty("userPropertyname")</em>
                </td>
                <td>Generates the value of a
                    specific user attribute. By means of "=" this value can be compared to another fixed
                    value.</td>
            </tr>
            <tr>
                <td>
                    <em>getPassed("</em>[integer]<em>")</em>
                </td>
                <td>Generates the Boolean TRUE
                    (=Passed) or FALSE (=Failed) from a course element with specified ID</td>
            </tr>
            <tr>
                <td>
                    <em>getScore("</em>[integer]<em>")</em>
                </td>
                <td>Generates the score from a
                    course element with specified ID</td>
            </tr>
            <tr>
                <td>
                    <em>getAttempts("</em>[integer]<em>")</em>
                </td>
                <td>Generates the number of
                    completed attempts from a course element with specified ID. Can be applied to course
                    elements of the
                    type <em>Test</em>, <em>Self-test</em>
                    (possible return values 0 or 1) and <s><em>Task
                            (deprecated)</em></s> (return value = number of files handed
                    in).</td>
            </tr>
            <tr>
                <td>
                    <em>getLastAttemptDate("</em>[integer]<em>")</em>
                </td>
                <td>Generates the date of the
                    last attempt from a course element with the specified ID. Can be applied like the
                    getAttempts method.</td>
            </tr>
            <tr>
                <td>
                    <em>getInitialEnrollmentDate("</em>[integer]<em>")</em>
                </td>
                <td>Generates the date of the
                    first registration of the relevant course participant from the course
                    element <em>Enrolment</em> with specified ID.</td>
            </tr>
            <tr>
                <td>
                    <em>getRecentEnrollmentDate("</em>[integer]<em>")</em>
                </td>
                <td>Generates the date of the
                    last registration of the relevant course participant from the course
                    element <em>Enrolment</em> with specified ID.</td>
            </tr>
            <tr>
                <td>
                    <em>getInitialCourseLaunchDate(0)</em>
                </td>
                <td>Generates the date of a
                    course participant's first course attendance.</td>
            </tr>
            <tr>
                <td>
                    <em>getRecentCourseLaunchDate(0)</em>
                </td>
                <td>Generates the date of a
                    course participant's last course attendance.</td>
            </tr>
            <tr>
                <td>
                    <em>getPassedWithCourseId("</em>[integer-1]<em>","</em>[integer-2]<em>")</em>
                </td>
                <td>Generates the Boolean TRUE
                    (=Passed) or FALSE (=Failed) from the course element ID=[integer-2] of the course
                    ID=[integer-1]</td>
            </tr>
            <tr>
                <td>
                    <em>getScoreWithCourseId("</em>[integer-1]<em>","</em>[integer-2]<em>")</em>
                </td>
                <td>Generates the score from
                    the course element ID=[integer-2] of the course ID=[integer-1]</td>
            </tr>
            <tr>
                <td>
                    <em>hasUserProperty("</em>[userPropertyname]<em>","</em>[string]<em>")</em>
                </td>
                <td>Generates TRUE, if
                        [string] corresponds to the relevant user's value of the userproperty
                        [<em>userPropertyname</em>] .</td>
            </tr>
            <tr>
                <td>
                    <em>userPropertyStartswith("</em>[userPropertyname]<em>","</em>[substring]<em>")</em>
                </td>
                <td>Generates TRUE,
                        if the userproperty [<em>userPropertyname</em>]
                            starts
                            with [substring].
                </td>
            </tr>
            <tr>
                <td>
                    <em>userPropertyEndswith("</em>[userPropertyname]<em>","</em>[substring]<em>")</em>
                </td>
                <td>Generates TRUE, if
                        the userproperty [<em>userPropertyname</em>] ends with
                        [substring].</td>
            </tr>
            <tr>
                <td>
                    <em>isInUserProperty("</em>[userPropertyname]<em>","</em>[substring]<em>")</em>
                </td>
                <td>Generates TRUE, if
                        [substring] corresponds to part of the relevant user's value of
                        the userproperty
                        [<em>userPropertyname</em>].</td>
            </tr>
            <tr>
                <td>
                    <em>isNotInUserProperty("</em>[userPropertyname]<em>","</em>[substring]<em>")</em>
                </td>
                <td>Generates TRUE, if
                        [substring] does not show up in the value of the userproperty
                        [<em>userPropertyname</em>].</td>
            </tr>
            <tr>
                <td>
                    <em>hasNotUserProperty("</em>[userPropertyname]<em>","</em>[string]<em>")</em>
                </td>
                <td>Generates TRUE, if
                        [string] does not corresponds to the relevant user's value of the userproperty
                        [<em>userPropertyname</em>].</td>
            </tr>
            <tr>
                <td><em>hasLanguage("de")</em></td>
                <td>Generates TRUE if the
                    respective user has set German as the system language. For English, replace "de" with
                    "en".</td>
            </tr>
            <tr>
                <td rowspan="5" style="background-color: #F4F5F7;">
                    <strong>Units</strong>
                </td>
                <td><em>min</em></td>
                <td>Minutes</td>
            </tr>
            <tr>
                <td><em>h</em></td>
                <td>Hours</td>
            </tr>
            <tr>
                <td><em>d</em></td>
                <td>Days</td>
            </tr>
            <tr>
                <td><em>w</em></td>
                <td>Weeks</td>
            </tr>
            <tr>
                <td><em>m</em></td>
                <td>Months</td>
            </tr>
            <tr>
                <td rowspan="9" style="background-color: #F4F5F7;">
                    <strong>Operators</strong>
                </td>
                <td>=</td>
                <td>equal</td>
            </tr>
            <tr>
                <td>&gt;</td>
                <td>greater than</td>
            </tr>
            <tr>
                <td>&lt;</td>
                <td>less than</td>
            </tr>
            <tr>
                <td>&gt;=</td>
                <td>greater/equal</td>
            </tr>
            <tr>
                <td>&lt;=</td>
                <td>less/equal</td>
            </tr>
            <tr>
                <td>*</td>
                <td>Multiplication</td>
            </tr>
            <tr>
                <td>/</td>
                <td>Division</td>
            </tr>
            <tr>
                <td>+</td>
                <td>Addition</td>
            </tr>
            <tr>
                <td>-</td>
                <td>Subtraction</td>
            </tr>
            <tr>
                <td rowspan="2" style="background-color: #F4F5F7;">
                    <strong>Booleans</strong>
                </td>
                <td>&amp;</td>
                <td>Logical AND</td>
            </tr>
            <tr>
                <td>|</td>
                <td>Logical OR</td>
            </tr>
        </thead>
    </table>


!!! info "User attributes (UserProperty)"
    Various expert rules require the use of user attributes in order to filter
    eligible users for access content. Those rules enable to limit access rights
    depending on name, gender, address, field of studies and so on. Those user
    attributes are usually visible in the user profile.

    OpenOlat provides standardized terms for those attributes. The following
    expert rules require the use of user attributes:

      * getUserProperty _( "[userPropertyname]")_
      * hasUserProperty("[ _userPropertyname] ", "[string]")_
      * userPropertyStartswith(" [ _userPropertyname ]", "[substring]")_
      * userPropertyEndswith(" [ _userPropertyname ]", "[substring]")_
      * isInUserProperty(" [ _userPropertyname ]", "[substring]")_
      * isNotInUserProperty("[ _userPropertyname ]", "[substring]")_
      * hasNotUserProperty("[ _userPropertyname ]", "[string]")_

    The following user attributes are available in OpenOlat. Please note that
    access restrictions using user attributes can only be successful if those user
    attributes are used and generally filled in throughout your system. Simply
    check your user profile in the the personal menu in Configuration/Profile for
    available user attributes. For questions, please contact your system
    administrator.

    <table>
        <thead>
            <tr style="background-color: #F4F5F7;">
                <th colspan="2">User data</th>
                <th colspan="2">Contact data</th>
                <th colspan="2">Address data</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <th style="background-color: #F4F5F7;">userName</th>
                <td>User name</td>
                <th style="background-color: #F4F5F7;">telPrivate</th>
                <td>Phone number private</td>
                <th style="background-color: #F4F5F7;">street</th>
                <td>Street</td>
            </tr>
            <tr>
                <th style="background-color: #F4F5F7;">firstName</th>
                <td>First name</td>
                <th style="background-color: #F4F5F7;">telMobile</th>
                <td>Phone number mobile</td>
                <th style="background-color: #F4F5F7;">extendedAddress</th>
                <td>Extra address line</td>
            </tr>
            <tr>
                <th style="background-color: #F4F5F7;">lastName</th>
                <td>Last name</td>
                <th style="background-color: #F4F5F7;">telOffice</th>
                <td>Phone number office</td>
                <th style="background-color: #F4F5F7;">poBox</th>
                <td>P.O.Box</td>
            </tr>
            <tr>
                <th style="background-color: #F4F5F7;">email</th>
                <td>E-mail address</td>
                <th style="background-color: #F4F5F7;">skype</th>
                <td>Skype ID</td>
                <th style="background-color: #F4F5F7;">zipCode</th>
                <td>Zip code</td>
            </tr>
            <tr>
                <th style="background-color: #F4F5F7;">creationDateDisplayProperty
                </th>
                <td>User creation date</td>
                <th style="background-color: #F4F5F7;">xing</th>
                <td>Xing</td>
                <th style="background-color: #F4F5F7;">region</th>
                <td>Region / Canton</td>
            </tr>
            <tr>
                <th style="background-color: #F4F5F7;">lastloginDateDisplayProperty
                </th>
                <td>User last login</td>
                <th style="background-color: #F4F5F7;">homepage</th>
                <td>Homepage</td>
                <th style="background-color: #F4F5F7;">city</th>
                <td>City</td>
            </tr>
            <tr>
                <th style="background-color: #F4F5F7;">birthDay</th>
                <td>Date of birth</td>
                <th style="background-color: #F4F5F7;"><br></th>
                <td><br></td>
                <th style="background-color: #F4F5F7;">country</th>
                <td>Country</td>
            </tr>
            <tr>
                <th style="background-color: #F4F5F7;">gender</th>
                <td>Gender</td>
                <th style="background-color: #F4F5F7;"><br></th>
                <td><br></td>
                <th style="background-color: #F4F5F7;">countryCode</th>
                <td>Country code</td>
            </tr>
            <tr>
                <td colspan="6"><br></td>
            </tr>
            <tr style="background-color: #F4F5F7;">
                <th colspan="2">Organization</th>
                <th colspan="2">Professional contact data</th>
                <th colspan="2">Varia</th>
            </tr>
            <tr>
                <th style="background-color: #F4F5F7;">institutionalName</th>
                <td>Institution</td>
                <th style="background-color: #F4F5F7;">department</th>
                <td>
                    <p>Department / Company</p>
                </td>
                <th style="background-color: #F4F5F7;">typeOfUser</th>
                <td>
                    <p>Type of user</p>
                </td>
            </tr>
            <tr>
                <th style="background-color: #F4F5F7;">institutionalUserIdentifier</th>
                <td>Institution identifier<br>(registration
                    number)</td>
                <th style="background-color: #F4F5F7;">officeStreet</th>
                <td>
                    <p>Address / P.O. box</p>
                </td>
                <th style="background-color: #F4F5F7;">rank</th>
                <td>
                    <p>Service grade / employment title</p>
                </td>
            </tr>
            <tr>
                <th style="background-color: #F4F5F7;">institutionalEmail</th>
                <td>
                    <p>Institutional e-mail</p>
                </td>
                <th style="background-color: #F4F5F7;">extendedOfficeAddress</th>
                <td>
                    <p>Extended office address</p>
                </td>
                <th style="background-color: #F4F5F7;">socialSecurityNumber</th>
                <td>
                    <p>Social security number</p>
                </td>
            </tr>
            <tr>
                <th style="background-color: #F4F5F7;">orgUnit</th>
                <td>
                    <p>Organizational unit /<br>study group</p>
                </td>
                <th style="background-color: #F4F5F7;">officeZipCode</th>
                <td>
                    <p>Office ZIP</p>
                </td>
                <th style="background-color: #F4F5F7;">degree</th>
                <td>
                    <p>Academic degree</p>
                </td>
            </tr>
            <tr>
                <th style="background-color: #F4F5F7;">studySubject</th>
                <td>
                    <p>Field of studies</p>
                </td>
                <th style="background-color: #F4F5F7;">officeCity</th>
                <td>
                    <p>Office city</p>
                </td>
                <th style="background-color: #F4F5F7;">position</th>
                <td>
                    <p>Role / position</p>
                </td>
            </tr>
            <tr>
                <th style="background-color: #F4F5F7;">graduation</th>
                <td>Graduation year</td>
                <th style="background-color: #F4F5F7;">officeCountry</th>
                <td>
                    <p>Office country</p>
                </td>
                <th style="background-color: #F4F5F7;">userInterests</th>
                <td>Expertise</td>
            </tr>
            <tr>
                <th style="background-color: #F4F5F7;"><br></th>
                <td><br></td>
                <th style="background-color: #F4F5F7;">officeMobilePhone</th>
                <td>
                    <p>Office mobile phone</p>
                </td>
                <th style="background-color: #F4F5F7;"><br></th>
                <td><br></td>
            </tr>
        </tbody>
    </table>  
  
    Examples on how to apply "getUserProperty":

      * Only course participants of a specific field of study should be granted access:
        
            getUserProperty("studySubject") = "Mechanical Engineering"

    Now anybody who needs access must first complete the field "field of study" in
    their profile and state it as "Mechanical Engineering".

      * The other way round, should you intend to grant access only to those who have not stated their field of study in their profile, you can express the corresponding rule as follows:
        
            getUserProperty("studySubject") = ""

      * Should you want to grant access only course participants who have completed the field of study in their profile (no matter what the study subjects are), the rule can be defined as follows:
        
            getUserProperty("studySubject") = "" = false

    or

            getUserProperty("studySubject") = "" = 0

There are various options to interrelate single rules to each other. The two
most important operators to combine attributes are:

  * AND conjunction: &
  * OR conjunction: |

Please note that an OR conjunction precedes an AND conjunction. In order to
handle an AND conjunction first you have to use brackets.

Example: The expert rule (inGroup("Participants IntensiveCourse") |
isCourseCoach(0)) means that either participants of an intensive course or all
coaches of groups will have access to a course element.

Some examples are listed below in order to show you how to use the expert
syntax.

  

!!! info "Examples of expert rules in the tabs "Visibility", "Access" and "Score" (structural elements)"

    **inLearningGroup( "Amateur") = 0**  
    With the exception of the group _«Amateur»_ this course element is visible for
    all participants.  

    * * *

    **(now >= date("22.03.2018 12:00")) & (now <= date("23.08.2018 18:00")) |
    inLearningGroup("Tutor")**  
    This course element is visible for all participants between 22-3-2018 and
    23-8-2018. For members of the learning group _«Tutor»_ it is always visible.  

    * * *

    **(now >= date("03.09.2018 00:00")) & (now <= date("13.10.2018 00:00")) &
    inRightGroup("Assessors")| isUser("kmiller")**  
    This course element is visible for all participants of the right group
    _«Assessors»_ between 3-9-2018 and 13-10-2018. For the person with the user
    name _«kmiller»_ it is always visible.  

    * * *

    **hasAttribute( "swissEduPersonStudyBranch3","6200")**  
    Only students of human medicine have access to this course element.  
    See also:  
    AAI attributes  

    * * *

    **hasAttribute( "swissEduPersonHomeOrganization","uzh.ch")**  
    Only students of the University of Zurich have access to this course element.  
    See also:  
    AAI attributes  

    * * *

    **isInAttribute( "surname","Mue")**  
    Generates TRUE for all persons whose attribute _surname_ contains the letter
    sequence "Mue". E.g. gives TRUE for the value "Mueller" or "Muehlebacher"  
    See also:  
    AAI attributes  

    * * *

    **isInAttribute( "eduPersonEntitlement","https://vam.uzh.ch/")**  
    Generates TRUE for all persons whose attribute _eduPersonEntitlement_ contains
    the value "https://vam.uzh.ch/". E.g. gives TRUE for the value
    "https://vam.uzh.ch/surgery"  
    See also:  
    AAI attributes  

    * * *

    **(getUserProperty( "orgUnit") = "Sales")**  
    Checks if a person is part of the organizational unit 'Sales.' This can be
    useful if e.g. data are automatedly transferred from LDAP.  

    * * *

    **(getPassed( "69742969114730") | getPassed("69742969115733") |
    getPassed("69742969118009")) * 10**  
    This rule is set in the tab _«Score»_ -> _«Processing score»_ of the course
    element _Structure_ . The course element _Structure_ shows 10 points if one of
    the tests (course element IDs "69742969114730", "69742969115733" or
    "69742969118009") was passed. Otherwise 0 points.  

    * * *

    **(getScore( "69742969114730") + getScore("69742969115733") +
    getScore("69742969118009")) >= 140 | getPassed("69978845384688")**  
    This rule is set in the tab _«Score»_ -> _«Passed if»_ of the course element
    _Structure_ . The course element _Structure_ shows _«Passed»_ , if a minimum
    of 140 points in all tests is achieved or if _«Passed»_ is entered manually.
    (Element _Assessment_ with ID "69978845384688").  

    * * *

    **getAttempts( "70323786958847") > 0**  
    Generates TRUE, as soon as the relevant course participant has completed the
    test with specified ID for the first time.  

    * * *

    **getAttempts( "70323524635734") <= 3**  
    Generates FALSE, as soon as the relevant course participant has put more than
    3 files into the storage folder of the course element ~~_Task (deprecated)_~~
    .  

    * * *

    **getLastAttemptDate( "70323524635734") + 24h < now**  
    Generates TRUE when the last test attempt is older than 24 hours  

    * * *

    **getInitialEnrollmentDate( "70323786958847") <= date("26.5.2005 18:00")**  
    Generates TRUE for those participants who enrolled in an available group
    before 6 p.m. on May 26th, 2005, by means of the course element _Enrolment_
    with specified ID.  

    * * *

    **getInitialEnrollmentDate( "70323786958847") + 2h > now**  
    Generates TRUE within two hours starting at the moment of registration for
    those participants who have enrolled in an available group by means of the
    course element _Enrolment_ with specified ID. This way it is clear that every
    participant can only work on e.g. a script within a particular time frame.  

    * * *

    **(getInitialCourseLaunchDate(0) >= never) | (getInitialCourseLaunchDate(0) +
    2h > now)**  
    Generates TRUE if a course participant has not yet taken any courses or during
    the first two hours after taking a course. This way it is possible represent
    that each course participant can only see courses for a certain period of
    time.  

    * * *

    **(getRecentCourseLaunchDate(0) + 10min < now)**  
    Generates TRUE if a user is active for more than 10 min within a course.  

    * * *

    **(getCourseBeginDate(0) <= today) & (getCourseEndDate(0) >= today)**  
    Returns the value TRUE if today's date lies in between the start and end date
    of the execution period.  

    * * *

    **isAssessmentMode(0)**  
    Returns the value TRUE if the course is within an assessment.  

    * * *

    **hasUserProperty( "email","john.doe@uzh.ch")  
    **Generates TRUE, if the course participant is registered at UZH with the
    listed e-mail address.  

    * * *

    **userPropertyEndswith( "email","@uzh.ch")**  
    Generates TRUE, if the e-mail address of the course participant ends with
    _@[u](http://openolat.org)zh.ch_.  

    * * *

    **isInUserProperty( "email","doe@openo")**  
    Generates TRUE, if the term _doe@openo_ is a part of the e-mail address of the
    course participant.  

    * * *

    **isNotInUserProperty( "email","doe@openo")**  
    Generates FALSE, if the term _doe@openo_ s a part of the e-mail address of the
    course participant.  

    * * *

    !!! warning ""   
        Please note that the IDs of the course elements mentioned above are only
        examples. To create your course, you have to make reference to the relevant
        numbers available on the first tab _«Title and description»_ of the favored
        course element.

## Use of AAI attributes

If you are enrolled at swiss academia or any other institution with access to
an AAI infrastructure, by means of AAI attributes you can set access rules
within a course to make sure that only course participants with specific user
attributes (e.g. members of a certain organization) will have access to your
course material. AAI means "Authentication and Authorization Infrastructure"
and allows university members to use systems of other participating
institutions with only one user name and password. For further information on
AAI please go to e.g. Switch or to Deutsches Forschungsnetz .

Available attributes and possible values are described in the AAI Attribute
Specification on the
[Switch](https://www.switch.ch/aai/docs/AAI_Attr_Specs.pdf "Switch") and the
[DFN-AAI](https://www.aai.dfn.de/der-dienst/attribute/ "DFN-AAI") site (in
german). The two most common attributes at swiss universities can be found in
the following table along with examples of their corresponding expert rules:

  

| Attribute | Description | Example Expert rule and Explication |  
|---|---|---|
| swissEduPerson-HomeOrganization | University or home organization | hasAttribute ("swissEduPersonHomeOrganization", "[uzh.ch](http://uzh.ch/)"): only members of the Zurich University will get access. |
| swissEduStudyBranch3 | Field of study, 3rd classification | hasAttribute ("swissEduPersonStudyBranch3","6400"): only veterinary medicine students will have access. |
  
### Utilization

You can retrieve AAI attributes by using the syntax  
 ** _hasAttribute( "_[AttrName] _" ,"_[string] _" )_** or  
 ** _isInAttribute( "_[AttrName] _" ,"_[substring] _" )_**.

Where:

| Value | Description |
| ---|--- | 
| **[AttrName]** |  is the attribute name you can find in the following table and also in the Specification of AAI attributes (pdf file) (column _LDAP names_ ) on page 5. |
|  **[string]** |  is the value of the AAI attribute with the name [AttrName]. |
| **[substring]** |  is any part of [string] . | 


!!! info "AAI retrievals for example: John Doe"
    | Variable <br>You can retrieve AAI attributes by using the syntax <br>hasAttribute("[AttrName]","[string]" or <br> isInAttribute("[AttrName]","[substring]"). | Example value [string] | Description  
    |---|---|---|
    | surname| Doe | Last name |
    | givenName| John | First name |
    | mail | [john.doe@uzh.ch](mailto:john.doe@uzh.ch) | Preferred e-mail address |
    | swissEduPersonHomeOrganization | [uzh.ch](https://www.uzh.ch/) | Home organisation/university |
    | swissEduPersonHomeOrganizationType | university | Type of home organisation |
    | eduPersonAffiliation | student| Position within this organisation |
    | [swissEduPersonStudyBranch1](https://www.switch.ch/aai/support/documents/attributes/swissedupersonstudybranch1/) | 4 | Field of study 1st classification |
    | [swissEduPersonStudyBranch2](https://www.switch.ch/aai/support/documents/attributes/swissedupersonstudybranch2/) | 42 (=Natural sciences) | Field of study 2nd classification |
    | [swissEduPersonStudyBranch3](https://www.switch.ch/aai/support/documents/attributes/swissedupersonstudybranch3/) | 4600 (=Chemistry) | Field of study 3rd classification |
    | swissEduPersonStudyLevel | 15 | Description of study level |
    | eduPersonEntitlement | <https://vam.uzh.ch/surgery> | Access right to resource |
    | employeeNumber | 01-234-567 | Registration number (only for students at Zurich university) |
    | organizationalUnit | 1 | Unity of home organisation e.g. faculty (only for employees) |
  
You will find the link to a list of possible attribute values in the
Specification of AAI attributes (pdf file) appendix, as of page 20.
[Specification of AAI attributes (pdf
file)](http://www.switch.ch/aai/docs/AAI_Attr_Specs.pdf){:target="\_blank"}

For further information on attribute values or the application of AAI
attributes in Switzerland please go to [Switch](http://www.switch.ch/
"Switch"), and for Germany go to [Deutsches
Forschungsnetz](https://www.aai.dfn.de/en/){:target="\_blank"}.

