---
search:
    exclude: true
---

# Zugriffsbeschränkungen im Expertenmodus

  * [Einstieg in die Expertenregeln](#einstieg-in-die-expertenregeln)
  * [Konfiguration von Expertenregeln](#konfiguration-von-expertenregeln)
    * [Arbeiten mit den Konstanten "TRUE" und "FALSE"](#arbeiten-mit-den-konstanten-true-und-false)
  * [Einsatz von AAI-Attributen](#einsatz-von-aai-attributen)
    * [Anwendung](#anwendung)

* * *

!!! info ""
    Die Einstellungen in den Tabs "Sichtbarkeit" und "Zugang" stehen nur in
    [herkommlichen Kursen](../campus_course/learning_path_vs_conventional_courses.de.md) zur Verfügung. Der Expertenmodus kann
    also **nur in herkömmlichen Kursen** und nicht in "[Lernpfad-Kursen](../campus_course/learning_path_vs_conventional_courses.de.md)" verwendet werden.

## Einstieg in die Expertenregeln

Bei vielen Kursbausteinen können Sie unter den Tabs Sichtbarkeit und Zugang
weitere Einstellungen vornehmen. Sie können den Kursbaustein beispielsweise
für Kursteilnehmer sperren, nur für bestimmte Gruppen zugänglich machen oder
ihn datumsabhängig freischalten.

Bei komplizierten Sichtbarkeits- und Zugangsregeln können Sie den
Expertenmodus verwenden. Dies erlaubt Ihnen, die Sichtbarkeit und den Zugang
eines Kursbausteins frei nach Ihren Bedürfnissen zu konfigurieren.
Beispielsweise können Sie den Zugang zu einem Kursbaustein nur für bestimmte
Benutzernamen freischalten, mehrere Einschränkungen miteinander verknüpfen
oder mit relativen Daten arbeiten. Ein Beispiel soll dies erläutern:

!!! info "Beispiel"

    Sie haben den Kursbausteine datumsabhängig freigeschaltet, damit Sie sich im
    Kursverlauf nicht mehr darum kümmern müssen. In den Tabs _Sichtbarkeit_ und
    _Zugang_ des Bausteins können Sie hierfür im einfachen Modus das genau
    Anfangs- und Enddatum eingeben. Möchten Sie diesen Kurs in einem anderen
    Semester erneut verwenden, müssen Sie das Datum ändern, damit der Baustein
    freigeschaltet wird. Dies ist machbar, wenn nur ein Kursbaustein einer solchen
    Regel unterliegt. Bei mehreren solcher Regeln können Ihnen Expertenregeln
    diese Arbeit abnehmen: Sie geben lediglich für die jeweiligen Kursbausteine
    ein relatives Datum an, das sich auf das Anfangs- oder Enddatum des Kurses
    bezieht.

    Vereinfacht bedeutet dies, dass z.B. der Baustein nicht vom 01.12.2021 bis
    31.12.2021 freigeschaltet wird, sondern zum "Kursende" (31.01.2022), und exakt
    zum "Kursende plus 7 Tage" nicht mehr zugänglich ist. So müssen Sie bei einem
    erneuten Einsatz des Kurses nur das Kursanfangs- und Kursenddatum ändern.

Expertenregeln dienen in erster Linie dazu, Ihnen Arbeit abzunehmen oder sie
zu erleichtern, daher lohnt es sich, sie etwas näher zu betrachten. Wie bei
einer Sprache folgen die Expertenregeln einer Syntax oder Satzbauregel. Sollte
man einen syntaktischen Fehler machen, dann weist OLAT Sie darauf hin. Dies
hilft einem vor allem am Anfang, wenn man keine oder nur geringe
Programmierkenntnisse hat. Eine Expertenregel überprüft, ob ein bestimmtes
Attribut WAHR oder FALSCH ist.

Als Einstieg in die Syntax der Expertenregel empfiehlt es sich, zunächst im
einfachen Modus eine Regel zu definieren. Sie erstellen z.B. eine "Einzelne
Seite" und klicken unter dem Tab "Zugang" auf "Für Lernende gesperrt".

Dann klicken Sie auf "Expertenmodus anzeigen" und sehen Ihre erste
Expertenregel:
    
    (  ( isCourseCoach(0) | isCourseAdministrator(0) ) )

Der gesamte Ausdruck ist doppelt eingeklammert. Die beiden äußeren Klammern
kann man in diesem Fall auch weglassen. Probieren Sie es einfach aus.
CourseCoach ist der Kursbetreuer und CourseAdministrator der Kursbesitzer. Der
senkrechte Strich in der Mitte "|" steht für den Booleschen Operator ODER.
Diese Expertenregel ist WAHR für den Betreuer ODER den Besitzer. Nur diese
beiden haben Zugang auf diese "Einzelne Seite".

Nun ändern Sie den Booleschen Operator in " **&** ":
    
    isCourseCoach(0) & isCourseAdministrator(0)

Dies bedeutet, dass nur Betreuer, die gleichzeitig auch Besitzer sind, Zugriff
haben. Diese Einstellung ist nur im Expertenmodus möglich.

Sie können beliebig viele Szenarien durchspielen und weitere Attribute und
Operatoren einfügen. Um sich in die Funktion der Regeln besser einzuarbeiten,
finden Sie in diesem Kapitel weitere Attribute und Beispiele, deren
Auswirkungen näher erläutert werden.

## Konfiguration von Expertenregeln

Eine Expertenregel prüft, ob ein Attribut einen bestimmten Wert besitzt.

| Attribut| Beschreibung | Beispiel Expertenregel |
| --- |--- |--- |
| isGuest| nur für Gäste zugänglich | isGuest(0) |
| isCourseCoach | nur für Benutzer verfügbar, die eine Gruppe betreuen | isCourseCoach(0) |
| isUser | nur für einen bestimmten Benutzer verfügbar | isUser("pmuster") |
  
###  **Arbeiten mit den Konstanten "TRUE" und "FALSE"**

Die Konstanten "true" und "false" prüfen das Vorhandensein ("true" bzw. "1")
oder Nicht-Vorhanden-Sein ("false" bzw. "0") eines Attributs. Man spricht von
einer sogenannten Boolschen Variablen (benannt nach George Boole dem Begründer
der Boolschen Algebra). Dabei handelt es sich um eine Variable, die nur
endlich viele Werte oder Zustände einnehmen kann. In diesem Fall kann die
Variable nur zwei Zustände oder Werte annehmen ("true" = "1" = wahr, vorhanden
oder "false" = "0" = falsch, nicht-vorhanden).

!!! info ""
    Zur praktischen Erläuterung im OLAT-Kontext soll uns eine Expertenregel für
    den Zugang zu einem Kurs dienen.

    **Fall 1:** Nur Gast-Nutzer sollen Zugang zum Kurs erhalten. Der jeweilige
    Nutzer erhält also Zugang, wenn er das Attribut "isGuest" wahr ist. Für diese
    Expertenregel gibt es drei Alternativen:

    **isGuest(0)** oder **isGuest(0)=1** oder **isGuest(0)=true**

    **Fall 2:** Hier sollen alle Nutzer außer den Gast-Nutzern einen **** Zugang
    erhalten. Der jeweilige Nutzer erhält also Zugang, wenn das Attribut "isGuest"
    **** nicht wahr bzw. nicht vorhanden ist. Für diese Expertenregel gibt es zwei
    Alternativen:

    **isGuest(0)=0** oder **isGuest(0)=false**

Eine ausführliche Liste aller wichtigen Bestandteile für die Expertenregeln
finden Sie in der nachfolgenden Box. Weiter unten finden Sie konkrete
Beispiele.

!!! info "Funktionen, Operatoren und weitere Bestandteile der Expertenregeln"
    <table>
        <thead>
            <tr style="background-color: #F4F5F7;">
                <th>Typ</th>
                <th>Syntax</th>
                <th>Bedeutung</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td rowspan="3" style="background-color: #F4F5F7;"><strong>Konstanten</strong></td>
                <td><em>TRUE</em> oder <em>1</em></td>
                <td>Wahr</td>
            </tr>
            <tr>
                <td><em>FALSE</em> oder <em>0</em></td>
                <td>Falsch</td>
            </tr>
            <tr>
                <td><em>ANY_COURSE</em></td>
                <td>Abfrage soll für jeden Kurs gelten (nur für
                    isCourseAdministrator(), isCourseCoach(), isCourseParticipant()</td>
            </tr>
            <tr>
                <td style="background-color: #F4F5F7;"><strong>Variable</strong></td>
                <td><em>now</em></td>
                <td>Momentane Server-Systemzeit</td>
            </tr>
            <tr>
                <td rowspan="33" style="background-color: #F4F5F7;"><strong>Funktionen</strong></td>
                <td><em>date("</em>[date]<em>")</em></td>
                <td>Datum abfragen</td>
            </tr>
            <tr>
                <td><em>inLearningGroup("</em>[string]<em>")</em></td>
                <td>Gibt TRUE für alle Mitglieder der Lerngruppe [string]</td>
            </tr>
            <tr>
                <td><em>inRightGroup("</em>[string]<em>")</em></td>
                <td>Gibt TRUE für alle Mitglieder der Rechtegruppe [string]</td>
            </tr>
            <tr>
                <td><em>isLearningGroupFull("</em>[string]<em>")</em></td>
                <td>Gibt für die angegebene Lerngruppe den Boolean TRUE (=voll) oder
                    FALSE (=nicht voll) zurück.</td>
            </tr>
            <tr>
                <td><em>isUser("</em>[string]<em>")</em></td>
                <td>Gibt TRUE für den Benutzer mit dem Benutzernamen [string]</td>
            </tr>
            <tr>
                <td><em>inLearningArea("</em>[string]<em>")</em></td>
                <td>Gibt TRUE für alle Mitglieder der Gruppen im Lernbereich [string]
                </td>
            </tr>
            <tr>
                <td><em>isGlobalAuthor(0)</em></td>
                <td>Gibt TRUE für alle Mitglieder mit OLAT Autorenrechten</td>
            </tr>
            <tr>
                <td><em>isCourseAdministrator(0)</em></td>
                <td>Gibt TRUE für alle Besitzer Ihres Kurses (Lernressource)</td>
            </tr>
            <tr>
                <td><em>isCourseAdministrator(<em>ANY_COURSE</em>)</em>
                </td>
                <td>Gibt TRUE für alle Benutzer die Besitzer eines
                    beliebigen Kurses auf dem System sind (Lernressource)</td>
            </tr>
            <tr>
                <td><em>isCourseCoach(0)</em></td>
                <td>Gibt TRUE für alle Benutzer, die eine Lerngruppe oder den gesamten
                    Kurs betreuen</td>
            </tr>
            <tr>
                <td><em>isCourseCoach(<em>ANY_COURSE</em>)</em></td>
                <td>Gibt TRUE für alle Benutzer, die eine Lerngruppe eines
                    beliebigen Kurses oder einen beliebigen gesamten Kurs betreuen</td>
            </tr>
            <tr>
                <td><em>isCourseParticipant(0)</em></td>
                <td>Gibt TRUE für alle Teilnehmer des Kurses</td>
            </tr>
            <tr>
                <td><em>isCourseParticipant(<em>ANY_COURSE</em>)</em></td>
                <td>Gibt TRUE für alle Benutzer, die in einem beliebigen
                    Kurs als Teilnehmer eingetragen sind</td>
            </tr>
            <tr>
                <td><em>isGuest(0)</em></td>
                <td>Gibt TRUE für alle Benutzer, die OLAT als Gäste besuchen</td>
            </tr>
            <tr>
                <td><em>hasAttribute("</em>[AttrName]<em>","</em>[string]<em>")</em>
                </td>
                <td>Gibt TRUE, wenn [string] dem Wert des AAI-Attributes [AttrName] des
                    jeweiligen Benutzers entspricht.</td>
            </tr>
            <tr>
                <td>
                    <em>isInAttribute("</em>[AttrName]<em>","</em>[substring]<em>")</em>
                </td>
                <td>Gibt TRUE, wenn [substring] einem Teil des Wertes des AAI-Attributs
                    [AttrName] des jeweiligen Benutzers entspricht. <br>
                        [AAI - Generelle Informationen](https://www.switch.ch/aai/)<br>
                        [Spezifikation der AAI-Attribute (pdf-Datei)](https://www.switch.ch/aai/docs/AAI_Attr_Specs.pdf)        
                </td>
            </tr>
            <tr>
                <td><em>getUserProperty("</em>[userPropertyname]<em>")</em></td>
                <td>Gibt den Wert des spezifizierten Benutzerattributes zurück. Mit "="
                    kann dieser Wert mit einem fixen Wert verglichen werden.</td>
            </tr>
            <tr>
                <td><em>getPassed("</em>[integer]<em>")</em></td>
                <td>Gibt vom Kursbaustein mit spezifizierter ID den Boolean TRUE
                    (=Bestanden) oder FALSE (=Nicht bestanden) zurück</td>
            </tr>
            <tr>
                <td><em>getScore("</em>[integer]<em>")</em></td>
                <td>Gibt vom Kursbaustein mit spezifizierter ID die Anzahl Punkte
                    zurück</td>
            </tr>
            <tr>
                <td><em>getAttempts("</em>[integer]<em>")</em></td>
                <td>Gibt vom Kursbaustein mit spezifizierter ID die Anzahl
                    abgeschlossener Versuche zurück. Kann auf Kursbausteine vom Typ <em>Test</em>,
                    <em>Selbsttest </em>(mögliche Rückgabewerte 0 oder 1) und Aufgabe (mit kontinuierlicher
                    Abgabe) (Rückgabewert = Anzahl abgegebener Dateien) angewendet werden. Gilt nicht für
                    den neuen Aufgabenbaustein, da dort unter Abgabe die maximale Anzahl der abzugebenden
                    Dokumente definiert werden kann.
                </td>
            </tr>
            <tr>
                <td><em>getLastAttemptDate("</em>[integer]<em>")</em></td>
                <td>Gibt vom Kursbaustein mit spezifizierter ID das Datum des letzen
                    Versuches zurück. Die Anwendung ist gleich wie die getAttempts Methode.</td>
            </tr>
            <tr>
                <td><em>getInitialEnrollmentDate("</em>[integer]<em>")</em></td>
                <td>Gibt vom Kursbaustein <em>Einschreibung</em> mit spezifizierter ID
                    das Datum des erstmaligen Einschreibens des betreffenden Kursteilnehmers zurück.</td>
            </tr>
            <tr>
                <td><em>getRecentEnrollmentDate("</em>[integer]<em>")</em></td>
                <td>Gibt vom Kursbaustein <em>Einschreibung</em> mit spezifizierter ID
                    das Datum des letzten Einschreibens des betreffenden Kursteilnehmers zurück.</td>
            </tr>
            <tr>
                <td><em>getInitialCourseLaunchDate(0)</em></td>
                <td>Gibt das Datum des erstmaligen Kursbesuchs des betreffenden
                    Kursteilnehmers zurück.</td>
            </tr>
            <tr>
                <td><em>getRecentCourseLaunchDate(0)</em></td>
                <td>Gibt das Datum des letzten Kursbesuchs des betreffenden
                    Kursteilnehmers zurück.</td>
            </tr>
            <tr>
                <td>
                    <em>getPassedWithCourseId("</em>[integer-1]<em>","</em>[integer-2]<em>")</em>
                </td>
                <td>Gibt vom Kursbaustein mit ID=[integer-2] des Kurses mit
                    ID=[integer-1] den Boolean TRUE (=Bestanden) oder FALSE (=Nicht bestanden) zurück</td>
            </tr>
            <tr>
                <td>
                    <em>getScoreWithCourseId("</em>[integer-1]<em>","</em>[integer-2]<em>")</em>
                </td>
                <td>
                    <p>Gibt vom Kursbaustein mit ID=[integer-2] des Kurses mit ID=[integer-1] die Anzahl
                        Punkte zurück</p>
                </td>
            </tr>
            <tr>
                <td><em>hasUserProperty("</em>[userPropertyname]<em>",
                        "</em>[string]<em>")</em></td>
                <td>
                    <p>Gibt TRUE, wenn [string] dem Wert des Benutzerattribut [userPropertyname] des
                        jeweiligen Benutzers entspricht.</p>
                </td>
            </tr>
            <tr>
                <td><em>userPropertyStartswith("</em>[userPropertyname]<em>",
                        "</em>[substring]<em>")</em></td>
                <td>Gibt TRUE, wenn das Benutzerattribut [userPropertyname]
                    des jeweiligen Benutzers mit [substring] beginnt.</td>
            </tr>
            <tr>
                <td><em>userPropertyEndswith("</em>[userPropertyname]<em>",
                        "</em>[substring]<em>")<em></td>
                <td>
                    <p>Gibt TRUE, wenn das Benutzerattribut [userPropertyname] des jeweiligen Benutzers
                        mit [substring] endet.</p>
                </td>
            </tr>
            <tr>
                <td><em>isInUserProperty("</em>[userPropertyname]<em>",
                        "</em>[substring]<em>")<em></td>
                <td>
                    <p>Gibt TRUE, wenn das Benutzerattribut [userPropertyname] des jeweiligen Benutzers
                        [substring] enthält.</p>
                </td>
            </tr>
            <tr>
                <td><em>isNotInUserProperty("</em>[userPropertyname]</em>",
                        "[substring]<em>")<em></td>
                <td>Gibt TRUE, wenn das Benutzerattribut
                    [userPropertyname] des jeweiligen Benutzers [substring] nicht enthält.</td>
            </tr>
            <tr>
                <td><em>hasNotUserProperty("</em>[userPropertyname]<em>",
                        "</em>[string]<em>")<em></td>
                <td>Gibt TRUE, wenn der jeweilige Benutzer das
                    Benutzerattribut [userPropertyname] nicht besitzt.</td>
            </tr>
            <tr>
                <td rowspan="5" style="background-color: #F4F5F7;"><strong>Einheiten</strong></td>
                <td><em>min</em></td>
                <td>Minuten</td>
            </tr>
            <tr>
                <td><em>h</em></td>
                <td>Stunden</td>
            </tr>
            <tr>
                <td><em>d</em></td>
                <td>Tage</td>
            </tr>
            <tr>
                <td><em>w</em></td>
                <td>Wochen</td>
            </tr>
            <tr>
                <td><em>m</em></td>
                <td>Monate</td>
            </tr>
            <tr>
                <td rowspan="9" style="background-color: #F4F5F7;"><strong>Operatoren</strong></td>
                <td>=</td>
                <td>gleich</td>
            </tr>
            <tr>
                <td>&gt;</td>
                <td>grösser als</td>
            </tr>
            <tr>
                <td>&lt;</td>
                <td>kleiner als</td>
            </tr>
            <tr>
                <td>&gt;=</td>
                <td>grösser gleich</td>
            </tr>
            <tr>
                <td>&lt;=</td>
                <td>kleiner gleich</td>
            </tr>
            <tr>
                <td>*</td>
                <td>Multiplikation</td>
            </tr>
            <tr>
                <td>/</td>
                <td>Division</td>
            </tr>
            <tr>
                <td>+</td>
                <td>Addition</td>
            </tr>
            <tr>
                <td>-</td>
                <td>Subtraktion</td>
            </tr>
            <tr>
                <td rowspan="2" style="background-color: #F4F5F7;"><strong>Booleans</strong></td>
                <td>&amp;</td>
                <td>Logisches UND</td>
            </tr>
            <tr>
                <td>|</td>
                <td>Logisches ODER</td>
            </tr>
        </tbody>
    </table>

  
!!! info "Benutzerattribute (UserProperty)"

    Verschiedene Expertenregeln benötigen Benutzerattribute um Einschränkungen
    vornehmen zu können. Dies ermöglicht bspw. die Einschränkung von Benutzern
    nach Name, Geschlecht, Adresse, Studienfach und weitere. Diese
    Benutzerattribute sind in der Regel im Benutzerprofil enthalten. OLAT stellt
    standardisierte Begriffe für diese Attribute bereit. Folgende Expertenregeln
    benötigen Benutzerattribute:

      * getUserProperty _( " _[userPropertyname]_ ")_
      * hasUserProperty("[ _userPropertyname] ", "[string]")_
      * userPropertyStartswith(" [ _userPropertyname] ", "[substring]")_
      * userPropertyEndswith(" [ _userPropertyname] ", "[substring]")_
      * isInUserProperty(" [ _userPropertyname] ", "[substring]")_
      * isNotInUserProperty("[ _userPropertyname] ", "[substring]")_
      * hasNotUserProperty("[ _userPropertyname] ", "[string]")_

    Die folgenden Benutzerattribute stehen in OLAT zur Verfügung. Bitte beachten
    Sie, dass eine Einschränkung mittels Benutzerattributen nur dann erfolgreich
    sein kann, wenn diese in ihrem System auch genutzt werden, und standardmässig
    ausgefüllt sind. Überprüfen Sie im persönlichen Menü unter
    Konfiguration/Profil die verfügbaren Benutzerattribute. Bei Fragen
    kontaktieren Sie ihren Systemadministrator.

    <table>
        <thead>
            <tr style="background-color: #F4F5F7;">
                <th colspan="2">Benutzerdaten</th>
                <th colspan="2">Kontaktdaten</th>
                <th colspan="2">Adressdaten</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <th style="background-color: #F4F5F7;">userName</th>
                <td>Benutzername</td>
                <th style="background-color: #F4F5F7;">telPrivate</th>
                <td>Telefon privat</td>
                <th style="background-color: #F4F5F7;">street</th>
                <td>Strasse</td>
            </tr>
            <tr>
                <th style="background-color: #F4F5F7;">firstName</th>
                <td>Vorname</td>
                <th style="background-color: #F4F5F7;">telMobile</th>
                <td>Telefon Mobil</td>
                <th style="background-color: #F4F5F7;">extendedAddress</th>
                <td>Adresszusatz</td>
            </tr>
            <tr>
                <th style="background-color: #F4F5F7;">lastName</th>
                <td>Nachname</td>
                <th style="background-color: #F4F5F7;">telOffice</th>
                <td>Telefon Geschäft</td>
                <th style="background-color: #F4F5F7;">poBox</th>
                <td>Postfach</td>
            </tr>
            <tr>
                <th style="background-color: #F4F5F7;">email</th>
                <td>E-Mail-Adresse</td>
                <th style="background-color: #F4F5F7;">skype</th>
                <td>Skype ID</td>
                <th style="background-color: #F4F5F7;">zipCode</th>
                <td>PLZ</td>
            </tr>
            <tr>
                <th style="background-color: #F4F5F7;">creationDateDisplayProperty</th>
                <td>Erstelldatum des Benutzers</td>
                <th style="background-color: #F4F5F7;">xing</th>
                <td>Xing</td>
                <th style="background-color: #F4F5F7;">region</th>
                <td>Region / Kanton</td>
            </tr>
            <tr>
                <th style="background-color: #F4F5F7;">lastloginDateDisplayProperty</th>
                <td>Letzter Login des Benutzers</td>
                <th style="background-color: #F4F5F7;">homepage</th>
                <td>Homepage</td>
                <th style="background-color: #F4F5F7;">city</th>
                <td>Stadt</td>
            </tr>
            <tr>
                <th style="background-color: #F4F5F7;">birthDay</th>
                <td>Geburtsdatum</td>
                <th style="background-color: #F4F5F7;"><br></th>
                <td><br></td>
                <th style="background-color: #F4F5F7;">country</th>
                <td>Land</td>
            </tr>
            <tr>
                <th style="background-color: #F4F5F7;">gender</th>
                <td>Geschlecht</td>
                <th style="background-color: #F4F5F7;"><br></th>
                <td><br></td>
                <th style="background-color: #F4F5F7;">countryCode</th>
                <td>Länderkürzel</td>
            </tr>
            <tr>
                <td colspan="6"><br></td>
            </tr>
            <tr style="background-color: #F4F5F7;">
                <th colspan="2">Organisation</th>
                <th colspan="2">Berufliche Kontaktdaten</th>
                <th colspan="2">Verschiedenes</th>
            </tr>
            <tr>
                <th style="background-color: #F4F5F7;">institutionalName</th>
                <td>Institution</td>
                <th style="background-color: #F4F5F7;">department</th>
                <td>Dienststelle / Firma</td>
                <th style="background-color: #F4F5F7;">typeOfUser</th>
                <td>Art von Benutzer</td>
            </tr>
            <tr>
                <th style="background-color: #F4F5F7;">institutionalUserIdentifier</th>
                <td>Institutionsnummer (Matrikelnummer)</td>
                <th style="background-color: #F4F5F7;">officeStreet</th>
                <td>Dienstadresse - Strasse</td>
                <th style="background-color: #F4F5F7;">rank</th>
                <td>Dienstgrad / Amtsbezeichnung</td>
            </tr>
            <tr>
                <th style="background-color: #F4F5F7;">institutionalEmail</th>
                <td>Institutions E-Mail</td>
                <th style="background-color: #F4F5F7;">extendedOfficeAddress</th>
                <td>Dienstadresszusatz</td>
                <th style="background-color: #F4F5F7;">socialSecurityNumber</th>
                <td>Sozialversicherungs-nummer</td>
            </tr>
            <tr>
                <th style="background-color: #F4F5F7;">orgUnit</th>
                <td>Organisationseinheit / Studiengruppe</td>
                <th style="background-color: #F4F5F7;">officeZipCode</th>
                <td>Dienst-Postleitzahl</td>
                <th style="background-color: #F4F5F7;">degree</th>
                <td>Akademischer Grad</td>
            </tr>
            <tr>
                <th style="background-color: #F4F5F7;">studySubject</th>
                <td>Studienfach</td>
                <th style="background-color: #F4F5F7;">officeCity</th>
                <td>Dienstadresse - Stadt</td>
                <th style="background-color: #F4F5F7;">position</th>
                <td>Funktion / Stellung</td>
            </tr>
            <tr>
                <th style="background-color: #F4F5F7;">graduation</th>
                <td>Abschlussjahr</td>
                <th style="background-color: #F4F5F7;">officeCountry</th>
                <td>Dienstadresse - Land</td>
                <th style="background-color: #F4F5F7;">userInterests</th>
                <td>Expertise</td>
            </tr>
            <tr>
                <th style="background-color: #F4F5F7;"><br></th>
                <td><br></td>
                <th style="background-color: #F4F5F7;">officeMobilePhone</th>
                <td>Dienstmobiltelefon</td>
                <th style="background-color: #F4F5F7;"><br></th>
                <th style="background-color: #F4F5F7;"><br></th>
            </tr>
        </tbody>
    </table>
  
    Beispiele für die Anwendung:

      * Es sollen nur Kurteilnehmer eines bestimmten Studienganges Zugang erhalten:
        
            getUserProperty("studySubject") = "Maschinenbau"

    Nun muss, wer Zugang haben möchte, in seinem Profil, im Feld Studienfach
    Maschinenbau eingetragen haben.

      * Sollen nur Kursteilnehmer Zugang erhalten, die nichts im Feld Studiengang eingetragen haben, lautet die Regel:
        
            getUserProperty("studySubject") = ""

      * Sollen nur Kursteilnehmer Zugang erhalten, die irgendeinen Studiengang eingetragen haben, muss die Regel lauten:
        
            getUserProperty("studySubject") = "" = false

    oder    

            getUserProperty("studySubject") = "" = 0

  

Es gibt verschiedene Möglichkeiten, die einzelnen Regeln miteinander zu
verknüpfen. Die beiden wichtigsten Operatoren zur Verknüpfung von Attributen
sind:

  * UND-Verknüpfung: &
  * ODER-Verknüpfung: |

Bitte beachten Sie, dass eine ODER-Verknüpfung vor einer UND-Verknüpfung
gemacht wird. Damit die UND-Verknüpfung zuerst gemacht wird, müssen Klammern
gesetzt werden.

Beispiel: Die Expertenregel (inGroup("Teilnehmende Intensivkurs") ****|****
isCourseCoach(0)) lässt entweder Teilnehmende des Intensivkurses oder alle
Gruppenbetreuer auf den Kursbaustein zugreifen.

Nachfolgend sind einige Beispiele aufgeführt, die Ihnen zeigen, wie Sie die
Expertensyntax verwenden können.

 **Beispiele für Expertenregeln in den Tabs «Sichtbarkeit», «Zugang» und
«Punkte» (Struktur-Baustein)**

!!! info "Beispiele für Expertenregeln in den Tabs "Sichtbarkeit", "Zugang" und "Punkte" (Struktur-Baustein)"

    **isGuest(0)** oder **isGuest(0)=1** oder **isGuest(0)=true**  
    Ausschliesslich Gäste haben Zugang auf den Kursbaustein.  
      
    * * *
    
    **inLearningGroup( "Anfänger") = 0**  
    Mit Ausnahme der Gruppe _" Anfänger"_ ist der Kursbaustein für alle
    Kursteilnehmer sichtbar.  
      
    * * * 
      
    **(now >= date("22.03.2004 12:00")) & (now <= date("23.08.2004 18:00")) |
    inLearningGroup("Betreuer")**  
    Der Kursbaustein ist zwischen dem 22.03.2004 und 23.08.2004 für alle
    Kursteilnehmer sichtbar, während er für Mitglieder der Lerngruppe _"
    Betreuer"_ jederzeit sichtbar ist.  
      
    * * * 

    **(now >= date("03.09.2004 00:00")) & (now <= date("13.10.2004 00:00")) &
    inRightGroup("Assessoren")| isUser("kmiller")**  
    Der Kursbaustein ist zwischen dem 03.09.2004 und 13.10.2004 für alle
    Kursteilnehmer der Rechtegruppe _" Assessoren"_ sichtbar, während er für die
    Person mit dem Benutzernamen _" kmiller"_ jederzeit sichtbar ist.  
      
    * * * 

    **hasAttribute( "swissEduPersonStudyBranch3","6200")**  
    Ausschliesslich Studierende der Humanmedizin haben Zugriff auf den
    Kursbaustein.  
    Siehe auch:  
    AAI-Attribute  
    [Spezifikation der AAI-Attribute (pdf-Datei)](http://www.switch.ch/aai/docs/AAI_Attr_Specs.pdf)  
      
    * * *

    **hasAttribute(
    "swissEduPersonHomeOrganization","[uzh.ch](http://uzh.ch/)")**  
    Ausschliesslich Studierende der Universität Zürich haben Zugriff auf den
    Kursbaustein.  
    Siehe auch:  
    AAI-Attribute  
    [Spezifikation der AAI-Attribute (pdf-Datei)](http://www.switch.ch/aai/docs/AAI_Attr_Specs.pdf)  
      
    * * *

    **isInAttribute( "surname","Mue")**  
    Gibt TRUE für alle Personen, deren Attribut _surname_ die Buchstabenfolge
    "Mue" enthaltet. Gibt z.B. TRUE für den Wert "Mueller" oder "Muehlebacher"  
    Siehe auch:  
    AAI-Attribute  
    [Spezifikation der AAI-Attribute (pdf-Datei)](http://www.switch.ch/aai/docs/AAI_Attr_Specs.pdf)  
      
    * * *

    **isInAttribute(
    "eduPersonEntitlement","[https://vam.uzh.ch](https://vam.uzh.ch/)")**  
    Gibt TRUE für alle Personen, deren Attribut _eduPersonEntitlement_ den Wert
    "[https://vam.uzh.ch](https://vam.uzh.ch/)" enthaltet. Gibt z.B. auch TRUE für
    den Wert "<https://vam.uzh.ch/surgery>"  
    Siehe auch:  
    AAI-Attribute  
    [Spezifikation der AAI-Attribute (pdf-
    Datei)](http://www.switch.ch/aai/docs/AAI_Attr_Specs.pdf)  
      
    * * *

    **(getUserProperty( "orgUnit") = "Sales")**  
    Prüft ob eine Person in der Organisationseinheit "Sales" ist. Sinnvoll z.B.
    wenn die Daten automatisiert aus LDAP übernommen werden.  
      
    * * *

    **(getPassed( "69742969114730") | getPassed("69742969115733") |
    getPassed("69742969118009")) * 10**  
    Diese Regel wird im Tab _" Punkte"_ -> _" Punkte verarbeiten"_ des Bausteins
    _Struktur_ gesetzt. Der Baustein _Struktur_ zeigt 10 Punkte, wenn einer der
    Tests (Kursbaustein-IDs "69742969114730", "69742969115733" oder
    "69742969118009") bestanden wurde, sonst 0 Punkte.  
      
    * * *

    **(getScore( "69742969114730") + getScore("69742969115733") +
    getScore("69742969118009")) >= 140 | getPassed("69978845384688")**  
    Diese Regel wird im Tab _" Punkte"_ -> _" Bestanden wenn"_ des Bausteins
    _Struktur_ gesetzt. Der Bausteins _Struktur_ zeigt ein _" Bestanden"_, wenn in
    allen Tests zusammen minimal 140 Punkte erzielt werden oder wenn manuell ein
    _" Bestanden"_ gesetzt wird (Kursbaustein _Bewertung_ mit ID
    "69978845384688").  
      
    * * *

    **getAttempts( "70323786958847") > 0 **  
    Gibt TRUE, sobald der betreffende Kursteilnehmer den Test mit spezifizierter
    ID ein erstes Mal abgeschlossen hat.  
      
    * * *

    **getAttempts( "70323524635734") <= 3 **  
    Gibt FALSE, sobald der betreffende Kursteilnehmer mehr als 3 Dateien in den
    Abgabeordner des Kursbaustein ~~_Aufgabe (deprecated)_~~ gelegt hat. Gilt
    nicht für den neuen Aufgabenbaustein, da dort unter Abgabe die maximale Anzahl
    der abzugebenden Dokumente definiert werden kann.  
      
    * * *

    **getLastAttemptDate( "70323524635734") + 24h < now **  
    Gibt TRUE, wenn der letzte Testversuch 24 Stunden zurückliegt.  
      
    * * *

    **getInitialEnrollmentDate( "70323786958847") <= date("26.5.2005 18:00")**  
    Gibt TRUE für diejenigen Kursteilnehmer, die sich vor 18 Uhr des 26. Mai 2005
    über den Kursbaustein _Einschreibung_ mit spezifizierter ID in eine zur
    Auswahl stehende Gruppe eingeschrieben haben.  
      
    * * *

    **getInitialEnrollmentDate( "70323786958847") + 2h > now**  
    Gibt TRUE während zwei Stunden ab Einschreibezeitpunkt für diejenigen
    Kursteilnehmer, die sich über den Kursbaustein _Einschreibung_ mit
    spezifizierter ID in eine zur Auswahl stehende Gruppe eingeschrieben haben. So
    kann abgebildet werden, dass jeder Kursteilnehmer nur während einer bestimmten
    Zeitdauer z.B. ein Skript bearbeiten kann.  

    * * *  
      
    **(getInitialCourseLaunchDate(0) >= never) | (getInitialCourseLaunchDate(0) + 2h > now)**  
    Gibt TRUE, wenn der Kursteilnehmer den Kurs noch nicht besucht hat oder
    während zwei Stunden seit dem ersten Kursbesuch. So kann abgebildet werden,
    dass jeder Kursteilnehmer nur während einer bestimmten Zeitdauer den Kurs
    sehen kann.  
      
    * * *

    **(getRecentCourseLaunchDate(0) + 10min < now) **  
    Gibt TRUE, wenn sich der Benutzer seit mehr als 10 Minuten im Kurs bewegt.  
      
    * * *

    **(getCourseBeginDate(0) <= today) & (getCourseEndDate(0) >= today)**  
    Gibt den Wert TRUE zurück, wenn das heutige Datum zwischen Beginn- und
    Enddatum des Durchführungszeitraums des Kurses liegt.  
      
    * * *

    **isAssessmentMode(0)**  
    Gibt TRUE sobald der Kurs innerhalb eine Prüfung ist.  

    * * *

    **hasUserProperty( "email","john.doe@[openolat.org](https://openolat.org/)")  
    **Gibt TRUE, wenn der Kursteilnehmer in OLAT mit der eingetragenen E-Mail-
    Adresse registriert ist.  
      
    * * *

    **userPropertyEndswith( "email","@[openolat.org](http://openolat.org/)")**  
    Gibt TRUE, wenn die E-Mail-Adresse des Kursteilnehmers auf
    _@[openolat.org](https://openolat.org/)_ endet.  

    * * *

    **isInUserProperty( "email","doe@openo")**  
    Gibt TRUE, wenn der Begriff _doe@openo_ ein Teil der E-Mail-Adresse des
    Kursteilnehmers ist.  

    * * *

    **isNotInUserProperty( "email","doe@openo")**  
    Gibt FALSE, wenn der Begriff _doe@openo_ ein Teil der E-Mail-Adresse des
    Kursteilnehmers ist.  
      
    * * *

    **isNotInUserProperty( "email","doe@openo")**  
    Gibt FALSE, wenn der Begriff _doe@openo_ ein Teil der E-Mail-Adresse des
    Kursteilnehmers ist.  

    * * *

    !!! warning ""  
        Bitte beachten Sie, dass die oben erwähnten Kursbaustein-IDs Beispiele sind.
        Wenn Sie Ihren Kurs erstellen, müssen Sie jeweils die Nummern referenzieren,
        die auf dem ersten Tab _" Titel und Beschreibung"_ des gewünschten
        Kursbausteins sichtbar sind.

## Einsatz von AAI-Attributen

Wenn Sie sich an einer Schweizer Hochschule befinden, oder einer anderen
Institution die auf eine AAI - Infrastruktur Zugriff hat, können Sie mit AAI-
Attributen im Kurs Zugriffsregeln setzen, damit nur Kursteilnehmer mit
bestimmten Benutzerattributen (z.B. Teilnehmer, die einer bestimmten
Institution angehören) auf das Kursmaterial zugreifen können. Die Abkürzung
AAI steht für "Authentication and Authorization Infrastructure" und ermöglicht
es Angehörigen einer Hochschule, mit nur einem Benutzernamen und Passwort
Zugriff auf Systeme aller teilnehmenden Hochschulen zu erhalten. Weitere
Informationen zu AAI finden Sie z.B. bei [Switch](https://www.switch.ch/de/aai/){:target="\_blank"} 
oder dem [deutschen Forschungsnetz](https://www.aai.dfn.de/){:target="\_blank"}.

Verfügbare Attribute und mögliche Werte sind in der AAI Attribute
Specification bei [Switch](https://www.switch.ch/aai/docs/AAI_Attr_Specs.pdf){:target="\_blank"} 
(Dokument in englischer Sprache) und [DFN-
AAI](https://www.aai.dfn.de/der-dienst/attribute/){:target="\_blank"} beschrieben. Zwei
an schweizer Hochschulen häufig gebrauchte Attribute und Beispiele der
entsprechenden Expertenregeln finden Sie in der folgenden Tabelle:

| Attribut | Beschreibung | Beispiel Expertenregel und Erklärung |
| --- | --- | --- |  
| swissEduPerson-HomeOrganization | Universität oder Heimorganisation| hasAttribute ("swissEduPersonHomeOrganization", "[uzh.ch](https://www.uzh.ch/{:target="\_blank"})"): Nur Angehörige der Universität Zürich sind zugelassen. | 
| swissEduStudyBranch3| Studienrichtung 3. Klassifikation| hasAttribute ("swissEduPersonStudyBranch3","6400"): Nur Studierende der Studienrichtung Veterinärmedizin sind zugelassen. |
  
### Anwendung

Sie können AAI-Attribute mit der Syntax

 ** _hasAttribute( "_[AttrName] _" ,"_[string] _" )_** oder  
**_isInAttribute( "_[AttrName] _" ,"_[substring] _" )_** abfragen.

Dabei gilt folgendes: 

| Wert | Beschreibung |
| ---|--- |
| **[AttrName]** |  ist der Attributnamen, den Sie in der nachfolgenden Tabelle und auch in der Spezifikation der AAI-Attribute (pdf-Datei) (Spalte _LDAP Namen_ ) auf Seite 5 vorfinden.  
| **[string]** |  ist der Wert des AAI-Attributes mit Namen [AttrName]. |
| **[substring]**|  ist ein beliebig grosser Teil von [string]. |
  
!!! info "Abfragen für das Beispiel: Hans Muster"
    | Variable <br>Sie können AAI-Attribute mit der Syntax **_hasAttribute( "_[AttrName] _" ,"_[string] _" )_** oder **_isInAttribute( "_[AttrName] _" ,"_[substring] _" )_** abfragen.| Beispiel Wert <br>\[string\] | Beschreibung |
    |---|---|---|  
    swissEduPersonUniqueID | [845938727494@uzh.ch](mailto:845938727494@uzh.ch)| Eindeutige persönliche Identifikationsnummer   |
    | surname | Muster | Nachname   |
    | givenName | Hans | Vorname   |
    | mail | [hans.muster@uzh.ch](mailto:hans.muster@uzh.ch) | Bevorzugte E-Mail-Adresse   |
    | swissEduPersonHomeOrganization | [uzh.ch](http://uzh.ch/){:target="\_blank"}| Heimorganisation/Universität   |
    | swissEduPersonHomeOrganizationType | university | Art der Heimorganisation   |
    | eduPersonAffiliation | student | Funktion innerhalb der Heimorganisation  
    | [swissEduPersonStudyBranch1](https://www.switch.ch/aai/support/documents/attributes/swissedupersonstudybranch1/){:target="\_blank"} | 4 | Studienrichtung 1. Klassifikation |
    | [swissEduPersonStudyBranch2](https://www.switch.ch/aai/support/documents/attributes/swissedupersonstudybranch2/){:target="\_blank"} | 42  (=Naturwissenschaften) | Studienrichtung 2. Klassifikation |  
    | [swissEduPersonStudyBranch3](https://www.switch.ch/aai/support/documents/attributes/swissedupersonstudybranch3/){:target="\_blank"} | 4600 (=Chemie) | Studienrichtung 3. Klassifikation |
    swissEduPersonStudyLevel | 15 | Beschreibung des Studien-Fortschrittes   |
    | eduPersonEntitlement | <https://vam.uzh.ch/surgery> | Zugriffsrecht auf Ressource   |
    | employeeNumber | 01-234-567 | Matrikelnummer (nur für Studierende der Universität Zürich)   |
    | organizationalUnit | 1 | Einheit der Heimorganisation z.B. Fakultät (nur für Mitarbeiter) |
  
Einen Link zur Liste der möglichen Attribut-Werte finden Sie im Appendix der
Spezifikation der AAI-Attribute (pdf-Datei) ab Seite 20. [Spezifikation der
AAI-Attribute (pdf-Datei)](http://www.switch.ch/aai/docs/AAI_Attr_Specs.pdf){:target="\_blank"}

Für weitere Informationen zu Werten oder der Anwendung von AAI-Attributen,
wenden Sie sich in der Schweiz bitte an [Switch](https://www.switch.ch/de/){:target="\_blank"}, und in Deutschland an das [Deutsche
Forschungsnetz](https://www.aai.dfn.de/){:target="\_blank"}.
