# Schritt 5: Kurs veröffentlichen

Der **Kursbesitzer** veröffentlicht den Kurs für die Teilnehmer, sodass diese auf die Lerninhalte zugreifen können.  
Der Kurs ist nun auch im Bereich "Kurse" auffindbar.  
Alle **Mitglieder** der Lernressource haben Zugriff.

→ [Publikationsstatus](../../manual_user/learningresources/Access_configuration.de.md)  
→ [Kurse](../../manual_user/area_modules/Courses.de.md#catalog)  
→ [Zugriffskonfiguration](../../manual_user/learningresources/Access_configuration.de.md)

## Zugriffskonfiguration für Campusgruppe "\_ UZH-Studierende mit Modulbuchung"

Die Zugriffskonfiguration muss für die Campusgruppe "\_ UZH-Studierende mit Modulbuchung" nicht mehr separat gesteuert werden, da diese Teilnehmer bereits automatisch via Modulbuchungssystem eingefügt werden.

## Zugriffskonfiguration für Campusgruppe "\_ Manuell hinzugefügte Teilnehmende"

Wenn Sie möchten, dass sich Gäste, Auditoren oder externe Zuhörer selbständig in die Campusgruppe "\_ Manuell hinzugefügte Teilnehmende" einschreiben können, ist es nötig, die [Zugriffskonfiguration](../../manual_user/learningresources/Access_configuration.de.md) auf "Offen" zu haben. So kann der Kurs über die Suche aufgefunden werden und Teilnehmer der Campusgruppe "\_ Manuell hinzugefügte Teilnehmende" können sich selbständig über den "[Einschreibe](../../manual_user/learningresources/Administration_and_Organisation.de.md#enrolment)"-Baustein für den Kurs einschreiben. Zudem muss der [Kurs für die Organisation "OLAT"](../../manual_user/learningresources/Access_configuration.de.md) freigegeben sein da Studierende der Campusgruppe "\_ Manuell hinzugefügte Teilnehmende" ausserhalb der Organisation "uzh.ch" sind.

Bleibt die [Zugriffskonfiguration](../../manual_user/learningresources/Access_configuration.de.md) auf "Privat", muss der Kursbesitzer die Teilnehmer der Campusgruppe "\_ Manuell hinzugefügte Teilnehmende" manuell in die [Mitgliederverwaltung](../../manual_user/learningresources/Members_management.de.md) einfügen, da diese den Kurs nicht über die Suche auffinden können.
