# Step 5: Publish course

The **course owner** publishes the course for the participants so that they can access the learning content.  
The course can now also be found in the "Courses" area.  
All **members** of the learning resource have access.

→ [Publication status](../../manual_user/learningresources/Access_configuration.md)  
→ [Courses](../../manual_user/area_modules/Courses.md#catalog)  
→ [Access configuration](../../manual_user/learningresources/Access_configuration.md)

## Access configuration for the campus group "\_ UZH students with booking modules"

The access configuration no longer has to be controlled separately for the campus group "\_ UZH students with booking modules", as these participants are already automatically inserted via the module booking system.

## Access configuration for the campus group "\_ Manually added participants"

If you want guests, auditors or external listeners to be able to independently enrol in the campus group "\_ Manually added participants", the [access configuration](../../manual_user/learningresources/Access_configuration.md) must be set to "Open". The course can be found using the search function and participants in the campus group "\_ Manually added participants" can register for the course independently using the ["Enroll" module](../../manual_user/learningresources/Administration_and_Organisation.md#enrolment). In
addition, the course must be approved for the "OLAT" organisation because students of the campus group "\_ Manually added participants" are outside the "uzh.ch" organisation.

If the [access configuration](../../manual_user/learningresources/Access_configuration.md) remains "Private", the course owner must manually add the participants of the campus group "\_ Manually added participants" to the [member administration](../../manual_user/learningresources/Members_management.md), as they cannot find the course using the search function.
