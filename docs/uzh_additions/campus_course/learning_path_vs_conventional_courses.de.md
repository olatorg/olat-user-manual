# Campuskurse: Herkömmlicher Kurs versus Lernpfadkurs mit Lernpfad oder mit Lernfortschritt


Mit dem OLAT Update von August 2023 können Campuskurse zusätzlich zu den bisherigen Kurstypen 'Herkömmlicher Kurs' und "[Lernpfadkurs](../../manual_user/learningresources/Learning_path_course.de.md)" nun auch als Kurs vom Typ 'Lernpfadkurs mit Lernfortschritt' erstellt werden.

Wählen Sie beim Erstellen Ihres Campuskurses den gewünschten Typ aus:

![](attachments/step_1_create_campus_course_de/Campuskurs-Erstellen.png){ class="lightbox" }


Danach können Sie zwischen folgenden Optionen wählen:

  * [Option 1: Standardkurs mit Materialienordner und Kommunikationsmoglichkeiten erstellen](/campus_course/#option-1-standardkurs-mit-materialienordner-und-kommunikationsmoglichkeiten)
  * [Option 2: Campuskurs auf Basis eines eigenen Kurses erstellen](/campus_course/#option-2-campuskurs-auf-basis-eines-eigenen-kurses)
  * [Option 3: Campuskurs des Vorsemesters weiterführen](/campus_course/#option-3-campuskurs-des-vorsemesters-weiterfuhren)

!!! warning "Umwandeln eines bereits erstellten Campuskurses"

    Wurde ein Campuskurs einmal erstellt, gibt es keine automatische Möglichkeit, diesen von einem Lernpfad-Kurs in einen herkömmlichen Kurs oder umgekehrt, von
    einem herkömmlichen Kurs in einen Lernpfad-Kurs umzuwandeln. **Mit dieser Aktion würde die Anbindung an das Modulbuchungssystem verloren gehen.**

    Sollten Sie den Kurs im "falschen" Typ erstellt haben, löschen Sie bitte den Campuskurs. Ein Ausloggen und erneutes Einloggen in OLAT wird dazu nötig sein,
    damit die Liste Ihrer Campuskurse wieder aktualisiert wird. Danach können Sie Ihren Campuskurs im gewünschten Typ erneut erstellen.

    Möchten Sie einen Lernpfadkurs in Kurs mit Lernfortschritt umwandeln, gehen Sie bitte gemäss untenstehender Anleitung vor. Dies ist eine Konfiguration welche Sie selbständig vornehmen können und bedingt keine Löschung des bereits erstellten Campuskurses so wie es bei der Umwandlung von herkömmlich zu Lernpfad oder umgekehrt bedingt.

Bei Unsicherheiten oder Fragen steht Ihnen der [OLAT-Support](https://www.zi.uzh.ch/de/support/e-learning-and-examination/staff/olat.html) gerne zur Verfügung.

## Neues Kursdesign "Mit Lernfortschritt"

Das Kursdesign "Mit Lernfortschritt" unterscheidet sich in drei Einstellungen zum Kursdesign "Lernpfad":

* Navigation
  * Freie Navigation durch die Kursbausteine ohne sequenzielle Reihenfolge und damit ohne obligatorische Erledigung des vorhergehenden Kursbausteines wie es im Lernpfad üblich wäre.<br /><br />
  Diese Einstellung finden Sie in Ihrem Kurs unter Administration - Kurseditor - Tab Lernpfad des obersten Kursbausteines - "Abfolge der Lernschritte"<br /><br />
  ![](attachments/learning_path_vs_conventional_courses_de/Kurseditor-Lernpfad.png){ class="lightbox" }

* Layout
  * Keine Anzeige des Pfades in der Navigation
  * Anzeige der Kursbausteine-Icons in der Navigation
  * Diese beiden Einstellungen finden Sie in Ihrem Kurs unter Administration - Einstellungen - Layout - "Icons im Menü anzeigen" und "Pfad im Menü anzeigen"<br /><br />
  ![](attachments/learning_path_vs_conventional_courses_de/Einstellungen-Layout.png){ class="lightbox" }<br /><br />
  * Diese Einstellung ist dafür zuständig, ob die Navigation im Kurs als Pfad oder in Kursbaustein-Optik angezeigt wird<br /><br />
  ![](attachments/learning_path_vs_conventional_courses_de/Navigation-Lernpfad.png){ class="lightbox thumbnail" }<br /><br />
  ![](attachments/learning_path_vs_conventional_courses_de/Navigation-Lernfortschritt.png){ class="lightbox thumbnail" }

  Weitere Informationen über die neuen Kursdesigns können Sie hier oder im Autorenbereich bei der Erstellung eines neuen Kurses entnehmen:
  
  ![](attachments/learning_path_vs_conventional_courses_de/Lernfortschritt-Schritt1.png){ class="lightbox" }

  ![](attachments/learning_path_vs_conventional_courses_de/Lernfortschritt-Erklaerung.png){ class="lightbox" }


