# Step 4: Set up course access for participants

The **course owner** configures how the participants can access the course.

→ [Access configuration](../../manual_user/learningresources/Access_configuration.md)

## Learning area

[Learning area](../../manual_user/learningresources/Using_Course_Tools.md) is an object that combines several groups.  
The "campus learning area" combines the campus groups "\_ UZH students with booking modules" and "\_ Manually added participants", i.e. campus learning area = "\_ UZH students with booking modules" \+ "\_ Manually added participants".  
Group-dependent visibility and access rules are possible in campus courses. 
There you select "Only for the learning areas: Campus learning area", which, as described above, consists of the campus groups "\_ UZH students with booking modules" and "\_ Manually added participants".

## Campus groups "\_ UZH students with booking modules" und "\_ Manually added participants"

!!! info "Campus groups"

    With the update to OLAT 15 (August 2021), the campus groups have been renamed:

    Campus group A → UZH students with booking modules

    Campus group B → Manually added participants

### Campus group "\_ UZH students with booking modules"

The list of members of the campus group "\_ UZH students with booking modules" shows you which students have booked your module. This list is synchronised several times a day with the booking data of the module booking system.  
Participants in the campus group "\_ UZH students with booking modules" cannot be deleted as this is linked to registration in the module booking system.  
In the event of cancellation, the students will automatically be removed from the campus group "\_ UZH students with booking modules" the next time they are synchronised with the module booking system.

### Campus group "\_ Manually added participants"

The campus group "\_ Manually added participants" has no such connection to the module booking system. Auditors, guests or external students who have not been booked via the module booking system can participate in this group.  
The member administration of the campus group "\_ Manually added participants" is taken over by the lecturers.  
In order for students to be able to enrol independently in the campus group "_Manually added participants", the course must be freely accessible. More information on this in [step 5](step_5_publish_course.md).

## Check the number of participants

To check which students have registered for the module/course, start your course and click on "My course" in the top right corner and then on the link "\_ UZH students with booking modules".  
If the number of participants does not match the number in the module booking system, this can have various causes:

  * Students have booked the module, but have never registered in OLAT with their UZH address, so there is no OLAT user account for the students.
    * The students log into OLAT once, after which their user account is automatically assigned to the campus course the next time it is synchronised with the booking system.
  * The students booked the module, but not the events.
    * To do this, please contact the person responsible for SAP at your institute so that the module booking can be checked.

For other cases not listed, send a message to [OLAT-Support](mailto:lms-support@zi.uzh.ch).
