# Campus courses: conventional course vs. learning path course with learning path or with learningprogress


With the OLAT update of August 2023, campus courses can now also be created as courses of the type 'Learning path course with learning progress' in addition to the previous course types 'Conventional course' and '[Learning path](../../manual_user/learningresources/Learning_path_course.md)'.

When creating your campus course, select the desired type:

![](attachments/step_1_create_campus_course/Campuskurs-Erstellen_EN.png){ class="lightbox" }

You can then choose between the following options:

  * [Option 1: Create a standard course along with download folder and the possibility to communicate](/campus_course/#option-1-standard-course-with-materials-folder-and-communication-tools)
  * [Option 2: Create a campus course by using one of your own courses](/campus_course/#option-2-campus-course-based-on-your-own-course)
  * [Option 3: Continue a campus course of a previous semester](/campus_course/#option-3-continue-the-campus-course-from-the-previous-semester)

!!! warning "Converting an already created campus course"

    Once a campus course has been created, there is no automatic way to convert it from a learning path course to a conventional course or vice versa, from a conventional course to a learning path course. **This action would lose the link to the module booking system.**

    If you have created the course in the "wrong" type, please delete the campus course. Logging out and logging in again in OLAT will be necessary for this so that the list of your campus courses is updated again. Afterwards you can create your campus course in the desired type again.

    If you want to change a learning path course into a course with learning progress, please follow the instructions below. This is a configuration that you can perform by yourself and does not require deletion of the campus course already created, as is the case with the conversion from conventional to learning path or vice versa.


If you have any questions, please do not hesitate to contact [OLAT support](https://www.zi.uzh.ch/en/support/e-learning-and-examination/staff/olat.html).

## New coursedesign "With learning progress"

The course design "With learning progress" differs from the course design "Lenrpath" in three settings:

* Navigation
  * Free navigation through course elements without sequential order and thus without obligatory completion of the previous course element as it would be usual in a learning path.<br /><br />
  You can find this setting in your course under Administration - Course editor - Tab Learning path of the uppermost course element - "Sequence of steps".<br /><br />![](attachments/learning_path_vs_conventional_courses/EN-Courseeditor-Learningpath-Sequence.png){ class="lightbox" }

* Layout
  * No display of the path in the navigation
  * Display of course element icons in navigation
  * These two settings can be found in your course under Administration - Settings - Layout - "Display icons in menu" and "Display path in menu"<br /><br />![](attachments/learning_path_vs_conventional_courses/EN-Settings-Layout.png){ class="lightbox" }

  * This setting is responsible for whether the navigation in the course is displayed as a path or in course element optics<br /><br />![](attachments/learning_path_vs_conventional_courses/EN-learningpath.png){ class="lightbox thumbnail" }<br /><br />
  ![](attachments/learning_path_vs_conventional_courses/EN-learningprogress.png){ class="lightbox thumbnail" }

You can find more information about the new course designs here or in the authoring menu when creating a new course:

![](attachments/learning_path_vs_conventional_courses/EN-Authoring-create-courses.png){ class="lightbox" }

![](attachments/learning_path_vs_conventional_courses/EN-coursedesigns.png){ class="lightbox" }
