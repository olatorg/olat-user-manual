# Campus courses (UZH only)
??? abstract "Table of Contents"

    * [Basics](#basics)<br />
        * [What is a "Campus course"?](#definition)<br />
        * [Prerequisites](#prerequisites)<br />
        * [Advantages](#advantages)<br />
    * [Course creation](#creation)<br />
        * [Course Design](#coursedesign)<br />
        * [Options](#options)<br />
    * [Roles and rights](#roles-and-rights)<br />
        * [Members management](#members-management)<br />
        * [Learning area](#learning-area)<br />
        * [Delegations](#delegations)<br />
        * [Learning resource manager](#learning-resource-manager)<br />
    * [Course content](#course-content)<br />
    * [Publication and lifecycle](#publication-and-lifecycle)<br />
        * [Publish](#publish)<br />
        * [During the course](#during-the-course)<br />
        * [Finish](#finish)<br />
        * [Deleting](#deleting)<br />
    * [FAQ](#faq)


## Basics {: #basics }
### What is a "Campus Course"? {: #definition }
Campus courses are linked to SAP, which connects them to the UZH’s module booking system. The SAP data is synchronized multiple times a day. 

![](attachments/campus_courses_uzh_only/Campus_course_synchronisation.png)

Lecturers can choose whether to create a campus course for their lecture.   

When lecturers create a campus course, students who have booked the module are automatically added to the campus group "_UZH Students with booking modules" of the campus course. This grants students access to the course materials. No further enrollment in the campus course is required. 

In a "normal" OLAT course, students must enroll in the OLAT course in addition to booking the module. 

### Prerequisites
A lecture must be part of one of the following organizational units:

* Faculty of Arts and Social Sciences  
* Faculty of Business, Economics and Informatics 
* Faculty of Science 
* Human Medicine of the Faculty of Medicine  
* School for Transdisciplinary Studies (STS)  

Lecturers must meet the following three requirements: 

* Listed as lecturers in the [UZH course catalog](https://studentservices.uzh.ch/uzh/anonym/vvz/index.html?sap-language=EN&sap-ui-language=EN){ target="\_blank"}
* UZH personnel number and UZH short name  
* OLAT account linked to UZH and access to the author area  


### Advantages {: advantages }

For students 

* Courses appear in the course list in OLAT after module booking (under "Active" or "In preparation").

For instructors

* A fully functional course including member synchronization, a materials folder, and an e-mail template, can be created in just two clicks. 
* The campus course is updated multiple times a day with new data. Title, course code, description and members remain up to date without manual editing. 
* In the campus group "_ UZH students with booking modules ", the names of students who have booked the module (and corresponding course) can be found. 
* Additional course elements can be added as needed (just like in conventional courses). 
<br /><br />
<hr />

## Course Creation {: #creation }

Under "Campus courses," lecturers will find a list of their own courses according to the UZH course catalog. By clicking the "Create" link, the corresponding campus course can be generated. 

![](./attachments/Campus_courses_create.png){ class="border lightbox" }

### Course Design {: #coursedesign }

There are three designs available to create a campus course:

* [Learning Path Course](learning_path_vs_conventional_courses.md)
* [Learning Path Course with learning progress](learning_path_vs_conventional_courses.md) 
* [Conventional course (Classic)](learning_path_vs_conventional_courses.md)

<br />

![](./attachments/Campuskurs_Klassisch.png){ class="border lightbox" }


Which course design is right for you depends primarily on __how you want to structure your teaching__. If you wish to provide participants with an overview of their progress with the learning content, the two "Learning Path" course designs are suitable. If the course should be completed in a sequential order, choose "With learning path," and if the order should be flexible, choose "With learning progress." Additionally, the classic design without progress tracking and without a fixed sequence is available. 


If you want to create your campus course __based on an already existing OLAT course__ in the next step, the course design of the selected base course will apply.  
<br /><br />

#### Classic (type: Conventional Course) {: #typeclassic }
For this course, a Template with the following components will be used:  

* [Enrolment](../../manual_user/learningresources/Course_Element_Enrolment.md) (for the registration of auditors)
* [Folder](../../manual_user/learningresources/Course_Element_Folder.md) (for "Materials")
* [Notification](../../manual_user/learningresources/Course_Element_Notifications.md) (page for notifications in the course)
* [Forum](../../manual_user/learningresources/Course_Element_Forum.md)
* [Kaltura](../kaltura/) (to stream lecture recordings and other videos)
* [Task](../../manual_user/learningresources/Course_Element_Task.md) (for the assignment of tasks and for the submission of students’ papers)
<br />
    In the "Area for instructors" (visible only to course owners and coaches): 
    * [E-mail course element](../../manual_user/learningresources/Course_Element_EMail.md) (@Students; for communication with students)
    * [Participant list](../../manual_user/learningresources/Course_Element_Participant_List.md)
    * [E-mail course element](../../manual_user/learningresources/Course_Element_EMail.md) (@OLAT Support; for support requests to the OLAT Support)
    * Recommended elements:
        * [Literature](../literature/) (to provide a bibliography) 
        * [Zoom](../supporting_technologies/zoom_video_conferences.md) (for setting up online lectures via your UZH zoom account)
        
![](attachments/Campuskurs_option_1.png){ class="lightbox" }

![](attachments/Campuskurs_option_1_fertigstellen.png){ class="lightbox" }
 
The following settings are used:  

* [Publication Status](../../manual_user/learningresources/Access_configuration.md): Published (Learners will see the course immediately under "Courses" – "Active")
* [Share (Administration > Settings > Tab: Share)](../../manual_user/learningresources/Access_configuration/#tab-share):
    * Bookable and open offers (the course can be found and accessed via the course search) 
    * Offer: “Offer without booking” (participants are synchronized with module booking)  
    * Organization: OLAT, UZH, faculty, institute (if available)  

The campus course is ready to use immediately. 
<br /><br />

#### With learning path (type: learning path course) 

This course uses a template with the following modules: 

* [Course element "Enrolment"](../../manual_user/learningresources/Course_Element_Enrolment.md) "Registration" (for auditors)
* [Course element "HTMLpage"](../../manual_user/learningresources/Course_Element_HTML_Page/) with "instructions for instructors" 


The following settings are used:

* [Publication Status](../../manual_user/learningresources/Access_configuration.md): In preparation
* [Share](../../manual_user/learningresources/Access_configuration/#tab-share):
    * Open access (no registration required; the course can be found and accessed via the course search) 
    * Organisation: OLAT, UZH, Faculty, Institute (if applicable)

The campus course is not yet ready for use. This template differs significantly from the template for standard campus courses. Post-processing is necessary. 

Further information can be found in the [Semester checklist for lecturers](../semesterchecklist_for_lecturers/)
<br /><br />

#### With learning progress (Type: learning path course) 

This course uses a template with the following modules:  

* [Course element "Enrolment"](../../manual_user/learningresources/Course_Element_Enrolment.md) "Registration" (for auditors)
* [Course element "HTMLpage"](../../manual_user/learningresources/Course_Element_HTML_Page/) with "guidelines for instructors"  

The following settings are used:

* [Publication status](../../manual_user/learningresources/Access_configuration.md): In preparation
* [Share](../../manual_user/learningresources/Access_configuration/#tab-share):
    * Bookable and open offers (no registration required; the course can be found and accessed via the course search)   
    * Organisation: OLAT, UZH, faculty, institute (if applicable)  

The campus course is not yet ready for use. This template differs significantly from the template used for standard campus courses. Further editing is absolutely required. 


### Options

You can choose from three options for creating your campus course 
<br /><br />

#### Option 1: Standard course with materials folder and communication tools 
A new course is created based on the template. 

If you are creating an OLAT course for the first time, this is the only option available for you.  
<br /><br />

####  Option 2: Campus course based on your own course 
The new campus course is created based on the structure and course element configurations of an OLAT course you have already created. 

![](attachments/Campuskurs_option_2.png){ class="lightbox" }

Select the course that will serve as a template for the new campus course. Here, you will find a list of all courses visible in your authoring area that matches the previously selected course type. 

![](attachments/Campuskurs_option_2_auswahl.png){ class="lightbox" }

The materials from the old course, as well as the configurations of the course elements, will be transferred to the new campus course.  
<br /><br />

####  Option 3: Continue the campus course from the previous semester  
This option is suitable for multi-semester events. The campus course will be continued with the same course members (according to module enrollment).

![](attachments/Campuskurs_option_3.png){ class="lightbox" }

Select the course that should be continued. Here, you will find a list of all courses visible in your authoring area that matches the previously selected course type. 

![](attachments/Campuskurs_option_3_auswahl.png){ class="lightbox" }

The titles of these campus courses contain two semester abbreviations, e.g.  "**24HS/25FS** _Name of the course_", making them easy to identify as continued campus courses. 
<br /><br />

### Finish

![](attachments/Campuskurs_option_3_fertigstellen.png){ class="lightbox" }
<br /><br />

### Period 

Starting from **February 1st**, you can create the campus courses **for the current spring semester and the following fall semester**.  

Starting from **August 2nd** (National Day on August 1st!), you can create the campus courses **for the current fall semester and the following spring semester.** 
<br /><br />
<hr />

## Roles and Rights {: #roles-and-rights }
### Members management 

Related topics: [Members management](../../manual_user/learningresources/Members_management.md) / [Access configuration](../../manual_user/learningresources/Access_configuration.md)
<br /><br />

#### Course Owners  
**Course owners** can add other course owners and coaches so that they can assist with setting up, administrating and/or assessing.  

!!! info "Delegations / Learning resource manager"

    In campus courses, all users registered as delegates are automatically registered as course owners. 
    Learning resource managers who create campus courses are also registered as course owners 
    Related topics: [Delegations](../delegation/), [Learning resource management](#learning-resource-manager)
<br />

####  Group management 

Following campus groups are created automatically: 

* \_ UZH Students with booking modules
* \_ Manually added participants 

The campus group "_ UZH Students with booking modules" is synchronized multiple times daily with the booking data from the module booking system. Group participants cannot be deleted. In case of cancellation, students will automatically be removed from the group during the next synchronization.  

The campus group "_ Manually added participants" does not have a connection to the module booking system. Auditors or external students who were not booked through the module booking system can be added to this group. According to the default settings of the campus course template, users can register in this group via the course element “Enrollment”. Course owners are responsible for this.  

### Learning area 

A [Learning area](../../manual_user/learningresources/Using_Course_Tools/#learning_area) combines multiple groups. 

The "Campus learning area" brings together the "_ UZH Students with booking modules" and "_ Manually added participants".  

In campus courses, visibility and access rules can be defined based on groups or learning areas. 

### Delegations

Delegates of instructors can create and administer their campus courses. The delegation is particularly recommended for e-learning coordinators or assistants who take over the creation and administration for the instructors they support. 

Delegations can only be granted to OLAT authors and apply exclusively to campus courses.  

!!! info "Request delegation rights "

    Request a delegation with the consent of the instructor from [OLAT Support](mailto:lms-support@zi.uzh.ch).

### Learning resource manager 

Learning resource manager for an institute or faculty at UZH can create and administer campus courses for that institute or faculty. 

If you create a campus course, you will automatically be registered as the course owner of the campus course.  

For the learning resource management to function at the faculty or institute level, in campus courses the organizations "OLAT," "UZH," the relevant faculty, and the relevant institute are added automatically in the share settings.  

!!! info "Request “Learning resource manager” rights"

    To optain the role of “Learning resource manager” for an institute or faculty, written consent from the faculty or institute is required. Please request the application form from [OLAT Support](mailto:lms-support@zi.uzh.ch). 
<br />
<hr />

## Course Content {: #course-content }

**Course owners** can add new course elements and configure existing ones through the course editor, allowing the desired learning content to be presented to the participants.   

Related Topics: [Course elements](../../manual_user/learningresources/Course_Elements/), [general configuration of course elements](../../manual_user/learningresources/General_Configuration_of_Course_Elements/), [course editor](../../manual_user/learningresources/Using_additional_Course_Editor_Tools/), [course settings](../../manual_user/learningresources/Course_Settings/), [publish course elements](../../manual_user/learningresources/Using_additional_Course_Editor_Tools/#publishing )

### Custom Settings 
If your desired configuration deviates from the standard course template, you will need to adjust it independently. This is especially important when creating a campus course using option 2 ("Create a campus course by using one of your own courses already existing.") as the settings of the individual components must be carefully reviewed. For example, the recipient group in the course element e-mail “@Students” must be adjusted to the current campus learning area. 
<br /><br />
<hr />

## Publication and lifecycle {: #publication-and-lifecycle}
Related topics: [Publication status](../../manual_user/learningresources/Access_configuration/), [courses](../../manual_user/area_modules/Courses/), [access configuration](../../manual_user/learningresources/Access_configuration/) 

### Publish
__Course owners__ publish their course for the participants, allowing them to access the learning content. From this point on, the course is available to participants in the sections "Courses" and "Active" from the main menu item “Courses” and the __members__ have access to the course content according to the settings.  

### During the course
__Course owners__ can add new learning content or make changes during the ongoing course. Any changes must always be published.

### Finish
Once course owners end a course (status "finished"), course members still have access to the course but can only read. The course will no longer appear under "Active," but will be found in the "Finished courses" tab.  

### Deleting
Once course owners delete a course (Administration > Delete), the course will appear for course owners in the "Deleted learning resources" tab of the menu “Authoring”. Members will no longer have access.  
<br /><br />
<hr />

## FAQ {: #faq }

[Where can I create campus courses?](#where-can-i-create-campus-courses)<br />
[How do I convert a conventional course into a campus course?](#how-do-i-convert-a-conventional-course-into-a-campus-course)<br />
[Where is the campus course I just created?](#where-is-the-campus-course-i-just-created)<br />
[How can I merge campus courses or integrate campus groups into a conventional course?](#merge)<br />
[How do I check the number of participants in my course?](#checkmembers)<br />
[Why does the number of participants in my course not match the module bookings?](#membersdiff)<br />
[How can auditors find my course?](#auditoraccess)<br />
[How do I define new recipient groups in the email course element "@Students"?](#mailrecipients)

<br /><br />

### Where can I create campus courses? 

As an instructor at UZH with courses in the current semester, you should see an entry for "Campus Courses" in the main navigation of OLAT, alongside "Courses," "Groups," and "Authoring" 
    
It may happen that the "Campus Courses" tab is not displayed or that there are missing entries. Possible causes for this may include: 

1. Change of university (recently employed at UZH)
2. Change of employment status (new employment contract and new employee number at UZH)
3. Multiple OLAT accounts
4. OLAT registration occurred only a few minutes/hours ago (the next synchronization is still pending) 
5. Missing prerequisites for creating a campus course (see section "[Prerequisites](#prerequisites)") 

If none of these reasons apply to you, or if you are unable to resolve the issue yourself (reasons 1, 2, or 3), please send an email from your UZH email address to the [LMS Support](mailto:lms-support@zi.uzh.ch) with the following information:

* Exact course title (as per the course catalog) 
* OLAT username (visible under “Profile” in the black personal menu accessible via your user image in the top right corner of the application window) 
* Suspected cause 

<hr />

### How do I convert a conventional course into a campus course? 

Click on the link "create" in the "Campus Courses" tab and select the option  [Option 2: Campus course based on your own coursen](#option-2-campus-course-based-on-your-own-course)". Use the existing course as the template.

<hr />

### Where is the campus course I just created? 

You can find already created campus courses in the menu item "Authoring > My courses". Additionally, a link to your course will appear in the tab "Active” of the menu item "Courses", as you are registered as the course owner.  

For course participants, the campus course is only visible under “Courses > Active”, after the course status (published) and [share settings](../../manual_user/learningresources/Access_configuration/#tab-share) have been configured. 

<hr />

### How can I merge campus courses or integrate campus groups into a conventional course?  {: #merge }

#### Practical example 
There is a lecture for a module and, due to the high number of participants, three tutorials. The allocation to the three tutorial groups is done through the module booking system. The material for all four events is to be provided in a single course, namely in the campus course for the lecture. 
The courses are referred to below as 'Main Course' (Campus course created for the lecture) and 'Additional Course 1-3' (Campus courses created for the tutorials). It does not matter whether the standard template or a custom template is used for creation."  

#### Approach 

1. Preparations:
    1. Create all four campus courses or have all four campus courses created  erstellen.  
    2. Have yourself enrolled as the course owner in all four campus courses. 
2. Open your "Additional Course 1" and copy the title of the course (_Additional Course 1_).  
3. Rename the campus group: In "Additional Course 1" go to "Administration" and select "Members management". In the navigation on the left side of the screen, select "Groups". Click on"_ UZH Students with booking modules" and then "Administration". Enter a new unique group name, for example, 'BM _Additional Course 1_, Group 1' ("BM" for"\_ UZH Students with <strong>b</strong>ooking <strong>m</strong>odules"). Click on "Finish".
4. Go one step back in the browser history or click on the left-facing angle icon to the left of the course title to return to the group overview of the group management and copy the ID of the group you just renamed. 
5. Open the ‘Main Course’ and go to “Administration > Members Management” and select “Groups”.  
6. Select 'Add Group' (button on the right side) and go to the filter tab 'Courses'. Search for the title of the 'Main Course' and add it. 
__Now, the renamed campus group '\_ UZH Students with booking modules' from 'Additional Course 1' is integrated into the 'Main Course' and can be used there for visibility and access control.__
7. Now repeat steps 2-6 for the other ‘Additional Courses’ 2 and 3 and the Campus group "_ UZH Students with booking modules" from these courses.<br /><br />__Recommendation:__ Place a note in 'Additional Course 1' in the top course element stating that the course will not be filled with materials and that the materials will be placed in the 'Main Course'. For this, enter an appropriate text in the top course module under the 'Visibility' tab in 'Information when visible and no access'. In the 'Access' tab, check the option 'Blocked for learners'.
8. Publish the changes.

#### Alternative option without synchronization 

Alternatively, you can merge campus groups manually by exporting the participants from one campus group and importing them into another group. With this option, the connection to the module booking is lost. The synchronization of participants from the module booking system only works with the originally linked (externally managed) campus group '_ UZH Students with Module Booking' of the campus course." 

<hr />

### How do I check the number of participants in my course? {: #checkmembers }

To compare the number of participants in your course with the number of bookings in teh module booking system, go to "Administration" (top left corner) > "Members management" and select the tab "Participant". The number of entries represents the total of course members with the role "Participant". 
To check the number of students with module booking in your course (syncronized with the UZH module booking system) select "Groups" in the left side navigation. In the row "_ UZH students with booking modules" you find the number of enrolled students in the column "Participants".

<hr />

### Why does the number of participants in my course not match the module bookings? {: #membersdiff }

If the number of participants does not match the number of students in the module booking system, there may be various reasons for this:  

* Some students have never logged into OLAT with their UZH identity. Therefore, there is no OLAT account for these individual students yet.   
    * Solution: Once the students have created an OLAT account, they will be added to your course during the next synchronization with the module booking system.    

* The students have booked the module, but not the individual events.  
    * Solution: Please contact the SAP administrators at your institute so they can check the module booking 

If your issue is not listed, please contact [LMS Support](mailto:lms-support@zi.uzh.ch).

<hr />

### How can auditors find my course? {: #auditoraccess }

To allow auditors to enroll in the campus group "_ Manually added participants" you need to set the following share settings for your course under “Administration > Tab:[Share](../../manual_user/learningresources/Access_configuration/#tab-share)”:

* Access for participants: “Bookable and open offers” 
* Organizations: "OLAT" (additional organizations possible) 

The course can then be found through the search function, and interested individuals can independently enroll in the course via the "[Enrolment](../../manual_user/learningresources/Administration_and_Organisation/#enrolment)" course element.

Alternatively, you can manage the campus group "_ Manually added participants" by manually adding and removing the individuals concerned. In this case, please make the following release settings for your course under “Administration > Tab: [Share](../../manual_user/learningresources/Access_configuration/#tab-share)”.

* Access for participants: “Private” 
* Organizations: "OLAT" (additional organizations possible) 

<hr />

### How do I define new recipient groups in the email course element "@Students"? {: #mailrecipients }

When creating a campus course using Option 2 ("Create a campus course by using one of your own courses already existing."), you need to reselect the recipient groups in the course element "@Students" (in course editor mode under tab "Recipient").  

For campus courses created as "standard course" (Option 1) and for continued campus courses (Option 3) this follow-up is not necessary.  

#### Procedure 
1. Open your campus course through the menu "Authoring > My courses". 
2. Start the course editor by selecting "Administration" and "course editor". 
3. Select the course element “@Students” from the course menu and select the “Recipient” tab. 
4. Select the option “Distribution to participants > All participants of the selected groups” and then select “Select learning area".  
5. Check the box for "Campus learning area" (this includes the members of your course groups " _ UZH students with booking modules" and " _ Manually added participants”. 
6. Select "Apply" and save the changed setting by clicking the “Save” button at the bottom.
7. Publish the changes.  