# Schritt 4: Kurszugang für Teilnehmer einrichten

Der **Kursbesitzer** konfiguriert, wie die Teilnehmer auf den Kurs zugreifen können.

→ [Zugriffskonfiguration](../../manual_user/learningresources/Access_configuration.de.md)

## Lernbereich

[Lernbereich](../../manual_user/learningresources/Using_Course_Tools.de.md) ist ein Objekt, welches mehrere Gruppen zusammenfasst.  
Der "Campuslernbereich" fasst die Campusgruppen "\_ UZH-Studierende mit Modulbuchung" und "\_ Manuell hinzugefügte Teilnehmende" zusammen, das heisst Campuslernbereich = "\_ UZH-Studierende mit Modulbuchung" \+ "\_ Manuell hinzugefügte Teilnehmende".  
In Campuskursen sind gruppenabhängige Sichtbarkeits- und Zugangsregeln möglich. Dort wählen Sie "Nur für die Lernbereiche: Campuslernbereich" aus, welcher wie oben beschrieben, aus den Campusgruppen "\_ UZH-Studierende mit Modulbuchung" und "\_ Manuell hinzugefügte Teilnehmende" besteht.

## Campusgruppe "\_ UZH-Studierende mit Modulbuchung" und "\_ Manuell hinzugefügte Teilnehmende"

!!! info "Campusgruppen"
    Mit dem Update auf OLAT 15 (August 2021) wurden die Campusgruppen umbenannt:

    Campusgruppe A → UZH-Studierende mit Modulbuchung

    Campusgruppe B → Manuell hinzugefügte Teilnehmende

### Campusgruppe "\_ UZH-Studierende mit Modulbuchung"

Die Mitgliederliste der Campusgruppe "\_ UZH-Studierende mit Modulbuchung" zeigt Ihnen, welche Studierenden Ihre Veranstaltung gebucht haben. Diese Liste wird mehrmals täglich mit den Buchungsdaten des Modulbuchungssystems synchronisiert.  
Teilnehmer der Campusgruppe "\_ UZH-Studierende mit Modulbuchung" können nicht gelöscht werden da dies an die Einschreibung des Modulbuchungssystems gebunden ist.  
Bei einer Stornierung werden die Studierenden automatisch bei der nächsten Synchronisation mit dem Modulbuchungssystem aus der Campusgruppe "\_ UZH-Studierende mit Modulbuchung" ausgetragen.

### Campusgruppe "\_ Manuell hinzugefügte Teilnehmende"

Die Campusgruppe "\_ Manuell hinzugefügte Teilnehmende" hat keine solche Anbindung an das Modulbuchungssystem. In dieser Gruppe können Auditoren, Gäste oder externe Studierende, die nicht via Modulbuchungssystem gebucht wurden, teilnehmen.  
Die Mitgliederverwaltung der Campusgruppe "\_ Manuell hinzugefügte Teilnehmende" wird von den Dozierenden übernommen.  
Damit sich Studierende selbständig in die Campusgruppe "\_ Manuell hinzugefügte Teilnehmende" einschreiben können, bedingt es, dass der Kurs frei zugänglich ist. Weitere Informationen dazu in [Schritt 5](step_5_publish_course.de.md).

## Anzahl Teilnehmer überprüfen

Um zu überprüfen, welche Studierenden sich für die Veranstaltung eingeschrieben haben, starten Sie Ihren Kurs und klicken oben rechts auf "Mein Kurs" und dann auf den Link "\_ UZH-Studierende mit Modulbuchung".  
Sollte die Anzahl Teilnehmer nicht mit der Anzahl in Modulbuchungssystem übereinstimmen, kann dies verschiedene Ursachen haben:

  * Studierende haben zwar das Modul gebucht, haben sich aber noch nie mit ihrer UZH-Adresse in OLAT angemeldet, es existiert also noch kein OLAT Benutzerkonto für die Studierenden.
    * Die Studierenden loggen sich einmalig in OLAT ein, danach wird ihr Benutzerkonto automatisch dem Campuskurs zugewiesen bei der nächsten Synchronisation mit dem Buchungssystem.
  * Die Studierenden haben zwar das Modul, nicht aber die Veranstaltungen gebucht.
    * Hierzu wenden Sie sich bitte an die SAP-verantwortlichen Ihres Instituts damit die Modulbuchung überprüft wird.

Für weitere, nicht aufgelistete Fälle senden Sie eine Nachricht an den [OLAT-Support](mailto:lms-support@zi.uzh.ch).
