# Campuskurse 
??? abstract "Inhaltsübersicht"

    * [Basics](#basics)<br />
        * [Was ist ein "Campuskurs?](#definition)<br />
        * [Voraussetzungen](#voraussetzungen)<br />
        * [Vorteile](#advantages)<br />
    * [Course creation](#creation)<br />
        * [Kursdesign](#kursdesign)<br />
        * [Optionen](#optionen)<br />
    * [Roles and rights](#roles-and-rights)<br />
        * [Mitgliederverwaltung](#mitgliederverwaltung)<br />
        * [Lernbereich](#lernbereich)<br />
        * [Delegationen](#delegationen)<br />
        * [Lernressourcenverwaltung](#lernressourcenverwaltung)<br />
    * [Course contents](#course-contents)<br />
    * [Publication and lifecycle](#publication-and-lifecycle)<br />
        * [Veröffentlichung](#veroffentlichung)<br />
        * [Durchführung](#durchfuhrung)<br />
        * [Beenden](#beenden)<br />
        * [Löschen](#loschen)<br />
    * [FAQ](#faq)


## Grundlagen {: #basics }
### Was ist ein “Campuskurs”? {: #definition }
Campuskurse haben eine Anbindung an SAP und damit an das Modulbuchungssystem der UZH. Die SAP-Daten werden mehrmals täglich synchronisiert. 

![](attachments/campus_courses_uzh_only/Synchronisation_VVZ-OLAT.png)

Dozierende können selbständig entscheiden, ob sie zu einer Lehrveranstaltung einen Campuskurs anlegen.  

Falls Dozierende für ihre Lehrveranstaltung einen Campuskurs erstellen, werden Studierende, die das Modul gebucht haben, automatisch in die Campusgruppe **“\_ UZH-Studierende mit Modulbuchung”** des Campuskurses eingetragen. Die Studierenden haben dann Zugang zu den Lehrmaterialien der Lehrveranstaltung. Eine zusätzliche Einschreibung in den Campuskurs ist nicht nötig.  

Bei einem “normalen” OLAT-Kurs müssen sich Studierende zusätzlich zur Modulbuchung in den OLAT-Kurs einschreiben. 

### Voraussetzungen {: #voraussetzungen }
Lehrveranstaltungen müssen zu einer der folgenden Organisationseinheiten gehören:

* Philosophische Fakultät 
* Wirtschaftswissenschaftliche Fakultät 
* Mathematisch-naturwissenschaftliche Fakultät 
* Humanmedizin der Medizinischen Fakultät 
* School for Transdisciplinary Studies (STS) 

Dozierende müssen die drei folgenden Voraussetzungen erfüllen:

* Dozierende gemäss [Vorlesungsverzeichnis der UZH](https://courses.uzh.ch/){ target="\_blank"}
* UZH-Personalnummer und UZH-Shortname 
* mit der UZH verlinkter OLAT-Account und Zugang zum Autorenbereich 


### Vorteile {: advantages }

Für Studierende:

* Kurse erscheinen nach der Modulbuchung in der Kursliste in OLAT <br />(unter “Aktiv” oder “In Vorbereitung”). 

Für Dozierende:

* Mit zwei Mausklicks kann ein sofort einsatzbereiter Kurs mit Mitgliedersynchronisation, einem Materialienordner und einem E-Mail-Baustein erstellt werden. 
* Der Campuskurs wird mehrmals täglich auf neue Daten überprüft. Titel, Lehrveranstaltungskürzel, &#8209;beschreibung und Mitglieder bleiben ohne manuelle Bearbeitung aktuell. 
* In der Campusgruppe "_ UZH-Studierende mit Modulbuchung" sind die Namen der Studierenden zu finden, die das Modul (und somit die Lehrveranstaltung) gebucht haben. 
* Weitere Kursbausteine können bei Bedarf eingebunden werden (wie bei normalen Kursen). 
<br /><br />
<hr />

## Kurserstellung {: #creation }

Unter "Campuskurse" finden Dozierende eine Liste ihrer eigenen Lehrveranstaltungen gemäss Vorlesungsverzeichnis der UZH. Durch einen Klick auf den Link "Erstellen" kann der zugehörige Campuskurs erzeugt werden. 

![](attachments/Campuskurse.png){ class="border lightbox" }

### Kursdesign {: #coursedesign }

Es stehen drei Designs zur Auswahl, um einen Campuskurs zu erstellen:

* [Lernpfadkurs](learning_path_vs_conventional_courses.de.md)
* [Lernpfadkurs mit Lernfortschritt](learning_path_vs_conventional_courses.de.md) 
* [Herkömmlicher Kurs (klassisch)](learning_path_vs_conventional_courses.de.md)

<br />

![](attachments/campuscourse_classic.png){ class="border lightbox" }


Welches Kursdesign für Sie in Frage kommt, hängt in erster Linie davon ab, __wie Sie ihren Kurs didaktisch aufbauen wollen__. Falls Sie den Kursteilnehmenden einen Überblick über den Bearbeitungsstand der Lerninhalte geben möchten, kommen für Sie die beiden Kursdesigns vom Typ Lernpfad in Frage. Falls der Kurs in einer sequenziellen Reihenfolge durchgearbeitet werden soll, wählen Sie “Mit Lernpfad”, falls die Reihenfolge frei sein soll, wählen Sie “Mit Lernfortschritt”. Daneben steht Ihnen das klassische Design ohne Lernfortschritt und ohne fixe Reihenfolge zur Verfügung. 


Wenn Sie im nächsten Schritt Ihren __Campuskurs auf Basis eines bereits von Ihnen erstellten OLAT-Kurses erzeugen möchten__, kommt das Kursdesign des gewählten Basiskurses zur Anwendung. 
<br /><br />

#### Klassisch (Typ: Herkömmlicher Kurs) {: #typeclassic }
Für diesen Kurs wird ein Template mit folgenden Bausteinen verwendet: 

* [Einschreibung](../../manual_user/learningresources/Course_Element_Enrolment.md) (für die Anmeldung von Auditor\*innen)
* [Ordner](../../manual_user/learningresources/Course_Element_Folder.md) (für "Materialien")
* [Mitteilungen](../../manual_user/learningresources/Course_Element_Notifications.md) (Seite für Mitteilungen im Kurs)
* [Forum](../../manual_user/learningresources/Course_Element_Forum.md)
* [Kaltura](../kaltura/) (für Vorlesungsaufzeichnungen und Videos)
* [Aufgabe](../../manual_user/learningresources/Course_Element_Task.md)
<br />
Im Strukturbaustein "Bereich für Dozierende" (nur für Kursbesitzer\*innen und Betreuer\*innen sichtbar):
    * [E-Mail-Baustein](../../manual_user/learningresources/Course_Element_EMail.md) (@Studierende; für die Kommunikation mit Studierenden)
    * [Teilnehmer*innen-Liste](../../manual_user/learningresources/Course_Element_Participant_List.md)
    * [E-Mail-Baustein](../../manual_user/learningresources/Course_Element_EMail.md) (@OLAT-Support; für Supportanfragen an den OLAT-Support)
    * Empfohlene Bausteine:
        * [Literatur](../literature/) (zur Verfügung stellen einer Literaturliste)
        * [Zoom](../supporting_technologies/zoom_video_conferences.de.md) (Online-Sitzungen über den UZH-Zoom-Account einrichten)

![](attachments/Campuskurs_option_1.png){ class="lightbox" }

![](attachments/Campuskurs_option_1_fertigstellen.png){ class="lightbox" }
 
Folgende Einstellungen werden genutzt: 

* [Publikationsstatus](../../manual_user/learningresources/Access_configuration.md): Veröffentlicht (die Lernenden sehen den Kurs sogleich unter "Kurse" – "Aktiv")
* [Freigabe](../../manual_user/learningresources/Access_configuration/#tab-freigabe):
    * Offen ohne Buchung (der Kurs kann über die Kurssuche aufgefunden und aufgerufen werden)
    * Organisation: OLAT, UZH, Fakultät, Institut (falls vorhanden) 

Der Campuskurs ist sofort einsatzbereit. 
<br /><br />

#### Mit Lernpfad (Typ: Lernpfadkurs) 

Für diesen Kurs wird ein Template mit folgenden Bausteinen verwendet:

* [Einschreibung](../../manual_user/learningresources/Course_Element_Enrolment.md) (für die Anmeldung von Auditor\*innen)
* ein Kurselement (Kursbaustein HTML-Seite) "Anweisung für Dozierende" 

Folgende Einstellungen werden genutzt:

* [Publikationsstatus](../../manual_user/learningresources/Access_configuration.md): Vorbereitung
* [Freigabe](../../manual_user/learningresources/Access_configuration/#tab-freigabe):
    * Offen ohne Buchung (der Kurs kann über die Kurssuche aufgefunden und aufgerufen werden)
    * Organisation: OLAT, UZH, Fakultät, Institut (falls vorhanden)

Der Campuskurs ist noch nicht einsatzbereit. Dieses Template unterscheidet sich stark vom Template für herkömmliche Campuskurse. Eine Nachbearbeitung ist zwingend notwendig. 

Weitere Informationen unter [Semestercheckliste für Dozierende](../semesterchecklist_for_lecturers/)
<br /><br />

#### Mit Lernfortschritt (Typ: Lernpfadkurs) 

Für diesen Kurs wird ein Template mit folgenden Bausteinen verwendet:  

* Einschreibung (für die Anmeldung von Auditor\*innen)
* ein Kurselement Anweisung für Dozierende  

Folgende Einstellungen werden genutzt:  

* [Publikationsstatus](../../manual_user/learningresources/Access_configuration.md): Vorbereitung
* [Freigabe](../../manual_user/learningresources/Access_configuration/#tab-freigabe):
    * Offen ohne Buchung (der Kurs kann über die Kurssuche aufgefunden und aufgerufen werden)  
    * Organisation: OLAT, UZH, Fakultät, Institut (falls vorhanden)  

Der Campuskurs ist noch nicht einsatzbereit. Dieses Template unterscheidet sich stark vom Template für herkömmliche Campuskurse. Eine Nachbearbeitung ist zwingend notwendig. 


### Optionen

Sie können aus drei Optionen wählen, wie ihr Campuskurs erstellt werden soll.
<br /><br />

#### Option 1: Standardkurs mit Materialienordner und Kommunikationsmöglichkeiten 
Es wird ein neuer Kurs auf Basis des Templates erstellt.  
Wenn Sie zum ersten Mal einen OLAT-Kurs erstellen, kommt für Sie nur diese Option in Frage. 
<br /><br />

####  Option 2: Campuskurs auf Basis eines eigenen Kurses 
Der neue Campuskurs wird entsprechend der Struktur und den Kursbausteinkonfigurationen eines bereits von Ihnen erstellten OLAT-Kurses erzeugt.

![](attachments/Campuskurs_option_2.png){ class="lightbox" }

Wählen Sie den Kurs aus, der als Vorlage für den neuen Campuskurs dienen soll. Sie finden hier alle Kurse aufgelistet, welche unter Ihrem Autorenbereich zu sehen sind und dem zuvor gewählten Kurstyp entsprechen. 

![](attachments/Campuskurs_option_2_auswahl.png){ class="lightbox" }

Das Material aus dem alten Kurs sowie die Konfigurationen der Kursbausteine werden dabei für den neuen Campuskurs übernommen. 
<br /><br />

####  Option 3: Campuskurs des Vorsemesters weiterführen 
Diese Option kommt für mehrsemestrige Veranstaltungen in Frage. Der Campuskurs wird mit denselben Kursmitgliedern (entsprechend der Modulbuchung) weitergeführt.

![](attachments/Campuskurs_option_3.png){ class="lightbox" }

Wählen Sie den Kurs aus, der weitergeführt werden soll. Sie finden hier alle Kurse aufgelistet, welche unter Ihrem Autorenbereich zu sehen sind und dem zuvor gewählten Kurstyp entsprechen. 

![](attachments/Campuskurs_option_3_auswahl.png){ class="lightbox" }

Die Titel dieser Campuskurse enthalten zwei Semesterkürzel, z.B. "**20HS/21FS** Fundamental Challenges in GIScience" und sind somit sehr einfach als weitergeführte Campuskurse zu erkennen. 

### Fertigstellen

![](attachments/Campuskurs_option_3_fertigstellen.png){ class="lightbox" }

Wählen Sie "Fertigstellen" aus, um das Erstellen des Campuskurses abzuschliessen. 

### Zeitpunkt 

Jeweils ab dem __1. Februar__ können Sie die Campuskurse __für das aktuelle Frühjahrssemester und das darauffolgende Herbstsemester__ erstellen. 

Jeweils ab dem __2. August__ (Nationalfeiertag 1. August!) können Sie die Campuskurse __für das aktuelle Herbstsemester und das darauffolgende Frühjahrssemester__ erstellen. 
<br /><br />
<hr />

## Rollen und Rechte {: #roles-and-rights }
### Mitgliederverwaltung 

Verwandte Themen: [Mitgliederverwaltung](../../manual_user/learningresources/Members_management.md) / [Zugriffskonfiguration](../../manual_user/learningresources/Access_configuration.md)
<br /><br />

#### Kursbesitzer\*innen und Kursbetreuer\*innen 
Kursbesitzer\*innen können andere Kursbesitzer\*innen und –betreuer\*innen hinzufügen, so dass diese mit dem Einrichten, Durchführen und/oder Bewerten helfen können. 

!!! info "Delegationen / Lernressourcenverwaltung"

    Bei Campuskursen werden automatisch alle Delegationen als Kursbesitzer\*innen eingetragen. 
    Lernressourcenverwalter\*innen, die Campuskurse erstellen, werden ebenfalls als Kursbesitzer\*innen eingetragen.
    Verwandte Themen: [Delegationen](#delegationen), [Lernressourcenverwaltung](#lernressourcenverwaltung)
<br />

####  Gruppenverwaltung 

Folgende Campusgruppen werden automatisch angelegt: 

* \_ UZH-Studierende mit Modulbuchung
* \_ Manuell hinzugefügte Teilnehmende 

Die Campusgruppe "_ UZH-Studierende mit Modulbuchung" wird mehrmals täglich mit den Buchungs&shy;daten des Modulbuchungssystems synchronisiert. Die Gruppenteilnehmenden können nicht gelöscht werden. Bei einer Stornierung werden die Studierenden automatisch bei der nächsten Synchronisation aus der Gruppe ausgetragen. 

Die Campusgruppe "_ Manuell hinzugefügte Teilnehmende" hat keine Anbindung an das Modul&shy;buchungssystem. Zu dieser Gruppe können Auditor\*innen oder externe Studierende, die nicht via Modulbuchungssystem gebucht wurden, hinzugefügt werden. Die Mitgliederverwaltung wird gemäss Template über den Einschreibebaustein geregelt. Die Verantwortung liegt bei den Kursbesitzer\*innen. 

### Lernbereich 

Ein [Lernbereich](../../manual_user/learningresources/Using_Course_Tools/#learning_area) fasst mehrere Gruppen zusammen. 

Der "Campuslernbereich" fasst die Campusgruppen "\_ UZH-Studierende mit Modulbuchung" und "\_ Manuell hinzugefügte Teilnehmende" zusammen. 

In Campuskursen sind gruppen- oder lernbereichsabhängige Sichtbarkeits- und Zugangsregeln möglich (in Kursen vom Typ: Herkömmlicher Kurs) bzw. gruppen- oder lernbereichsabhängige Ausnahme- und Durchführungseinstellungen (in Kursen vom Typ: Lernpfad). 

### Delegationen 

Delegierte von Dozierenden können deren Campuskurse erstellen und administrieren. Die Delegation empfiehlt sich im Besonderen für E-Learning-Koordinator\*innen oder Assistent\*innen, welche die Erstellung und Administration für die von ihnen betreuten Dozierenden übernehmen.  

Delegationen können nur an OLAT-Autor\*innen vergeben werden und beziehen sich ausschliesslich auf Campuskurse. 

!!! info "Delegationsrecht beantragen"

    Beantragen Sie eine Delegation mit dem Einverständnis der dozierenden Person beim [OLAT-Support](mailto:lms-support@zi.uzh.ch). 

### Lernressourcenverwaltung 

Lernressourcenverwalter\*innen für ein Institut oder eine Fakultät der UZH können Campuskurse für dieses Institut oder diese Fakultät erstellen und administrieren.  

Falls Sie einen Campuskurs erstellen, werden Sie automatisch als Kursbesitzer\*in des Campuskurses eingetragen. 

Damit die Lernressourcenverwaltung auf Fakultäts- oder Institutsebene funktioniert, erhalten Campuskurse eine automatische Organisationsfreigabe für die Organisationen “OLAT”, “UZH”, die betroffene Fakultät und das betroffene Institut. 

!!! info "Rolle “Lernressourcenverwaltung” beantragen"

    Um die Rolle “Lernressourcenverwaltung” für ein Institut oder für eine Fakultät zu erhalten, ist das schriftliche Einverständnis der Fakultät oder des Instituts notwendig. Bitte bestellen Sie das Antragsformular beim [OLAT-Support](mailto:lms-support@zi.uzh.ch). 
<br />
<hr />

## Kursinhalte {: #course-contents }

Kursbesitzer\*innen können über den Kurseditor neue Kursbausteine hinzufügen und bestehende konfigurieren, sodass die gewünschten Lerninhalte für die Teilnehmer\*innen dargestellt werden können.  

Verwandte Themen: [Kursbausteine](../../manual_user/learningresources/Course_Elements/), [Allgemeine Konfiguration von Kursbausteinen](../../manual_user/learningresources/General_Configuration_of_Course_Elements/), [Kurseditor](../../manual_user/learningresources/Using_additional_Course_Editor_Tools/), [Kurseinstellungen](../../manual_user/learningresources/Course_Settings/), [Kursbausteine publizieren](../../manual_user/learningresources/Using_additional_Course_Editor_Tools/#publizieren )

### Benutzerdefinierte Einstellungen 
Falls Ihre gewünschte Konfiguration von der Standard Kurs-Vorlage abweicht, müssen diese selbständig angepasst werden. Insbesondere beim Erstellen eines Campuskurses mit der Option&nbsp;2 ("Campuskurs auf Basis eines eigenen Kurses erstellen") müssen die Einstellungen der einzelnen Bausteine sorgfältig überprüft werden. Die Empfängergruppe im E-Mail-Baustein muss beispielsweise zwingend angepasst werden.
<br /><br />
<hr />

## Publikation und Nutzung {: #publications-and-lifecycle}
Verwandte Themen: [Publikationsstatus](../../manual_user/learningresources/Access_configuration/), [Kurse](../../manual_user/area_modules/Courses/), [Zugriffskonfiguration](../../manual_user/learningresources/Access_configuration/#tab-freigabe) 

### Veröffentlichung
__Kursbesitzer\*innen__ veröffentlichen ihren Kurs für die Teilnehmer\*innen, sodass diese auf die Lerninhalte zugreifen können. Für Teilnehmer\*innen ist der Kurs ab diesem Zeitpunkt im Bereich “Kurse” unter “Aktiv” auffindbar und die __Mitglieder__ haben Zugriff auf die Kursinhalte gemäss Einstellungen. 

### Durchführung
__Kursbesitzer\*innen__ können während dem laufenden Kurs neue Lerninhalte hinzufügen oder Änderungen vornehmen. Änderungen müssen dabei stets publiziert werden.

### Beenden
Sobald Kursbesitzer\*innen einen Kurs beenden (Status “Beendet”) haben __Kursmitglieder__ weiterhin Zugriff auf den Kurs, können aber nur noch lesen. Der Kurs befindet sich nicht mehr unter “Aktiv”, sondern im Tab “Beendete Kurse”. 

### Löschen
Sobald Kursbesitzer\*innen einen Kurs löschen (Administration > Löschen) erscheint der Kurs für Kursbesitzer\*innen im Tab “Gelöschte Lernressourcen”. Mitglieder haben keinen Zugriff mehr.  
<br /><br />
<hr />

## FAQ {: #faq }

[Wo kann ich Campuskurse erstellen?](#wo-kann-ich-campuskurse-erstellen)<br />
[Wie kann ich einen normalen Kurs in einem Campuskurs umwandeln?](#wie-kann-ich-einen-normalen-kurs-in-einem-campuskurs-umwandeln)<br />
[Wo ist mein erstellter Campuskurs?](#wo-ist-mein-erstellter-campuskurs)<br />
[Wie kann ich Campuskurse zusammenlegen bzw. wie kann ich Campusgruppen in einen normalen Kurs einbinden?](#zusammenlegen)<br />
[Wie überprüfe ich die Teilnehmerzahl meines Kurses?](#teilnehmerzahlprufen)<br />
[Weshalb stimmt die Teilnehmerzahl meines Kurses nicht mit der Modulbuchung überein?](#teilnehmerzahlkontrolle)<br />
[Wie können Auditor*innen meinen Kurs finden?](#auditorenzugang)<br />
[Wie definiere ich neue Empfänger(gruppen) im E-Mail Kursbaustein "@Studierende"?](#mailempfanger)

<br /><br />

### Wo kann ich Campuskurse erstellen?

Als Dozent\*in der UZH mit Veranstaltungen im aktuellen Semester der UZH sollten Sie in der Hauptnavigation von OLAT neben “Kurse”, “Gruppen” und “Autorenbereich” einen Eintrag “Campuskurse” sehen.
    
Es kann vorkommen, dass Ihnen der Tab "Campuskurse" nicht angezeigt wird oder dort Einträge fehlen. Dies kann folgende Ursachen haben:

1. Wechsel der Universität (neu an der UZH angestellt)
2. Änderung des Beschäftigungsverhältnisses (neuer Arbeitsvertrag und neue Mitarbeiternummer an der UZH)
3. mehrere OLAT-Konten
4. OLAT-Registrierung erst vor wenigen Minuten/Stunden (die nächste Synchronisierung ist noch ausstehend)
5. fehlende Voraussetzung für die Erstellung eines Campuskurses (vgl. Abschnitt "[Voraussetzungen](#voraussetzungen)") 

Falls keiner dieser Gründe auf Sie zutrifft oder falls Sie das Problem nicht selbst lösen (Gründe 1, 2 oder 3) können, senden Sie eine E-Mail über Ihre UZH-E-Mail-Adresse an den [LMS-Support](mailto:lms-support@zi.uzh.ch) mit folgenden Angaben:

* genauer Kurstitel (gemäss Vorlesungsverzeichnis)
* OLAT-Benutzername (nach OLAT-Login unten links einsehbar neben “eingeloggt als”)
* vermutete Ursache

<hr />

### Wie kann ich einen normalen Kurs in einem Campuskurs umwandeln?

Klicken Sie im Tab "Campuskurse" auf den Link "erstellen" und wählen Sie die Option "[Campuskurs auf Basis eines eigenen Templates erstellen](#option-2-campuskurs-auf-basis-eines-eigenen-kurses)". Als Template verwenden Sie den bereits vorhandenen Kurs. 

<hr />

### Wo ist mein erstellter Campuskurs?

Bereits erstellte Campuskurse finden Sie im Tab "Autorenbereich" unter "Meine Kurse". Zudem erscheint ein Link zu Ihrem Kurs auch im Tab "Kurse" unter "Aktiv", da Sie als Kursbesitzer\*in eingetragen sind.  

Für Kursteilnehmer\*innen ist der Campuskurs erst unter “Aktive Kurse” sichtbar, nachdem [Status und Freigabe](../../manual_user/learningresources/Access_configuration/) konfiguriert wurden. 

<hr />

### Wie kann ich Campuskurse zusammenlegen bzw. wie kann ich Campusgruppen in einen normalen Kurs einbinden? {: #zusammenlegen }

#### Anwendungsbeispiel
Zu einem Modul gibt es eine Vorlesung und - aufgrund der hohen Teilnehmerzahl – drei Übungen. Die Aufteilung auf die drei Übungsgruppen erfolgt über das Modulbuchungssystem. Das Material zu allen vier Veranstaltungen soll in einem einzigen Kurs bereitgestellt werden, nämlich im Campuskurs zur Vorlesung.  

Die Kurse werden nachfolgend bezeichnet als "Hauptkurs" und "Zusätzlicher Kurs 1-3". Es spielt keine Rolle, ob bei der Erstellung das Standard-Template oder ein eigenes Template verwendet wird. 

#### Vorgehensweise

1. Vorbereitung:
    1. Erstellen Sie alle vier Campuskurse bzw. lassen Sie alle vier Campuskurse erstellen.  
    2. Lassen Sie sich als Kursbesitzer\*in in alle vier Campuskurse eintragen.
2. Öffnen Sie den "Zusätzlichen Kurs 1" und kopieren Sie den Titel des Kurses (_Zusätzlicher Kurs 1_). 
3. Gruppe umbenennen: Rufen Sie im "Zusätzlichen Kurs 1" unter "Administration" die "Mitgliederverwaltung" auf. Klicken Sie In der "Mitgliederverwaltung" in der Navigation am linken Bildschirmrand auf "Gruppen". Klicken Sie auf die Gruppe "\_ UZH-Studierende mit Modulbuchung" und dann auf "Administration". Geben Sie der Gruppe einen eindeutigen Namen, z.B. “MB _Zusätzlicher Kurs 1_, Gruppe 1” ("MB" für "\_ UZH-Studierende mit <strong>M</strong>odul<strong>b</strong>uchung"). Klicken Sie auf "Fertigstellen". 
4. Gehen Sie einen Schritt zurück im Browserverlauf oder klicken Sie auf das nach links gerichtete Winkelsymbol links vom Kurstitel, um zur Gruppenübersicht der Gruppenverwaltung zurückzukommen und kopieren Sie die ID der eben umbenannten Gruppe.  
5. Öffnen Sie den ‘Hauptkurs’ und gehen Sie zu “Administration > Mitgliederverwaltung” und wählen Sie “Gruppen” aus.
6. Wählen Sie “Gruppe hinzufügen" (Button auf der rechten Seite) und gehen Sie zum Filter-Tab “Kurse”. Suchen Sie den Titel des "Hauptkurses" und fügen Sie ihn hinzu.  
__Nun ist die umbenannte Campusgruppe "\_ UZH-Studierende mit Modulbuchung" aus dem "Zusätzlichen Kurs 1" in den "Hauptkurs" eingebunden und kann dort für die Sichtbarkeits- und Zugangssteuerung einzelner Kursbausteine verwendet werden.__
7. Wiederholen Sie nun die Schritte 2-6 für die anderen "Zusätzlichen Kurse" resp. die Campusgruppe "\_ UZH-Studierende mit Modulbuchung" in diesen Kursen.<br /><br />
__Empfehlung:__ Platzieren Sie im "Zusätzlichen Kurs 1" im obersten Kursbaustein einen Hinweis für Betreuer:innen, dass der Kurs nicht mit Material befüllt wird und die Materialien im "Hauptkurs" platziert werden. Tragen Sie dafür im obersten Kursbaustein im Tab "Sichtbarkeit" unter "Information, wenn sichtbar und kein Zugang" einen entsprechenden Text ein. Im Tab "Zugang" setzen Sie das Häkchen im Feld "Für Lernende gesperrt". 
<br /><br />
8. Publizieren Sie die Änderungen. 


#### Alternative Option ohne Synchronisation

Alternativ können Sie Kurse manuell zusammenführen, indem die Teilnehmenden aus einer Campusgruppe exportiert und in eine andere Gruppe importiert werden. Bei dieser Option geht die Anbindung an das Vorlesungsverzeichnis verloren. Die Synchronisation der Teilnehmenden aus dem Vorlesungsverzeichnis funktioniert nur mit der Campusgruppe "\_ UZH-Studierende mit Modulbuchung" des Campuskurses.

<hr />

### Wie überprüfe ich die Teilnehmerzahl meines Kurses? {: #teilnehmerzahlprufen }

Um die Teilnehmerzahl Ihres Kurses mit der Anzahl Buchungen im Modulbuchungssystem zu vergleichen, öffnen Sie Ihren Kurs, gehen Sie zu "Administration" (in der linken oberen Ecke) > "Mitgliederverwaltung" und wählen Sie den Tab "Teilnehmer\*innen" aus. Die Anzahl Einträge entspricht dem Total aller Kursmitglieder mit der Kursrolle "Teilnehmer\*in".
Um die Anzahl Studierende mit Modulbuchung zu überprüfen (synchronisiert mit der Modulbuchungssystem der UZH), wählen Sie im Menü links "Gruppen" aus. In der Zeile "\_ UZH-Studierende mit Modulbuchung" finden Sie in der Spalte "Teilnehmer\*in" die Anzahl Kursmitglieder mit Modulbuchung.

<hr />

### Weshalb stimmt die Teilnehmerzahl meines Kurses nicht mit der Modulbuchung überein? {: #teilnehmerzahlkontrolle }

Sollte die Anzahl Teilnehmer*innen nicht mit der Anzahl Studierender im Modulbuchungssystem übereinstimmen, kann dies verschiedene Ursachen haben: 

* Einzelne Studierende haben sich noch nie mit ihrer UZH-Identität in OLAT angemeldet. Es existiert also noch kein OLAT- Konto für einzelne Studierenden.  
    * _Lösung:_ Sobald die Studierenden ein OLAT-Konto erstellt haben, werden sie ein bei der nächsten Synchronisation mit dem Modulbuchungssystem Ihrem Kurs hinzugefügt.    

* Die Studierenden haben zwar das Modul, nicht aber die Veranstaltungen gebucht.  
    * _Lösung:_ Wenden Sie sich bitte an die SAP-Verantwortlichen Ihres Instituts, 
    damit diese die Modulbuchung überprüfen können. 

Für weitere, nicht aufgelistete Fälle senden Sie eine Nachricht an den [LMS-Support](mailto:lms-support@zi.uzh.ch).

<hr />

### Wie können Auditor\*innen meinen Kurs finden? {: #auditorenzugang }

Wenn sich Auditor\*innen selbständig in die Campusgruppe "\_ Manuell hinzugefügte Teilnehmende" einschreiben können sollen, müssen Sie die folgenden [Freigabe-Einstellungen](../../manual_user/learningresources/Access_configuration/#status-der-veroffentlichung) für Ihren Kurs vornehmen:

* Zugang für Teilnehmer\*innen: “Buchbare und offene Angebote”
* Organisationen: "OLAT” (zusätzliche Organisationen möglich)

Der Kurs kann dann über die Suche gefunden werden und interessierte Personen können sich selbständig über den "[Einschreibe](h../../manual_user/learningresources/Course_Element_Enrolment/)"-Baustein in den Kurs einschreiben.

Alternativ können Sie die Campusgruppe "\_ Manuell hinzugefügte Teilnehmende" durch manuelles Hinzufügen und Entfernen der betroffenen Personen selbst verwalten. Nehmen Sie in dem Fall die folgenden [Freigabe-Einstellungen](../../manual_user/learningresources/Access_configuration/#status-der-veroffentlichung) für Ihren Kurs vor:

* Zugang für Teilnehmer\*innen: “Privat” 
* Organisationen: "OLAT” (zusätzliche Organisationen möglich)

<hr />

### Wie definiere ich neue Empfänger(gruppen) im E-Mail Kursbaustein "@Studierende"? {: #mailempfanger }

Wenn Sie einen Campuskurs mit der Option 2 erstellen ("Campuskurs auf Basis eines eigenen Kurses erstellen"), müssen Sie beim neuen Campuskurs die Empfängergruppen im Kursbausteinen "@Studierende" neu auswählen (im Kurseditor Modus im Tab "Empfänger"). 

Bei Campuskursen, die als "Standardkurs" erstellt werden (Option 1) und bei "weitergeführten" Kursen (Option 3) ist diese Nachbearbeitung nicht erforderlich. 

#### Vorgehen 
1. Öffnen Sie Ihren Campuskurs über das Menü "Autorenbereich > Meine Kurse". 
2. Starten Sie den Kurseditor über "Administration" und "Kurseditor". 
3. Wählen Sie in der Kursnavigation links den Kursbaustein "@Studierende" aus und gehen Sie zum Tab "Empfänger".  
4. Wählen Sie die Option "Versand an Teilnehmer\*innen > Alle Teilnehmer\*innen der selektierten Gruppen" und danach "Lernbereich auswählen".  
5. Setzen Sie ein Häkchen bei "Campuslernbereich" (dies beinhaltet die Mitglieder Ihrer Kursgruppen "\_ UZH-Studierende mit Modulbuchung" und "\_ Manuell hinzugefügte Teilnehmende").
6. Wählen Sie "Übernehmen" und speichern Sie die geänderte Einstellung über die Schaltfläche “Speichern” ganz unten.
7. Publizieren Sie die Kursänderungen. 