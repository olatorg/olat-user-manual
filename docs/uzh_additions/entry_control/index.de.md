# Kursbaustein “Zutrittskontrolle“

Der neue OLAT-Kursbaustein “Zutrittskontrolle“ ermöglicht eine präzise Verwaltung des Zugangs zu bestimmten Inhalten innerhalb eines OLAT-Kurses. Kursbesitzer\*innen können sicherstellen, dass nur berechtigte Kursteilnehmer\*innen Zugang erhalten. 

Mit dem Kursbaustein “Zutrittskontrolle” können verschiedene [Anwendungsszenarien](#anwendungsszenarien) abgebildet werden.<br /><br /><figure>![](./attachments/UZHcard.png)<figcaption>Für die Zutrittskontrolle wird der Barcode der UZH Card (Matrikelnummer) gescannt</figcaption></figure>

Der Ablauf einer Zutrittskontrolle gliedert sich in folgende drei Teilbereiche:

1. __Definition von Zulassungsgruppen:__ Vor Beginn einer Veranstaltung legen die Kursbesitzer\*&shy;innen Zulassungs&shy;gruppen fest und ordnen diesen Gruppen die entsprechenden Kursteilnehmer\*&shy;innen zu. Dadurch wird sichergestellt, dass nur autorisierte Personen Zugang erhalten.
2. __Durchführung der Zutrittskontrolle:__ Die Identität der Kursteilnehmer\*&shy;innen wird überprüft, um sicherzustellen, dass sie zur Zulassungsgruppe gehören. Hierbei wird die bestehende UZH Card verwendet, deren Barcode (mit Matrikelnummer) mit einem Handy oder einer Webcam gescannt werden kann. Nach erfolgreicher Identifikation werden die Kursteilnehmer\*innen automatisch oder manuell der Zutrittsgruppe hinzugefügt.
3. __Eintragung in die Zutrittsgruppen:__ Basierend auf der Zutrittskontrolle werden die Teilnehmer\*innen in eine Zutrittsgruppe eingetragen. Diese Gruppen sind das Ergebnis des Zutrittskontroll-Prozesses und gewährleisten einen geordneten Zugang zu den Inhalten.<br /><br /><figure>![](./attachments/Zutrittskontrolle-Ablauf.png)<figcaption>Ablauf des OLAT-Kursbausteins “Zutrittskontrolle“</figcaption></figure>

Alle Änderungen des Zutrittsstatus der Kursteilnehmer\*innen im Kursbaustein werden protokolliert, wodurch eine lückenlose Nachverfolgung gewährleistet ist. Die Zutrittsgruppen können darüber hinaus für die Konfiguration anderer Kursbausteine innerhalb von OLAT genutzt werden, um eine konsistente Verwaltung des Kurses zu ermöglichen. 

!!! info "Barcode der UZH Card: Matrikel- oder Personalnummer"

    Auf der Rückseite der UZH Card ist im Barcode die Matrikel- oder die Personalnummer abgebildet.
    Im Kursbaustein “Zutrittskontrolle” kann der Barcode sowohl mit der Matrikel- als auch mit der Personalnummer gescannt werden. Wenn das Mitglied einer Zulassungsgruppe eine Matrikelnummer hat, wird die Matrikelnummer verwendet. Ist keine Matrikelnummer vorhanden, wird geprüft, ob das Mitglied eine Personalnummer hat. Wenn ja, wird diese Personalnummer für die Zutrittskontrolle verwendet.

<hr />

## Im Kurseditor (für Kursbesitzer*innen)

Im Kurseditor können die Tabs für die zentralen Einstellungen des Bausteins verwendet werden.

### Tab “Titel und Beschreibung”

Hier können Sie einen Titel und eine Beschreibung des Kursbausteins eingeben.
Die hinzugefügte Beschreibung ist für die Kursteilnehmer*innen im Kursrun sichtbar.

!!! info "Weitere Informationen finden Sie im Kapitel [“Titel und Beschreibung“](../../manual_user/learningresources/General_Configuration_of_Course_Elements/#desc)."

### Tab “Layout“

Hier können Sie die Layout-Optionen ändern.

!!! info "Weitere Informationen finden Sie im Kapitel [“Layout“](../../manual_user/learningresources/General_Configuration_of_Course_Elements/#tab-layout)."

### Tab “Sichtbarkeit“ (nur bei herkömmlichen Kursen)

Die Sichtbarkeit eines Kursbausteins zu ändern, ist eine von drei Arten, um den Zugriff auf Kursbausteine einzuschränken.
Wenn Sie die Sichtbarkeit eines Kursbausteins einschränken, erscheint dieser nicht mehr in der Kursnavigation.

!!! info "Weitere Informationen finden Sie im Kapitel [“Sichtbarkeit und Zugang“](../../manual_user/learningresources/General_Configuration_of_Course_Elements/#access)."

### Tab “Zugang” (nur bei herkömmlichen Kursen)

Im Tab “Zugang“ des Kursbausteins können Sie definieren, wer Zugang zu diesem Baustein hat.

!!! info "Weitere Informationen finden Sie im Kapitel [“Sichtbarkeit und Zugang“](../../manual_user/learningresources/General_Configuration_of_Course_Elements/#access)."

### Tab “Lernpfad”(nur bei Lernpfadkursen)

In Ihrem [Lernpfadkurs](../../manual_user/learningresources/Learning_path_course_Course_editor/) definieren Sie im Tab “Lernpfad“, ob die Bearbeitung des Kursbausteins obligatorisch oder freiwillig ist. Ferner können Sie einen Zeitraum festlegen, in dem der Kursbaustein bearbeitet werden kann.

Jeder Kursbaustein verfügt über individuelle Erledigungskriterien, eine Übersicht finden Sie im Kapitel [“Lernpfad“](../../manual_user/learningresources/Learning_path_course/).

!!! info "Weitere Informationen finden Sie im Kapitel [“Lernpfad“](../../manual_user/learningresources/Learning_path_course/)."

!!! warning "Herkömmliche Kurse verfügen nicht über den Tab “Lernpfad“ und haben stattdessen die Tabs ["Sichtbarkeit"](../../manual_user/learningresources/General_Configuration_of_Course_Elements/#access)“ und “[Zugang](../../manual_user/learningresources/General_Configuration_of_Course_Elements/#access)“ inklusive [Expertenmodus](../access_restrictions_in_expert_mode/)."

### Tab “Konfiguration”

![](./attachments/Zutrittskontrolle-Kurseditor.png)<br /><br />

#### Abschnitt “Zulassungsgruppen”
Für eine Zutrittskontrolle wird mindestens eine Zulassungs&shy;gruppe benötigt. Eine Zulassungsgruppe ist eine Gruppe, deren Mitglieder an einer bestimmten Veranstaltung teilnehmen dürfen.

Kursbesitzer*innen haben die Möglichkeit, bestehende OLAT-Gruppen auszuwählen oder neue Gruppen zu erstellen.
Die Sortierung der Gruppen kann bei Bedarf nachträglich geändert werden.

??? info "Zulassungsgruppen entfernen"

    Zulassungsgruppen können im Tab “Konfiguration” des Kurseditors entfernt werden. Bitte beachten Sie, dass erstellte Gruppen nur im Kursbaustein “Zutrittskontrolle” entfernt werden können. Die entfernten Gruppen bleiben als Gruppen (mit den schon vorher eingetragenen Mitgliedern) in Ihrem OLAT-Kurs bestehen. Falls gewünscht, können Sie die gewünschten Gruppen in der Mitgliederverwaltung endgültig löschen.

??? info "Gruppen in der Mitgliederverwaltung verwalten"

    Grundsätzlich ist es möglich, die Zulassungs- und Zutrittsgruppen in der Mitgliederverwaltung zu verwalten.
    Bitte beachten Sie, dass wir von diesem Vorgehen abraten, da Änderungen nicht nachvollziehbar sind und so z.B. Studierende in mehreren Zulassungsgruppen eingetragen sein können.
    Falls ein Mitglied trotzdem in mehr als einer Zulassungsgruppe eingetragen ist, erscheint eine entsprechende Fehlermeldung.
<br />

#### Abschnitt “Zutrittskontrolle”

##### Vereinfachte Prüfung der Identität
Wenn Sie die vereinfachte Prüfung der Identität aktivieren, müssen Sie den Zutritt nicht bei jedem Scan explizit bestätigen. Das bedeutet, dass Sie schneller scannen können, aber auch, dass Sie  nicht die Möglichkeit haben, die Detaildaten (E-Mail-Adresse und Zulassungsgruppe) genauer zu überprüfen.

* Bei einem erfolgreichen Scan erscheint eine grüne Meldung, die nach 2 Sekunden wieder verschwindet, so dass der nächste Barcode gescannt werden kann.

* Bei einem nicht erfolgreichen Scan erscheint eine rote Meldung mit einer Zusammenfassung der Fehler. Diese Meldung wird bis zum nächsten Scan angezeigt.

##### Scan-Personen
Kursbesitzer\*innen und Kursbetreuer\*innen können die Zutrittskontrolle im Kursrun des Kursbausteins durchführen.

Falls Sie für die eigentliche Zutrittskontrolle die Hilfe von zusätzlichen Scan-Personen benötigen, können Sie hier eine oder mehrere Gruppen mit Scan-Personen hinzufügen. Diese Scan-Personen haben im Kurs keinen Zugriff auf die Administration mit u.a. der Mitgliederverwaltung und dem Bewertungswerkzeug. Sie können nur die Zutrittskontrolle im Kursrun des Kursbausteins durchführen.

Die Gruppen können ausgewählt oder neu erstellt werden.
<br /><br />

#### Abschnitt “Zutrittsgruppen”
Sobald die Zulassungsgruppen definiert sind, werden die entsprechenden Zutrittsgruppen definiert.
Pro Zulassungsgruppe werden zwei Zutrittsgruppen definiert:

* eine Gruppe für die Anwesenden (Suffix: " - anwesend")
* eine Gruppe für die Abwesenden (Suffix: " - abwesend")

Die Suffixe sind fix und können nicht geändert werden.

<hr />

## Im Kursrun (für Kursbesitzer\*innen und -betreuer\*innen)

Im Kursrun können Sie Mitglieder zu den __Zulassungsgruppen__ hinzufügen, die __Zutrittskontrolle__ durchführen und die Mitglieder der __Zutrittsgruppen__ überprüfen.

!!! info "Alle Tabellen (mit ihren Daten) können als Excel-Datei exportiert werden."

### Tab “Zulassungsgruppen”

#### Abschnitt “Zulassungsgruppen“

![](./attachments/Zutrittskontrolle-Tab-Zulassungsgruppen.png)

Die im Kurseditor definierten Zulassungsgruppen werden hier aufgelistet und Mitglieder können hinzugefügt werden.

??? info "Mitglieder hinzufügen"

    Ein Mitglied kann über die __Personensuche__ hinzugefügt werden. Wenn Sie viele Mitglieder auf einmal hinzufügen möchten, empfehlen wir die __Massensuche__.
    In der Massensuche können Sie mehrere Mitglieder hinzufügen, indem Sie eine Liste mit Anmeldenamen, E-Mail-Adressen oder Matrikelnummern eingeben.
    Wenn eine Person z.B. an verschiedenen Hochschulen studiert, kann es sein, dass sie mehrere OLAT-Konten mit derselben Matrikelnummer hat.
    In diesem Fall erhalten Sie beim Hinzufügen des Mitgliedes eine Meldung und die Möglichkeit, das gewünschte OLAT-Konto auszuwählen.

??? info "Betreuer\*innen oder Teilnehmer\*innen-rechte für die Mitglieder?"

    Beim Hinzufügen von Mitgliedern zu einer Zulassungsgruppe können Sie entscheiden, welche Rechte die Mitglieder haben sollen. Im Kursbaustein “Zutrittskontrolle” können sowohl Gruppenbetreuer\*innen als auch Gruppenteilnehmer\*innen in die entsprechenden Zutrittsgruppen eingetragen werden.
    In diesem Zusammenhang gibt es keinen Unterschied zwischen den Rollen Gruppenbetreuer\*innen und Gruppenteilnehmer\*innen - beide können gleichermassen in die Zulassungsgruppen eingetragen werden.
    Bedenken Sie aber, dass die Betreuer\*innen-Rolle auf einer Gruppe für anderen Kurswerkzeuge wie Bewertungswerkzeug Auswirkungen hat, sofern Sie die gleichen Gruppen weiterverwenden.
    Wir empfehlen, den Mitgliedern nur die Teilnehmer\*innen-Rechte zu vergeben. Somit ist jedes Mitglied ein Gruppenteilnehmender.

!!! info "Ein Mitglied kann nur einer Zulassungsgruppe gehören. Wenn Sie versuchen, ein Mitglied zu mehr als einer Zulassungsgruppe hinzuzufügen, erhalten Sie eine entsprechende Fehlermeldung."
<br /><br />

#### Abschnitt “Mitglieder”
Im Abschnitt “Mitglieder” sind alle Mitglieder der Zulassungsgruppen aufgelistet.
Mitglieder können hier aus einer Zulassungsgruppe entfernt werden.

![](./attachments/Zutrittskontrolle-Tab-Zutrittskontrolle.png)

### Tab “Zutrittskontrolle”

Die eigentliche Kontrolle erfolgt im Tab “Zutrittskontrolle”. Hier haben Sie die Möglichkeit, Mitglieder aus den vordefinierten Zulassungsgruppen automatisch oder manuell einzutragen. Daraus resultieren dann die Zutrittsgruppen für Anwesende/Abwesende.

![](./attachments/Zutrittskontrolle-scan.jpg)

#### Abschnitt “Automatisch eintragen”
Da mehrere Kursbesitzer\*innen und Kursbetreuer\*innen die Zutrittskontrolle gleichzeitig durchführen können, gibt es die Möglichkeit, die Mitgliederliste mit der Option "Meine zu kontrollierenden Zulassungsgruppen" einzuschränken. Damit ist jede Person nur für die eigene Zulassungsgruppe verantwortlich.

Falls die Daten und das Foto auf der UZH Card mit der Person übereinstimmen, klicken Sie auf den Button “Barcode scannen”. Die Kamera wird aktiviert und Sie können den Barcode scannen. OLAT liest die Daten ein und die Person wird in die entsprechende Zutrittsgruppe eingetragen.

!!! info "Um die Kamera im Browser verwenden zu können, benötigen Sie eine funktionierende Kamera. Ausserdem muss die Verwendung der Kamera im Browser explizit erlaubt sein."
    
    ![](./attachments/Zutrittskontrolle-Barcode-scannen-Kamera-erlauben.png)

Beim automatischen Eintragen (&thinsp;= Barcode scannen) gibt es 3 mögliche Ergebnisse:

* Ergebnis 1: Zutritt bestätigen (blaue Meldung)
* Ergebnis 2: Zutritt nicht möglich - Mitglied bereits eingetragen (gelbe Meldung)
* Ergebnis 3: Zutritt nicht möglich - Mitglied kann nicht eingetragen werden (rote Meldung)<br /><br />

__Ergebnis 1: Zutritt bestätigen (blaue Meldung)__

* Wenn ein Mitglied einer Zulassungsgruppe noch nicht eingetragen ist und der Barcode erfolgreich gescannt wurde, können Sie den Zutritt bestätigen.<br /><br />
![](./attachments/Zutrittskontrolle-Barcode-scannen-Zutritt-bestaetigen.png)<br /><br />

* Eine Person kann mehrere OLAT-Konten mit derselben Matrikelnummer haben. Wird der Barcode erfolgreich gescannt, erhält die scannende Person einen Hinweis, dass die Person mehrere OLAT-Konten mit derselber Matrikelnummer hat und dass es wichtig ist, dass sich diese Person mit dem richtigen OLAT-Konto einloggt.<br /><br />
![](./attachments/Zutrittskonrolle-Barcode-scannen-Zutritt-bestaetigen-mehrere-OLAT-Konten.png)<br /><br />

__Ergebnis 2: Zutritt nicht möglich - Mitglied bereits eingetragen (gelbe Meldung)__

* Wenn ein Mitglied bereits in einer Zutrittsgruppe eingetragen ist, kann dieses Mitglied nicht erneut eingetragen werden.<br /><br />
![](./attachments/Zutrittskontrolle-Barcod-scannen-Zutritt-nicht-moeglich-schon-eingetragen.png)<br /><br />

__Ergebnis 3: Zutritt nicht möglich - Mitglied kann nicht eingetragen werden (rote Meldung)__

* Ist der Barcode nicht als Matrikel- oder Personalnummer in OLAT erfasst, kann kein OLAT-Konto gefunden werden und der Zutritt ist nicht möglich.<br /><br />
![](./attachments/Zutrittskontrolle-Barcode-scannen-Zutritt-nicht-moeglich-Barcode-nicht-gefunden.png)<br /><br />

* Wenn der Barcode erfolgreich gescannt werden konnte, aber kein Mitglied in der zu kontrollierenden Zulassungsgruppe gefunden wurde, kann dieses Mitglied nicht eingetragen werden.<br /><br />
![](./attachments/Zutrittskontrolle-Barcode-scannen-Zutritt-nicht-moeglich-Zulassungsgruppe.png)<br /><br />

#### Abschnitt “Mitglieder überprüfen und manuell eintragen”
In diesem Abschnitt werden alle Mitglieder der Zulassungsgruppen aufgelistet. Hier können Sie überprüfen, ob sie schon in einer Anwesend- oder Abwesend-Gruppe eingetragen sind.
Hier gibt es auch die Möglichkeit, manuell ein oder mehrere Mitglieder in eine Zutrittsgruppe (anwesend oder abwesend) einzutragen.
Mitglieder können jederzeit wieder ausgetragen werden.

!!! info "Info"

    Jede Änderung im Kursbaustein “Zutrittskontrolle” wird im Änderungsverlauf dokumentiert.
    Klicken Sie auf den Namen eines Mitglieds, um eine Detailsicht zu öffnen und den Änderungsverlauf einzusehen.
    
<br />

#### Abschnitt “Scan-Personen verwalten”
In diesem Abschnitt können, falls im Kurseditor konfiguriert, die Scan-Personen hinzugefügt werden.
Die Mitglieder dieser Gruppe sind berechtigt, die Zutrittskontrolle durchzuführen. Sie sehen nur den Tab “Zutrittskontrolle” und haben keinen Zugriff auf andere Funktionen im Kurs und in diesem Kursbaustein. Die Scan-Personen sind an sich Kursteilnehmer\*innen mit dem Recht, Zutrittskontrollen durchzuführen.

??? info "Betreuer\*innen oder Teilnehmer\*innen-Rechte für die Mitglieder?"

    Beim Hinzufügen von Mitgliedern zur Gruppe der Scan-Personen können Sie entscheiden, welche Rechte die Mitglieder haben sollen. Wir empfehlen, den Mitgliedern nur die Teilnehmer\*innenrechte zu vergeben. Somit ist jedes Mitglied ein*e Gruppenteilnehmer*in. Im Kursbaustein “Zutrittskontrolle” können sowohl Gruppenbetreuer\*innen als auch Gruppenteilnehmer\*innen die Zutrittskontrolle durchführen. Hier wird nicht unterschieden.
    Da es sich bei diesen Gruppen aber auch um Kursgruppen handelt, kann es zu unterschiedlichen Auswirkungen kommen, wenn Sie diese Gruppen auch für andere Anwendungen verwenden.
<br />

#### Abschnitt “Abschliessen der Zutrittskontrolle”
Kursbesitzer\*innen und Kursbetreuer\*innen haben die Möglichkeit, die Zutrittskontrolle abzuschliessen. Beim Abschliessen werden alle Mitglieder, die noch nicht in einer Zutrittsgruppe eingetragen sind, automatisch in die entsprechende Abwesenheitszutrittsgruppe eingetragen.

Nach dem Abschliessen können keine Mitglieder mehr ein- oder ausgetragen werden.
Änderungen werden im Änderungsverlauf eingetragen.

Bei Bedarf kann die Zutrittskontrolle erneut geöffnet werden, falls Mitglieder doch noch aus- und eingetragen werden müssen.
<br />

### Tab “Zutrittsgruppen”

Im Tab “Zutrittsgruppen” kann das Ergebnis der Zutrittskontrolle eingesehen werden.

![](./attachments/Zutrittskontrolle-Tab-Zutrittsgruppen.png)
<br /><br />

#### Abschnitt “Zutrittsgruppen”
In der Übersicht der Zutrittsgruppen können Sie die Anzahl der Mitglieder überprüfen.
<br /><br />

#### Abschnitt “Mitglieder”
In der Mitgliederliste können Sie für jedes Mitglied die entsprechende Zutrittsgruppe einsehen.

!!! info "Info"

    Das Resultat des Kursbausteins “Zutrittskontrolle” sind die Zutrittsgruppen. Diese Zutrittsgruppen können Sie in anderen Kursbausteinen verwenden, um den Zugriff zu organisieren.
    In herkömmlichen Kursen können Sie die Optionen unter “Zugang” und “Sichtbarkeit” verwenden. In Lernpfadkursen können Sie die Ausnahmeregeln unter “Lernpfad” verwenden.

<hr />

## Im Kursrun (für Kursteilnehmer*innen)

Für die Kursteilnehmer\*innen hat dieser Kursbaustein nur informativen Charakter.
Im Kursrun können sie überprüfen, in welcher Zulassungsgruppe oder Zutrittsgruppe sie eingetragen sind.
Falls die Zulassungsgruppe eine Beschreibung hat, ist diese auch für die Kursteilnehmer\*innen sichtbar.

![](./attachments/Zutrittskontrolle-Kursteilnehmer.png)

??? info "Sichtbarkeit des Kursbausteins für die Kursteilnehmer*innen"

    Anhand der Sichtbarkeitsregeln in einem herkömmlichen Kurs können Sie einstellen, ob der Kursbaustein “Zutrittskontrolle” für die Kursteilnehmer\*innen sichtbar oder nicht sichtbar ist.
    Die Einschränkung gilt nicht für Kursbesitzer\*innen und &#8209;betreuer\*innen.

<hr />

### Anwendungsszenarien

* [Anwendungsszenario 1: Prüfung vor Ort](#anwendungsszenario-1-prufung-vor-ort)
* [Anwendungsszenario 2: Workshop](#anwendungsszenario-2-workshop)
<br /><br />

#### Anwendungsszenario 1: Prüfung vor Ort

__Ziel:__ Bei einer Online-Prüfung vor Ort möchten Sie sicherstellen, dass nur die Studierenden, welche anwesend sind, die Prüfung absolvieren können (d.h. Studierende können nicht von zu Hause an der Prüfung teilnehmen).

__Vorgehensweise:__

* Aus SAP wird eine Liste mit den Studierenden exportiert, die an der Prüfung teilnehmen dürfen.

* In OLAT wird in einem herkömmlichen Kurs der Kursbaustein “Zutrittskontrolle” hinzugefügt.

* Die benötigten Zulassungsgruppen werden hinzugefügt. Aus der SAP-Liste können die Teilnehmenden mit ihrer Matrikelnummer den Zulassungsgruppen hinzugefügt werden.

* Vor Prüfungsbeginn werden die UZH Cards der Studierenden überprüft und gescannt. Alle Anwesenden werden automatisch in die Anwesenheitsgruppe eingetragen.

* In OLAT wird der Kursbaustein “Test“ so konfiguriert, dass er nur für die Zutrittsgruppe “Anwesend” sichtbar/zugänglich ist. Nur Studierende, die in dieser Anwesend-Gruppe eingetragen sind, können die Prüfung auch starten.

* Zusätzlich werden die An- und Abwesenheiten in eine Excel-Liste exportiert.
<br />
<br />

#### Anwendungsszenario 2: Workshop

__Ziel:__ Während dem Semester gibt es in einer Veranstaltung einen Workshop. Sie möchten wissen, wer am Workshop dabei war.

__Vorgehensweise:__

* Fügen Sie in den bestehenden OLAT-Campuskurs den Kursbaustein “Zutrittskontrolle” hinzu. Dieser Kursbaustein ist für die Kursteilnehmenden nicht sichtbar.

* Als Zulassungsgruppen fügen Sie die Campusgruppen “\_UZH-Studierende mit Modulbuchung“ und “\_Manuell hinzugefügte Teilnehmende” hinzu.

* Während oder nach dem Workshop werden die UZH Cards der Studierenden gescannt.

* Nach dem Scannen exportieren Sie die An- und Abwesenheitslisten ins Excel.

<hr />

## Tipps und Known Issues

### Internet

Bitte beachten Sie, dass für die Nutzung des Kursbausteins eine funktionierende und stabile Internetverbindung erforderlich ist.
Wenn Sie die Zutrittskontrolle mit einem Tablet oder einem Computer (mit Webcam) durchführen möchten und feststellen, dass das WLAN nicht immer stabil ist, können Sie das Handy als Hotspot verwenden.

### Zeitaufwand pro Scan

Während der ersten Tests haben wir festgestellt, dass die Scan-Personen durchschnittlich 12 Sekunden pro Scan benötigen. Dies beinhaltet die Überprüfung der Identität und das Scannen des Barcodes.
Ausgehend von dieser Schätzung kann die Zeit für die gesamte Zutrittskontrolle berechnet werden:

|      | 1 Scan-Person | 2 Scan-Personen | 4 Scan-Personen | 10 Scan-Personen | 20 Scan-Personen |
|------|:-------------:|:---------------:|:---------------:|:----------------:|:----------------:|
|50 Teilnehmende|10 Minuten|5 Minuten|2 Minuten 30 Sekunden|1 Minute|30 Sekunden|
|100 Teilnehmende|20 Minuten|10 Minuten|5 Minuten|2 Minuten|1 Minute|
|500 Teilnehmende|1 Stunde 40 Minuten|50 Minuten|25 Minuten|10 Minuten|5 Minuten|
|1000 Teilnehmende|3 Stunden 20 Minuten|1 Stunde 40 Minuten|50 Minuten|20 Minuten|10 Minuten|
|2000 Teilnehmende|6 Stunden 40 Minuten|3 Stunden 20 Minuten|1 Stunde 40 Minuten|40 Minuten|20 Minuten|
|5000 Teilnehmende|16 Stunden 40 Minuten|8 Stunden 20 Minuten|4 Stunden 10 Minuten|1 Stunde 40 Minuten|50 Minuten|

### Scan-Geräte

Für das Scannen wird OLAT im Browser auf einem mobilen Gerät verwendet. Im Kursbaustein wird die Kamera des Gerätes verwendet.
Da es viele verschiedene Geräte gibt, ist es nicht möglich, genau zu kontrollieren, wie die Kamera vom Gerät verwendet wird. Daher kann es zu Unterschieden im Verhalten zwischen den verschiedenen Geräten kommen.

Aufgrund unserer bisherigen Erfahrungen haben wir diese Übersichtstabelle erstellt. Sie dient als Hilfestellung und wir empfehlen, einen Testlauf mit dem Scan-Gerät durchzuführen.

| Betriebssystem | Hinweise |
|:--------------:|:--------:|
|Android|funktioniert normalerweise problemlos|
|iOS (iPhone)|kann manchmal etwas mühsamer funktionieren (siehe Kapitel “[Hinweise für iOS-Benutzende](#hinweise-für-ios-benutzende)")|
|iPadOS (iPad)|funktioniert normalerweise problemlos|

### Hinweise für iOS-Benutzende

#### Kamera erlauben

Um im OLAT-Kursbaustein “Zutrittskontrolle” scannen zu können, muss die Kamera explizit erlaubt werden.
Jedes Mal, wenn das Dialogfenster zum Scannen geöffnet wird, muss die Kamera erlaubt werden.
Anstatt die Kamera jedes Mal neu zu erlauben, können Sie die Kamera dauerhaft erlauben.

Mehr Infos: [https://support.apple.com/de-ch/guide/iphone/iph168c4bbd5/ios](https://support.apple.com/de-ch/guide/iphone/iph168c4bbd5/ios){target=_blank}

#### Auswahl der Kamera

iPhones haben oft mehrere rückseitige Kameras. Hier ist es wichtig, die richtige Kamera zu wählen. Tests haben gezeigt, dass die rückseitige Dual-Weitwinkelkamera in den meisten Fällen am besten funktioniert.
Um die Kamera im Scan-Dialog zu wechseln, stoppen Sie zunächst das Scannen, danach können Sie eine andere Kamera auswählen.

#### Fokus der Kamera

Manchmal hat die Kamera Probleme mit der Fokussierung. In diesem Fall ist es am besten, die Seite neu zu laden und die Kamera neu zu starten.

### Tipps

* Das Scannen funktioniert am besten im “Portrait-Modus“. Je nach Handy können Sie die Kamera so einstellen, dass das Bild nicht rotiert.

* Im Tab “Zutrittskontrolle” der Kursrun-Sicht für Kursbesitzer\*innen und -Betreuer\*innen können die zu kontrollierenden Zulassungsgruppen ausgewählt werden. 

* Damit das Gerät den Barcode richtig lesen kann, sind ausreichende Lichtverhältnisse erforderlich. Es wird empfohlen, die geplanten Geräte vorab unter den gegebenen Lichtverhältnissen zu testen.

### Known Issues

* Beim Scannen mit dem Handy kann es vorkommen, dass Sie plötzlich wieder auf die Seite mit der Kursliste gelangen. Sie können den Kurs und den Kursbaustein “Zutrittskontrolle” wieder öffnen und weitermachen. Es gehen keine Daten verloren.

### Identitätskontrolle und Scanning organisieren

Die Zutrittskontrolle umfasst die Überprüfung der Identität und das Scannen der UZH Card der Teilnehmenden. Dies kann auf unterschiedliche Weise organisiert werden.

1. **Gleichzeitig die Identität überprüfen und scannen**
Eine Aufsichtsperson oder eine Gruppe von Aufsichtspersonen überprüft zuerst die Identität des Teilnehmenden und scannt direkt danach den Barcode.

2. **Erst die Identität prüfen, dann scannen**
Bei vielen Teilnehmenden empfehlen wir,  dass eine erste Gruppe von Aufsichtspersonen die Identität der Teilnehmenden überprüft. Eine zweite Gruppe von Aufsichtspersonen kann sich dann auf das Scannen der UZH Cards konzentrieren.

### Übersicht der Rechte für jede (OLAT-)Rolle

Je nach (OLAT-)Rolle erhalten Sie eine andere Sicht auf den Kursbaustein “Zutrittskontrolle“. 
Bitte beachten Sie, dass der Kursbaustein für die entsprechende Rolle sichtbar sein muss.

|         | Kursbesitzer*in | Kursbetreuer*in |Scan-Person | Kursteilnehmer*in |
|---------|:---------------:|:---------------:|:----------:|:-----------------:|
|Kurseditor| x | 
|Mitgliederverwaltung | x |
|Bewertungswerkzeug | x | x |
|Tab “Zulassungsgruppen“ | x | x |
|Tab “Zutrittskontrolle” | x | x | x |
|Tab “Zutrittsgruppen“ | x | x |
|Teilnehmer*innensicht mit Gruppeninfo und &#8209;Beschreibung | | | | x |