# Course element “Entry control”

The new OLAT course element “Entry control” enables precise management of access to specific content within an OLAT course. Course owners can ensure that only legitimate course participants are granted access. 

Various [use cases](#use-cases) can be mapped using the course module “Entry control”.<br /><br /><figure>![](./attachments/UZHcard.png)<figcaption>The barcode of the UZH card (matriculation number) is scannend for the purpose of entry control</figcaption></figure>

The entry control process is divided into the following three sections:

1. __Definition of admission groups:__ Prior to the start of an event, course owner define admission groups and assign students to these groups. This ensures that only legitimate individuals are granted access.

2. __Conduct entry control:__ The identity of students is verified to ensure that they belong to the admission group. This is done using the existing UZH card, whose barcode (with matriculation number) can be scanned using a mobile phone or webcam. After successful identification, students are automatically or manually added to the entry group.

3. __Allocation to entry groups:__ Based on the entry control, students are added to an entry group. These groups are the result of the entry control process and ensure organised access to content.<br /><br /><figure>![](./attachments/Entry-control-process.png)<figcaption>
Process of the OLAT course module “Entry control”</figcaption></figure>

All changes to the entry status of course participants in the course element are logged, which ensures seamless tracking. The entry groups can also be used to configure other course elements within OLAT in order to ensure consistent administration of the course.

!!! info "Barcode on the UZH card: matriculation number or employee number"

    The barcode on the back of the UZH card shows either the matriculation number or the employee number.
    In the course element “Entry control”, the barcode can be scanned with either the matriculation number or the employee number. If the member of an admission group has a matriculation number, the matriculation number will be used. If there is no matriculation number, the system checks if the member has an employee number. If so, the employee number is used for the entry control.

<hr />

## In the course editor (for course owners)

In the course editor, the tabs can be used for the central settings of the module.

### Tab “Title and description”

Here you can enter a title and description for the course element.
The description you add will be visible to students during the course.

!!! info "For more information, see the chapter ["Title and description“](../../manual_user/learningresources/General_Configuration_of_Course_Elements/#desc)."

### Tab “Layout"

Here you can change the layout options.

!!! info "For more information, see the chapter [“Layout“](../../manual_user/learningresources/General_Configuration_of_Course_Elements/#tab-layout)."

### Tab “Visibility" (only for conventional courses)

Changing the visibility of a course element is one of three ways to restrict access to course elements.
If you restrict the visibility of a course element, it will no longer appear in the course navigation.

!!! info "For more information, see the chapter ["Visibility"](../../manual_user/learningresources/General_Configuration_of_Course_Elements/#access)."

### Tab “Access” (only for conventional courses)

In the tab “Access” of the course element you can define who has access to this element.

!!! info "For more information, see the chapter [“Access“](../../manual_user/learningresources/General_Configuration_of_Course_Elements/#access)."

### Tab “Learning path”(only for learning path courses)

In case of a [learning path course](../../manual_user/learningresources/Learning_path_course_Course_editor/), you can define in the tab “Learning path” whether the execution of the course element is obligatory or voluntary. Furthermore, you can define a time period during which the course element can be processed.

Each course element has individual completion criteria, an overview of which can be found in the "learning path" section.

!!! info "For more information, see the chapter ["Learning path"](../../manual_user/learningresources/Learning_path_course/)."

!!! warning "Conventional courses do not have the "Learning path" Tab and instead have the ["Visibility"](../../manual_user/learningresources/General_Configuration_of_Course_Elements/#access) und ["Access"](../../manual_user/learningresources/General_Configuration_of_Course_Elements/#access) tabs including [export mode](../access_restrictions_in_expert_mode/)."

### Tab “Configuration”

![](./attachments/Entry-control-course-editor.png)<br /><br />

#### Section “Admission groups”

At least one admission group is required for an en entry control. An admission group is a group whose members are allowed to attend a certain course.

Course owners can select OLAT groups or create new ones.
The order of the groups can be changed later if required.

??? info "Remove admission groups"

    Admission groups can be removed in the “Configuration” tab of the course editor. Please note that created groups can only be removed in the course element “Entry control". Removed groups will remain in your OLAT course as groups (with the previously assigned members). If you want to, you can permanently delete such groups in your members management.

??? info "Managing groups in the members management"

    It is theoretically possible to manage the admission and entry groups in the members management.
    Please note that we do not recommend this as changes cannot be tracked and students may be entered in more than one admission group.
    If a member is entered in more that one admission group, an error message will appear.
<br />

#### Section “Entry control”

##### Simplified identity verification
If you enable simplified identity verification, you do not have to explicitly confirm entry each time you scan. This means that you can scan more quickly, but you do not have the opportunity to verify the details (email address and admission group).

* If the scan is successful, a green message appears which disappears after 2 seconds to allow the next barcode to be scanned.

* If the scan is unsuccessful, a red message appears summarising the errors. This message will remain until the next scan.

##### Scan persons
Course owners and coaches can conduct the entry control in the course run.

If you need the help of an additional scan person for the actual entry control, you can create one or more groups and add scan persons to them. The scan persons do not have access to the administration of the course, including members management and the assessment tool. They can only conduct the entry control in the course run of the course element.

The groups can be selected or created from scratch.
<br /><br />

#### Section “Entry groups”
Once the admission groups have been defined, the corresponding entry groups are automatically created.
For each admission group, two entry groups are created:

* a group for those present (suffix: " - present")
* a group for absentees (suffix: " - absent")

The suffixes are fixed and cannot be changed.

<hr />

## In the course run (for course owners and coaches)

In the course run you can add members to __admission groups__, conduct the __entry control__ and check the members of the __entry groups__.

!!! info "All tables (and their data) can be exported to an Excel file."

### Tab “Admission groups”

####  Section “Admission groups"

![](./attachments/Entry-control-tab-admission-groups.png)

Here you can see a list of the admission groups which were defined in the course editor and add members to them.

??? info "Add members"

    You can add a member using the __user search__. If you want to add many members at once, we recommend using the __bulk search__.
    In the bulk search you can add several members by entering a list of usernames, email or matriculation numbers.
    If a person studies at different institutions, he or she may have several OLAT accounts with the same matriculation number. In this case you will get a message when adding this member and you can choose the OLAT account you want to add.

??? info "Coach or participant rights for members?"

    When adding members to an admission group, you can decide what rights the members should have. In the course element “Entry control", both group coaches and group participants can be added to the relevant admission groups.
    In this context, there is no difference between the roles of group coaches and group participants - they can both be added to admission groups in the same way.
    Please note, however, that the coach role in a group will affect other course tools, such as the assessment tool, if you continue to use the same groups.
    We recommend that you only assign participant rights to members. This means that every member is a group participant.

!!! info "A member can only belong to one admission group. If you try to add a member to more than one admission group, you will receive an error message."
<br /><br />

#### Section “Members”
All members of the admission groups are listed in the “Members” section.
Members can be removed from an admission group here.

![](./attachments/Entry-control-tab-entry-control.png)

### Tab “Entry control”

The actual entry control takes place in the “Entry control” tab. Here you have the option of automatically or manually adding members from the pre-defined admission groups. This results in the "Present/Absent" entry groups.

![](./attachments/Zutrittskontrolle-scan.jpg)

#### Section “Automatic entry”
As multiple course owners and coaches can conduct the entry control simultaneously, with the option "My admission groups to be checked" it is possible to restrict the list of members to these users. This means that each person is responsible for their own admission group.

If the details and photo on the UZH card match the person, click on the “Scan barcode” button. The camera is activated and you can scan the barcode. OLAT reads the data and the person is added to the corresponding entry group.

!!! info "To use the camera in the browser, you need a working camera. In addition, the use of the camera in the browser must be explicitly allowed."
    
    ![](./attachments/Entry-control-scan-barcode-request-permission.png)

There are 3 possible outcomes for automatic entry (&thinsp;= barcode scanning):

* Result 1: Confirm entry (blue message)
* Result 2: Entry not possible - member already added (yellow message)
* Result 3: Entry not possible - member cannot be added (red message)<br /><br />

__Result 1: Confirm entry (blue message)__

* If a member of an admission group has not yet been added and the barcode has been successfully scanned, you can confirm entry.<br /><br />
![](./attachments/Entry-control-scan-barcode-confirm-entry.png)<br /><br />

* A person can have several OLAT accounts with the same matriculation number. If the barcode is successfully scanned, the person who scans it receives a message that the person has several OLAT accounts with the same matriculation number and that it is important that this person logs in with the correct OLAT account.<br /><br />
![](./attachments/Entry-control-scan-barcode-confirm-entry-several-OLAT-accounts.png)<br /><br />

__Result 2: Entry not possible - member already added (yellow message)__

* If a member has already been added to an entry group, they cannot be added again.<br /><br />
![](./attachments/Entry-control-scan-barcode-entry-not-possible-already-entered.png)<br /><br />

__Result 3: Entry not possible - member cannot be added (red message)__

* If the barcode is not registered as a matriculation or employee number in OLAT, no OLAT account can be found and entry is not possible.<br /><br />
![](./attachments/Entry-control-scan-barcode-entry-not-possible-barcode-not-found.png)<br /><br />

* If the barcode has been successfully scanned but no member of the admission group to be checked is found, the member cannot be added.<br /><br />
![](./attachments/Entry-control-scan-barcode-entry-not-possible-no-entry-group.png)<br /><br />

#### Section “Member verification and manual entry”
This section lists all the members of the admission groups. Here you can check if they have already been entered in a “Present” or “Absent” group.
It is also possible to manually add one or more members to an entry group (present or absent).
Members can be removed at any time.

!!! info "Info"

    All changes made to the course element “Entry control” are recorded in the change log.
    Click on a member’s name to open a detailed view and see the change log.

<br />

#### Section “Manage scan persons”
If configured in the course editor, you can add scan persons in the section.
Members of this group will have access to the entry control. They will only see the tab “Entry control” and will not have access to any other functions in the course or in this course element. Scan persons are course participants who have the right to conduct the entry control.

??? info "Coach or participant rights for members?"

    When you add members to the group with scan persons, you can decide what right the members should have. We recommend that you only give members participants rights. This means that every member is a group participant. In the course element “Entry control", both group coaches and group participants can conduct the entry control. No distinction is made here.
    However, as these groups are also course groups, there may be different effects if you use these groups for other purposes.
<br />

#### Section “Closure of the entry control”

Course owners and course supervisors have the option of closing an entry control. When the entry control is closed, all members who are not yet entered in an entry group are automatically entered in the corresponding absence entry group.

After closing, members can no longer be entered or removed.
Changes are entered in the change history.

If necessary, access control can be reopened and members can be added and removed again.
<br />

### Tab “Entry groups”

The result of the entry control can be viewed in the tab “Entry groups".

![](./attachments/Entry-control-tab-entry-groups.png)
<br /><br />

#### Section “Entry groups”
You can check the number of members in the overview of the entry groups.
<br /><br />

#### Section “Members”
You can view the corresponding entry group for each member in the member list.

!!! info "Info"

    Entry groups are the result of the course element “Entry control". You can use these entry groups in other course elements to organise access.
    In conventional courses you can use the options under “Access” and “Visibility". In learning path courses you can use the exceptions under “Learning path".

<hr />

## In the course run (for course participants)

This course element is for information purposes only.
In the course run, you will be able to see which admission or entry group you are enrolled in.
If the admission group has a description, this will also be visible to the course participants.

![](./attachments/Entry-control-course-participant.png)

??? info "Visibility of the course module for the course participants"

    You can use the visibility rules in a conventional course to determine whether or not the course element “Entry control” is visible to course participants. This restriction does not apply to course owners or coaches.

<hr />

### Use cases

* [Use case 1: On-site examination](#use-case-1-on-site-examination)
* [Use case 2: Workshop](#use-case-2-workshop)
<br /><br />

#### Use case 1: On-site examination

__Objective:__ For an on-site online exam, you want to ensure that only your students who are present can take the exam (i.e. students cannot take the exam from home).

__Procedure:__

* A list of students who are allowed to take the exam is exported from SAP.

* In OLAT, the course element “Entry control” is added to a conventional course.

* The necessary admission groups are added. Students can be added to the admission groups from the SAP is using their matriculation number.

* Students' UZH cards are checked and scanned before the start of the exam. All students are automatically added to the entry group.

* The course element “Test” ist configured in OLAT in such a way that it is only visible/accessible for the entry group “Present". Only students who are entered in the entry group can start the test.

* In addition, attendances and absences are exported to an Excel list.
<br /><br />

#### Use case 2: Workshop

__Objective:__ During the semester there is a workshop in a course. You want to know who attended the workshop.

__Procedure:__

* Add the course element “Entry control” to your existing OLAT campus course. This course element is not visible for course participants.

* Add the campus groups “\_UZH-Students with booking modules” und "\_Manually added participants” as admission groups.

* The student’s UZH cards will be checked during or after the workshop.

* After scanning, export the attendance and absence list to excel.

<hr />

## Tips and known issues 

### Internet connection

Please note that a working and stable internet connection is required to use the course element.
If you are using a tablet or computer (with webcam) for the entry control and find that the Wi-Fi is not always stable, you can use your mobile phone as a hotspot.

### Time required per scan

During initial testing, we found that the scan persons took an average of 12 seconds per scan. This includes identity verification and barcode scanning.
Based on this estimate, the time required for the entry control process can be calculated:

|      | 1 scan person | 2 scan persons | 4 scan persons | 10 scan persons | 20 scan persons |
|------|:-------------:|:---------------:|:---------------:|:----------------:|:----------------:|
|50 participants|10 minutes|5 minutes|2 minutes 30 seconds|1 Minute|30 seconds|
|100 participants|20 minutes|10 minutes|5 minutes|2 minutes|1 minute|
|500 participants|1 hour 40 minutes|50 minutes|25 minutes|10 minutes|5 minutes|
|1000 participants|3 hours 20 minutes|1 hour 40 minutes|50 minutes|20 minutes|10 minutes|
|2000 participants|6 hours 40 minutes|3 hours 20 minutes|1 Stunde 40 minutes|40 minutes|20 minutes|
|5000 participants|16 hours 40 minutes|8 hours 20 minutes|4 hours 10 minutes|1 hour 40 minutes|50 minutes|

### Scan devices

OLAT is used in the browser of a mobile device for automatic entry. The camera of the device is used in the course element.
As there are many different devices, it is not possible to control precisely how the camera of the device is used. Therefore, there may be differences in the behaviour between different devices.

Based on the experience we have gained so far, we have created an overview in the table below. We recommend that you carry out a test run with the scanning device.

| Operating system | Notes |
|:----------------:|:-----:|
|Android|usually works without problems|
|iOS (iPhone)|can sometimes be a little more tedious (see the "[Notes for iOS users](#notes-for-ios-users)” section)|
|iPadOS (iPad)|usually works without problems|

### Notes for iOS users

#### Allow camera

In order to be able to scan in the OLAT course element “Entry control", the camera must be explicitly allowed. The camera must be allowed every time the dialog window for scanning is opened.
Instead of authorising the camera each time, you can authorise it permanently.

For more information: [https://support.apple.com/en-gb/guide/iphone/iph168c4bbd5/ios](https://support.apple.com/en-gb/guide/iphone/iph168c4bbd5/ios){target=_blank}

#### Selecting the camera

iPhones often have multiple cameras. It is important to choose the right one. Tests have shown that the dual wide-angle rear camera works best in most cases.
To change the camera in the scan dialogue, first stop scanning and then select a different camera.

#### Camera focus

Sometimes the camera has trouble focusing. The best thing to do is to reload the page and restart the camera.

### Tips

* Scanning works best in portrait mode. Depending on your phone, you can set the camera to prevent from rotating.

* The admission groups to be checked can be selected in the “Entry control” tab of the course run view for course owners and coaches.

* Adequate lighting is a prerequisite for correct bar code reading. It is recommended to test the planned devices in advance under the given lighting conditions.

### Known Issues

* When scanning with your mobile phone, you may suddenly be taken back to the course list page. You can reopen the course and the course element “Entry control” and continue. No data is lost.

### Setup of identity verification and scanning

Entry control involves checking the identity of participants and scanning their UZH cards. This can be configured in several ways.

1. **Simultaneous identity verification and scanning**
A supervisor or group of supervisors first checks the identity of the participants and then immediately scans the barcode.

2. **Verify identity first, then scan**
For large groups of participants, it is recommended that a first group of supervisors verify the identity. A second group of supervisors can then scan the verified participants (without having to concentrate on the identity check).

### Overview of the rights for each (OLAT) role

Depending on your (OLAT) role you will have a different view of the course element “Entry control".
Please note that the course element has to be visible for the corresponding role.

|         | Course owner | Coach | Scan person | Course member |
|---------|:------------:|:-----:|:-----------:|:-------------:|
|Course editor| x | 
|Members management | x |
|Assessment tool | x | x |
|Tab "Admission groups" | x | x |
|Tab "Entry control" | x | x | x |
|Tab "Entry groups" | x | x |
|Participant view with group info and description | | | | x |