# Kursbaustein “Zutrittskontrolle“

## Übersicht

![](./attachments/Zutrittskontrolle-scan.jpg)

Der Kursbaustein “Zutrittskontrolle“ ermöglicht es Kursbesitzer:innen, eine Einlass- und Anwesenheitskontrolle anhand einer Teilnehmendenliste durchzuführen und zu dokumentieren. 

Die Identitätsprüfung erfolgt durch Scannen des Barcodes auf der UZH Card (Matrikel- oder Personalnummer) mittels Handykamera oder Webcam. 
Nach erfolgreicher Verifizierung werden die Teilnehmer:innen automatisch oder manuell einer definierten Zutrittsgruppe (z.B. “anwesend”) hinzugefügt. Der Zugang zu bestimmten Kursinhalten kann optional ausschliesslich für diese Gruppe freigeschaltet werden.

#### *Definition der Begrifflichkeiten*
**Einlasskontrolle:** Der Vorgang des Scannens und der Identitätsprüfung.

**Zutrittskontrolle:** Der Kursbaustein, der den Einlass- und Anwesenheitsprozess ermöglicht.

**Zulassungsgruppe:** Definiert, wer grundsätzlich an der Veranstaltung teilnehmen darf.

**Zutrittsgruppe:** Listen auf, welche Teilnehmenden durch den Scan als „anwesend“ oder “abwesend” erfasst wurden – diese Gruppen dienen als Voraussetzung für den Zugriff auf bestimmte Kursinhalte.

**Gruppe der Scan-Personen:** Personen, die berechtigt sind, den Scan durchzuführen

## Einrichtung im Kurseditor

1.	**Kursbaustein hinzufügen:** Fügen Sie im Kurseditor den Baustein "Zutrittskontrolle" an der gewünschten Position ein.
2.	**Titel und Beschreibung:** Vergeben Sie einen aussagekräftigen Titel und eine kurze Beschreibung.<br /><br />
![](./attachments/Zutrittskontrolle-Kurseditor.png)

3. **Konfiguration:**
    1. **Zulassungsgruppen definieren:** Wählen Sie Gruppen aus, die für den Zugang berechtigt sind, oder erstellen Sie diese neu.
    2. **Vereinfachte Prüfung der Identität:** Entscheiden Sie, ob beim Scan die Prüfung der Detaildaten (E-Mail-Adresse und Zulassungsgruppe) und explizite Bestätigung des Scans übersprungen werden soll.
    3. **Scan-Personen festlegen:** Legen Sie eine oder mehrere Gruppen für Scan-Personen fest. Mitglieder dieser Gruppen sind berechtigt, die UZH Cards zu scannen. Scan-Personen ohne weitere Kursrollen haben keinen Zugriff auf die Kursadministration.
    4. **Zutrittsgruppen prüfen:** Ändern Sie bei Bedarf die Suffixe der beiden automatisch angelegten Zutrittsgruppen.
4. Publizieren Sie die Änderungen.

## Durchführung der Zutrittskontrolle

Die Zutrittskontrolle kann durch Kursbesitzer:innen, -betreuer:innen und zusätzlich definierte Scan-Personen durchgeführt werden. 

!!! info "Ansicht & Rechte der Scan-Personen"

    Scan-Personen ohne weitere Kursrollen sehen im Kursbaustein “Zutrittskontrolle” nur den Tab “Zutrittskontrolle” ohne die Möglichkeit Scanpersonen zu verwalten und die Zutrittskontrolle endgültig abzuschliessen.

![](./attachments/Zutrittskontrolle-Tab-Zutrittskontrolle.png)

((RUE: folgende Aufzählung muss meiner Meinung nach noch überarbeitet werden > verschiedene Usecases darstellen))

1.	**Zutrittskontrolle starten:** Öffnen Sie in der normalen Kursansicht den Tab "Zutrittskontrolle" im betreffenden Baustein des veröffentlichten Kurses.
2.	**Automatisch eintragen:** Wählen Sie die vereinbarten Zulassungsgruppen aus und klicken Sie auf “Barcode scannen”.
3.	**Automatische Aufnahme:** Nach erfolgreichem Scan werden die Teilnehmenden automatisch der Zutrittsgruppe hinzugefügt.
4.	**Mitglieder überprüfen und manuell eintragen:** Nehmen Sie die Zuordnung einzelner Mitglieder manuell vor, falls der Barcode-Scan nicht möglich ist. Alle Änderungen an der Gruppenzugehörigkeit der Teilnehmenden sind über die Auswahl der einzelnen Mitglieder einsehbar.
5.	**Scan-Personen verwalten:** Passen Sie als Kursbesitzer:in oder -betreuer:in bei Bedarf die Liste der berechtigten Scan-Personen an.
6.	**Zutrittskontrolle abschliessen:** Beenden Sie als Kursbesitzer:in oder betreuer:in den Scan-Vorgang und speichern Sie die Ergebnisse.

Im Tab “Zutrittsgruppen” sehen Sie als Kursbesitzer:in oder –betreuer:in die einzelnen Zutrittsgruppen und die an- und abwesenden Mitglieder. Die einzelnen Tabellen können bei Bedarf exportiert werden.

## Tipps zum Scan

| Betriebssystem | Hinweise |
|:--------------:|:--------:|
|Android|funktioniert normalerweise problemlos|
|iOS (iPhone)|kann manchmal etwas mühsamer funktionieren (siehe Kapitel “[Hinweise für iOS-Benutzende](#hinweise-für-ios-benutzende)")|
|iPadOS (iPad)|funktioniert normalerweise problemlos|

* **Geeignete Geräte verwenden:** Nutzen Sie Geräte mit funktionsfähiger Kamera, wie Smartphones oder Webcams. Wir empfehlen trotzdem die Durchführung eines Testlaufs mit den geplanten Geräten.
    * **Hinweise für iPhone-Nutzer:innen:** Erlauben Sie den Kamerazugriff im Browser und wählen Sie am besten die rückseitige Dual-Weitwinkelkamera. Stellen Sie sicher, dass die Kamera korrekt fokussiert. Ein Neuladen der Seite und ein Neustart der Kamera kann bei Problemen helfen. 
* **Stabile Internetverbindung:** Sorgen Sie während des Scan-Vorgangs für eine zuverlässige Internetverbindung.
* **Zeitaufwand pro Scan:** Planen Sie pro Scan mit Identitätskontrolle etwa 12 Sekunden ein.
* **Organisation der Identitätskontrolle:** Stellen Sie ausreichend Personal für den Scan-Vorgang bereit, um Wartezeiten zu minimieren.
* **Barcode richtig erkennen:** Bei Schwierigkeiten mit dem Scannen der UZH Card reinigen Sie die Karte und sorgen Sie für ausreichende Lichtverhältnisse.
* **Internen Test durchführen:** Fügen Sie die Scan-Personen vorübergehend als normale Mitglieder/Gruppenteilnehmer:innen zu einer Zulassungsgruppe hinzu. Die Scan-Personen können nun die Handhabung durch Scannen der eigenen UZH-Card testen.  

## Tipps zur Mitglieder- und Gruppenverwaltung
#### *Zulassungsgruppen verwalten*
Im Tab “Zulassungsgruppe” des laufenden Kurses werden die im Kurseditor definierten Zulassungsgruppen ausgelistet.

![](./attachments/Zutrittskontrolle-Tab-Zulassungsgruppen.png)

* **Einzelne Mitglieder hinzufügen:**  Fügen Sie bei Bedarf neue Mitglieder als Gruppenteilnehmer:in einer Zulassungsgruppe hinzu. Ein Mitglied kann nur einer Zulassungsgruppe gehören. Wenn Sie versuchen, ein Mitglied zu mehr als einer Zulassungsgruppe hinzuzufügen, erhalten Sie eine entsprechende Fehlermeldung.
* **Nicht berechtigte Mitglieder entfernen:** Entfernen Sie nicht berechtigte Mitglieder aus der Zulassungsgruppe.

#### *Zulassungsgruppen entfernen*
Zulassungsgruppen können im Tab “Konfiguration” des Kurseditors entfernt werden. Aus der Zutrittskontrolle entfernte Zulassungsgruppen bleiben mit ihren Mitgliedern im Kurs bestehen. Falls Sie eine Gruppe und ihre Mitglieder stattdessen aus dem Kurs löschen möchten, müssen Sie die gewünschte Gruppe über die Mitgliederverwaltung des Kurses löschen.

#### *Gruppenverwaltung in der Mitgliederverwaltung des Kurses*
Grundsätzlich ist es möglich, die Zulassungs- und Zutrittsgruppen über die Mitgliederverwaltung des Kurses zu verwalten. Wir raten allerdings von diesem Vorgehen ab, da Änderungen nicht nachvollziehbar sind und Studierende fälschlicherweise in mehreren Zulassungsgruppen eingetragen werden können.

#### *Rechte der einzelnen Kursrollen*

|         | Kursbesitzer:in | Kursbetreuer:in |Scan-Person |
|---------|:---------------:|:---------------:|:----------:|
|Kurseditor| x | 
|Mitgliederverwaltung | x |
|Bewertungswerkzeug | x | x |
|Tab “Zulassungsgruppen“ | x | x |
|Tab “Zutrittskontrolle” | x | x | x |
|Tab “Zutrittsgruppen“ | x | x |


## Baustein aus Teilnehmendensicht
Falls der Baustein für Kursteilnehmer:innen eingeblendet ist, wird diesen angezeigt, in welcher Zulassungsgruppe oder Zutrittsgruppe sie eingetragen sind. Falls die Zulassungsgruppe eine Beschreibung hat, ist diese für die Kursteilnehmer:innen sichtbar.

![](./attachments/Zutrittskontrolle-Kursteilnehmer.png)


