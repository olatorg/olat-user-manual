# Delegation

Delegates of an author can create and administer his campus courses (UZH only). The delegation is particularly recommended for e-learning coordinators or assistants who take on the creation and administration for the lecturers they support.

Delegations can only be given to OLAT authors and relate exclusively to [campus courses](../campus_course/index.md).

!!!info
    Apply for a delegation with the consent of the author concerned from [OLAT Support](mailto:lms-support@zi.uzh.ch).