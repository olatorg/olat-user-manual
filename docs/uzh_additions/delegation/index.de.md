# Delegation

Delegierte eines Autors können dessen Campuskurse (nur UZH) erstellen und administrieren. Die Delegation empfiehlt sich im Besonderen für E-Learning-Koordinator*innen oder Assistent*innen, welche die Erstellung und Administration für die von ihnen betreuten Dozent*innen übernehmen.

Delegationen können nur an OLAT-Autor*innen vergeben werden und beziehen sich ausschliesslich auf [Campuskurse](../campus_course/index.de.md).

!!! info "Info"
    Beantragen Sie eine Delegation mit dem Einverständnis der dozierenden Person beim [OLAT-Support](mailto:lms-support@zi.uzh.ch).
