# Prüfungen

In OLAT können Tests oder andere Aufgaben zur Leistungsüberprüfung
durchgeführt werden. Unter "[Kursbaustein > Wissensüberprüfung](../../manual_user/learningresources/Course_Elements.de.md/#wissensuberprufung)" finden Sie eine Übersicht über alle Kursbausteine zur Wissensüberprüfung.

## Prüfungsmodus

Um Prüfungen unter kontrollierten Bedingungen durchzuführen, gibt es in
OLAT den sogenannten Prüfungsmodus. Dabei werden alle OLAT-Bestandteile
ausgeblendet. Nur die prüfungsrelevanten Kursbausteine werden dabei
angezeigt. Eine solche Prüfung ist immer zeitlich begrenzt, und kann eine Vor-
und eine Nachlaufzeit beinhalten. Während diesen Zeiten ist der Zugriff auf
alle OLAT-Funktionen ebenfalls eingeschränkt.

!!! attention "Achtung"
    Der Prüfungsmodus ist ausschliesslich auf den separaten Prüfungsumgebungen
    verfügbar, aber nicht auf der unter [lms.uzh.ch](http://lms.uzh.ch){:target="\_blank"} zugänglichen Lernplattform.


## Safe Exam Browser (SEB) 

Es ist möglich, dass die Nutzung des Safe Exam Browsers (SEB) eine Voraussetzung für
die Durchführung der Prüfung ist. Dies wird Ihnen ebenfalls angezeigt, für den
Fall, dass Sie OLAT nicht bereits im Safe Exam Browser gestartet haben. Ist
dies der Fall, müssen Sie OLAT mit dem Safe Exam Browser starten, um die
Prüfung durchführen zu können.

* * *

!!! info ""
    Weitere Informationen zu Prüfungen mit OLAT (UZH) und zu den Prüfungsdienstleistungen an der UZH finden Sie unter [UZH-Teachingtools](https://teachingtools.uzh.ch/de/tools/pruefungssoftware){:target="\_blank"} sowie auf der [Webseite der Zentralen Informatik der UZH](https://www.zi.uzh.ch/de/support/e-learning-and-examination/staff/online-exam.html){:target="\_blank"}.