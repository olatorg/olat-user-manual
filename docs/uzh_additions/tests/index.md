#  Tests

OLAT supports the execution of tests or other tasks in order to assess
achievements. Under "[Types of Course Elements > Assessment](../../manual_user/learningresources/Course_Elements.md/#assessment)" you can find an overview of all course elements for knowledge assessment.

## Assessment mode

In order to perform a test in a controlled environment, OLAT offers the
assessment mode. While the assessment mode is in effect, access to all OLAT
functions are restricted, bar the configured relevant course elements and the
logout. Such a regulated assessment always has a defined duration and may
include preparation and follow-up time, during which all OLAT functions are
also inaccessible.

!!! attention "Attention"
    Assessment (or Exam) mode is only available on separate exam environments, but not on the learning management system OLAT under [lms.uzh.ch](http://lms.uzh.ch){:target="\_blank"}.


## Safe Exam Browser (SEB) 

It is possible that the use of the Safe Exam Browser (SEB) is required in order to
take the assessment. You will be notified of this requirement in the unlikely
case that you didn’t already start OLAT in the Safe Exam Browser. In that
case, however, you will need to log in to OLAT with the Safe Exam Browser in
order to take the exam.

* * *

  
!!! info ""
    For more information on exams and our examination services, please visit
    [EPIS](https://teachingtools.uzh.ch/en/tools/pruefungssoftware){:target="\_blank"} or on the [website of central IT of UZH](https://www.zi.uzh.ch/de/support/e-learning-and-examination/staff/online-exam.html){:target="\_blank"}. (currently in german only)