# ZOOM Videokonferenzen

!!! info
      Die folgenden Anleitungen gelten für Angehörige der UZH. Wenn Sie Zoom in OLAT-Kursen für andere Bildungsinstitutionen integrieren wollen, wenden Sie sich bitte an den IT-Support ihrer Institution.


!!! info ""
    Weitere Informationen über den Einsatz und das Nutzen von ZOOM an der UZH finden Sie auf
    [der Support-Seite der Zentralen Informatik der
    UZH](https://www.zi.uzh.ch/de/support/audio-video/zoom.html){:target="\_blank"}.

## Für Zoom registrieren, die App installieren und nutzen

Bitte folgen Sie den Anweisungen auf den unten aufgelisteten Anleitungswebseiten der
Zentralen Informatik der UZH.

  1. Zoom registrieren: <https://www.zi.uzh.ch/de/support/audio-video/zoom/registerinstall.html#Benutzerkonto_f%C3%BCr_ZOOM_registrieren>{:target="\_blank"}
  2. Die Zoom App installieren: <https://www.zi.uzh.ch/de/support/audio-video/zoom/registerinstall.html#ZOOM-App_installieren>{:target="\_blank"}
  3. Die Zoom App nutzen: <https://www.zi.uzh.ch/de/support/audio-video/zoom/use.html>{:target="\_blank"}

## Zoom Account einrichten

Um eine Zoom Videokonferenz in OLAT zu integrieren, müssen Sie sich bei Zoom mit Ihrer Organisationsidentität anmelden.

  1. Gehen Sie auf <https://zoom.us/web/sso/login?#/sso>{:target="\_blank"}
  2. Ergänzen Sie das Feld "Firmendomäne/Company Domain" zu: _**uzh**.zoom.ch_
  3. Klicken Sie auf "*Fortfahren*"
  4. Die Login-Seite der Universität Zürich öffnet sich. Melden Sie sich mit Ihrem Shortname oder Ihrer E-Mail-Adresse und Ihrem Passwort (Active Directory Kennwort) an.

Der Account ist nun erfolgreich erstellt.

## Integration von Zoom in OLAT

Es gibt 4 Möglichkeiten, ein Zoom Meeting oder Webinar in einem OLAT-Kurs zu
veröffentlichen:

  1. Veröffentlichen des Zoom-Links in einem [Kursbaustein vom Typ "Externe Seite"](../../manual_user/learningresources/Knowledge_Transfer.de.md#external_page)
  2. Veröffentlichen des Zoom-Links in einem [Kursbaustein vom Typ "Einzelne Seite"](../../manual_user/learningresources/Course_Element_HTML_Page.de.md)
  3. Versenden des Zoom-Links per E-Mail (mittels [Kursbaustein vom Typ "E-Mail"](../../manual_user/learningresources/Administration_and_Organisation.de.md#mail))
  4. **neu ab OLAT 5:** Integration mit dem [Kursbaustein "Zoom"](../../manual_user/learningresources/Course_Element_Zoom.de.md)
  
!!! warning "Die Integration von Zoom über den Kursbaustein "LTI-Seite" wird nicht mehr unterstützt. Verwenden Sie stattdessen den Kursbaustein "Zoom"."


!!! info "Info"

    Die maximale Verfügbarkeit der Cloud Recordings von Zoom Meetings beträgt 60 Tage. Aufzeichnungen, welche länger als 60 Tage verfügbar sein sollen (z.B. Vorlesungsaufzeichnungen), müssen vor Ablauf der 60 Tage im Zoom Account heruntergeladen werden und können über den Kaltura Kursbaustein in OLAT dauerhaft veröffentlicht werden.


!!! note "Hinweis"

    Falls Sie beim Einrichten Ihres Zoom-Accounts Probleme mit der Anmeldung als UZH-Angehörige haben, wenden Sie sich an den [IT Servicedesk der UZH](https://www.zi.uzh.ch/de/support.html){:target="\_blank"}.

Weitere Informationen zur Verwendung von Zoom in der Lehre sind verfügbar unter:

[![](attachments/zoom_video_conferences_de/teachingtool.png)](https://teachingtools.uzh.ch/de/tools/lehrveranstaltungen-mit-zoom){:target="\_blank"}
