# Unterstützende Technologien

Dieses Kapitel enthält Anleitungen zu verschiedenen Technologien, die bei der
Arbeit mit OLAT hilfreich sind. Die Anleitungen sind primär für OLAT-Benutzer
gedacht, die OLAT intensiv nutzen und erfahrene Autoren sind.

Hier finden Sie Informationen zum Einsatz von WebDAV (Unterstützung des Dateitransfers 
vom lokalen Rechner zu OLAT-Ordnern) und dazu, wie ZOOM Videokonferenzen in OLAT eingebunden werden können.

  * [WebDAV](webdav.de.md)
  * [ZOOM Videokonferenzen](zoom_video_conferences.de.md)
