# WebDAV

WebDAV steht für "Web-based Distributed Authoring and Versioning" und ist ein
offener Standard zur Übermittlung von Dateien im Internet. OLAT unterstützt
dieses Protokoll und ermöglicht so einen einfachen Dateitransfer von Ihrem
Rechner zu OLAT-Ordnern.

## Vorteile von WebDAV

Ohne WebDAV können Dateien nur über herkömmliche Upload-Formulare in OLAT
hochgeladen werden. Dabei wählen Sie jede Datei einzeln aus.

Mit WebDAV hingegen können Sie von Ihrem Rechner bequem mit "Drag and Drop"
nebst einzelnen Dateien auch ganze Verzeichnisse in OLAT-Ordner kopieren. Sie
erhalten mit WebDAV Zugriff auf alle Ablageordner Ihrer Kurse.

## Dateien hochladen ohne WebDAV

Über die Funktion "Datei hochladen" können Sie eine einzelne Datei auswählen.

![](attachments/webdav_de/WebDAV-Baustein-Ordner-DE.png)

![](attachments/webdav_de/WebDAV-Ordner-hochladen-DE.png)


## WebDAV-fähige OLAT-Ordner

Über WebDAV können Sie auf folgende OLAT-Ordner zugreifen:

  * [Persönlicher Ordner](../../manual_user/personal_menu/Personal_Tools.de.md#personlicher-ordner)
  * [Ordner von Gruppen](../../manual_user/groups/Using_Group_Tools.de.md)
  * [Ordner - Kurselement](../../manual_user/learningresources/Knowledge_Transfer.de.md#folder) (alle Kursmitglieder)
  * [Ablageordner von Kursen](../../manual_user/learningresources/Storage_folder.de.md) (nur Kursbesitzer)
  * [Ressourcenordner](../../manual_user/learningresources/index.de.md#ressourcenordner) (alle Mitglieder)

## Voraussetzungen

Microsoft Windows, Mac OSX, iOS, Android und Linux unterstützen WebDAV für
Drag and Drop Dateiübermittlung standardmässig. Inzwischen bieten auch diverse
Anwenderprogramme (z.B. Microsoft Office) WebDAV-Funktionalität an.

Um einen Ordner auf OLAT über WebDAV zu erreichen, benötigen Sie:

  * WebDAV-Link: WebDAV-Adresse des OLAT-Servers, wie sie unterhalb von WebDAV-fähigen Ordnern steht, oder im Home unter Einstellungen/WebDAV zu finden ist,
  * Ihren OLAT-Benutzernamen,
  * Ihr OLAT-/WebDAV-Passwort.

## WebDAV Passwort in OLAT setzen

Falls Sie mit SWITCH AAI-Konto auf OLAT zugreifen, können Sie sich Ihr WebDAV-
Passwort in den Einstellungen auf der Home-Seite einrichten. Wählen Sie hierzu
den Link "Einstellungen" und klicken anschliessend im Tab "WebDAV" auf die
Schaltfläche "Passwort einrichten". Wenn Sie bereits über ein OLAT-Passwort
verfügen, verwenden Sie dieses für den WebDAV-Zugang.

Für den WebDAV-Zugang benötigen Sie Ihren OLAT-Benutzernamen und das WebDAV-
Passwort. Ihren OLAT-Benutzernamen sehen Sie nach dem Einloggen unten links
("Eingeloggt als ....").

![](attachments/webdav_de/Webdav-DE.png)

  

## Einrichten der WebDAV-Verbindung

### Windows 7/8/10

  1. Klicken Sie im Startmenü auf "Computer".
  2. Klicken Sie im folgenden Fenster in der Menuleiste oben auf "Netzlaufwerk verbinden". Sollte dies nicht sichtbar sein, klicken Sie auf den Doppelpfeil in der Menüleiste, und dann auf "Netzlaufwerk verbinden".
  3. Wählen Sie einen Buchstaben für das Laufwerk.
  4. Wählen Sie ganz unten den Punkt "Verbindung mit einer Webseite herstellen auf der Sie Dokumente und Bilder speichern können" aus.
  5. Klicken Sie auf "Weiter".
  6. Markieren Sie "Eine benutzerdefinierte Webadresse auswählen".
  7. Klicken Sie auf "Weiter".
  8. Geben Sie den WebDAV-Link ein. (Für OLAT: `https://lms.uzh.ch/webdav`)
  9. Klicken Sie auf "Weiter".
  10. Geben Sie nun Ihren OLAT-Benutzernamen und Ihr Passwort ein.
  11. Sie können einen Namen für die WebDAV-Verbindung eingeben.
  12. Klicken Sie auf "Fertigstellen".

### Mac

  1. Öffnen Sie im Finder das Menu "Gehe zu" und dann "Mit Server verbinden." und geben dort den WebDAV-Link (für OLAT: `https://lms.uzh.ch/webdav`) ein.
  2. Geben Sie nun Ihren OLAT-Benutzernamen und Ihr Passwort ein.
  3. Klicken Sie auf "OK"  

### Linux

Für Linux-Benutzer gibt es drei Möglichkeiten:

  1. KDE: im Konqueror `webdavs:// + benutzername + @` \+ WebDAV-Link eingeben. Beispiel: `webdavs://pmuster@www.olat.uzh.ch/olat/webdav/`.
  2. Gnome: `davs:// + benutzername + @` \+ WebDAV-Link eingeben. Beispiel: `davs://pmuster@www.olat.uzh.ch/olat/webdav/`.
  3. FUSE: WebDAV-Verzeichnisse können direkt ins Filesystem gemountet werden (geht auch unter OSX, mehr dazu auf der [FUSE-Website](http://fuse.sourceforge.net/){:target="\_blank"}.

### 3rd Party Software

[Cyberduck](https://cyberduck.io/){:target="\_blank"} oder [Filezilla](https://filezilla-
project.org/){:target="\_blank"} sind open source Tools die auch für WebDAV genutzt werden
können.

## Ordnerstruktur

Wenn Sie die Verbindung erfolgreich eingerichtet haben, öffnet sich auf Ihrem
Rechner ein Verzeichnis, das die folgenden Unterverzeichnisse enthält:

  *  **coursefolders** : Ablageordner und Ordnerelement aller Kurse, die Sie besitzen bzw. deren Mitglied sie sind. Ablageordner sehen in der Regel nur OLAT-Benutzer mit Autorenrechten. Alle anderen Benutzer finden hier Kursordner.
  *  **groupfolders** : Alle Gruppen, in denen Sie eingetragen sind und auf deren Ordner Sie Zugriff haben.
  *  **home** : Ihre beiden persönlichen Ordner (mit den Unterordnern "private" und "public").
  *  **sharedfolders** : Alle Ressourcenordner, die Sie besitzen, oder auf die Sie aufgrund von Teilnehmerrechten zugreifen dürfen. Besitzer und Betreuer erhalten Lese- und Schreibrechte, Teilnehmer nur Leserechte


![](attachments/webdav_de/WebDAV-Mac.png)

## Bekannte Fehler mit WebDAV

  * Finder auf Mac gibt einen Error Code 43 aus bei Umlauten in Datei und Ordnernamen (lassen sich dann nicht mehr löschen - Ordner oder Datei muss via OLAT gelöscht oder umbenennt werden)
