# Supporting technologies

This chapter contains instructions on various technologies that are helpful
when working with OLAT. The instructions are primarily intended for OLAT users
who use OLAT intensively and are experienced authors.

You can find here instructions for the use of WebDAV, which
makes it easier to transfer files from the local computer to OLAT folders and on how to integrate ZOOM video conferences in OLAT.

  * [WebDAV](webdav.md)
  * [Zoom video conferences](zoom_video_conferences.md)
