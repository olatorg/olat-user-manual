# WebDAV

WebDAV stands for "Web-based Distributed Authoring and Versioning" and is an
open standard for transferring files on the internet. OLAT supports this
protocol and thus enables an easy file transfer from your computer to OLAT
folders.  

## Advantages of WebDAV

Without WebDAV files can only be uploaded to OLAT via conventional upload
forms. You select each file individually.

With WebDAV, on the other hand, you can easily copy individual files as well
as entire directories from your computer into OLAT folders using "drag and
drop". With WebDAV you have access to all folders of your courses.

## Upload files without WebDAV

You can select a single file via the "Upload file" function.

![](attachments/webdav/WebDAV-element-folder-EN.png)

![](attachments/webdav/WebDAV-upload-file-EN.png)

## WebDAV-enabled OLAT folders

You can access the following OLAT folders via WebDAV:

  * [Personal folder](../../manual_user/personal_menu/Personal_Tools.md#personal-folder)
  * [Folder of groups](../../manual_user/groups/Using_Group_Tools.md)
  * [Folder](../../manual_user/learningresources/Knowledge_Transfer.md#folder) \- course element (all course members)
  * [Folder of courses](../../manual_user/learningresources/Storage_folder.md) (course owners only)
  * [Resource Folder](../../manual_user/learningresources/index.md#resource-folder) (all members)

## Requirements

Microsoft Windows, Mac OSX, iOS, Android and Linux support WebDAV for drag and
drop file transfer by default. In the meantime, various application programs
(e.g. Microsoft Office) also offer WebDAV functionality.

To access a folder on OLAT via WebDAV you need:

  * WebDAV link: WebDAV address of the OLAT server as it can be found below WebDAV-enabled folders or in the Home under Settings/WebDAV,
  * your OLAT username,
  * your OLAT/WebDAV password.

## Set WebDAV password in OLAT

If you access OLAT with a SWITCH AAI account, you can set your WebDAV password
in the settings on the Home page. To do so, select the link "Settings" and
then click on the button "Set password" in the tab "WebDAV". If you already
have an OLAT password, use it for WebDAV access.

For WebDAV access you need your OLAT username and the WebDAV password. You
will see your OLAT username at the bottom left after logging in ("Logged in as
....").

![](attachments/webdav/Webdav-EN.png)

## Setting up the WebDAV connection

### Windows 7/8/10

  1. Click on "Computer" in the start menu.
  2. In the following window, click on "Connect network drive" in the menu bar at the top. If this is not visible, click on the double arrow in the menu bar and then on "Connect network drive".
  3. Select a letter for the drive.
  4. At the bottom, select the item "Connect to a website where you can save documents and pictures".
  5. Click on "Next".
  6. Select "Select a custom web address".
  7. Click on "Next".
  8. Enter the WebDAV link. (For OLAT: `https://lms.uzh.ch/webdav`)
  9. Click on "Next".
  10. Now enter your OLAT username and password.
  11. You can enter a name for the WebDAV connection.
  12. Click on "Finish".

### Mac

  1. In the Finder open the menu "Go to" and then "Connect to server." and enter the WebDAV link (for OLAT: `https://lms.uzh.ch/webdav`).
  2. Now enter your OLAT username and password.
  3. Click on "OK".

### Linux

For Linux users there are three possibilities:

  1. KDE: in Konqueror, enter webdavs:// + username + @ + WebDAV link. Example: `webdavs://username@www.olat.uzh.ch/olat/webdav/.`
  2. Gnome: enter davs:// + username + @ + WebDAV link. Example: `davs://username@www.olat.uzh.ch/olat/webdav/.`
  3. FUSE: WebDAV directories can be mounted directly in the file system (also works under OSX, see the [FUSE-Website](http://fuse.sourceforge.net/ "FUSE-Website") for more information.

### 3rd party software

[Cyberduck](https://cyberduck.io/) or [Filezilla](https://filezilla-
project.org/) are open source tools that can also be used for WebDAV.

## Folder structure

Once you have successfully set up the connection, a directory will open on
your computer containing the following subdirectories:

  *  **coursefolders** : storage folder and folder element of all courses you own or are a member of. As a rule, only users with author rights can see storage folders. All other users will find coursefolders here.
  *  **groupfolders** : All groups in which you are registered and to whose folders you have access.
  *  **home** : Your two personal folders (with the subfolders "private" and "public").
  *  **sharedfolders** : All resource folders that you own or to which you have access due to participant rights. Owners and supervisors are given read and write rights, participants only read rights.


![](attachments/webdav/WebDAV-Mac.png)

## Known Issues with WebDAV

  * Finder on Mac gives error code 43 for umlauts in file and folder names (cannot be deleted anymore - please delete or rename via OLAT)

 