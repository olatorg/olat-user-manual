# Zoom video conferences

!!! info
    The following instructions apply to members of UZH. If you want to integrate Zoom into OLAT courses for other educational institutions, please contact the IT support of your institution.

!!! info ""
    For more information on how to use and benefit from ZOOM at UZH, please visit [the
    support page of the UZH's central IT
    department](https://www.zi.uzh.ch/en/support/audio-video/zoom.html){:target="\_blank"}.

## Register for Zoom, install and use the app

Please follow the instructions as described in the UZH central IT manual.

  1. Register for Zoom: <https://www.zi.uzh.ch/en/support/audio-video/zoom/registerinstall.html#Register_user_account_for_ZOOM>{:target="\_blank"}
  2. Install die Zoom app: <https://www.zi.uzh.ch/en/support/audio-video/zoom/registerinstall.html#Installing_the_ZOOM-App>{:target="\_blank"}
  3. Use the Zoom app: <https://www.zi.uzh.ch/en/support/audio-video/zoom/use.html>{:target="\_blank"}

## Set up Zoom Account

In order to integrate a Zoom videoconference into OLAT, you have to login at Zoom with your organizational identity.

  1. Go to <https://zoom.us/web/sso/login?en=signin#/sso>{:target="\_blank"}
  2. Complete the field "Company Domain": _**uzh**.zoom.ch_
  3. Click on "*Continue*"
  4. The login page of the University of Zurich opens. Log in with your short name or your e-mail address and your password (Active Directory password).

The account is now successfully created.

## Integration of Zoom in OLAT

There are 4 ways to publish a Zoom meeting or webinar in an OLAT course:

  1. Publishing of the zoom link in a [course element of the type "external site"](../../manual_user/learningresources/Knowledge_Transfer.md#external_page)
  2. Publishing of the zoom link in a [course element of the type "Single Page"](../../manual_user/learningresources/Course_Element_HTML_Page.md)
  3. Sending the zoom link by e-mail (via [course element of the type "e-mail"](../../manual_user/learningresources/Administration_and_Organisation.md#mail))
  4. **new since OLAT 5:** Integration with the [course element "Zoom"](../../manual_user/learningresources/Course_Element_Zoom.md)  

!!! warning "The integration of Zoom via the course element "LTI page" is no longer supported. Use the course element "Zoom" instead."


!!! info

    The maximum availability of Zoom cloud recordings is 60 days. Recordings that should be available for longer than 60 days (e.g. lecture recordings) have to be downloaded in the Zoom account within 60 days and can be published for permanent access via the Kaltura course element in OLAT.


!!! note

    If you are facing problems during the setup of the Zoom account with the login as member of the UZH, please contact the [IT Servicedesk of UZH](https://www.zi.uzh.ch/en/support.html){:target="\_blank"}.


More information on using Zoom in teaching is available at:

[![](attachments/zoom_video_conferences_de/teachingtool.png)](https://teachingtools.uzh.ch/en/tools/lehrveranstaltungen-mit-zoom){:target="\_blank"}
