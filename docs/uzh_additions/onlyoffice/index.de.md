# OnlyOffice

Mit dem OnlyOffice-Editor können Dokumente direkt in OLAT erstellt und durch einzelne oder kollaborativ durch mehrere Benutzende online bearbeitet werden. Unterstützt werden Dateiformate zur Textverarbeitung (.docx), Tabellenkalkulation (.xlsx) und Präsentationen (.pptx). Die Dokumente werden zur Bearbeitung in einem separaten OnlyOffice-Fenster/Tab geöffnet.  

In folgenden Kursbausteinen kann OnlyOffice für die Erstellung und Bearbeitung von Dokumenten verwendet werden:

* [Dokument](#kursbaustein-dokument)  
* [Ordner](#neues-dokument-erstellen)  
* [Aufgabe](#kursbaustein-aufgabe)  
* [Gruppenaufgabe](#kursbaustein-aufgabe)
* [Teilnehmer\*innen Ordner](#neues-dokument-erstellen) 

## Unterstützte Dokumententypen

Mit OnlyOffice können folgende Dateiformate erstellt und im Online-Editor kollaborativ bearbeitet werden:

* Word (.docx) 
* Excel (.xlsx) 
* PowerPoint (.pptx) 

Dateien dieser Formate, welche bereits in OLAT gespeichert sind oder von einer Benutzerin oder einem Benutzer neu hochgeladen werden, können ebenfalls mit dem OnlyOffice-Editor online bearbeitet werden.

## Besondere Hinweise

!!! info "Hinweis:"

    * Wenn das Fenster bzw. der Tab mit dem OnlyOffice-Editor geschlossen wird, dauert es bis zu 10 Sekunden, bis die Änderungen in OLAT angezeigt werden.
    * Bei der kollaborativen Bearbeitung eines Dokuments werden die Änderungen erst dann in OLAT aktualisiert, wenn alle kollaborativ Bearbeitenden ihr Fenster oder ihren Tab mit dem OnlyOffice-Editor geschlossen haben.
    * Die Anzahl gleichzeitig Bearbeitender ist auf 300 begrenzt.

## Anwendungsbeispiele von OnlyOffice in OLAT

Die nachfolgenden Beispiele zeigen, wo OnlyOffice in OLAT verfügbar ist: 

### Kursbaustein Dokument
Im Tab "Dokument" des Kursbausteins Dokument können Dateien der Formate .docx, .xlsx und .pptx erstellt werden.

![](attachments/KB_Dokument.png){class="lightbox thumbnail-xl border" alt="Neues Dokument erstellen im Kursbaustein Dokument"}

### Neues Dokument erstellen
Neu können überall dort in OLAT, wo Dokumente hochgeladen oder erstellt werden können, zusätzlich die Dateitypen .docx, .xlsx und .pptx gewählt werden: 

![](attachments/Neues_Dokument_erstellen.png){class="lightbox thumbnail-xl border" alt="Neues Dokument erstellen in den Kursbausteinen Ordner und Teilnehmer\*innen-Ordner"}

### Kursbaustein Aufgabe
In den Kursbausteinen "Aufgabe" und "Gruppenaufgabe" können neu die Aufgabenstellung(en) direkt in OLAT mit OnlyOffice erstellt werden. 

![](attachments/KB_Aufgabe-Aufgabe_erstellen.png){class="lightbox thumbnail-xl border" alt="Neue Dateiformate im Kursbaustein Aufgabe und Gruppenaufgabe"}

### Abgabe
Die Aufgaben-Dateien können als Dokumentenvorlage für die abzugebende Lösungen zur Verfügung gestellt werden. Die Teilnehmenden können das Aufgabendokument online bearbeiten und als Lösungsdokument einreichen.

![](attachments/KB_Aufgabe-Aufgabe_ist_Abgabe.png){class="lightbox thumbnail-xl border" alt="Aufgabendokument als Lösungsvorlage zur Verfügung stellen"}
