# OnlyOffice

With the OnlyOffice editor, documents can be created directly in OLAT and edited online by individual users or collaboratively by several users. File formats for word processing (.docx), spreadsheets (.xlsx) and presentations (.pptx) are supported. The documents are opened in a separate OnlyOffice window/tab for editing.  

OnlyOffice can be used to create and edit documents in the following course elements:

* [Document](#course-element-document)  
* [Folder](#create-new-document)  
* [Task](#course-element-task)  
* [Group task](#course-element-task)
* [Participant folder](#create-new-document) 

## Supported document types

With OnlyOffice, the following file formats can be created and collaboratively edited in the online editor

* Word (.docx) 
* Excel (.xlsx) 
* PowerPoint (.pptx) 

Files in these formats that are already saved in OLAT or are newly uploaded by a user can also be edited online with the OnlyOffice editor.

## Special notes

!!! info "Note:"

    * When the window or tab with the OnlyOffice editor is closed, it takes up to 10 seconds for the changes to be displayed in OLAT.
    * When collaboratively editing a document, the changes are only updated in OLAT when all collaborative editors have closed their window or tab with the OnlyOffice editor.
    * The number of simultaneous users is limited to 300.

## Application examples of OnlyOffice in OLAT

The following examples show where OnlyOffice is available in OLAT: 

### Course element Document
In the tab "Document" of the course element Document files of the formats .docx, .xlsx and .pptx can be created.

![](attachments/KB_Dokument.png){class="lightbox thumbnail-xl border" alt="Create new document in the course element Document"}

### Create new document
The file types .docx, .xlsx and .pptx can now also be selected wherever documents can be uploaded or created in OLAT: 

![](attachments/Create_new_document.png){class="lightbox thumbnail-xl border" alt="Create new document in the course elements Folder and Participants\*folder"}

### Course element Task
In the course elements "Assignment" and "Group assignment" the assignment(s) can now be created directly in OLAT with OnlyOffice. 

![](attachments/KB_Aufgabe-Aufgabe_erstellen.png){class="lightbox thumbnail-xl border" alt="New file formats in the course element Task and Group Task"}

### Submission
The task document can be used as a template for the solution. Participants can edit the task document online and submit it as a solution document.

![](attachments/KB_Aufgabe-Aufgabe_ist_Abgabe.png){class="lightbox thumbnail-xl border" alt="Provide task document as solution template"}