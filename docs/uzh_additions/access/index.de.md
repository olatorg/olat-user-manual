# Kursbaustein "Programmieren in ACCESS"

ACCESS ist eine Software, die vom Institut für Informatik der Universität Zürich angeboten wird. ACCESS bietet die Möglichkeit, Programmierkenntnisse in einer betreuten Kursumgebung sowohl zu erwerben als auch zu unterrichten. 

Der Kursbaustein "Programmieren in ACCESS" bietet eine komfortable Verlinkung zu den Programmieraufgaben in ACCESS. Die Aufgaben selbst werden dann direkt in [ACCESS](https://access.ifi.uzh.ch/){ target=:blank } gelöst.

!!! Info "Bei Fragen zu ACCESS wenden Sie sich bitte an [https://access.ifi.uzh.ch/contact](https://access.ifi.uzh.ch/contact){ target=:blank }."

## Im Kurseditor 
Im Kurseditor können die Tabs für die zentralen Einstellungen des Bausteins verwendet werden.  

### Tab "Titel und Beschreibung" 
Hier können Sie einen Titel und eine Beschreibung des Kursbausteins eingeben. 

!!! Info ""
    Weitere Informationen finden Sie im Kapitel ["Titel und Beschreibung"](https://docs.olat.uzh.ch/de/manual_user/learningresources/General_Configuration_of_Course_Elements/#desc){ target=:blank}. 

### Tab "Layout" 
Hier können Sie die Layout-Optionen ändern. 

### Tab "Sichtbarkeit" 
Die Sichtbarkeit eines Kursbausteins zu ändern ist eine von drei Arten, um den Zugriff auf Kursbausteine einzuschränken. Wenn Sie die Sichtbarkeit eines Kursbausteins einschränken, erscheint dieser nicht mehr in der Kursnavigation. 

!!! Info ""
    Weitere Informationen finden Sie im Kapitel ["Sichtbarkeit"](https://docs.olat.uzh.ch/de/manual_user/learningresources/General_Configuration_of_Course_Elements/#access). 

### Tab "Zugang" 
Im Tab "Zugang" des Kursbausteins können Sie definieren, wer Zugang zu diesem Baustein hat. 

!!! Info ""
    Weitere Informationen finden Sie im Kapitel ["Zugang"](https://docs.olat.uzh.ch/de/manual_user/learningresources/General_Configuration_of_Course_Elements/#access). 

### Tab "Lernpfad" 
In Ihrem [Lernpfadkurs](https://docs.olat.uzh.ch/de/manual_user/learningresources/Learning_path_course_-_Course_editor/) definieren Sie im Tab "Lernpfad", ob die Bearbeitung des Kursbausteins obligatorisch oder freiwillig ist. Ferner können Sie einen Zeitraum festlegen, in dem der Kursbaustein bearbeitet werden kann.

Jeder Kursbaustein verfügt über individuelle Erledigungskriterien, eine Übersicht finden Sie im Kapitel ["Lernpfad"](https://docs.olat.uzh.ch/de/manual_user/learningresources/Learning_path_course/). 

!!! Info ""
    Weitere Informationen finden Sie im Kapitel ["Lernpfad"](https://docs.olat.uzh.ch/de/manual_user/learningresources/Learning_path_course/). 

!!! Note "Hinweis:"
    Herkömmliche Kurse verfügen nicht über den Tab "Lernpfad" und haben stattdessen die Tabs ["Sichtbarkeit"](https://docs.olat.uzh.ch/de/manual_user/learningresources/General_Configuration_of_Course_Elements/#access) und ["Zugang"](https://docs.olat.uzh.ch/de/manual_user/learningresources/General_Configuration_of_Course_Elements/#access) inklusive [Expertenmodus](../../manual_user/learningresources/Access_Restrictions_in_the_Expert_Mode/).
 
### Tab "Konfiguration" 

![](attachments/ACCESS_DE_Kurseditor_Konfiguration.png){ class="lightbox" }

#### Host URL
URL zum entsprechenden ACCESS Host, z.B. 'https://host.test.ch' oder die komplette ACCESS Kurs-URL mit Kursname z.B. 'https://access.beispiel.uzh.ch/api/courses/beispielkurs'. Diese Werte finden Sie in [ACCESS](https://access.ifi.uzh.ch/){ target=:blank }. 

Bei der Eingabe der kompletten ACCESS Kurs URL wird beim Speichern der Kursname extrahiert und automatisch im Feld 'ACCESS Kursname' eingesetzt.

#### Kursname 
ACCESS Kursname, z.B. 'beispielkurs'. Der Name wird automatisch aus der kompletten Kurs URL extrahiert, falls diese bei "Host URL" angegeben wurde. Ansonsten finden Sie diesen Wert in [ACCESS](https://access.ifi.uzh.ch/){ target=:blank }. 

Falls die komplette Kurs URL in "Host URL" angeben wurde, muss der Kursname zwingend mit dem entsprechenden Abschnitt nach dem letzten Slash "/" in der URL übereinstimmen. 

#### API Token 
Token Wert für den entsprechenden ACCESS Kurs, z.B. 'jyvvur-7Bivdu-rukzoz'. Diesen Wert finden Sie in [ACCESS](https://access.ifi.uzh.ch/){ target=:blank }. 

#### Test ACCESS Aufruf 
Beim Klicken auf die Schaltfläche "Test ACCESS Aufruf mit den eingegebenen Werten" werden die Verknüpfung mit dem ACCESS Kurs überprüft. 

* Bei **Erfolg** erscheint: "Der Aufruf des ACCESS Systems war erfolgreich!" 
* Bei einem **Fehler** erscheint: "Der Aufruf des ACCESS Systems war nicht erfolgreich! Bitte prüfen Sie die Eingabe der Host URL, Kursname, Tokens und die Verfügbarkeit des ACCESS Systems." 

!!! Warning "Wichtiger Hinweis" 
    Um eine eindeutige Zuordnung zwischen Teilnehmer*innen eines OLAT und eines ACCESS Kurses sicherzustellen, kann ein ACCESS Kurs nur mit einem einzigen OLAT Kurs verknüpft werden. Wird derselbe ACCESS Kurs mit einem anderen OLAT Kurs verknüpft, wird eine entsprechende Fehlermeldung dargestellt. 

#### Automatische Synchronisierung

Für jeden ACCESS Kursbaustein kann eine automatische Teilnehmersynchronisierung ein- bzw. ausgeschaltet werden. Die Funktion entspricht der manuellen ausgelösten Synchronisierung, aber wird automatisch täglich ausgelöst (standardmässig um 23:00 Uhr). 
Für die automatische Teilnehmersynchronisierung muss ein Enddatum angegeben werden. Das Enddatum definiert, bis zu welchem Datum die Synchronisation aktiv ist. Der maximal einstellbare Zeitraum entspricht 6 Monate, das Enddatum kann aber jederzeit verlängert werden.  

## Im Kursrun

### Als Kursbetreuer\*in
Kursbetreuer\*innen sehen in dieser Ansicht zwei Tabs mit den Informationen zu den Teilnehmer\*innen und zu der Synchronisation.  

#### Tab "Teilnehmer" 
In diesem Tab werden die Teilnehmer*innen des Kurses angezeigt. 

![](attachments/ACCESS_DE_Kursrun_Betreuer_Tab-Teilnehmer.png){ class="lightbox" }

#### Tab "Synchronisation" 
Die Synchronisation kann manuell oder automatisch erfolgen. Die automatische Synchronisation kann in der Kurs-Konfiguration eingeschaltet werden.

![](attachments/ACCESS_DE_Kursrun_Betreuer_Tab-Synchronisation.png){ class="lightbox" }

* **Manuell:** Die Kursteilnehmer müssen mit ACCESS synchronisiert werden. Dafür steht der bzw. dem Administrator des Kurses der Button "OLAT-Kursteilnehmer mit ACCESS jetzt synchronisieren" zur Verfügung. Die Synchronisation wird sofort ausgeführt. 
* **Automatisch:** Falls die automatische Synchronisation eingeschaltet wurde, wird das letzte Synchronisationsdatum und das Enddatum der Synchronisation angezeigt. Die automatische Synchronisation kann für maximal 6 Monate aktiviert sein. Sie kann aber jederzeit, d.h. auch vor Ablauf dieser Frist um weitere 6 Monate verlängert werden. Dazu muss der Button "Dauer der Synchronisierung verlängern" gedrückt und ein neues Ablaufdatum definiert werden. 


### Als Teilnehmer*in 
Teilnehmer*innen sehen in dieser Ansicht die ihnen zugewiesenen ACCESS-Aufgaben und die Aufgabenzuteilungen.

![](attachments/ACCESS_DE_Kursrun_Teilnehmer_Aufgaben.png){ class="lightbox" }

OLAT stellt die entsprechenden Links (blau) zu ACCESS zur Verfügung, die Aufgaben werden aber direkt in ACCESS gelöst. Bei Klick auf einen Link wird ACCESS in einem neuen Tab geöffnet. Das OLAT Tab bleibt dabei bestehen. 
Auf der ACCESS Seite muss man sich einmalig (pro Session) über Switch edu-ID einloggen. 

In OLAT wird die höchste Punktzahl, die für die Aufgabe vergeben wird, dargestellt und die Anzahl der verbleibenden Versuche. Zum Aktualisieren der Aufgabenzuteilung, der höchsten Punktzahl und der Anzahl der verbleibenden Versuche in OLAT muss der entsprechende Kursbaustein im OLAT Kurs-Menu angeklickt werden. 

Unterschied zwischen "Aufgabenzuteilung" und "Aufgabe 'Task X' in ACCESS lösen":

* <b>Aufgabenzuteilung:</b> Link zu der zugeteilten Aufgabe in ACCESS. Eine Aufgabe ist eine Zusammenstellung aus einem oder mehreren Tasks. In ACCESS können die jeweiligen Tasks der Aufgabe gelöst werden. 
* <b>Aufgabe 'Task X' in ACCESS lösen:</b> Direkter Link zum Task X der zugeteilten Aufgabe. 


## Hilfe 

Bei Fragen zu ACCESS wenden Sie sich bitte an [https://access.ifi.uzh.ch/contact](https://access.ifi.uzh.ch/contact){ target=:blank }.
