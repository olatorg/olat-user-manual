# Course element "Programming in ACCESS"

ACCESS is a software offered by the Department of Informatics at the University of Zurich. ACCESS offers the possibility to both acquire and teach programming skills in a supervised course environment. 

The course element "Programming in ACCESS" provides a convenient link to the programming tasks in ACCESS. The tasks themselves are then solved directly in [ACCESS](https://access.ifi.uzh.ch/){ target=:blank }.

!!! Info "For questions about ACCESS, please contact [https://access.ifi.uzh.ch/contact](https://access.ifi.uzh.ch/contact){ target=:blank }."

## Course editor 
In the course editor, the tabs for the central settings of the course element are at your disposal. 

### Tab "Title and description" 
Here you can enter a title and a description of the course element. 

!!! Info ""
    For more information, see the ["Titel and Description"](https://docs.olat.uzh.ch/manual_user/learningresources/General_Configuration_of_Course_Elements/#desc){ target=:blank} chapter. 

### Tab "Layout" 
Here you can change the layout options.

### Tab "Visibility" 
Changing the visibility of a course element is one of three ways to restrict access to course elements. If you restrict the visibility of a course element, it will no longer appear in the course navigation.

!!! Info ""
    For more information, see the ["Visibility"](https://docs.olat.uzh.ch/manual_user/learningresources/General_Configuration_of_Course_Elements/#access) chapter. 

### Tab "Access" 
In the tab "Access" of the course element you can define who has access to this element. 

!!! Info ""
    For more information, see the ["Access"](https://docs.olat.uzh.ch/manual_user/learningresources/General_Configuration_of_Course_Elements/#access) chapter. 

### Tab "Learning Path" 
In case of a [learning path course](https://docs.olat.uzh.ch/manual_user/learningresources/Learning_path_course_-_Course_editor/), you can define in the tab "learning path" whether the execution of the course element is obligatory or voluntary. Furthermore, you can define a time period in which the course element can be processed. 

The "Learning Path" tab also defines which criterion must be met for the course element to be considered "completed". An overview can be found in the ["Learning path"](https://docs.olat.uzh.ch/manual_user/learningresources/Learning_path_course/)chapter. 

!!! Info ""
    For more information, see the ["Learning Path"](https://docs.olat.uzh.ch/manual_user/learningresources/Learning_path_course/) chapter. 

!!! Note
    Conventional courses do not have the "Lernpfad" tab.Instead they have the ["Visibility"](https://docs.olat.uzh.ch/manual_user/learningresources/General_Configuration_of_Course_Elements/#access) and ["Access"](https://docs.olat.uzh.ch/manual_user/learningresources/General_Configuration_of_Course_Elements/#access) tabs including [expert mode](../../manual_user/learningresources/Access_Restrictions_in_the_Expert_Mode/).
 
### Tab "Configuration" 

![](attachments/ACCESS_EN_Course-Editor_Configuration.png){ class="lightbox" }

#### Host URL
URL to the corresponding ACCESS host, e.g. 'https://access.example.uzh.ch' or the complete ACCESS course URL with course name, e.g. 'https://access.example.uzh.ch'/api/courses/example-course'. These values can be found in [ACCESS](https://access.ifi.uzh.ch/){ target=:blank }. 

When entering the complete ACCESS course URL, the course name will be extracted and automatically inserted in the 'ACCESS course name' field when saving.

#### Course name 
ACCESS Course name, e.g. 'example-course'. The name is automatically extracted from the complete course URL if it was specified in "Host URL". Otherwise, you can find this value in [ACCESS](https://access.ifi.uzh.ch/){ target=:blank }. 

If the complete course URL was specified in "Host URL", it is mandatory that the course name matches the corresponding section after the last slash "/" in the URL. 

#### API Token 
Token Value for the corresponding ACCESS course, e.g. 'jyvvur-7Bivdu-rukzoz'. You can find this value in [ACCESS](https://access.ifi.uzh.ch/){ target=:blank }. 

#### Test ACCESS Call 
When clicking the button "Test ACCESS call with the entered values" the link with the ACCESS course will be checked. 

* If **successful**, the following message is shown: "The call to the ACCESS system was successful!"  
* If an **error occurs**, the following message is shown: "The call to the ACCESS System was not successful! Please check the entry of the Host URl, Course Name, Tokens and the availability of the ACCESS System."

!!! Warning "Important note" 
    In order to ensure a unique assignment between participants of an OLAT and an ACCESS course, an ACCESS course can only be linked to a single OLAT course. If the same ACCESS course is linked to another OLAT course, a corresponding error message will be displayed.  

#### Automatic synchronization 
In order to ensure a unique assignment between participants of an OLAT and an ACCESS course, an ACCESS course can only be linked to a single OLAT course. If the same ACCESS course is linked to another OLAT course, a corresponding error message will be displayed. 

An expiration date must be specified for automatic participant synchronization. The expiration date defines until which date the synchronization is active. The maximum period that can be set corresponds to 6 months, but the end date can be extended at any time. 

## Course run  

### As course owner 
In this view, course owners see two tabs with information about participants and synchronization. 

#### Tab "Participants" 
This tab displays the participants of the course. 


![](attachments/ACCESS_EN_Course-Run_Coach_Tab-Participants.png){ class="lightbox" }

#### Tab "Synchronization" 
Synchronization can be done manually or automatically. Automatic synchronization can be switched on in the course configuration. 

![](attachments/ACCESS_DE_Kursrun_Betreuer_Tab-Synchronisation.png){ class="lightbox" }

* **Manually:** Course participants must be synchronized with ACCESS. For this purpose, the administrator of the course can use the button "Synchronize OLAT course participants with ACCESS now". The synchronization will be carried out immediately. 
* **Automatic:** If automatic synchronization has been enabled, the last synchronization date and the end date of synchronization are displayed. Automatic synchronization can be activated for a maximum of 6 months. However, it can be extended for another 6 months at any time, i.e., even before this period expires. To do this, the "Extend duration of synchronization" button must be clicked, and a new expiration date defined. 


### As participant 
Participants will see their assigned ACCESS tasks and task assignments in this view. 

![](attachments/ACCESS_EN_Course-Run_Participants_Tasks.png){ class="lightbox" }

OLAT provides the corresponding links (blue) to ACCESS, however the tasks are solved directly in ACCESS. Clicking on a link will open ACCESS in a new tab. The OLAT tab will remain open. 
On the ACCESS page, you must log in once (per session) via Switch edu-ID. 

In OLAT, the highest score assigned for the task will be displayed as well as the number of remaining attempts. To update the task assignment, the highest score and the number of remaining attempts in OLAT you must click on the corresponding course element in the OLAT course menu. 

Difference between "Task assignment" and "Solve task 'Task X' in ACCESS": 

* <b>Task assignment:</b> link to the assigned task in ACCESS. An assignment is a set of one or more tasks. In ACCESS, the respective tasks of the task can be solved. 
* <b>Solve task 'Task X' in ACCESS:</b> Direct link to Task X of the assigned task. 


## Help 

If you have any questions about ACCESS, please contact [https://access.ifi.uzh.ch/contact](https://access.ifi.uzh.ch/contact){ target=:blank }.
