# UZH Additions

OLAT is based on OpenOlat, therefore you can find a large part of the OpenOlat documentation under the tab [OpenOlat](../../manual_user/). The additions and customizations made by the University of Zurich are listed here.

## Registration and login, Campus courses

* [Registration, login and logout](registration/)
[//]: # ( * [Semester checklist for UZH lecturers](semesterchecklist_for_lecturers/) )
* [Campus courses (UZH only)](campus_course/)
* [Delegation](delegation/)

## UZH course elements and tools

* [Entry control](entry_control/)
* [Kaltura](kaltura/)
* [Literature](literature/)
* [OnlyOffice](onlyoffice/)
* [Peer Review](peer_review/)
* [Programming in ACCESS](access/)
* [Supporting technologies](supporting_technologies/)

## Course administration and course access

* [FAQs Courses](faqs_courses/)
* [Access restrictions in expert mode](access_restrictions_in_expert_mode/)

## Examples of use

* [Examples of use](example_of_use)


