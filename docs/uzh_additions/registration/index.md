# Registration, login and logout

You can view OLAT as a guest, but the functionalities are rather limited.  
When you register, you have access to your personal courses and other content.

* [Registration with OLAT](../registration/registration_with_a_switch_edu-id.md)

* [Registration for UZH members](../registration/registration_for_uzh_members.md)

* [Login to and logout from OLAT](../registration/login_and_logout_in_olat.md)
  

!!! info "Info"
    As a guest, you can use [guest access](../../manual_user/general/Guest_access.md) to access learning content that has been released for guests.

