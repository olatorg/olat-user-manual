# Registration with a institutional account (not UZH)

## Registration as a member of other educational institutions

You will receive the access data for OLAT directly from your educational institution (college, university, university hospital, technical college, ...). To register as an OLAT user, you need your "AAI" user name and the corresponding password. If you have this access data, please proceed as follows:

  1. go to the website [lms.uzh.ch](http://lms.uzh.ch/){target=_blank}
  2. select your educational institution (e.g. Uni Luzern, Uni Basel, ...) in the menu  
![](attachments/registration_with_a_institutional_account_not_uzh/EN-Login.png)

  3. click on "Login"
  4. now enter the access data you received from your educational institution
  5. click on "Login"
  6. follow the further steps in the registration process

