# Registration with OLAT

  * [Creating of the Switch edu-ID and registration of your OLAT account](#creating)
  * [Linking of an educational institution in your Switch edu-ID account](#linking)
  * [Registration of your OLAT account](#registration)
  * [Questions about the registration](#faq)

* * *

To create an OLAT account and to access [https://lms.uzh.ch](https://lms.uzh.ch){target=_blank} you need a Switch edu-ID account. Members (registered students and employees) of an affiliated educational institution additionally have to link their institution in the Switch edu-ID account.

* __If you don't have a Switch edu-ID account yet,__ proceede with [_step 1._](#creating)
* __If you allready have a Switch edu-ID account and are *enrolled or employed* at an affiliated institution,__ procede with [_step 2._](#linking)<br />
All affiliated institutions are listed on the __dropdownlist of organisations on the [OLAT login page](https://lms.uzh.ch){target="\_blank"}.__  
* __If you allready have a Switch edu-ID account and are *not* enrolled at or employed by one of the organisations on the list,__ continue with [_step 3._](#registration)

<br />

## 1. Creating of the Switch edu-ID account {: #creating}

* Go to [www.eduid.ch](https://eduid.ch/){ target=_blank } and choose __"Create account".__<br /><br />
    ![](../registration/attachments/registration_with_a_switch_edu-id/eduid-ch_landingpage_EN.png){ class="lightbox" }
    <br /><br />
    Follow the instructions on the Switch registration page.<br /><br />
    To login at OLAT with Switch edu-ID a __Multi-Factor-Authentication (MFA) is needed.__ Set up a MFA in your [Switch edu-ID account](https://eduid.ch/account/security){target="\_blank"} under the tab "Security".<br />

    ??? Note "Multi-Factor-Authentication"

        You have the choice between the "Passkey" and "Authenticator App" methods:<br /><br />
        The Passkey method allows you to log in without a password (whether you can set up Passkey depends on the browser and operating system or system settings used; a smartphone or tablet is usually required). The Passkey method also allows you to use an external USB security key with a fingerprint sensor if you do not want to use a smartphone or tablet.<br /><br />
        &bull; We recommend the option with "Authenticator App" (smartphone/tablet is required). You can use any authenticator app (e.g. the Microsoft Authenticator App or the Google Authenticator App or another authenticator app from your app store).
        <br /><br />
        &bull; Multi-factor authentication with a code via SMS is still offered for exceptional cases, but is not recommended by Switch.
        __For members of UZH, multi-factor-authentication via SMS will no longer be possible from 16.12.2024.__
<br />

Once your Switch edu-ID account has been successfully activated, you receive a confirmation by e-mail.

* __If you are _enrolled at or employed by_ an educational institution,__ you must link your institution in your Switch edu-ID account. If this is the case for you, __go on to [step 2.](#linking)__<br />
All affiliated institutions are listed on the [dropdownlist of organisations on the OLAT login page](https://lms.uzh.ch){target="\_blank"}.<br />

* __Auditors, participants of a CAS or DAS continuing education course of UZH and other persons who are not a member of any of the affiliated institutions _do not have to link an institution in their Switch edu-ID account_.__<br />If this is the case for you, **proceed directly to [step 3.](#registration)**

<br />

## 2. Linking of an educational institution in your Switch edu-ID account {: #linking}
(For members of an affiliated educational institution only)

  * Call up the linking page provided by your organisation. For members of UZH this is the page [eduid.uzh.ch/linking](https://eduid.uzh.ch/linking){target=_blank}. 

  * If your organisation does not provide a linking page, you can link directly in your [Switch edu-ID account](https://eduid.ch/account/profile){target="_blank"}. (You can find further information on the linking of organisations on the [Switch edu-ID help page](https://eduid.ch/help#affiliations){target=_blank} to do so.)<br /><br />
  
## 3. Registration of your OLAT account {: #registration} 
 
  * Go to the OLAT login page [https://lms.uzh.ch](https://lms.uzh.ch){target=_blank}, select your institution from the drop-down list and go to "Login". Auditors and other persons that are neither employed by nor immatriculated at any of the institutions on the list choose "Switch edu-ID".

  * You will be redirected to the Switch edu-ID login page. After a successful login you will be redirected to the OLAT registration page.

  * Select the desired language, choose a user name (no uppercase letters allowed) and accept the terms of use.
  Your OLAT account is now ready, [start your first course](../../manual_user/area_modules/Courses.md#my-courses).


## Questions about the registration {: #faq}
??? question "What is the Switch edu-ID?"

    The [Switch edu-ID](https://www.switch.ch/en/edu-id){target="_blank"} is used by a large number of educational and research institutions in Switzerland to log in to their offerings and services, such as OLAT. Individuals who do not belong to an educational or research institution (e.g. auditors and participants of continuing education courses) can create a Switch edu-ID account as a private person and use it to log in to OLAT (Institution: "Switch edu-ID"). With a Switch edu-ID account you have a lifelong and secure identity for all academic services.

??? question "Where can I find more help on how to create a Switch edu-ID?"

    Here you can find Switch's instructions for creating a Switch edu-ID: [https://eduid.ch/help#account_management](https://eduid.ch/help#account_management){target=_blank}

??? question "Can I have multiple OLAT accounts?"

    It is possible to create a separate OLAT account for each institution for which you have received login data and which is listed in the drop-down list on the OLAT login page. An additional institution can be added in your Switch edu-ID account under "Organisations." In addition, you can create a private OLAT account by selecting "Switch edu-ID" as institution on the OLAT login page. Note: to set up a private OLAT account a private e-mail address is required in your Switch edu-ID account. 
    Your OLAT courses will only be visible in the OLAT account you have booked the course with.  
    An example: 

      * You are enrolled at university A. An OLAT course you attend at university A will only be visible if you have selected university A from the drop-down list when logging in and you have added university A as your organisation in your Switch edu-ID account.
      * An OLAT course you attend as an auditor at university B will only be visible if you log into OLAT as a private person (institution to be selected: Switch edu-ID).

<br />

<br />

<br />

<br />

<br />

<br />

<br />

<br />

<br />

<br />

<br />

<br />