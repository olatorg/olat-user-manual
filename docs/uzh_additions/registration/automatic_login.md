# Automatic Login

If your password is stored in your browser, OLAT will log you in automatically.  
There might be problems if the wrong [institution has been selected on OLAT's homepage](login_and_logout_in_olat.md). In this case the browser will redirect you to a wrong AAI login.  
Delete the browser cookies and the cache to solve the problem.

## Clear cache and cookies

Instructions for the browsers Google Chrome, Mozilla Firefox, Edge / Internet Explorer and Safari can be found at the following links:

  * [Clear cache and cookies settings](https://kb.iu.edu/d/ahic){target=_blank}.
  * While in your browser, press **Ctrl **+ ** Shift **+ ** Delete **simultaneously on the keyboard to open the appropriate window to clear your browser's cache and cookies.  

## Private browser window

To prevent the above problem, it might be helpful to open [OLAT in a private browser window](open_olat_in_private_browser_window.md).

