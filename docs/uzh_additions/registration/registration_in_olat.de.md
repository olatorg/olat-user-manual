# Registrierung bei OLAT

  * [Registrierung für UZH-Angehörige](registration_for_uzh_members.de.md)
  * [Registrierung mit einer Switch edu-ID](registration_with_a_switch_edu-id.de.md)
  * [Registrierung mit Institutionskonto (nicht UZH)](registration_with_a_institutional_account_not_uzh.de.md)

