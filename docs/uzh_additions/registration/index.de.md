# Registrierung, An- und Abmeldung

Sie können OLAT als Gast einsehen, aber die Funktionalitäten sind eher
beschränkt.  
Wenn Sie sich registrieren, haben Sie Zugriff auf Ihre persönlichen Kurse und
auf andere Inhalte.

* [Registrierung bei OLAT](../registration/registration_with_a_switch_edu-id.de.md)

* [Registrierung für Angehörige der UZH](../registration/registration_for_uzh_members.de.md)

* [An- und Abmeldung bei OLAT](../registration/login_and_logout_in_olat.de.md)

  
!!! info "Info"
    Als Gast können Sie mit dem [Gastzugang](../../manual_user/general/Guest_access.de.md) auf für Gäste freigegebene Lerninhalte zugreifen.
