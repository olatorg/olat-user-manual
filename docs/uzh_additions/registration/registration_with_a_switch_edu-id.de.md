# OLAT-Registrierung

  * [Erstellen eines Switch edu-ID-Kontos](#creating)
  * [Verlinkung der Bildungsinstitution im Switch edu-ID-Konto](#linking)
  * [Registrierung des OLAT-Kontos](#registration)
  * [Fragen zur Registrierung](#faq)

* * *

Für den Zugang zu [https://lms.uzh.ch](https://lms.uzh.ch){target=_blank} benötigen Sie ein Switch edu-ID-Konto. Angehörige (immatrikulierte Studierende und Angestellte) einer angebundenen Bildungsinstitution müssen ihre Institution im Switch edu-ID-Konto verlinken. 

* __Wenn Sie noch kein Switch edu-ID Konto haben,__ gehen Sie zu [_Schritt 1._](#creating)
* __Wenn Sie bereits ein Switch edu-ID Konto haben und an einer angebundenen Institution _immatrikuliert oder angestellt_ sind,__ fahren Sie direkt mit [_Schritt 2_](#linking) fort.<br />Eine Übersicht über die angebundenen Institutionen liefert die __Organisationsauswahl auf der [OLAT-Loginseite](https://lms.uzh.ch){target="\_blank"}.__
* __Wenn Sie bereits ein Switch edu-ID-Konto haben und bei *keiner* der Institutionen auf der Organisationsliste immatrikuliert oder angestellt sind,__ fahren Sie direkt mit [_Schritt 3_](#registration) fort.

<br />

## 1. Erstellen eines Switch edu-ID-Kontos {: #creating}

* Gehen Sie auf [www.eduid.ch](https://eduid.ch/){ target="_blank" } und wählen Sie __“Konto erstellen”.__ <br /><br />
    ![](../registration/attachments/registration_with_a_switch_edu-id_de/eduid-ch_landingpage_DE.png){ class="lightbox" }
    <br /><br />
    Folgen Sie den Anweisungen auf der Switch-Registrierungsseite.<br /><br />
    Für die Anmeldung bei OLAT mit Switch edu-ID ist eine __Mehrfaktor-Authentisierung (Multi-Faktor-Authentisierung) erforderlich.__ Richten Sie diese im [Switch edu-ID-Konto](https://eduid.ch/account/security){target="\_blank"} im Tab "Sicherheit" ein.<br />

    ??? Note "Multi-Faktor-Authentisierung"

        Sie haben die Wahl zwischen den Methoden "Passkey" und "Authenticator App":<br /><br />
        &bull; Die Methode mit Passkey ermöglicht ein Login ohne Passwort (ob Sie Passkey einrichten können ist abhängig vom verwendeten Browser und vom verwendeten Betriebssystem bzw. Systemeinstellungen, ein Smartphone oder Tablett wird in der Regel benötigt). Die Methode Passkey ermöglicht zusätzlich die Verwendung eines externen USB-Sicherheitsschlüssels mit Fingerabdrucksensor, falls Sie kein Smartphone oder Tablet verwenden möchten.<br /><br />
        &bull; Wir empfehlen die Option mit "Authenticator App" (Smartphone/Tablet wird benötigt). Sie können eine beliebige Authenticator App verwenden (z.B. die Microsoft Authenticator App oder die Google Authenticator App oder eine andere Authenticator App aus Ihrem App-Store).<br /><br />
        &bull; Eine Multi-Faktor-Authentisierung mit Code per SMS wird für Ausnahmefälle noch angeboten, wird aber von Switch nicht empfohlen. 
        __Für Angehörige der UZH ist ab 16.12.2024 die Multi-Faktor-Authentisierung via SMS nicht mehr möglich.__
<br />

Sobald die Registrierung des Switch edu-ID Kontos abgeschlossen ist, werden Sie per E-Mail darüber informiert.

* __Falls Sie an einer angebundenen Bildungsinstitution _immatrikuliert oder angestellt_ sind,__ müssen Sie Ihre Institution in Ihrem Switch edu-ID-Konto verlinken. __Gehen Sie in diesem Fall weiter zu [_Schritt 2._](#linking)__<br />
Eine Übersicht über die angebundenen Institutionen liefert die __<a href="https://lms.uzh.ch" target="_blank">Organisationsauswahl auf der OLAT-Loginseite</a>.__

* __Auditor*innen/Gasthörende sowie Teilnehmende eines CAS oder DAS Weiterbildungs&shy;studiengangs der UZH und andere Personen, welche nicht Mitglied einer angebundenen Institution sind,__ müssen __keine__ Institution im Switch edu-ID Konto verlinken.<br />__Fahren Sie direkt mit [_Schritt 3_](#registration) fort.__
        
<br />

## 2. Verlinken der Bildungsinstitution im Switch edu-ID-Konto {: #linking}
(nur für Angehörige einer angebundenen Bildungsinstitution)

  * Rufen Sie die von Ihrer Organisation zur Verfügung gestellte Verlinkungsseite auf. Für Angehörige der UZH ist dies die Seite [eduid.uzh.ch/linking](https://eduid.uzh.ch/linking){target=_blank}.
  
  * Falls Ihre Organisation keine Verlinkungsseite zur Verfügung stellt, können Sie die Verlinkung direkt in Ihrem [Switch edu-ID-Konto](https://eduid.ch/account/profile){target="_blank"} vornehmen. (Weitere Informationen zur Verlinkung von Organisationen finden Sie auf der [Switch edu-ID Hilfe-Seite](https://eduid.ch/help#affiliations){target=_blank}.)
  
  <br />

## 3. Erstellen eines OLAT-Kontos {: #registration}
  
  * Gehen Sie auf [https://lms.uzh.ch](https://lms.uzh.ch){target=_blank}, wählen Sie Ihre Institution in der Dropdownliste aus und gehen Sie auf "Login". Gasthörende/Auditor*innen und andere Personen, welche bei keiner der Institutionen auf der Liste immatrikuliert oder angestellt sind, wählen "Switch edu-ID".

  * Sie werden zur Anmeldeseite von Switch edu-ID weitergeleitet. Nach erfolgreichem Login werden Sie zur Registrierungsseite von OLAT zurückgeleitet.
  
  * Wählen Sie die gewünschte Sprache aus, wählen Sie einen Benutzernamen (keine Grossbuchstaben) und akzeptieren Sie die Nutzungsbedingungen.

  Ihr OLAT-Konto ist nun bereit, [starten Sie Ihren ersten Kurs](../../manual_user/area_modules/Courses.de.md#meine-kurse).

<br />

## Fragen zur Registrierung {: #faq}
??? question "Was ist die Switch edu-ID?"

    Die [Switch edu-ID](https://www.switch.ch/de/edu-id){target="_blank"} wird von einer grossen Zahl von Bildungs- und Forschungsinstitutionen in der Schweiz für das Login zu ihren Angeboten und Diensten, wie z.B. OLAT, eingesetzt. Personen, die keiner Bildungs- oder Forschungs&shy;institution angehören (z.B. Auditor*innen und Teilnehmende von Weiterbildungsangeboten), können als Privatperson ein Switch edu-ID Konto erstellen und sich damit in OLAT anmelden (Institution: "Switch edu-ID"). Mit einem Switch edu-ID Konto haben Sie eine lebenslange und sichere Identität für alle akademischen Dienste. 

??? question "Wo finde ich weitere Hilfe zur Erstellung einer Switch edu-ID?"

    Hier finden Sie die Anleitung von Switch zum Erstellen einer Switch edu-ID: [https://eduid.ch/help#account_management](https://eduid.ch/help#account_management){target=_blank}

??? question "Kann ich mehrere OLAT-Konten haben?"

    Es ist möglich, für jede Institution, zu welcher Sie Logindaten erhalten haben und die in der Dropdownliste auf der OLAT-Loginseite aufgeführt ist, ein separates OLAT-Konto zu erstellen. Eine zusätzliche Institution müssen Sie in Ihrem Switch edu-ID Konto unter "Organisationen" hinzufügen. Zusätzlich können Sie ein privates OLAT-Konto erstellen, indem Sie auf der Loginseite von OLAT als Institution "Switch edu-ID" auswählen. Dazu muss in Ihrem Switch edu-ID Konto eine private E-Mail Adresse hinterlegt sein.
    Ihre OLAT-Kurse sind jeweils nur in jenem OLAT-Konto sichtbar, mit welchem Sie den Kurs gebucht haben.  
    Ein Beispiel:  

      * Sie sind an Hochschule A immatrikuliert. Ein OLAT-Kurs den Sie an Hochschule A besuchen ist nur in Ihrem OLAT-Konto sichtbar, wenn Sie Hochschule A beim Login aus der Dropdownliste ausgewählt haben und Sie diese Hochschule im Switch edu-ID Konto als Institution hinzugefügt haben. 
      * Ein OLAT-Kurs, den Sie an Hochschule B als Auditor oder Auditorin besuchen, ist nur sichtbar, wenn Sie sich als Privatperson in OLAT einloggen (auszuwählende Institution: Switch edu-ID).

<br />

<br />

<br />

<br />

<br />

<br />

<br />

<br />

<br />

<br />