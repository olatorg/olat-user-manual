# Registration in OLAT

  * [Registration for UZH members](registration_for_uzh_members.md)
  * [Registration with a Switch edu-ID](registration_with_a_switch_edu-id.md)
  * [Registration with a institutional account (not UZH)](registration_with_a_institutional_account_not_uzh.md)
