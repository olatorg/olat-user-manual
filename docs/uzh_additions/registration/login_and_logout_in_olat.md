# Logging in and out of OLAT

* [Log in at OLAT as a user ](#login)
* [Log out of OLAT as a user](#logout)
* [Reset the Switch edu-ID password](#reset)

## Logging in to OLAT as a user {: #login}

  1. go to the OLAT login page [lms.uzh.ch](https://lms.uzh.ch){target=_blank}
  
  2. select your educational institution and click on "Login".
  If you do not belong to any of the institutions in the drop-down list, select "Switch edu-ID" at the very end of the list (auditors of the UZH choose "Switch edu-ID" as well):

  3. you will be redirected to Switch's login page: 
  Enter your Switch login data (Switch edu-ID or SWITCHaai as indicated). Members of the UZH (enrolled students and employees of the UZH), have to enter the login data of their Switch edu-ID).

!!! Info "Multi-factor Authentication (MFA)"
    For logging into OLAT with Switch edu-ID a multi-factor authentication is required (entering an additional security code). Instructions on how to set up the multi-factor authentication for the Switch edu-ID login can be found on [Switch's help page](https://eduid.ch/help#two-step-login){target=_blank}.
  
After having logged in successfully the page with your courses marked as favorites will be displayed. When you log in for the first time this page is empty. Switch to the **menu item "Active "** to display courses where you are allready a member or switch to the **menu item "Search course "** to search OLAT for courses.

??? question "Which institution do I have to select from the dropdown list on the OLAT login page?"
    As a rule, select your **home institution** from the drop-down list, i.e. the institution at which you are enrolled or employed and which you have linked in the Switch edu-ID account.<br />
    If you are attending an OLAT course **as a private person**, which also includes **auditors, guest auditors and participants in MAS/DAS/CAS further education courses**, select "Switch edu-ID" as the institution.<br />

## Log out of OLAT as a user {: #logout}

You can log out of OLAT at any time by clicking on the user image (avatar) at the top right. This will open the personal user menu where you can find the "Log out" link which takes you back to the login page.

If you do not log out, but the browser window or browser is closed, your session continues until the session timeout.

![](attachments/login_and_logout_in_olat/logout_01_en.png)
![](attachments/login_and_logout_in_olat/logout_02_en.png)

!!! warning "Attention"

    If you want to use OLAT in public places it is important to log off of OLAT; to only close your browser window is not enough. As long as your session is active another person could use your OLAT login data at that computer.

!!! info "Session-Timeout"

    OLAT manages a session for every logged-in user. With every click (for example calling up a question in a test) the session time of the session starts again.
    If you do not click in OLAT within the session time, the session will expire.  
    The session timeout is not indicated. When the session time has expired, you have to log in again. All unsaved data will be lost. Therefore, it is recommendet to save your work regularly.

## Reset the Switch edu-ID password {: #reset}

If you have forgotten your Switch edu-ID password or would like to change your existing password go to <https://eduid.ch/web/reset-password/>{target=_blank} and follow the onscreen instructions.