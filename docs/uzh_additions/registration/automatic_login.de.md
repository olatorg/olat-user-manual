# Automatischer Login

Sollte Ihr Passwort im Browser gespeichert sein, wird OLAT Sie automatisch anmelden.  
Probleme kann es geben, wenn die falsche [Institution auf der Startseite von OLAT](login_and_logout_in_olat.de.md) gewählt wurde, so leitet der Browser Sie zu einem falschen AAI Login weiter.  
Browser Cookies und den Cache zu löschen löst das Problem.

## Cache und Cookies löschen

Anleitungen für die Browser Google Chrome, Mozilla Firefox, Edge / Internet
Explorer und Safari finden Sie unter folgenden Links:

  * Wenn Sie im Browser sind, drücken Sie **Ctrl **+ ** Shift **+ ** Delete **gleichzeitig auf der Tastatur um zum entsprechenden Menü zu gelangen. Siehe auch: [Cache und Cookies löschen Shortcuts.](https://www.cloudweb.ch/browser-cache-leeren/){target=_blank}
  * [Cache und Cookies löschen via Einstellungen](https://www.iqual.ch/de/internet-glossar/anleitung-cache-leeren-verschiedenen-browsern){target=_blank}.

## Privates Browserfenster

Um obigem Problem vorzubeugen, kann es helfen, [OLAT in einem privaten Browserfenster](open_olat_in_private_browser_window.de.md) aufzurufen.
