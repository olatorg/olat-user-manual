# Registrierung mit Institutionskonto (nicht UZH)

Sie erhalten die Zugangsdaten für OLAT direkt von Ihrer Bildungseinrichtung
(Hochschule, Universität, Universitätsspital, Fachhochschule usw.). Für die
Registrierung als OLAT-Nutzer*in benötigen Sie Ihren "AAI"-Benutzernamen und
das dazugehörige Passwort. Wenn Ihnen diese Zugangsdaten vorliegen, so gehen
Sie bitte wie folgt vor:

  1. Gehen Sie auf die Seite [lms.uzh.ch](http://lms.uzh.ch/){target=_blank}.
  2. Wählen Sie Ihre Bildungseinrichtung (z.B. Uni Luzern, Uni Basel usw.) im Menü aus:  
![](attachments/registration_with_a_institutional_account_not_uzh_de/Bildschirmfoto_2022-10-20_um_19.42.34.png)

  3. Klicken Sie auf Login.
  4. Geben Sie nun die Zugangsdaten ein, die Sie von Ihrer Bildungseinrichtung erhalten haben.
  5. Klicken Sie auf Anmelden resp. Login.
  6. Führen Sie die weiteren Registrierungsschritte durch.

Falls Sie Probleme beim Anmelden mit Ihren AAI-Zugangsdaten haben, wenden Sie
sich zuerst an die Institution, von welcher Sie die Zugangsdaten bekommen
haben.

