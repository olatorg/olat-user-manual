# OLAT in privatem Browserfenster öffnen

Sollte es bei OLAT zu Problemen mit den Cookies kommen (z.B. falsche Institution ausgewählt oder falscher Login erscheint, Autologin ist aktiv), kann es helfen, OLAT [in einem privaten Browserfenster zu öffnen](https://praxistipps.chip.de/anonym-und-inkognito-surfen-in-firefox-
chrome-oder-ie_1650){target=_blank}.  
Dadurch werden keine Cookies auf Ihrem Computer gespeichert.

## Cache und Cookies löschen

Bitte beachten Sie dazu den Artikel zum Thema automatischer Login und "[Cache und Cookies löschen](automatic_login.de.md)".
