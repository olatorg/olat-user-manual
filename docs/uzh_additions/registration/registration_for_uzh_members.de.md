# Registrierung für UZH-Angehörige

Angehörigen der UZH (Dozierenden, Studierenden, Angestellten) wird ein sogenanntes ITIM-Konto erstellt. Studierende erhalten bei Studienbeginn von der UZH-Verwaltung einen Passwortbrief und in ihrem Profil des [UZH Bewerbungsportals](http://studentservices.uzh.ch/uzh/application#/Logon){target="_blank"} eine Anleitung zum erstmaligen Setzen der Passwörter im [Identity Manager](https://identity.uzh.ch){target="_blank"} zugesandt.
Mitarbeitende (Dozierende und Angestellte) erhalten den Brief zu Arbeitsbeginn per Post zugestellt.

!!! info

    Weitere Informationen zum Ändern der UZH-Passwörter finden Sie unter <https://www.zi.uzh.ch/de/students/password-account/manage-password.html> und <https://www.zi.uzh.ch/de/support/identitaet-zugang/manage-password.html>.<br />
    Bei Problemen mit dem UZH-Login, wenden Sie sich bitte an die Kolleginnen und Kollegen vom [IT Service Desk](https://www.zi.uzh.ch/de/support.html){target=_blank}.

## Registrieren bei OLAT

  1. Falls Sie bereits ein Switch edu-ID Konto haben (z.B. als Benutzer*in der ZB Zürich), <br />fahren Sie direkt mit Punkt 2 weiter.<br />Falls Sie noch kein Switch edu-ID Konto haben, Gehen Sie auf [eduid.ch](https://eduid.ch){ target=_blank }, wählen Sie “Konto erstellen” und folgen Sie den Anweisungen auf der Seite. <br /><br />
  
  2. **Dozierende, Studierende und Mitarbeitende der UZH** müssen die UZH in Ihrem Switch edu-ID Konto verlinken. **Gehen Sie weiter zu Punkt 3.**<br />
  <br />
  **Auditoren/Gasthörende und Teilnehmende eines CAS oder DAS Weiterbildungsstudiengangs** müssen keine Institution verlinken. Gehen Sie auf die [OLAT-Loginseite](https://lms.uzh.ch){target=_blank}, wählen Sie "Switch edu-ID" als Organisation im Dropdown-Menü aus und gehen Sie auf Login. **Fahren Sie fort mit Punkt 5.**

  3. **(Für Dozierende, immatrikulierte Studierende und Mitarbeitende der UZH:)** Rufen Sie die Verlinkungsseite [eduid.uzh.ch/linking](https://eduid.uzh.ch/linking){target=_blank} auf und folgen Sie den angezeigten Anweisungen. Die Verlinkung kann auch direkt im edu-ID Konto gemacht werden gemäss folgender [Anleitung](https://eduid.ch/help#affiliations){target=_blank}).
  
  4. Gehen Sie auf die OLAT-Loginseite [https://lms.uzh.ch](https://lms.uzh.ch){target=_blank}, wählen Sie "Universität Zürich" als Organisation im Dropdown-Menü aus und gehen Sie auf Login. **Gasthörende/Auditoren wählen "Switch edu-ID".**

  5. Sie werden zur Anmeldeseite der Switch edu-ID weitergeleitet. Nach erfolgreichem Login werden Sie zur Registrierungsseite von OLAT zurückgeleitet.

  6. Wählen Sie dort die gewünschte Sprache aus, wählen Sie einen Benutzernamen (nur Kleinbuchstaben) und akzeptieren Sie die Nutzungsbedingungen.

!!! info

    Weitere Informationen zur Registrierung bei OLAT und zum Login mit Switch edu-ID Zugangsdaten finden Sie unter [OLAT-Registrierung](https://docs.olat.uzh.ch/de/uzh_additions/registration/registration_with_a_switch_edu-id/#faq) und auf der [Switch edu-ID FAQ Seite](https://www.zi.uzh.ch/en/support/identity-access/eduid-faq.html) der UZH.<br /><br />

    Sollten Sie Probleme mit Ihrem Switch edu-ID Passwort haben, können Sie das Switch edu-ID [Passwort zurücksetzen](https://eduid.ch/reset-password){target=_blank} oder gehen Sie auf die [Hilfe Seite von Switch](https://eduid.ch/help){target="_blank"} oder wenden Sie sich an den den [IT Service Desk](https://www.zi.uzh.ch/de/support.html){target=_blank}.<br /><br />
    
    Nach erfolgreicher Registrierung wird die vom Systemadministrator festgelegte Startseite angezeigt. Unter [Einstellungen](../../manual_user/personal_menu/Settings.de.md) können Sie anpassen, ob die Standard-Startseite, die zuletzt von Ihnen besuchte OLAT-Seite ("Sitzung wiederherstellen") oder eine von Ihnen frei wählbare URL angezeigt werden soll.
