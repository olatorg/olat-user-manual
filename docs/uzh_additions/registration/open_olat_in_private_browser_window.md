# OLAT in private browser window

If you encounter problems with cookies in OLAT (e.g. wrong institution selected or wrong login appears, autologin is active) it might help to open [OLAT in a private / incognito browser window](https://balsamiq.com/support/faqs/privatebrowsing/){target=_blank}.  
This way no cookies will be stored on your computer.

## Deleting Cache and Cookies

Please refer to the article on automatic login and "[Deleting cache and cookies](automatic_login.md)".
