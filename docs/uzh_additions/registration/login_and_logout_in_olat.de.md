# An- und Abmeldung bei OLAT

* [Als Benutzer bei OLAT anmelden (Login)](#login)
* [Als Benutzer bei OLAT ausloggen](#logout)
* [Switch edu-ID Passwort zurücksetzen](#reset)

## Als Benutzer bei OLAT anmelden \(Login\) {: #login}


  1. Gehen Sie auf die OLAT-Loginseite [lms.uzh.ch](https://lms.uzh.ch){target=_blank}
  
  2. Wählen Sie Ihre Ausbildungseinrichtung aus und klicken Sie auf "Login".
  Gehören Sie keiner der Institutionen in der Dropdown-Liste an, wählen Sie "Switch edu-ID" ganz am Ende der Liste aus (Auditor*innen und Gasthörende der UZH wählen ebenfalls "Switch edu-ID"):

  3. Sie werden zur Anmeldeseite von Switch weitergeleitet: 
  Geben Sie entsprechend den Angaben auf der Anmeldeseite entweder Ihre Switch edu-ID Logindaten bzw. Ihre SWITCHaai Logindaten ein. Angehörige der UZH (immatrikulierte Studierende und Angestellte der UZH) geben ihre Switch edu-ID Anmeldedaten ein.
  
!!! Info "Multi-Faktor-Authentisierung (MFA)"
    Für das Login in OLAT mit Switch edu-ID ist eine Multi-Faktor-Authentisierung (Eingabe eines zusätzlichen Sicherheitscodes) erforderlich. Die Anleitung dazu, wie Sie die Multi-Faktor-Authentisierung für das Switch edu-ID Login einrichten, finden Sie auf der [Hilfe-Seite von Switch](https://eduid.ch/help#two-step-login){target=_blank}
  
Nach erfolgreichem Login wird Ihnen die Seite mit Ihren als Favoriten markierten Kursen angezeigt. Beim ersten Login ist diese Seite leer. Wechseln Sie zum **Menüpunkt "Aktiv"** um Kurse anzuzeigen, in welchen Sie bereits Mitglied sind oder wechseln Sie zum **Menüpunkt "Kurs suchen"** um OLAT nach Kursen zu durchsuchen.

??? question "Welche Institution muss ich aus der Dropdownliste auf der OLAT-Loginseite auswählen?"
    In der Regel wählen Sie in der Dropdownliste Ihre **Heiminstitution** aus, also die Institution, an welcher Sie immatrikuliert oder angestellt sind und welche Sie im Switch edu-ID Konto verlinkt haben.<br />
    Falls Sie einen OLAT-Kurs **als Privatperson** besuchen, wozu auch **Auditorinnen, Gasthörer und Teilnehmende von DAS/CAS Weiterbildungsstudiengängen** zählen, wählen Sie als Institution "Switch edu-ID".<br />
    
## Als Benutzer bei OLAT ausloggen {: #logout}

Sie können sich jederzeit über den "Log out"-Link im Benutzermenü wieder aus OLAT ausloggen. Wählen Sie dazu Ihr Benutzerbild (Avatar) an, um das persönliche Benutzermenü zu öffnen und wählen Sie "Log out". So gelangen Sie zurück zur Login-Seite.

Wenn Sie sich nicht ausloggen, sondern das Browserfenster oder den Browser schliessen, läuft Ihre Session bis zum Session-Timeout weiter.

![](attachments/login_and_logout_in_olat/logout_01_de.png)
![](attachments/login_and_logout_in_olat/logout_02_de.png)


!!! warning "Achtung"

    Wenn Sie OLAT an öffentlichen Arbeitsplätzen verwenden, ist es wichtig, dass
    Sie sich nach der Arbeit mit OLAT ausloggen und nicht nur das Browserfenster
    schliessen. Solange Ihre Session läuft, könnte auf demselben Rechner eine
    andere Person mit Ihren Login-Daten in OLAT arbeiten.

!!! info "Session-Timeout"

    Für jeden eingeloggten Benutzer verwaltet OLAT eine Session. Bei jedem Klick
    (beispielsweise Aufrufen einer Frage in einem Test) beginnt die Session-Time
    der Session wieder neu zu laufen. Wenn Sie innerhalb der Session-Time keinen
    Klick in OLAT machen, läuft die Session ab.  
    Das Session-Timeout wird nicht signalisiert. Wenn die Session Time abgelaufen
    ist, müssen Sie sich erneut einloggen. Alle nicht gespeicherten Daten gehen
    dabei verloren. Speichern Sie deshalb Ihre Arbeit regelmässig.

## Switch edu-ID Passwort zurücksetzen {: #reset}

Wenn Sie Ihr Switch edu-ID-Passwort vergessen haben oder Ihr bestehendes Passwort ändern möchten, gehen Sie auf die Seite <https://eduid.ch/help#security>{target=_blank} und folgen Sie den angezeigten Anweisungen.
