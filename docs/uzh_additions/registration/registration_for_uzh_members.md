#  Registration for UZH members

A so-called ITIM account is created for members of the UZH (lecturers, students, employees etc.). Students receive a password letter from the UZH administration and instructions on how to set their passwords in the Security Identity Manager for the first time in their profile on the [UZH application portal](http://studentservices.uzh.ch/uzh/application#/Logon).
Employees and Lecturers receive that information prior to their first working day by mail.

!!! info 

    Further informatin on how to change your UZH passwords can be found under <https://www.zi.uzh.ch/en/support/identity-access/set-password.html> and on <https://www.zi.uzh.ch/en/support/identity-access/manage-password.html>.<br /><br />
    If you are facing problems in logging in with your UZH login data, please contact our colleagues from the [IT service desk](https://www.zi.uzh.ch/de/support.html){target=_blank}.

## Registration at OLAT

  1. If you already have a Switch edu-ID account (e.g. as user of the ZB Zurich), continue directly with point 2.<br />
  If you do not yet have a Switch edu-ID account, go to [eduid.ch](https://eduid.ch){ target=_blank }, select "Create account" and follow the instructions on the page.<br /><br />

  2. **Lecturers, students and employees of UZH** must link the UZH in their Switch edu-ID account.<br />**Go on with point 3.**<br /><br />
  **Auditors and participants of CAS and DAS further education programmes** don't need to link an institution. They go the OLAT login page [lms.uzh.ch](http://lms.uzh.ch/){target=_blank}, select "Switch edu-ID" from the dropdown list and choose "login". **Then go on with point 5**.  

  3. **(For lecturers, studenst with regular immatriculation and employees of the UZH:)** Open the linking tool [eduid.uzh.ch/linking](https://eduid.uzh.ch/linking){target=_blank} and follow the instructions. The linking process can be done from the edu-ID account directly following this [guide](https://eduid.ch/help#affiliations){target=_blank}).

  4. Go to the OLAT login page [https://lms.uzh.ch](https://lms.uzh.ch){target=_blank}, choose the organization "Universität Zürich" from the dropdown list and choose "login". **Auditors choose "Switch edu-ID".**

  5. You are forwarded to the login page of Switch edu-ID. After successful login you are sent back to the registration page of OLAT.

  6. Choose the desired language for the user interface, choose a username (no upper case letters) and accept the terms of use.

!!! info 

    Further information on the registration process and the login with Switch edu-ID can be found under [OLAT Registration](registration_with_a_switch_edu-id.md), on the [Switch edu-ID FAQ page of the central IT department of UZH](https://www.zi.uzh.ch/en/support/identity-access/eduid-faq.html) or on the [help page of Switch](https://eduid.ch/help){target="_blank"}.<br /><br />

    If you have problems with your Switch edu-ID login, you can [reset your Switch edu-ID password](https://eduid.ch/reset-password){target=_blank} or got to the [help page of Switch](https://eduid.ch/help){target="_blank"} or contact the [IT service desk](https://www.zi.uzh.ch/en/support.html){target=_blank}.<br /><br />
    
    After successful registration, the start page specified by the system administrator is displayed. Under [Settings](../../manual_user/personal_menu/Settings.md) you will find information on how you can set your personal start page.

