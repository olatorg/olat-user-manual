# FAQs Courses

  * [When will the campus courses for the upcoming semester be available? (UZH only)](#when-will-the-campus-courses-for-the-upcoming-semester-be-available-uzh-only)
  * [I cannot access the course - my courseoverview shows "Course In progress"](#i-cannot-access-the-course-my-courseoverview-shows-course-in-progress)
  * [The campus course for my course has already been created but the following message appears: "You do not have the necessary permission to open the learning resource". What does this mean? (UZH only)](#the-campus-course-for-my-course-has-already-been-created-but-the-following-message-appears-you-do-not-have-the-necessary-permission-to-open-the-learning-resource-what-does-this-mean-uzh-only)
  * [I booked several modules but can’t see a single campus course. Why? (UZH only)](#i-booked-several-modules-but-cant-see-a-single-campus-course-why-uzh-only)
  * [I just booked my modules but am not listed as a participant in the OLAT campus courses. Why not? (UZH only)](#i-just-booked-my-modules-but-am-not-listed-as-a-participant-in-the-olat-campus-courses-why-not-uzh-only)
  * [I cannot open the desired course element](#i-cannot-open-the-desired-course-element)
    * [Course authorization / Password protection](#course-authorization-password-protection)
    * [Course modifications](#course-modifications)
  * [Some of my campus courses have the notice "In progress". What does this mean? (UZH only)](#some-of-my-campus-courses-have-the-notice-in-progress-what-does-this-mean-uzh-only)
  * [I cannot open the desired course](#i-cannot-open-the-desired-course)
    * [Course authorization / Password protection](#course-authorization-password-protection_1)
    * [Course modifications](#course-modifications_1)
  * [Only some of my courses have campus courses. Why not all of them? (UZH only)](#only-some-of-my-courses-have-campus-courses-why-not-all-of-them-uzh-only)
  * [How can I add a certain course to my "Favorites"? (UZH only)](#how-can-i-add-a-certain-course-to-my-favorites-uzh-only)
  * [Where will I find my campus courses? (UZH only)](#where-will-i-find-my-campus-courses-uzh-only)

## When will the campus courses for the upcoming semester be available? (UZH only)

Starting February 1st, the campus courses for the current spring semester and
the following autumn semester can be created by the lecturers. Starting August
2nd (national holiday August 1st!) the campus courses for the current autumn
semester and the following spring semester can be created. The lecturer is
responsible for deciding whether and exactly when the campus course for your
course will be created and activated. OLAT Support has no control over this
matter. If you can’t find an OLAT course or a campus course, please ask your
lecturer about this matter directly during class.

Continuing courses can be continued at the earliest 60 days before the end of
the semester.

## Some of my campus courses have the notice "In progress". What does this mean? (UZH only)

Campus courses that haven’t yet been created have the entry " **In progress**
" in the list.

It is very possible that these courses were not created in the current
semester and that the course materials are available in another campus
course/another course. Please ask your lecturer about this matter directly
during class.

## The campus course for my course has already been created but the following message appears: "You do not have the necessary permission to open the learning resource". What does this mean? (UZH only)

In such cases, the lecturer has created the campus course but hasn’t yet
activated it; possible reasons are that information or course materials must
be updated. Please ask your lecturer about this matter directly during the
first class session. It happens from time to time that lecturers simply forget
to activate their courses.

## I booked several modules but can’t see a single campus course. Why? (UZH only)

There are several possible reasons.

In principle, campus courses can only be created by lecturers if the course in
question is administered by the Faculty of Arts and Social Sciences, the
Faculty of Science, the Faculty of Theology, or the Faculty of Business,
Economics and Informatics of the University of Zurich. If this is not the
case, only a "normal" OLAT course can be created.

If you have recently changed your higher education institution or if you have
taken a leave of absence from studies, it is possible that OLAT has not yet
correctly allocated your booked modules to your OLAT user account. In such
cases, it is necessary that you contact [OLAT-Support](https://www.zi.uzh.ch/en/support/e-learning-and-examination/staff/olat.html/).

## I just booked my modules but am not listed as a participant in the OLAT campus courses. Why not? (UZH only)

If you have just booked the module, you only have to wait until tomorrow to
find your name in the list. Your bookings are sent to OLAT over night and – as
long as the campus course has already been created – you will be entered in
the campus group "_ UZH students with booking modules" of the campus course
for your course the next morning.

## I cannot open the desired course

If you cannot open a course it may be that the course author has not given you
the relevant rights or the course is being updated at that very moment.

### Course authorization / Password protection

Given the case that you have just found an interesting course and want to get
to the course view. If you are notified that you do not have enough rights to
launch that course this means that the course is restricted to a certain group
or to a certain period of time. A course could also have been password
protected in an earlier OLAT version, which allows authors to restrict course
access independently of groups or time.

Perhaps you have been able to launch a course but later on you get an error
message telling you that its authorization has been changed. This means that
the course author has restricted the access in the meantime or closed that
course.

If the course author changes the password at a later date, all course members
must re-enter the password.

In all cases please contact the course author to grant you access.

### Course modifications

When working on a course and getting the message that the course has been
modified, the course author has just updated that course in the meantime.
Click on the button "Close course and restart" to get to the updated course
version.


## I cannot access the course - my courseoverview shows "Course In progress"

This message appears if the course owner is editing the course. As soon as the
changes have been published and released, you can open the course again as
usual via "Start course".

In rare cases, the publication may be forgotten after changes have been made.
Please contact your lecturer to ask if changes are still being made and if the
course can be published again.

## I cannot open the desired course element

If you cannot open a single course elements it may be that the course author
has not given you the relevant rights or the course is being updated at that
very moment.

### Course authorization / Password protection

Given the case that you have just found an interesting course and want to get
to the course view. If you are notified that you do not have enough rights to
launch that course this means that the course is restricted to a certain group
or to a certain period of time. A course could also have been password
protected in an earlier OLAT version, which allows authors to restrict course
access independently of groups or time.

Perhaps you have been able to launch a course but later on you get an error
message telling you that its authorization has been changed. This means that
the course author has restricted the access in the meantime or closed that
course.

If the course author changes the password at a later date, all users must re-
enter the password.

In all cases please contact the course author to grant you access.

### Course modifications

When working on a course and getting the message that the course has been
modified, the course author has just updated that course in the meantime.
Click on the button "Close course and restart" to get to the updated course
version.

## Only some of my courses have campus courses. Why not all of them? (UZH only)

For operational and technical reasons, it is not yet possible to create campus
courses for courses of the Faculty of Law and the Vetsuisse Faculty, as well
as for several courses of the Faculty of Medicine. These courses may offer
other eLearning options.

Please ask your lecturer about this matter directly during class.

## How can I add a certain course to my "Favorites"? (UZH only)

To do so, click the little white flag in the list under "Courses" → "My
courses".

Courses that are marked with a red flag appear in "Courses" → "Favorites".

## Where will I find my campus courses? (UZH only)

**Students:** After logging in, you can find your campus courses under "Courses" → "Active" (published courses) or "In preparation" (unpublished courses, not accessible yet. Please try again later).

**Lecturers/Course owners:** You can find a List of your Campus courses that have not been created yet in the main OLAT menu under "Campus courses".