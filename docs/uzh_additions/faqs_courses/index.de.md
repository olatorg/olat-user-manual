# FAQs Kurse

  * [Ab wann sind die Campuskurse für das nächste Semester verfügbar? (nur UZH)](#ab-wann-sind-die-campuskurse-fur-das-nachste-semester-verfugbar-nur-uzh)
  * [Bei einigen meiner Campuskurse steht "In Arbeit". Was bedeutet das? (nur UZH)](#bei-einigen-meiner-campuskurse-steht-in-arbeit-was-bedeutet-das-nur-uzh)
  * [Der Campuskurs zu meiner Lehrveranstaltung ist bereits erstellt worden, aber es erscheint die Meldung "Sie haben nicht ausreichende Berechtigungen, um die Lernressource zu starten". Was bedeutet dies? (nur UZH)](#der-campuskurs-zu-meiner-lehrveranstaltung-ist-bereits-erstellt-worden-aber-es-erscheint-die-meldung-sie-haben-nicht-ausreichende-berechtigungen-um-die-lernressource-zu-starten-was-bedeutet-dies-nur-uzh)
  * [Ich habe diverse Module gebucht, sehe aber keinen einzigen Campuskurs. Woran liegt das? (nur UZH)](#ich-habe-diverse-module-gebucht-sehe-aber-keinen-einzigen-campuskurs-woran-liegt-das-nur-uzh)
  * [Ich habe soeben meine Module gebucht, bin aber nicht in den OLAT-Campuskursen als Teilnehmer eingetragen. Warum nicht? (nur UZH)](#ich-habe-soeben-meine-module-gebucht-bin-aber-nicht-in-den-olat-campuskursen-als-teilnehmer-eingetragen-warum-nicht-nur-uzh)
  * [Ich kann den gewünschten Kursbaustein nicht öffnen](#ich-kann-den-gewunschten-kursbaustein-nicht-offnen)
    * [Berechtigung / Passwortschutz für Kursbausteine](#berechtigung-passwortschutz-fur-den-kurs)
    * [Änderungen am Kurs](#anderungen-am-kurs)
  * [Ich kann den Kurs nicht starten, meine Kurs-Ansicht zeigt "Kurs in Arbeit"](#ich-kann-den-kurs-nicht-starten-meine-kurs-ansicht-zeigt-kurs-in-arbeit)
  * [Ich kann den gewünschten Kurs nicht öffnen](#ich-kann-den-gewunschten-kurs-nicht-offnen)
    * [Berechtigung / Passwortschutz für den Kurs](#berechtigung-passwortschutz-fur-den-kurs)
    * [Änderungen am Kurs](#anderungen-am-kurs_1)
  * [Nur zu einem Teil meiner Lehrveranstaltungen gibt es "Campuskurse", warum nicht zu allen? (nur UZH)](#nur-zu-einem-teil-meiner-lehrveranstaltungen-gibt-es-campuskurse-warum-nicht-zu-allen-nur-uzh)
  * [Wie kann ich einen bestimmten Kurs zu meinen "Favoriten" hinzufügen?](#wie-kann-ich-einen-bestimmten-kurs-zu-meinen-favoriten-hinzufugen)
  * [Wo finde ich meine Campuskurse? (nur UZH)](#wo-finde-ich-meine-campuskurse-nur-uzh)

## Ab wann sind die Campuskurse für das nächste Semester verfügbar? (nur UZH)

Jeweils ab dem **1\. Februar** können die Campuskurse für das aktuelle
Frühlingssemester und das darauf folgende Herbstsemester von den Dozierenden
erstellt werden. Jeweils ab dem **2\. August** (Nationalfeiertag 1. August!)
können die Campuskurse für das aktuelle Herbstsemester und das darauf folgende
Frühlingssemester erstellt werden.

Weiterführende Veranstaltungen können frühestens 60 Tage vor Semesterende
weitergeführt werden.

Ob und wann genau der Campuskurs zu Ihrer Lehrveranstaltung erstellt und
freigeschaltet wird, liegt in der Hand des Dozierenden. Der OLAT-Support hat
hierauf keinerlei Einfluss.  
Wenn Sie einen OLAT-Kurs oder einen Campuskurs nicht finden, so sprechen Sie
Ihren Dozierenden direkt in der Lehrveranstaltung darauf an.

## Bei einigen meiner Campuskurse steht "In Arbeit". Was bedeutet das? (nur UZH)

Diese Meldung erscheint, wenn der Kursbesitzer gerade Änderungen vornimmt oder
den Kurs, nachdem er schon mal publiziert wurde, wieder auf "In Vorbereitung"
setzt. Nachdem der Kurs wieder veröffentlicht wurde, finden Sie wie gewöhnlich
die Option "Kurs starten" unter "Meine Kurse".

In seltenen Fällen kann es sein, dass die Veröffentlichung vergessen geht,
nachdem Änderungen gemacht wurden. Bitte nehmen Sie mit Ihrer Lehrperson
Kontakt auf, um nachzufragen, ob noch Änderungen gemacht werden und der Kurs
wieder für veröffentlicht werden kann.

## Der Campuskurs zu meiner Lehrveranstaltung ist bereits erstellt worden, aber es erscheint die Meldung "Sie haben nicht ausreichende Berechtigungen, um die Lernressource zu starten". Was bedeutet dies? (nur UZH)

In diesem Fall hat der Dozierende den Campuskurs schon erstellt, aber noch
nicht freigeschaltet, z.B. weil noch Informationen oder Material aktualisiert
werden muss.  
Sprechen Sie in diesem Fall Ihren Dozierenden in der ersten Sitzung direkt
darauf an. Gelegentlich kommt es vor, dass es einfach vergessen wurde, den
Kurs freizuschalten.

## Ich habe diverse Module gebucht, sehe aber keinen einzigen Campuskurs. Woran liegt das? (nur UZH)

Dies kann mehrere Gründe haben.  
Campuskurse können grundsätzlich nur dann von den Dozierenden erstellt werden,
wenn die Lehrveranstaltung organisatorisch zu der Philosophischen, der
Mathematisch-naturwissenschaftlichen, der Theologischen oder der
Wirtschaftswissenschaftlichen Fakultät der Universität Zürich gehört. Wenn
diese Voraussetzung nicht erfüllt ist, kann nur ein "normaler" OLAT-Kurs
erstellt werden.

Falls Sie die Hochschule gewechselt oder Ihr Studium für eine gewisse Zeit
unterbrochen haben, so kann es sein, dass OLAT Ihre Modulbuchungen noch nicht
korrekt Ihrem OLAT-Benutzerkonto zugeordnet hat. Nehmen Sie in so einem Fall
unbedingt Kontakt zum [OLAT-Support](https://www.zi.uzh.ch/en/support/e-learning-and-examination/staff/olat.html) auf.

## Ich habe soeben meine Module gebucht, bin aber nicht in den OLAT-Campuskursen als Teilnehmer eingetragen. Warum nicht? (nur UZH)

Wenn Sie gerade eben erst die Modulbuchung durchgeführt haben, so müssen Sie
lediglich bis morgen warten.  
In der Nacht werden Ihre Buchungen an OLAT gesendet und am nächsten Morgen
sind Sie - sofern der Campuskurs bereits erstellt wurde - in die Campusgruppe
"_ UZH-Studierende mit Modulbuchung" des Campuskurses zu der Lehrveranstaltung
eingetragen.

## Ich kann den gewünschten Kursbaustein nicht öffnen

Wenn Sie den gewünschten Kursbaustein nicht öffnen können, hat der Kursautor
Ihnen die Berechtigung dazu nicht gegeben oder ist gerade daran, den Kurs zu
aktualisieren.

### Berechtigung / Passwortschutz für Kursbausteine

Möglicherweise ist nicht der ganze Inhalt eines Kurses für Sie sichtbar, oder
Sie haben keinen Zugang zu gewissen Kursbausteinen. Der Kursautor hat gewisse
Kursbausteine nur für bestimmte Gruppen oder für eine bestimmte Zeitspanne
zugänglich oder sichtbar eingestellt.

Des weiteren ist es möglich, dass der Kursbaustein "Struktur" mit darunter
liegenden Kurselementen passwortgeschützt ist. Einzelne Elemente können auf
diese Weise gruppen- oder zeitunabhängig geschützt werden.

Kontaktieren Sie in beiden Fällen den Kursautor, damit er Ihnen erklären kann,
weshalb Sie keine Berechtigungen haben.

### Änderungen am Kurs

Wenn Sie einen Kurs bearbeiten und die Meldung bekommen, der Kurs wäre
verändert worden, hat der Kursautor im Hintergrund den Kurs gerade
aktualisiert. Klicken Sie auf "Kurs schliessen und neustarten", um zur
aktualisierten Version des Kurses zu gelangen.

## Ich kann den Kurs nicht starten, meine Kurs-Ansicht zeigt "Kurs in Arbeit"

Diese Meldung erscheint, sofern der Kursbesitzer den Kurs in Bearbeitung hat.
Sobald die Änderungen publiziert und freigegeben sind, können Sie den Kurs
über "Kurs starten" wie gewohnt wieder öffnen.

## Ich kann den gewünschten Kurs nicht öffnen

Wenn Sie den gewünschten Kurs nicht öffnen können, hat der Kursautor Ihnen die
Berechtigung dazu nicht gegeben oder ist gerade daran, den Kurs zu
aktualisieren.

### Berechtigung / Passwortschutz für den Kurs

Sie finden einen interessanten Kurs und möchten zur Kursansicht gelangen. Wenn
Sie dabei die Meldung bekommen, zu wenig Berechtigungen zu haben, um den Kurs
zu starten, ist der Kurs auf eine bestimmte Gruppe oder Zeitspanne
eingeschränkt worden. Es kann auch sein dass ein Kurs in einer früheren OLAT-
Version mit einem Passwort geschützt wurde. Auf diese Weise kann ein Autor den
Kurszugang gruppen- oder zeitunabhängig beschränken.

Möglicherweise haben Sie einen Kurs starten können und zu einem späteren
Zeitpunkt treffen Sie die Fehlermeldung an, dass sich Berechtigungen geändert
haben. Der Kursautor hat den Kurszugriff nachträglich eingeschränkt oder den
Kurs geschlossen.

Falls der Kursautor zu einem späteren Zeitpunkt das Passwort ändert, müssen
auch Kursmitglieder, die das alte Passwort bereits eingegeben hatten, das neue
Passwort eingeben.

Kontaktieren Sie in allen Fällen den Kursautor, damit er Ihnen die
Berechtigung erteilen oder das Passwort zukommen lassen kann.

### Änderungen am Kurs

Wenn Sie einen Kurs bearbeiten und die Meldung bekommen, der Kurs wäre
verändert worden, hat der Kursautor im Hintergrund den Kurs gerade
aktualisiert. Klicken Sie auf "Kurs schliessen und neustarten", um zur
aktualisierten Version des Kurses zu gelangen.

## Nur zu einem Teil meiner Lehrveranstaltungen gibt es "Campuskurse", warum nicht zu allen? (nur UZH)

Zu den Veranstaltungen der Rechtswissenschaftlichen Fakultät, der VetSuisse-
Fakultät und zu einigen Veranstaltungen der Medizinischen Fakultät können aus
organisatorischen und technischen Gründen bislang keine Campuskurse erstellt
werden. Eventuell gibt es zu diesen Veranstaltungen andere E-Learning-Angebote.

Bitte sprechen Sie die Dozierenden direkt in den Veranstaltungen darauf an.

## Wie kann ich einen bestimmten Kurs zu meinen "Favoriten" hinzufügen?

Klicken Sie hierfür in der Liste der Kurse unter "Aktiv" oder "In Vorbereitung" beim gewünschten Kurs einfach auf das kleine Fähnchen mit roter Kontur.  
Kurse, die mit einem roten Fähnchen markiert sind, erscheinen unter "Kurse" →
"Favoriten".

## Wo finde ich meine Campuskurse? (nur UZH)

**Studierende:** Ihre Campuskurse finden Sie nach dem Einloggen unter "Kurse" → "Aktiv" (Publizierte Kurse) oder "Kurse" → "In Vorbereitung" (Kurs nicht zugänglich da noch nicht publiziert, versuchen Sie es später nocheinmal).

**Dozierende/Kursbesitzer*innen:** Noch nicht erstellte Campuskurse finden Dozierende und Delegierte im OLAT-Menü unter "Campuskurse"
