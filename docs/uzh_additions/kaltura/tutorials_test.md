[public expiery link zur playlist](https://uzh.mediaspace.cast.switch.ch/mediashare/7e04cbf9c9233422/media/Medien-Galerie+via+Kaltura+Kursbaustein+hinzufu%CC%88gen+%7C+add+media+galery+via+Kaltura+course+element/0_endlqgt1){target="_blank"}

[Link zur Medienseite](https://uzh.mediaspace.cast.switch.ch/media/Medien-Galerie+via+Kaltura+Kursbaustein+hinzufu%CC%88gen+%7C+add+media+galery+via+Kaltura+course+element/0_endlqgt1){target="_blank"}

**Kanal: Bearbeiten > Wiedergabelisten: Aktionen <\>:**

[Link zum Teilen von Playlist](https://uzh.mediaspace.cast.switch.ch/playlist/dedicated/67890/0_det07uxa/){target="_blank"}

Einbettungscode (iFrame):
<iframe src="https://uzh.mediaspace.cast.switch.ch/embedplaylist/secure/embed/v2/0/playlistId/0_det07uxa/uiConfId/23449005" width="826" height="465" allowfullscreen webkitallowfullscreen mozAllowFullScreen allow="autoplay *; fullscreen *; encrypted-media *" referrerpolicy="no-referrer-when-downgrade" sandbox="allow-forms allow-same-origin allow-scripts allow-top-navigation allow-pointer-lock allow-popups allow-modals allow-orientation-lock allow-popups-to-escape-sandbox allow-presentation allow-top-navigation-by-user-activation" frameborder="0" title="Kaltura Player"></iframe>

**Kanal > Wiedergabeliste: Teilen**

* Link zur Medienseite <https://uzh.mediaspace.cast.switch.ch/media/Medien-Galerie+via+Kaltura+Kursbaustein+hinzufu%CC%88gen+%7C+add+media+galery+via+Kaltura+course+element/0_endlqgt1>
* Public expiry Link <https://uzh.mediaspace.cast.switch.ch/mediashare/7e04cbf9c9233422/media/Medien-Galerie+via+Kaltura+Kursbaustein+hinzufu%CC%88gen+%7C+add+media+galery+via+Kaltura+course+element/0_endlqgt1>
* Einbetten (iFrame):

<iframe id="kmsembed-0_endlqgt1" width="826" height="465" src="https://uzh.mediaspace.cast.switch.ch/embed/secure/iframe/entryId/0_endlqgt1/uiConfId/23449004/pbc/67890/st/0" class="kmsembed" allowfullscreen webkitallowfullscreen mozAllowFullScreen allow="autoplay *; fullscreen *; encrypted-media *" referrerPolicy="no-referrer-when-downgrade" sandbox="allow-downloads allow-forms allow-same-origin allow-scripts allow-top-navigation allow-pointer-lock allow-popups allow-modals allow-orientation-lock allow-popups-to-escape-sandbox allow-presentation allow-top-navigation-by-user-activation" frameborder="0" title="Medien-Galerie via Kaltura Kursbaustein hinzufügen | add media galery via Kaltura course element"></iframe>

**Link zum Kanal (Kanal-URL):**
<https://uzh.mediaspace.cast.switch.ch/channel/Verwendung%2Bvon%2BKaltura%2Bzur%2BPublikation%2Bvon%2BVideos%2Bin%2BOLAT/67890>