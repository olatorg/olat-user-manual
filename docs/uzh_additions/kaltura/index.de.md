# Kaltura

  * [Kaltura-Videoplattform](#kaltura-videoplattform)
  * [Kaltura-Kursbaustein](#kaltura-kursbaustein)
    * [Tab "Titel und Beschreibung"](#tab-titel-und-beschreibung)
    * [Tab "Layout"](#tab-layout)
    * [Tab "Sichtbarkeit"](#tab-sichtbarkeit)
    * [Tab "Zugang"](#tab-zugang)
    * [Tab "Konfiguration"](#tab-konfiguration)
  * [Meine Medien](#meine-medien)
  * [Kaltura How-Tos](#howtos)

## Kaltura Videoplattform

Die Videoplattform _Kaltura_ wird für Schweizer Hochschulen von SWITCH
angeboten. Mit dem OLAT-Kursbaustein Kaltura können Sie Ihre auf SWITCHcast MediaSpace vorhandenen Videos (z.B. Vorlesungsaufzeichnungen) in OLAT veröffentlichen oder ihre Videos direkt im Kaltura Kursbaustein hochladen und veröffentlichen.

[![](attachments/Schema_Kaltura_MeineMedien_thumb.png){ class="thumbnail-xl border"}<br />Schema: Kaltura "Meine Medien" als gemeinsame Videoquelle von SWITCHcast MediaSpace und den Kursbaustein "Kaltura" in OLAT sowie die unterschiedlichen Pulikationswege](attachments/Schema_Kaltura_MeineMedien.pdf)

Eine umfassende Dokumentation zu _Kaltura_/SWITCHcast MediaSpace finden Sie unter: <https://uzh.mediaspace.cast.switch.ch/Help>{:target="\_blank"}  


!!! info "Alter Kursbaustein 'SWITCHcast'"

    Kurse, die vor August 2022 erstellt wurden und Kopien solcher Kurse können den veralteten Kursbaustein "SWITCHcast" (der Vorgänger des Kursbausteins Kaltura) enthalten. Die alten SWITCHcast-Kursbausteine funktionieren nicht mehr und müssen durch den neuen Kursbaustein Kaltura ersetzt werden.

## Kaltura-Kursbaustein

### Tab "Titel und Beschreibung"

Hier können Sie einen Titel und eine Beschreibung des Kursbausteins eingeben.

!!! info ""
    Weitere Informationen finden Sie im Kapitel "[Titel und Beschreibung](../../manual_user/learningresources/General_Configuration_of_Course_Elements.de.md#desc)".

### Tab "Layout"

Hier können Sie die Layout-Optionen ändern.

### Tab "Sichtbarkeit"

Die Sichtbarkeit eines Kursbausteins ändern, ist eine von drei Arten, um den
Zugriff auf Kursbausteine einzuschränken.  
Wenn Sie die Sichtbarkeit eines Kursbausteins einschränken, erscheint dieser
nicht mehr in der Kursnavigation.

!!! info ""
    Weitere Informationen finden Sie im Kapitel
    "[Sichtbarkeit](../../manual_user/learningresources/General_Configuration_of_Course_Elements.de.md#access)".

### Tab "Zugang"

Im Tab "Zugang" des Kursbausteins können Sie definieren, wer Zugang zu diesem
Baustein hat.

Weitere Informationen finden Sie im Kapitel "[Zugang](../../manual_user/learningresources/General_Configuration_of_Course_Elements.de.md#access)".

### Tab "Konfiguration"

![](attachments/index_de/Kaltura_konfiguration.jpg)

Im Tab Konfiguration können Sie wählen, ob Sie die Medien-Galerie (ausgewählte Videos aus "Meine Medien") oder alle Videos aus "Meine Medien" anzeigen wollen. In den meisten Fällen wählen Sie die Option Medien-Galerie, um ausgewählte Videos anzuzeigen.

!!! tip "Hinweis"

    "Meine Medien" ist für alle Autor*innen auch unter den "Persönlichen
    Werkzeugen" zu finden und lässt sich mit einem Klick auf das User-Porträt oben rechts einblenden.

Weitere Hilfestellungen zur Verwendung von "Meine Medien" und Kaltura auf SWITCHcast MediaSpace finde Sie in der Kaltura-Hilfe:  

<https://uzh.mediaspace.cast.switch.ch/Help>{:target="\_blank"}

  
## Meine Medien
**OLAT-Autor\*innen** können Vorlesungsaufzeichnungen und Videos direkt über den Kursbaustein Kaltura in ihre OLAT-Kurse einbinden ("Zur Galerie hinzufügen").<br />
**Kursbetreuer\*innen** ohne Autorenrechte in OLAT können in einem bestehenden Kaltura-Baustein in ihrem betreuten Kurs Videos hinzufügen. 
OLAT-User haben Zugang zu ihren Videos und ihren Vorlesungsaufzeichnungen über den Menüpunkt “Meine Medien" im persönlichen Menü (Benutzerprofilbild anwählen und unter "Benutzerwerkzeuge" "Meine Medien" auswählen):<br />

![](attachments/index_de/meine_Medien_persönliche_Werkzeuge.png)

Zur Konfiguration und Verwaltung Ihrer Medien unter SWITCHcast Mediaspace konsultieren Sie bitte die Kaltura-Hilfe:  
<https://uzh.mediaspace.cast.switch.ch/Help>{:target="\_blank"}

<hr />

## Kaltura How-Tos {: #howtos}

### Videos im Kursbaustein Kaltura anzeigen

1. Im Kurseditor-Modus über "Kursbausteine einfügen" einen Kaltura Kursbaustein erstellen und die Änderung publizieren.
2. Den neuen Kaltura Kursbaustein anwählen (es dauert ein paar Sekunden bis der Kaltura Inhalt geladen ist)
3. Über die Schaltfläche "Zur Galerie hinzufügen" wird "Meine Medien" geladen.

    !!! question "Die Schaltfläche "Zur Galerie hinzufügen" ist nicht sichtbar"

        Wenn im Kaltura Kursbaustein eine Wiedergabeliste hinzugefügt wurde, wird neben dem Tab "Medien" (mit vorangestellter Anzahl geladener Medien) der Tab "Home" (hinzugefügte Wiedergabelisten) angezeigt. Wechseln Sie zum Tab "Medien", um die Schaltfläche "Zur Galerie hinzufügen" einzublenden.
    
    !!! note "Wiedergabelisten / Playlists"

        Über Kanal-Wiedergabelisten lassen sich Videos in einem Kaltura Kursbaustein gruppieren und die Reihenfolge der Videos lässt sich innerhalb einer Wiedergabeliste anpassen. 
        Wiedergabelisten erstellen Sie über das "Channel actions" Symbol ![Channel actions icon](attachments/channel_actions-icon.png){ class="size32 lightbox" style="vertical-align:middle;" }.

        Ein Tutorial zur Verwendung von Wiedergabelisten finden Sie [hier](https://uzh.mediaspace.cast.switch.ch/media/Playliste+in+Medien-Galerie+in+OLAT+erstellen+%7C+Create+playlist+in+media+galery/0_n5trysem){target="_blank"}.

    
4. Sie können (a) Videos aus "Meine Medien" auswählen, (b) eine Videodatei von Ihrem Gerät neu hochladen oder (c) ein neues Video aufzeichnen.

    1. **Ein Video aus "Meine Medien" auswählen**
        * Wählen Sie die gewünschten Videos aus (Checkbox links neben der Videovorschau anwählen).
        
            !!! note ""

                Über das Suchfeld ("Medien durchsuchen") können Sie mit Worten aus dem Videotitel oder mit Schlagworten (Tags) nach Videos suchen. Unter "Filter" und "Mehr Filter" können Sie die Suche einschränken. Setzen Sie die Suchbegriffe in Anführungszeichen, um nach exakten Begriffen zu suchen.

        * Wählen Sie die Schaltfläche "Veröffentlichen", um die markierten Videos zur Mediengalerie hinzuzufügen.

    2. **Ein Video hochladen**
        * Über "Neu hinzufügen > Medien-Upload" können Sie eine auf Ihrem Gerät gespeicherte Videodatei hochladen. 
        * Geben Sie zusätzliche Metadaten (Tags, Vortragende Person, Datum etc.) ein und wählen Sie "Speichern"
        * Wählen Sie "Weiter zu Kanal". Das neu hochgeladene Video ist im Kaltura Kursbaustein veröffentlicht.

    3. **Ein neues Video aufzeichnen**
        * Wählen Sie "Neu hinzufügen > Expressaufnahme".
        * Folgen Sie den Anweisungen im neuen Browserfenster.
        * Nachdem Sie die Aufnahme beendet haben, können Sie die Aufnahme zur Kontrolle ansehen, erneut aufzeichnen oder die Videodatei zur externen Bearbeitung herunterladen ("Eine Kopie herunterladen")
        * Wählen Sie "Dies verwenden", wenn Sie mit der Aufnahme zufrieden sind und ergänzen Sie die Metadaten (Titel, Tags etc.) der Aufnahme.
        * Schliessen Sie das Browserfenster/den Browsertab mit der Aufzeichnung und wählen Sie im Kursmenü den Kaltura Kursbaustein aus, um die Anzeige zu aktualisieren. Das neu aufgezeichnete Video ist in der Mediengalerie publiziert.
<hr />

### Videos aus "Meine Medien" einbetten

Wie kann ich ein Video aus "Meine Medien" (Kaltura/SWITCHcast MediaSpace) direkt (nicht über den Kursbaustein Kaltura) in einer OLAT-Kursseite (Kursbaustein "Einzelne Seite") oder einer externen Webseite einfügen? 

1. Veröffentlichen Sie Videos, die Sie später auf einer HTML-Seite des Kurses einbinden möchte, zuerst in einem Kaltura Baustein. 
2. Sperren Sie im Kurseditor Modus den Kaltura Kursbaustein für Lernende (im Tab "Sichtbarkeit" dieses Kaltura Kursbausteins).
3. Auf der Medienseite des Videos im Tab "Teilen / Share" > "Einbetten / Embed" die Playergrösse wählen, den Einbettungscode kopieren und via Editor in einer HTML Seite (Kursbaustein "einzelne Seite", Quellcode-Ansicht) einfügen.

<hr />

## Video Tutorials

[Tutorial Video zum Kursbaustein Kaltura](https://uzh.mediaspace.cast.switch.ch/media/OLAT+Tutorials+-+Kaltura+Baustein/0_96n1s1a0/12552){target="_blank"}

[Video Tutorials zur Verwendung von Kaltura in OLAT und zu SWITCHcast MediaSpace](https://uzh.mediaspace.cast.switch.ch/channel//17250){target="_blank"}<br />(Login auf SWITCHcast MediaSpace nötig)

&nbsp;

---
Weitere Anleitungen zur Verwendung von Videos in OLAT finden Sie unter [Video verwenden](../example_of_use/using_video.de.md)
