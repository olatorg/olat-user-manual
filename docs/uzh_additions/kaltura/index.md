# Course element "Kaltura"

  * [Kaltura video platform](#kaltura-video-platform)
  * [Kaltura course element](#kaltura-course-element)
      * [Tab "Title and description"](#tab-title-and-description)
      * [Tab "Layout"](#tab-layout)
      * [Tab "Visibility"](#tab-visibility)
      * [Tab "Access"](#tab-access)
      * [Tab "Configuration"](#tab-configuration)
  * [My Media](#my-media)
  * [Kaltura how tos](#howtos)

## Kaltura video platform

The video platform _Kaltura_ is offered to Swiss universities by SWITCH. The OLAT course element Kaltura allows you to publish your videos from SWITCHcast MediaSpace (lecture recordings for example) or to upload and publish videos directly in the course element.

[![](attachments/Schema_Kaltura_MeineMedien_en.png){ class="thumbnail-xl border"}<br />Diagram: Kaltura "My Media" as shared video source for SWITCHcast MediaSpace and the course element "Kaltura" in OLAT and the different ways to publish videos](attachments/Schema_Kaltura_MeineMedien_en.pdf)

Comprehensive documentation on Kaltura/SWITCHcast MediaSpace can be
found at <https://uzh.mediaspace.cast.switch.ch/Help>.

  
!!! info "Old course element 'SWITCHcast'"

    Courses created before August 2022 and copies of such courses might contain the depricate course element "SWITCHcast", which is the predecessor of the course element Kaltura. The old SWITCHcast course elements don't work anymore and have to be replaced by the new course element Kaltura.

  

## Kaltura course element

### Tab "Title and description"

Here you can enter a title and a description of the course element.

!!! info ""
    For more information, see the "[Title and description](../../manual_user/learningresources/General_Configuration_of_Course_Elements.md#desc)" chapter.

### Tab "Layout"

Here you can change the layout options.

### Tab "Visibility"

Changing the visibility of a course element is one of three ways to restrict
access to course elements.  
If you restrict the visibility of a course element, it will no longer appear
in the course navigation.

!!! info ""
    For more information, see the "[Visibility](../../manual_user/learningresources/General_Configuration_of_Course_Elements.md#access)" chapter.

### Tab "Access"

In the tab "Access" of the course element you can define who has access to
this element.

!!! info ""
    For more information, see the "[Access](../../manual_user/learningresources/General_Configuration_of_Course_Elements.md#access)" chapter.

### Tab "Configuration"

![](attachments/index/kaltura_configuration.png)

In the Configuration tab, you can choose whether you want to display the
Media Gallery (gallery of selected videos y our course participants can see) or all videos in your "My Media" (for course owners only). In most cases, you choose "Media Gallery" if you want to display videos.

!!! tip ""
    For all authors, "My Media" can also be found under "Personal Tools" and can
    be displayed by clicking on the user portrait in the upper right corner.

  

## My Media

**OLAT authors** can integrate lecture recordings and videos in OLAT via the course element Kaltura ("Add Media"). 
**Course coaches** without author rights in OLAT can add videos in a allready existing Kaltura course element of courses where they are coach.
OLAT users have access to their videos and lecture recordings via the menu item "My media" in the personal menu (select user profile picture and choose "My media" under "User tools":

![](attachments/index/my_media_personal_tools.png)

For help on configuration and use of your media on the SWITCHcast Mediaspace platform, please consult the Kaltura Help: <https://uzh.mediaspace.cast.switch.ch/Help>

<hr />

## Kaltura How Tos {: #howtos}

### Display videos in the Kaltura course element

1. create a Kaltura course element in course editor mode via "Insert course elements" and publish the change.
2. select the new Kaltura course element (it takes a few seconds until the Kaltura content is loaded)
3. use the "Add Media" button to load "My Media"

    !!! question "The "Add Media" button is not visible"

        If a playlist has been added to the Kaltura course element, the "Home" tab (added playlists) is displayed besides the "Media" tab (preceded by the number of loaded media). Select the "Media" tab to display the "Add Media" button.
    
    !!! note "Playlists"

        Channel playlists can be used to group videos in a Kaltura course element and the order of the videos can be adjusted within a playlist. You can create playlists via the "Channel actions" icon ![Channel actions Symbol](attachments/channel_actions-icon.png){ class="size32 lightbox" style="vertical-align:middle;" }.

        You can find a tutorial on how to use playlists [here](https://uzh.mediaspace.cast.switch.ch/media/Playliste+in+media+gallery+in+OLAT+create+%7C+Create+playlist+in+media+galery/0_n5trysem){target="_blank"}.


4. you can (a) select videos from "My Media", (b) upload a new video file from your device or (c) record a new video.

    1. **Select a video from "My Media "**
        * Select the desired videos (select the checkbox to the left of a video preview). 

            !!! note ""

                You can use the search field ("Search My Media") to search for videos with words from the video title or with keywords (tags). You can limit the search under "Filters" and "More Filters". Put the search terms in quotation marks to search for exact terms.

        * Select the "Publish" button to add the selected videos to the media gallery.

    2. **Upload a video**
        * Via "Add new > Media Upload" you can upload a video file saved on your device. 
        * Enter additional metadata (tags, presenter, date, etc.) and select "Save"
        * Select "Go to Media Gallery". The newly uploaded video is published in the Kaltura course element.

    3. **Record a new video**
        * Select "Add new > Express Capture".
        * Follow the instructions in the new browser window.
        * Once you have finished recording, you can view the recording to check it, record it again or download the video file for external editing ("Download a copy")
        * Select "Use this" if you are satisfied with the recording and add the metadata (title, tags, etc.) to the recording.
        * Close the browser window/tab with the recording and select the Kaltura course element in the course menu to refresh the display. You find the newly recorded video published in the media gallery.

<hr />

### Embedding videos from "My Media"

How can I embed a video from "My Media" (Kaltura/SWITCHcast MediaSpace) directly (not via the Kaltura course element) in an OLAT course page (course element "Single page") or an external website? 

1. publish videos that you want to embed later on a html page of the course at first in a Kaltura course element. 
2. block the Kaltura course element for learners in the "Visibility" tab of this Kaltura course element.
3. select the player size on the media page of the video in the "Share" > "Embed" tab, copy the embed code and paste it into an HTML page (course element "Single page", source code view) using the editor.

<hr />

## Video tutorials

[Video tutorial on the course element Kaltura (german version only)](https://uzh.mediaspace.cast.switch.ch/media/OLAT+Tutorials+-+Kaltura+Baustein/0_96n1s1a0/12552){target="_blank"}

[Video tutorials on using Kaltura in OLAT and on SWITCHcast MediaSpace](https://uzh.mediaspace.cast.switch.ch/channel//17250){target="_blank"} (mainly german content, login on SWITCHcast MediaSpace required)

&nbsp;

---
Further instructions on how to use videos in OLAT can be found under [Using video](../example_of_use/using_video.md).