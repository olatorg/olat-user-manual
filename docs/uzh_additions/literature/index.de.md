# Kursbaustein "Literatur"

Mit dem Kursbaustein "Literatur" können Kursbesitzer den Kursteilnehmer ihre Literaturlisten zur Verfügung stellen. Die Literaturlisten werden vorgängig ausserhalb von OLAT in einem externen Literaturverwaltungsprogramm erstellt und als Dateien im [BibTeX](https://de.wikipedia.org/wiki/BibTeX){target=_blank}-Format (.bib) exportiert. In OLAT können diese BibTex-Dateien in den Kursbaustein importiert werden. Die Kursteilnehmer können diese Literaturlisten betrachten und herunterladen.

  * [Im Kurseditor](#im-kurseditor)
    * [Tab "Titel und Beschreibung"](#tab-titel-und-beschreibung)
    * [Tab "Layout"](#tab-layout)
    * [Tab "Sichtbarkeit"](#tab-sichtbarkeit)
    * [Tab "Zugang"](#tab-zugang)
    * [Tab "Lernpfad"](#tab-lernpfad)
    * [Tab "Konfiguration"](#tab-konfiguration)
        * [Abschnitt "Literatursuche"](#abschnitt-literaturrecherche)
        * [Abschnitt "Formatierung"](#abschnitt-formatierung) 
        * [Abschnitt "Berechtigungen"](#abschnitt-berechtigungen) 
  * [Im Kursrun](#im-kursrun)
      * [//]: #
        * [Literaturliste importieren](#literaturliste-importieren)
        * [Literaturliste exportieren, drucken und löschen](#literaturliste-exportieren-drucken-und-loschen)
        * [Literaturliste bearbeiten](#literaturliste-bearbeiten)


## Im Kurseditor

Im Kurseditor können die Tabs für die zentralen Einstellungen des Bausteins verwendet werden.

### Tab "Titel und Beschreibung"

Hier können Sie einen Titel und eine Beschreibung des Kursbausteins eingeben.

!!! info ""
    Weitere Informationen finden Sie im Kapitel "[Titel und Beschreibung](../../manual_user/learningresources/General_Configuration_of_Course_Elements.de.md#desc)".

### Tab "Layout"

Hier können Sie die Layout-Optionen ändern.

### Tab "Sichtbarkeit"

Die Sichtbarkeit eines Kursbausteins ändern, ist eine von drei Arten, um den
Zugriff auf Kursbausteine einzuschränken.  
Wenn Sie die Sichtbarkeit eines Kursbausteins einschränken, erscheint dieser
nicht mehr in der Kursnavigation.

!!! info ""
    Weitere Informationen finden Sie im Kapitel
    "[Sichtbarkeit](../../manual_user/learningresources/General_Configuration_of_Course_Elements.de.md#access)".

### Tab "Zugang"

Im Tab "Zugang" des Kursbausteins können Sie definieren, wer Zugang zu diesem
Baustein hat.

Weitere Informationen finden Sie im Kapitel "[Zugang](../../manual_user/learningresources/General_Configuration_of_Course_Elements.de.md#access)".

### Tab "Lernpfad"

In Ihrem [Lernpfadkurs](../../manual_user/learningresources/Learning_path_course_Course_editor.de.md) definieren Sie im Tab "Lernpfad", ob die Bearbeitung des Kursbausteins obligatorisch oder freiwillig
ist. Ferner können Sie einen Zeitraum festlegen, in dem der Kursbaustein
bearbeitet werden kann.

Jeder Kursbaustein verfügt über individuelle Erledigungskriterien, eine
Übersicht finden Sie im Kapitel "[Lernpfad](../../manual_user/learningresources/Learning_path_course.de.md)".

!!! info ""
    Weitere Informationen finden Sie im Kapitel
    "[Lernpfad](../../manual_user/learningresources/Learning_path_course.de.md)".

!!! warning ""
    Herkömmliche Kurse verfügen nicht über den Tab "Lernpfad" und haben
    stattdessen die Tabs "[Sichtbarkeit](../../manual_user/learningresources/General_Configuration_of_Course_Elements.de.md#access)" und
    "[Zugang](../../manual_user/learningresources/General_Configuration_of_Course_Elements.de.md#access)" inklusive [Expertenmodus](../access_restrictions_in_expert_mode/index.de.md).  

### Tab "Konfiguration"

![](attachments/index_de/Literatur_konfiguration_1_DE.png){class="border"}

#### Abschnitt "Literaturrecherche"

Optional haben Sie die Möglichkeit, eine Literaturrecherche zu aktivieren. Voreingerichtet ist die Suche der Universitätsbibliothek Zürich. <br />URL: [https://hbzwwws04.uzh.ch/hbziframes/frontpage_directsearch_slsp_en.html](https://hbzwwws04.uzh.ch/hbziframes/frontpage_directsearch_slsp_en.html){target=_blank}

Mit einer entsprechenden eigenen URL können Sie eine andere Literatursuche einbinden.

Weiter können Sie hier den Titel der Literatursuche und im Feld "Höhe des Feldes" die vertikale Grösse des Anzeigebereichs einstellen.

#### Abschnitt "Formatierung"

Im Abschnitt "Formatierung" des Tabs "Konfiguration" können Sie die gewünschte Formatierung Ihrer Literaturlisten festlegen. Alle Literaturlisten werden die gleichen Formatierungen bekommen. Sobald eine Literaturliste hinzugefügt wurde, kann die Formatierung nicht mehr geändert werden.

!!! info "Info"

    Es stehen folgende Formatierungen zur Verfügung:

      - American Medical Association (AMA)
      - American Psychological Association (APA)
      - American Sociological Association (ASA)
      - Chicago Public Library (CPL)
      - Elsevier – Harvard (mit Titeln)
      - Hogrefe (Deutsche Gesellschaft für Psychologie)
      - IEEE
      - Nature
      - Proceedings of the National Academy of Sciences (PNAS)
      -	Science
      - Springer
    
Falls Sie eine andere Formatierung benötigen, können Sie über den Button «Zusätzliche Formatierung hinzufügen» im Feld «Name der Formatierung» nach der gewünschten Formatierung suchen. Im Feld «Beschreibung» wird der ausgewählte Begriff eingefügt, wobei Sie diese Beschreibung manuell anpassen können.  

#### Abschnitt "Berechtigungen"

Als Kursbesitzer:in haben Sie die Möglichkeit, Kursbetreuer:innen und/oder Kursteilnehmer:innen die notwendigen Berechtigungen zu vergeben, sodass sie Literaturlisten importieren, editieren und/oder löschen können.

### Duplizieren

Wird ein Kurs oder ein Literatur-Kursbaustein dupliziert, werden die vorhandenen Literaturlisten übernommen. 

## Im Kursrun

Im Kursrun können Sie, abhängig von Ihrer Kursrolle und den konfigurierten Berechtigungen, Literaturlisten importieren, exportieren, drucken, editieren und löschen.

![](attachments/index_de/Literatur_new_editoption_DE.png){class="border"}

#### Literaturliste importieren
Kursbesitzer:innen können im Kursrun eine oder mehrere Dateien im [BibTeX](https://de.wikipedia.org/wiki/BibTeX){target=_blank}-Format (.bib) mit Literatur importieren. Solange keine Datei hochgeladen wurde, erscheint der Hinweis "Keine Literaturliste vorhanden".

![](attachments/index_de/DE_Literatur_keineListeVorhanden.png){class="border"}


Ein Klick auf die Schaltfläche "Literaturliste importieren" öffnet das Importformular. Im Feld "Name" kann eine Bezeichnung für die zu importierende Liste festgelegt werden. Der Bereich "Bib-Datei" erlaubt das Hochladen der entsprechenden Literaturliste im BibTeX-Format (.bib). 
Nach dem Importieren wird die Literaturliste im gewünschten Format angezeigt. 

![](attachments/index_de/DE_Literatur_ListeImportieren.png)

#### Literaturliste exportieren, drucken und löschen
Importierte Literaturlisten können über die Schaltfläche "Literaturliste exportieren" jederzeit von allen Kursmitgliedern heruntergeladen werden. Diese Literaturlisten können über die Schaltfläche "Drucken" ausgedruckt oder über den Drucken-Dialog als PDF-Dokument gespeichert werden.
Ebenso können Kursbesitzer:innen einmal importierte Literaturlisten über die Schaltfläche "Literaturliste löschen" wieder entfernen. 

![](attachments/index_de/Literatur_new_editoption_DE.png){class="border"}

#### Literaturliste bearbeiten
Literaturlisten können über die Schaltfläche "Literaturliste editieren" bearbeitet werden. Hier kann z.B. der Name der Literaturliste geändert werden. Bitte beachten Sie, dass der Kursbaustein "Literatur" nicht als Literarturverwaltungsprogramm gedachtet ist. Kleine Änderungen können trotzdem vorgenommen werden.
