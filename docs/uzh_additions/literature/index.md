# Course element "Literature"

By using the course element "Literature," course owners are able to provide course participants with their literature lists. The literature lists will be created outside OLAT with an external reference management software and exported as files in the [BibTeX](https://en.wikipedia.org/wiki/BibTeX){target=_blank} format (.bib). In OLAT, these BibTex files will then be imported into the course element. Course participants can view these literature lists and download them.

  * [Course editor](#course-editor)
      * [Tab "Title and description"](#tab-title-and-description)
      * [Tab "Layout"](#tab-layout)
      * [Tab "Visibility"](#tab-visibility)
      * [Tab "Access"](#tab-access)
      * [Tab "Learning path"](#tab-learning-path)
      * [Tab "Configuration"](#tab-configuration)
        * [Section "Literature research"](#section-literature-research)
        * [Section "Formatting"](#section-formatting)
        * [Section "User rights"](#section-user-rights)
  * [Course run](#course-run)
      * [//]: #
        * [Import literature lists](#import-literature-lists)
        * [Export, print and delete literature lists](#export-print-and-delete-literature-lists)
        * [Edit literature lists](#edit-literature-lists)


## Course editor

In the course editor, the tabs for the central settings of the course element are at your disposal.

### Tab "Title and description"

Here you can enter a title and a description of the course element.

!!! info ""
    For more information, see the "[Title and description](../../manual_user/learningresources/General_Configuration_of_Course_Elements.md#desc)" chapter.

### Tab "Layout"

Here you can change the layout options.

### Tab "Visibility"

Changing the visibility of a course element is one of three ways to restrict
access to course elements.  
If you restrict the visibility of a course element, it will no longer appear
in the course navigation.

!!! info ""
    For more information, see the "[Visibility](../../manual_user/learningresources/General_Configuration_of_Course_Elements.md#access)" chapter.

### Tab "Access"

In the tab "Access" of the course element you can define who has access to
this element.

!!! info ""
    For more information, see the "[Access](../../manual_user/learningresources/General_Configuration_of_Course_Elements.md#access)" chapter.


### Tab "Learning path"

In case of a [learning path course](../../manual_user/learningresources/Learning_path_course_Course_editor.md), you can define in the tab "learning path" whether the execution of the course element is obligatory or voluntary. Furthermore, you can define a time period in which the course element can be processed.

The "Learning Path" tab also defines which criterion must be met for the course element to be considered "completed". An overview can be found in the chapter "[Learning path](../../manual_user/learningresources/Learning_path_course.md)".

!!! info ""
    For more information, see the chapter
    "[Learning path](../../manual_user/learningresources/Learning_path_course.md)".

!!! warning ""
    Conventional courses do not have the "Learning Path" tab. Instead they have the "[Visibility](../../manual_user/learningresources/General_Configuration_of_Course_Elements.md#access)" and "[Access](../../manual_user/learningresources/General_Configuration_of_Course_Elements.md#access)" tabs, including [expert mode](../../manual_user/learningresources/Access_Restrictions_in_the_Expert_Mode/).

### Tab "Configuration"

![](attachments/index_de/Literatur_konfiguration_1_EN.png){class="border"}

#### Section "Literature research"

Optionally, you have the possibility to activate a literature research. The search of the University Library of Zurich is preset.<br />
URL: [https://hbzwwws04.uzh.ch/hbziframes/frontpage_directsearch_slsp_en.html](https://hbzwwws04.uzh.ch/hbziframes/frontpage_directsearch_slsp_en.html){target=_blank}

With a corresponding own URL you can integrate another literature search.

Further you can set here the title of the literature search and in the field "Field height" the vertical size of the display area.

#### Section "Formatting"

In the "Formatting" section of the "Configuration" tab you can select the desired formatting of your literature lists. All literature lists will have the same formatting. Once a literature list has been added, the formatting can no longer be changed.

!!! info "Info"

    The following formatting styles are available:

      - American Medical Association (AMA)
      - American Psychological Association (APA)
      - American Sociological Association (ASA)
      - Chicago Public Library (CPL)
      - Elsevier – Harvard (with titles)
      - Hogrefe (Deutsche Gesellschaft für Psychologie)
      - IEEE
      - Nature
      - Proceedings of the National Academy of Sciences (PNAS)
      - Science
      - Springer

If you need a different formatting, you can use the "Add additional format" button to search for the desired formatting in the "Formatting name" field. In the "Description" field the selected term will be inserted and you can adjust this description manually.

#### Section "User rights"

As a course owner you have the possibility to assign the necessary permissions to course coaches and/or course participants so that they can import, edit and/or delete literature lists.

### Duplicate

If a course or a literature course element is duplicated, the existing literature lists are taken over. 

## Course run

In the course run, depending on your course role and configured permissions, you can import, export, print, edit and delete literature lists.

#### Import literature lists
In the course run, one or more files can be imported in the [BibTeX](https://en.wikipedia.org/wiki/BibTeX){target=_blank} format (.bib). As long as no file has been uploaded, "No literature list availabe" is displayed.

![](attachments/index/EN_Literature_noListeAvailable.png){class="border"}

By clicking on the "Import literature list" button, the import form opens. In the "Name" field, a title for the list to be imported can be specified. The "Bib file" area allows the upload of the literature list in the BibTeX format (.bib). 

![](attachments/index/EN_Literature_ImportList.png)

After importing the file, the literature list is displayed in the specified format. 

#### Export, print and delete literature lists
Imported literature lists can be downloaded via the "Export literature list" button by all course participants. These literature lists can be printed via the "Print" button or saved as a PDF document via the print dialog.
Furthermore, course owners can remove once imported literature lists via the button "Delete literature list". 

![](attachments/index_de/Literatur_new_editoption_EN.png){class="border"}

#### Edit literature lists
Literature lists can be edited via the "Edit literature list" button. Here, for example, the name of the literature list can be changed. Please note that the course element "Literature" is not intended as a literature management program. Small changes can still be made.
