# Kursbausteineinstellungen für Peer Review

Im Kurseditor können die Tabs für die zentralen Einstellungen des Bausteins
verwendet werden.

## Tab "Titel und Beschreibung"

Hier können Sie einen Titel und eine Beschreibung des Kursbausteins eingeben.

!!! info ""
    Weitere Informationen finden Sie im Kapitel "[Titel und Beschreibung](../../manual_user/learningresources/General_Configuration_of_Course_Elements.de.md#desc)".

## Tab "Sichtbarkeit"

Die Sichtbarkeit eines Kursbausteins zu ändern ist eine der drei Arten, um den
Zugriff auf Kursbausteine einzuschränken.  
Wenn Sie die Sichtbarkeit eines Kursbausteins einschränken, erscheint dieser
nicht mehr in der Kursnavigation.

!!! info ""
    Weitere Informationen finden Sie im Kapitel
    "[Sichtbarkeit](../../manual_user/learningresources/General_Configuration_of_Course_Elements.de.md#access)".

## Tab "Zugang"

Im Tab "Zugang" des Kursbausteins können Sie definieren, wer genau Zugang zu
diesem Baustein hat.

!!! info ""
    Weitere Informationen finden Sie im Kapitel "[Zugang](../../manual_user/learningresources/General_Configuration_of_Course_Elements.de.md#access)".

## Tab "Lernpfad"

In Ihrem [Lernpfadkurs](../../manual_user/learningresources/Learning_path_course_Course_editor.de.md) definieren Sie im Tab "Lernpfad", ob die Bearbeitung des Kursbausteins obligatorisch oder freiwillig
ist. Ferner können Sie einen Zeitraum festlegen, in dem der Kursbaustein
bearbeitet werden kann.

Jeder Kursbaustein verfügt über individuelle Erledigungskriterien, eine
Übersicht finden Sie im Kapitel "[Lernpfad](../../manual_user/learningresources/Learning_path_course.de.md)".

!!! info ""
    Weitere Informationen finden Sie im Kapitel
    "[Lernpfad](../../manual_user/learningresources/Learning_path_course.de.md)".

!!! warning ""
    Herkömmliche Kurse verfügen nicht über den Tab "Lernpfad" und haben
    stattdessen die Tabs "[Sichtbarkeit](../../manual_user/learningresources/General_Configuration_of_Course_Elements.de.md#access)" und
    "[Zugang](../../manual_user/learningresources/General_Configuration_of_Course_Elements.de.md#access)" inklusive [Expertenmodus](../access_restrictions_in_expert_mode/index.de.md).  

## Tab "Workflow"

Die zentrale Konfiguration des Kursbausteins erfolgt im ersten Schritt im Tab
"Workflow". Hier bestimmen Sie, welchen Kursbaustein "Aufgabe" Sie verknüpfen
wollen und welche Schritte von Peer Review Sie verwenden möchten.

![](attachments/settings_for_the_course_element_peer_review_de/DE_Workflow.png)

### Abschnitt "Aufgabe"

Um ein Peer Review durchführen zu können, brauchen wir eine Abgabe
von den Kursteilnehmern. Hierfür verwenden wir den Kursbaustein "Aufgabe".  
Nachdem Sie einen Kursbaustein "Aufgabe" hinzugefügt und mit einer absoluten
Abgabefrist konfiguriert haben, können Sie diesen mit dem Kursbaustein "Peer
Review" verknüpfen.

### Abschnitt "Workflow-Schritte des Peer Reviews"

Hier können Sie die gewünschten Workflow-Schritte aktivieren oder
deaktivieren und die entsprechenden Fristen einstellen.

  * Kalibrierung für die Teilnehmer aktivieren
  * Peer Review für die Teilnehmer aktivieren
  * Expert Review für die Experten aktivieren
  * Die Bewertung aktivieren

  
!!! info ""
    Weil wir zwei verschiedene Kursbausteine brauchen, ist es wichtig, dass diese
    Bausteine die gleichen Zugangs- und Sichtbarkeitsregeln haben. Bitte
    überprüfen Sie die entsprechende Konfiguration in Fall eines herkömmlichen
    Kurses.
  
!!! warning ""
    Bei einer Änderung der Inputfelder werden die Felder der anderen Tabs nicht
    immer korrekt aktualisiert.  
    Ein Workaround für dieses Update-Problem ist z.B., den Kursbaustein "Aufgabe"
    anzuklicken und dann wieder die Konfiguration in den gewünschten Tabs weiter
    zu erledigen.

## Tab "Review Kriterien"

Die Review Kriterien (vergleichbar mit einem Fragebogen) sind der zentrale
Punkt für das ganze Peer Review Konzept und bilden die Basis für die
Bewertungsstrategie.

![](attachments/settings_for_the_course_element_peer_review_de/Peer_Review_-_Review_Kriterien_-_DE.png)

Mit dem Knopf "Review Frage erstellen" kann eine neue Review Frage hinzugefügt
werden.  
Eine Review Frage beinhaltet immer die Frage an sich und minimal zwei
Antworten. Falls die Bewertung aktiviert ist, muss pro Antwortmöglichkeit auch
eine Punktzahl eingegeben werden. Diese Punktzahlen brauchen wir für die
weiteren Berechnungen.  
Zusätzlich können Sie pro Review Frage eine Begründung für die Antwort
einfordern. Wenn Sie diese Option auswählen, ist die Begründung für die
Kursteilnehmer ein Pflichtfeld, das sie ausfüllen müssen. Hier kann optional eine Zeichenbegrenzung eingestellt werden.

![](attachments/settings_for_the_course_element_peer_review_de/Peer_Review_-_Review_Frage_-_DE.png)

Solange die Kalibrierungsaufgaben noch nicht endgültig bestätigt sind und/oder
die Zuweisung des Peer Review noch nicht stattgefunden hat, können die Review-
Fragen editiert werden.

!!! warning ""
    Pro Kursbaustein "Peer Review" werden Review Kriterien erstellt. Das bedeutet,
    dass – wenn mehrere Aufgaben innerhalb des Kursbausteins "Aufgabe" erstellt
    werden – alle Aufgaben die gleichen Review Kriterien verwenden.

## Tab "Kalibrierung"

Hier wird die Kalibrierung konfiguriert.

![](attachments/settings_for_the_course_element_peer_review_de/Peer_Review_-_Tab_Kalibrirung_-_DE.png)

  *  **Anzahl Kalibrierungsaufgaben**
  *  **Anzahl Fragen:** kann nicht geändert werden, weil die Zahl mit der Anzahl der Review Fragen übereinstimmt.
  *  **Anzahl korrekte Antworten:** wie viele Fragen müssen die Kursteilnehmer korrekt beantworten, um die erlaubte Abweichung zu erreichen und so auch Punkte zu erhalten.
  *  **Wiederholungen:** falls Kursteilnehmer zu wenige Fragen korrekt beantworten (= nicht-erlaubte Abweichung), kann hier eingestellt werden, ob sie die entsprechenden Kalibrierungsaufgaben wiederholen dürfen und ob es eine oder zwei Wiederholungen gibt.

  
!!! warning ""
    Bei einer Änderung der Inputfelder werden die Felder der anderen Tabs nicht
    immer korrekt aktualisiert.  
    Ein Workaround für dieses Update-Problem ist z.B., den Kursbaustein "Aufgabe"
    anzuklicken und dann wieder die Konfiguration in den gewünschten Tabs weiter
    zu erledigen.
  

## Tab "Reviews"

### Abschnitt "Peer Review"
Falls der Workflow-Schritt "Peer Review" aktiviert wurde, kann hier konfiguriert werden wie viele Peer Reviews die Kursteilnehmer erledigen müssen. Sie haben eine Auswahl zwischen 1 und 5 Peer Reviews.

### Abschnitt "Expert Review"
Falls der Workflow-Schritt "Expert Review" aktiviert wurde, kann hier konfiguriert werden, wie die Zuweisung der Experten gemacht werden soll.
-	Kursbetreuer tragen sich selber als Experte ein
-	Kursbesitzer weisen die Kursbetreuer als Experten zu


## Tab "Bewertung"

Dieser Tab ist sichtbar, falls die Bewertung aktiviert ist.  
Abhängig von den aktivierten Workflow-Schritten werden die entsprechenden
Abschnitte gezeigt.

![](attachments/settings_for_the_course_element_peer_review_de/DE_Bewertung.png)

### Abschnitt "Kalibrierung"

Die allgemeinen Einstellungen der Kalibrierung werden im Tab "Kalibrierung"
erledigt.  
In diesem Tab und Abschnitt gibt es die bewertungsspezifischen Einstellungen:

  *  **Abzug bei Wiederholung:** falls es eine Wiederholung gibt, kann man den Abzug für jede Wiederholung einstellen.
  *  **Max. Punktzahl pro Kalibrierung:** kann nicht geändert werden, weil die Zahl mit der Anzahl der Review Fragen übereinstimmt.
  *  **Min. Punktzahl um zu bestehen:** Anzahl Punkte, welche Kursteilnehmende erzielen müssen, um den Schritt "Kalibrierung" zu bestehen. Falls konfiguriert, werden Abzüge auch verrechnet.

### Abschnitt "Qualität des Peer Reviews (Bewertung der erledigten Reviews)"

  *  **Punktzahl pro Peer Review:** maximale Punktzahl (dieser Wert kann frei gewählt werden) pro durchgeführtes Peer Review.
  *  **Erlaubte Abweichung (voller Kredit):** wie gross darf die Abweichung sein, um doch noch die maximale Punktzahl zu bekommen.
  *  **Erlaubte Abweichung (partieller Kredit):** die Abweichung, um doch noch einen Teil der maximalen Punktzahl zu bekommen.
  *  **Abzug (%) eines partiellen Kredits:** wie gross ist der Abzug bei einer erlaubten Abweichung (partieller Kredit).
  *  **Min. Punktzahl um zu bestehen:** wie viele Punkte muss der Kursteilnehmer erzielen, um beim Schritt "Peer Review" zu bestehen.

### Abschnitt "Qualität der Arbeit (Bewertung der erhaltenen Reviews)"

  *  **Max. Punktzahl pro Peer Review:** kann nicht geändert werden. Die Zahl entspricht der Summe der maximalen Punktzahl aus allen Review Fragen.
  *  **Min. Punktzahl um zu bestehen:** Anzahl Punkte, welche Kursteilnehmende erzielen müssen, um mit der eingereichten Arbeit zu bestehen.

### Abschnitt "Self Review"

  * **Max. Punktzahl des Self Reviews:** kann nicht geändert werden. Die Zahl entspricht der Summe der maximalen Punktzahl aus allen Review Fragen.
  * **Min. Punktzahl um zu bestehen:** Anzahl Punkte, welche Kursteilnehmende erzielen müssen, um mit der eingereichten Arbeit zu bestehen.


### Abschnitt "Expert Review"

  * **Max. Punktzahl pro Expert Review:** kann nicht geändert werden. Die Zahl entspricht der Summe der maximalen Punktzahl aus allen Review Fragen.
  * **Min. Punktzahl um zu bestehen:** Anzahl Punkte, welche Kursteilnehmende erzielen müssen, um mit der eingereichten Arbeit zu bestehen.

### Abschnitt "Bewertungsstrategie der Reviews"

Hier können Sie die Bewertungsstrategie der Reviews in Punkten oder Prozenten definieren.  
Jeder aktivierte Workflow-Schritt ist Teil der Bewertung.

  *  **Gesamtbewertung:** mit Punkten (kann nicht geändert werden)
  *  **Kalibrierung (Punkte/Prozente):** definieren Sie hier den Anteil der Kalibrierung für die Gesamtbewertung in Punkten oder Prozenten. Auf dieser Basis wird die Endnote berechnet.
  *  **Qualität des Peer Reviews (Punkte/Prozente):** definieren Sie hier den Anteil der Qualität des Peer Review für die Gesamtbewertung in Punkten oder Prozenten. Auf dieser Basis wird die Endnote berechnet.
  *  **Qualität der Arbeit (Punkte/Prozente):** definieren Sie hier den Anteil der Qualität der Arbeit für die Gesamtbewertung in Punkten oder Prozenten. Auf dieser Basis wird die Endnote berechnet.
  *  **Self Review (Punkte/Prozente):** definieren Sie hier den Anteil des Self Reviews für die Gesamtbewertung in Punkten oder Prozenten. Auf dieser Basis wird die Endnote berechnet.

  * **Expert Review (Punkte/Prozente):** definieren Sie hier den Anteil des Expert Reviews für die Gesamtbewertung in Punkten oder Prozenten. Auf dieser Basis wird die Endnote berechnet.
  
!!! warning ""
    Bei einer Änderung der Inputfelder werden die Felder der anderen Tabs nicht
    immer korrekt aktualisiert.  
    Ein Workaround für dieses Update-Problem ist z.B., den Kursbaustein "Aufgabe"
    anzuklicken und dann wieder die Konfiguration in den gewünschten Tabs weiter
    zu erledigen.
