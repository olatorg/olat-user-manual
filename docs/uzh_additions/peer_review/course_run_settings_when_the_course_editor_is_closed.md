# Course run: settings when the course editor is closed

Once you have configured all the settings, you can follow the whole process in the course run.
Use the gear icon to display or hide certain columns.

## Tab "All participants"

### Overview of the course participants' status

In the course run view, the course owner role always provides you with an overview of each course participant's status. If grading is enabled, you can also check some important grading data.

![](attachments/course_run_settings_when_the_course_editor_is_closed/EN_Overview.png)

### Detailed view of a course participant's status

In the overview table, you can click on "Details" as a course owner or coach. This allows you to view the details of individual course participants and to check their status.  
If grading is configured and grading data is already available, the grading details can also be checked here.

![](attachments/course_run_settings_when_the_course_editor_is_closed/Peer_Review_-_Detail_Teilnehmer_-_EN.png)

## Tab "Expert Review"
If the workflow step "Expert Review" is activated, this tab becomes visible. Here, the experts can carry out the expert reviews for the selected pieces of work or check the status of their expert reviews.

## Tab "Administration"

### Section "Course element 'Task'"

As the course element "Peer Review" always requires a course element "Task" having an absolute deadline, you can check this information here.

### Section "Calibration"

If the calibration step is activated, calibration tasks have to be created. In the "Administration" tab, you can create as many calibration tasks as you have specified in the "Configuration of the Calibration" (course editor).  
Please remember that you have already added the review criteria before, because the calibration tasks will also use the review criteria.

![](attachments/course_run_settings_when_the_course_editor_is_closed/Peer_Review_-_Kalibrierungsaufgaben_-_EN.png)

You can create files or upload existing files.  
For each calibration task, the review questions and the corresponding answer options are displayed.  
Now select the correct answer option for each review question.

![](attachments/course_run_settings_when_the_course_editor_is_closed/Peer_Review_-_Detail_Kalibrierungsaufgabe_-_EN.png)

Once all calibration tasks are created, they can be finally confirmed.  
From that time on, they can no longer be changed. If no start date for the calibration has been set, the course participants are able to start with the calibration immediately.

!!! warning ""
    Be aware that the calibration tasks' filenames are visible to the participants.
  

### Section "Peer review assignment"

Once the papers have been successfully submitted, you can randomly and anonymously assign them to course participants for their peer reviews. This can be done in the tab "Assignment". 
In order to carry out the assignment, an absolute submission deadline in the past and at least the number of configured peer reviews plus one have to be set.

![](attachments/course_run_settings_when_the_course_editor_is_closed/Peer_Review_-_Zuweisung_-_EN.png)

Click the "Assign Peer Review" button to perform the assignment.  
After a successful assignment, an assignment overview table will be displayed.
This table is only visible to the course owners.

![](attachments/course_run_settings_when_the_course_editor_is_closed/Peer_Review_-_Zuweisung_durchgefuhrt_-_EN.png)

## Tab "Grading"

Once the deadlines are over and the assessment is activated, you can 
finalize the assessment. 

Here you have 2 options:

1. **Finalise and release assessment**: The assessment will be finalised and immediately visible to the participants.
2. **Finalise assessment without release**: The assessment will be finalised but not yet be shared with the participants. You can use the "Release assessment" button to share the assessment at a later time.

After pressing one button, the calculation is carried out and no further changes can be made.

You can check the assessment in the course participant's detail view.


