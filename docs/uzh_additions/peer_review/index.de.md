# Kursbaustein "Peer Review"

Peer Review ist ein Konzept, das in einer didaktischen Umgebung sehr oft
verwendet wird.  
Es kann sowohl summativ als auch formativ verwendet werden.

Peer Review besteht an sich aus mehr oder weniger standardisierten Schritten:

  1. Konfiguration
  2. Aufgabe
  3. Review (Kalibrierung, Peer Review, Self Review und Expert Review)
  4. Bewertung

OLAT stellt hierfür den Kursbaustein "Peer Review" zur Verfügung.

* [Didaktisches Konzept](didactical_concept.de.md)
* [Vorgehen in OLAT](how_to_proceed_in_olat.de.md)
    * [Schritt 1: Konfiguration](how_to_proceed_in_olat.de.md#schritt-1-konfiguration)
        * [Konfiguration des Kursbausteins "Aufgabe"](how_to_proceed_in_olat.de.md#konfiguration-des-kursbausteins-aufgabe)
        * [Konfiguration des Kursbausteins "Peer Review"](how_to_proceed_in_olat.de.md#konfiguration-des-kursbausteins-peer-review)
    * [Schritt 2: Aufgabe](how_to_proceed_in_olat.de.md#schritt-2-aufgabe)
    * [Schritt 3: Review](how_to_proceed_in_olat.de.md#schritt-3-review)
        * [Schritt "Kalibrierung"](how_to_proceed_in_olat.de.md#schritt-kalibrierung)
            * [→ Erledigung der Kalibrierungsaufgaben](how_to_proceed_in_olat.de.md#erledigung-der-kalibrierungsaufgaben)
        * [Schritt "Peer Review"](how_to_proceed_in_olat.de.md#schritt-peer-review)
            * [→ Zuweisung des Peer Review](how_to_proceed_in_olat.de.md#zuweisung-des-peer-review)
            * [→ Erledigung des Peer Review](how_to_proceed_in_olat.de.md#erledigung-des-peer-review)
        * [Schritt "Expert Review"](how_to_proceed_in_olat.de.md#schritt-expert-review)
            * [→ Sich als Experte eintragen (und austragen)](how_to_proceed_in_olat.de.md#sich-als-experte-eintragen-und-austragen)
            * [→ Erledigung des Expert Review](how_to_proceed_in_olat.de.md#erledigung-des-expert-review)
        * [Übersicht für die Kursbesitzer und -betreuer](how_to_proceed_in_olat.de.md#ubersicht-fur-die-kursbesitzer-und-betreuer)
    * [Schritt 4: Bewertung](how_to_proceed_in_olat.de.md#schritt-4-bewertung)
        * [Bewertung der Kalibrierung](how_to_proceed_in_olat.de.md#bewertung-der-kalibrierung)
            * [→ Auf Ebene der Kalibrierungsaufgabe](how_to_proceed_in_olat.de.md#auf-ebene-der-kalibrierungsaufgabe)
            * [→ Endgültiger RSI, Gesamtpunktzahl und -beurteilung](how_to_proceed_in_olat.de.md#endgultiger-rsi-gesamtpunktzahl-und-beurteilung)
        * [Bewertung des Peer Review (Bewertung der erledigten Reviews)](how_to_proceed_in_olat.de.md#bewertung-des-peer-review-bewertung-der-erledigten-reviews)
            * [→ Auf der Ebene des Peer Review](how_to_proceed_in_olat.de.md#auf-der-ebene-des-peer-review)
            * [→ Gesamtpunktzahl und -beurteilung](how_to_proceed_in_olat.de.md#gesamtpunktzahl-und-beurteilung)
        * [Bewertung der Arbeit (Bewertung durch die erhaltenen Reviews)](how_to_proceed_in_olat.de.md#bewertung-der-arbeit-bewertung-durch-die-erhaltenen-reviews)
            * [→ Auf der Ebene des erhaltenen Review](how_to_proceed_in_olat.de.md#auf-der-ebene-des-erhaltenen-review)
            * [→ Gesamtpunktzahl und -beurteilung](how_to_proceed_in_olat.de.md#gesamtpunktzahl-und-beurteilung_1)
        * [Bewertung des Expert Reviews](how_to_proceed_in_olat.de.md#bewertung-des-expert-reviews)
            * [→ Auf der Ebene des Expert Reviews](how_to_proceed_in_olat.de.md#auf-der-ebene-des-expert-reviews)
            * [→ Gesamtpunktzahl und -beurteilung](how_to_proceed_in_olat.de.md#gesamtpunktzahl-und-beurteilung_2)
        * [Gesamtbewertung](how_to_proceed_in_olat.de.md#gesamtbewertung)

* [Kursbausteineinstellungen für Peer Review](settings_for_the_course_element_peer_review.de.md)
    * [Tab "Titel und Beschreibung"](settings_for_the_course_element_peer_review.de.md#tab-titel-und-beschreibung)
    * [Tab "Sichtbarkeit"](settings_for_the_course_element_peer_review.de.md#tab-sichtbarkeit)
    * [Tab "Zugang"](settings_for_the_course_element_peer_review.de.md#tab-zugang)
    * [Tab "Lernpfad"](settings_for_the_course_element_peer_review.de.md#tab-lernpfad)
    * [Tab "Workflow"](settings_for_the_course_element_peer_review.de.md#tab-workflow)
        * [Abschnitt "Aufgabe"](settings_for_the_course_element_peer_review.de.md#abschnitt-aufgabe)
        * [Abschnitt "Workflow-Schritte des Peer Reviews"](settings_for_the_course_element_peer_review.de.md#abschnitt-workflow-schritte-des-peer-reviews)
    * [Tab "Review Kriterien"](settings_for_the_course_element_peer_review.de.md#tab-review-kriterien)
    * [Tab "Kalibrierung"](settings_for_the_course_element_peer_review.de.md#tab-kalibrierung)
    * [Tab "Reviews"](settings_for_the_course_element_peer_review.de.md#tab-reviews)
    * [Tab "Bewertung"](settings_for_the_course_element_peer_review.de.md#tab-bewertung)
        * [Abschnitt "Kalibrierung"](settings_for_the_course_element_peer_review.de.md#abschnitt-kalibrierung)
        * [Abschnitt "Qualität des Peer Reviews (Bewertung der erledigten Reviews)"](settings_for_the_course_element_peer_review.de.md#abschnitt-qualitat-des-peer-reviews-bewertung-der-erledigten-reviews)
        * [Abschnitt "Qualität der Arbeit (Bewertung der erhaltenen Reviews)"](settings_for_the_course_element_peer_review.de.md#abschnitt-qualitat-der-arbeit-bewertung-der-erhaltenen-reviews)
        * [Abschnitt "Self Review"](settings_for_the_course_element_peer_review.de.md#abschnitt-self-review)
        * [Abschnitt "Expert Review"](settings_for_the_course_element_peer_review.de.md#abschnitt-expert-review)
        * [Abschnitt "Bewertungsstrategie der Reviews"](settings_for_the_course_element_peer_review.de.md#abschnitt-bewertungsstrategie-der-reviews)

* [Kursrun: Einstellungen bei geschlossenem Editor](course_run_settings_when_the_course_editor_is_closed.de.md)
    * [Tab "Alle Teilnehmer"](course_run_settings_when_the_course_editor_is_closed.de.md#tab-alle-teilnehmer)
        * [Übersicht über den Status der Teilnehmer](course_run_settings_when_the_course_editor_is_closed.de.md#ubersicht-uber-den-status-der-teilnehmer)
        * [Detailsicht eines Kursteilnehmers](course_run_settings_when_the_course_editor_is_closed.de.md#detailsicht-eines-kursteilnehmers)
    * [Tab "Expert Review"](course_run_settings_when_the_course_editor_is_closed.de.md#tab-expert-review)
    * [Tab "Verwaltung"](course_run_settings_when_the_course_editor_is_closed.de.md#tab-verwaltung)
        * [Abschnitt "Kursbaustein 'Aufgabe'"](course_run_settings_when_the_course_editor_is_closed.de.md#abschnitt-kursbaustein-aufgabe)
        * [Abschnitt "Kalibrierung"](course_run_settings_when_the_course_editor_is_closed.de.md#abschnitt-kalibrierung_1)
        * [Abschnitt "Zuweisung des Peer Reviews"](course_run_settings_when_the_course_editor_is_closed.de.md#abschnitt-zuweisung-des-peer-reviews)
    * [Tab "Bewertung"](course_run_settings_when_the_course_editor_is_closed.de.md#tab-bewertung_1)
