# Vorgehen in OLAT

Ein Kursbaustein "Peer Review" wird immer zusammen mit einem Kursbaustein
"Aufgabe" verwendet.  
Untenstehendes Schema zeigt eine Übersicht über die Verwendung des
Kursbausteins "Peer Review" im Zusammenhang mit dem Kursbaustein "Aufgabe".

<table>
    <thead>
        <tr>
            <th style="background-color: #F4F5F7;">Schritt</th>
            <th style="background-color: #F4F5F7;">Kursbaustein</th>
            <th style="background-color: #F4F5F7;">Kursbesitzer</th>
            <th style="background-color: #F4F5F7;">Kursbetreuer</th>
            <th style="background-color: #F4F5F7;">Kursteilnehmer</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td style="background-color: #F4F5F7;" rowspan="11"><strong>Schritt 1: Konfiguration</strong></td>
            <td style="background-color: #DEEBFF;" rowspan="2">KB "Aufgabe"</td>
            <td style="background-color: #DEEBFF;">
                Aufgabe konfigurieren&nbsp;(inkl. absolutes Abgabedatum einstellen) (Kurseditor: Tab "Workflow")</td>
            <td><br></td>
            <td><br></td>
        </tr>
        <tr>
            <td style="background-color: #DEEBFF;" colspan="2">Aufgabe erstellen (Kurseditor und/oder Kursrun)</td>
            <td><br></td>
        </tr>
        <tr>
            <td style="background-color: #E3FCEF;" rowspan="9"
               >KB "Peer Review"<br><br></td>
            <td style="background-color: #E3FCEF;">KB
                "Aufgabe" verknüpfen (Kurseditor: Tab "Workflow")</td>
            <td><br></td>
            <td><br></td>
        </tr>
        <tr>
            <td style="background-color: #E3FCEF;" colspan="1">Workflow-Schritt "Kalibrierung" aktivieren (inkl. Frist einstellen)
                (Kurseditor: Tab "Workflow") <sup>(2)</sup></td>
            <td colspan="1" ><br></td>
            <td colspan="1" ><br></td>
        </tr>
        <tr>
            <td style="background-color: #E3FCEF;">
                Workflow-Schritt "Peer Review" aktivieren (inkl. Frist einstellen) (Kurseditor: Tab "Workflow")
                <sup>(1)</sup></td>
            <td><br></td>
            <td><br></td>
        </tr>
        <tr>
            <td style="background-color: #E3FCEF;">
                Workflow-Schritt "Expert Review" aktivieren (inkl. Frist einstellen) (Kurseditor: Tab "Workflow") 
                <sup>(3)</sup></td>
            <td><br></td>
            <td><br></td>
        </tr>
        <tr>
            <td style="background-color: #E3FCEF;" colspan="1">Bewertung aktivieren (Kurseditor: Tab "Workflow") <sup>(4)</sup></td>
            <td colspan="1" ><br></td>
            <td colspan="1" ><br></td>
        </tr>
        <tr>
            <td style="background-color: #E3FCEF;">
                Review Fragen erstellen (Kurseditor: Tab "Review Kriterien") <sup>(1)(2)(3)(4)</sup></td>
            <td><br></td>
            <td><br></td>
        </tr>
        <tr>
            <td style="background-color: #E3FCEF;" colspan="1">Kalibrierung konfigurieren (Kurseditor: Tab "Kalibrierung")
                <sup>(2)</sup></td>
            <td colspan="1" ><br></td>
            <td colspan="1" ><br></td>
        </tr>
        <tr>
            <td style="background-color: #E3FCEF;" colspan="1">
                <p>Kalibrierungsaufgaben erstellen (Kursrun: Tab "Verwaltung") <sup>(2)</sup></p>
            </td>
            <td colspan="1" ><br></td>
            <td colspan="1" ><br></td>
        </tr>
        <tr>
            <td style="background-color: #E3FCEF;" colspan="1">Bewertung konfigurieren (Kurseditor: Tab "Bewertung") <sup>(4)</sup>
            </td>
            <td colspan="1" ><br></td>
            <td colspan="1" ><br></td>
        </tr>
        <tr>
            <td style="background-color: #F4F5F7;" colspan="1"><strong>Schritt 2: Aufgabe</strong></td>
            <td style="background-color: #DEEBFF;">KB
                "Aufgabe"</td>
            <td><br></td>
            <td><br></td>
            <td style="background-color: #DEEBFF;">
                Aufgabe innerhalb der Abgabefrist erledigen</td>
        </tr>
        <tr>
            <td style="background-color: #F4F5F7;" rowspan="4"><strong title="">Schritt 3: Review</strong></td>
            <td style="background-color: #E3FCEF;" rowspan="4">KB "Peer Review"</td>
            <td colspan="2" ><br></td>
            <td style="background-color: #E3FCEF;" colspan="1">Innerhalb der Frist die Kalibrierungsaufgaben endgültig erledigen <sup
                    title="">(2)</sup></td>
        </tr>
        <tr>
            <td style="background-color: #E3FCEF;">
                Zuweisung des Peer Reviews durchführen (Kursrun: Tab "Zuweisung") <sup>(1)</sup></td>
            <td><br></td>
            <td><br></td>
        </tr>
        <tr>
            <td><br></td>
            <td><br></td>
            <td style="background-color: #E3FCEF;">
                Innerhalb der Frist die Peer Reviews endgültig erledigen <sup>(1)</sup></td>
        </tr>
        <tr>
            <td style="background-color: #E3FCEF;" colspan="2">
                Innerhalb der Frist die Expert Reviews endgültig erledigen <sup>(3)</sup></td>
            <td><br></td>
        </tr>
        <tr>
            <td style="background-color: #F4F5F7;" rowspan="3"><strong>Schritt 4: Bewertung</strong></td>
            <td style="background-color: #E3FCEF;" rowspan="3"
               >KB "Peer Review"</td>
            <td><br></td>
            <td><br></td>
            <td style="background-color: #E3FCEF;">
                Feedback zur eigenen Arbeit einsehen <sup>(1)(3)</sup></td>
        </tr>
        <tr>
            <td style="background-color: #E3FCEF;" colspan="1">Bewertung abschliessen (Kursrun: Tab "Bewertung") <sup
                    title="">(4)</sup></td>
            <td colspan="1" ><br></td>
            <td colspan="1" ><br></td>
        </tr>
        <tr>
            <td style="background-color: #E3FCEF;" colspan="2">Bewertung pro Kursteilnehmer einsehen (Kursrun: Tab "Alle Teilnehmer")
                <sup>(4)</sup></td>
            <td style="background-color: #E3FCEF;" colspan="1">Bewertung einsehen <sup>(4)</sup></td>
        </tr>
    </tbody>
</table>

(1) Falls Sie den Workflow-Schritt "Peer Review" aktivieren wollen. / Falls
"Peer Review" aktiviert ist.  

(2) Falls Sie den Workflow-Schritt "Kalibrierung" aktivieren wollen. / Falls
"Kalibrierung" aktiviert ist. 

(3) Falls Sie den Workflow-Schritt "Expert Review" aktivieren wollen. / Falls "Expert Review" aktiviert ist. 

(4) Falls Sie die Berechnungen aktivieren wollen. / Falls die Berechnungen
aktiviert sind.

## Schritt 1: Konfiguration

Wenn Sie als Kursbesitzer in Ihrem Kurs den Kursbaustein "Peer Review"
verwenden wollen, brauchen Sie immer einen [Kursbaustein
"Aufgabe"](../../manual_user/learningresources/Assessment.de.md#course_element_task).

### Konfiguration des Kursbausteins "Aufgabe"

  * Im **Kursbaustein "Aufgabe"** machen Sie die gewünschten Einstellungen. Wichtig ist, dass Sie hier **eine absolute Abgabefrist** in der Zukunft einstellen.  
Es ist auch möglich, innerhalb des Kursbausteins "Aufgabe" mehrere
Aufgabenstellungen hinzuzufügen.

### Konfiguration des Kursbausteins "Peer Review"

  * Im **Kursbaustein "Peer Review"** verknüpfen Sie im Tab "Workflow", Abschnitt "Aufgabe" (Kurseditor), den entsprechenden Kursbaustein "Aufgabe".
  * Im Abschnitt "Workflow-Schritte des Peer Reviews" des Tabs "Workflow" (Kurseditor) aktivieren oder deaktivieren Sie die gewünschten **Workflow-Schritte und konfigurieren Sie die entsprechenden Fristen**:  

      * Workflow-Schritt "Kalibrierung"
      * Workflow-Schritt "Peer Review"
      * Workflow-Schritt "Expert Review"
      * "Bewertung"
  * Im Tab "**Review Kriterien**" (Kurseditor) erstellen Sie eine oder mehrere Review Fragen. Jede Review Frage besteht aus der eigentlichen Frage und minimal 2 Antwortmöglichkeiten. Falls die Bewertung aktiviert ist, braucht jede Antwortmöglichkeit eine Punktzahl. Zusätzlich können Sie auch eine Begründung für die Antwort einfordern.  
Die Review Kriterien werden in den verschiedenen Workflow-Schritten
("Kalibrierung", "Peer Review" und "Expert Review") verwendet.

  * Im Tab "**Kalibrierung**" (Kurseditor) konfigurieren Sie die Anzahl Kalibrierungsaufgaben, die Anzahl der korrekt zu beantworten Frage pro Aufgabe und die Anzahl der Wiederholungen. 
  * Im Tab "**Verwaltung**" (Kursrun) fügen Sie die Kalibrierungsaufgaben hinzu. Eine Kalibrierungsaufgabe besteht immer aus der eigentlichen Aufgabe (eine oder mehrere Dateien) und der korrekten Antwort pro Review Frage.
  * Im Tab "**Bewertung**" (Kurseditor) konfigurieren Sie pro aktivierten Workflow-Schritt die Bewertung. Im Abschnitt "Bewertungsstrategie der Reviews" definieren Sie den Anteil jedes aktivierten Workflow-Schritts für die Endnote.

## Schritt 2: Aufgabe

Nach der Konfiguration und dem erfolgreichen Publizieren der beiden
Kursbausteine können die Kursteilnehmer im Kursbaustein "Aufgabe" die Aufgabe
annehmen, diese erledigen und innerhalb der absoluten Abgabefrist abgeben.

## Schritt 3: Review

### Schritt "Kalibrierung"

Bevor der Workflow-Schritt "Kalibrierung" gestartet werden kann, muss der
Kursbesitzer die Kalibrierungsaufgaben erstellen (siehe "Konfiguration").

#### → _Erledigung der Kalibrierungsaufgaben_

  * Die Kalibrierungsaufgaben werden angezeigt.
  * Jede Kalibrierungsaufgabe besteht aus einer oder mehreren Dateien und den Review Fragen.
  * Alle Review Fragen müssen beantwortet werden, erst dann kann die Abgabe der Kalibrierungsaufgaben endgültig bestätigt werden.
  * Man bekommt eine Meldung, ob die Kalibrierungsaufgaben innerhalb der erlaubten Abweichung erledigt wurden.
  * Falls eine Wiederholung konfiguriert wurde, können die Kalibrierungsaufgaben mit einer nicht-erlaubten Abweichung innerhalb der Frist wiederholt werden.

### Schritt "Peer Review"

Bevor der Workflow-Schritt "Peer Review" gestartet werden kann, muss der
Kursbesitzer die Zuweisung des Peer Review durchführen.  
Die Zuweisung ist zufällig und gilt nur für die rechtzeitig abgegebenen
Arbeiten. Nach der Zuweisung können die Kursteilnehmer mit den Peer Reviews
anfangen. Falls es ein Startdatum für das Peer Review gibt, kann erst nach
diesem Startdatum angefangen werden.

#### → _Zuweisung des Peer Review_

Die Zuweisung des Peer Review kann nur stattfinden, wenn im verknüpften Kursbaustein des Typs "Aufgabe"

  1. eine absolute Abgabefrist in der Vergangenheit konfiguriert ist, und
  2. es abgegebene Arbeiten gibt.

Abhängig von der Anzahl Aufgaben und abgegebener Arbeiten im verknüpften Kursbaustein «Aufgabe» werden eine oder mehrere Zuweisungsoptionen verfügbar.

1. **Zufällige Zuweisung**<br />
    Hier bekommen die Teilnehmer zufällig die definierte Anzahl zu reviewender Arbeiten.<br />
    *Diese Option braucht mindestens eine abgegebene Arbeit mehr als die Anzahl Peer Reviews wie konfiguriert im Tab «Reviews» Abschnitt «Peer Review» des Kurseditors.*
 
2. **Immer die gleiche Aufgabe wie die eigene Aufgabe zuweisen**<br />
    Hier bekommen die Teilnehmer zufällig die definierte Anzahl zu reviewenden Arbeiten, aber die zu reviewenden Arbeiten haben die gleiche Aufgabe wie die eigene Aufgabe.<br />
    *Mindestens zwei Aufgaben im verknüpften Kursbaustein «Aufgabe» müssen definiert sein. Zudem braucht jede Aufgabe mindestens die Anzahl abgegebener Arbeiten wie im Tab «Reviews», Abschnitt «Peer Review» (Kurseditor Modus) konfiguriert.*
 
3. **Immer die gleiche Aufgabe aber nicht die eigene Aufgabe zuweisen**<br />
    Hier bekommen die Teilnehmer zufällig die definierte Anzahl zu reviewender Arbeiten, aber die zu reviewenden Arbeiten haben nicht die gleiche Aufgabe wie die eigene Aufgabe.<br />
    *Mindestens zwei Aufgaben im verknüpften Kursbaustein «Aufgabe» müssen definiert sein. Zudem braucht jede Aufgabe mindestens die Anzahl abgegebener Arbeiten wie im Tab «Reviews», Abschnitt «Peer Review» (Kurseditor Modus) konfiguriert.
    Die grösste Aufgaben-Gruppe darf nicht grösser sein als die Summe der anderen Aufgaben-Gruppen.*
 
Sobald die Bedingungen erfüllt sind, kann der Kursbesitzer die Zuweisung des Peer Review vornehmen. Diese Zuweisung kann später nicht mehr geändert werden.

#### → _Erledigung des Peer Review_

  * Direkt nach der Zuweisung des Peer Review kennen die Kursteilnehmer die Arbeiten, die sie reviewen müssen. Falls es ein Startdatum für das Peer Review gibt, werden die Arbeiten erst dann angezeigt.
  * Die zu reviewenden Arbeiten werden anonym angezeigt. Anhand der Review Kriterien bewerten die Kursteilnehmer die einzelnen Arbeiten.
  * Der Kursteilnehmer sieht jederzeit, wie weit er im Review-Prozess ist.
  * Am Ende wird die Erledigung des Peer Review endgültig bestätigt.

### Schritt "Expert Review"

Alle Mitglieder des Kurses mit den OLAT-Rollen "Kursbesitzer" und "Kursbetreuer" können Expert Reviews erledigen.
Sie können sich selber als Experten eintragen und die abgegebene Arbeit beurteilen. Falls es ein Startdatum für das Expert Review gibt, kann erst nach diesem Startdatum angefangen werden. Für das Expert Review muss eine Frist konfiguriert werden. Diese Frist wird den Kursteilnehmern kommuniziert und ab diesem Zeitpunkt sind die Expert Reviews für sie auch sichtbar.

#### → _Sich als Experte eintragen (und austragen)_
  * Kursbesitzer und Kursbetreuer können sich bei einem oder mehreren Kursteilnehmern als Experte eintragen. Sie können dann die abgegebene Arbeit innerhalb der Frist beurteilen.
  * Solange die Experten eine Arbeit noch nicht endgültig beurteilt haben, können sie sich auch wieder austragen.

#### → _Erledigung des Expert Review_
  * Nachdem Sie sich als Experte eingetragen haben, können Sie damit beginnen, die abgegebene Arbeit anhand der Review Kriterien zu beurteilen.
  * Abschliessend wird die Erledigung des Expert Review endgültig bestätigt.

### Übersicht für die Kursbesitzer und -betreuer

  * Auch die Kursbesitzer und -betreuer können in der Übersicht und auf den Detailseiten jederzeit überprüfen, wie weit ein Kursteilnehmer im Review-Prozess ist.
  * Die Kursbesitzer und -betreuer können die einzelnen Reviews ebenfalls einsehen.

  
!!! warning ""
    Die Peer-Review-Aufgaben sind anonym und zufällig, aber beachten Sie, dass die
    Dateinamen nicht anonymisiert werden können.  
    Wenn ein Kursteilnehmer z.B. seinen Namen im Dateinamen hinzufügt, ist dies
    auch für seine Peers sichtbar.
  

## Schritt 4: Bewertung

Nach der Erledigung des Peer Reviews und des Expert Reviews können die Kursteilnehmer das Feedback
ihrer eigenen Arbeiten einsehen (formativ).  
Falls die Bewertung aktiviert ist, kann der Kursbaustein "Peer Review" auch
summativ verwendet werden.

Im Workflow-Schritt "Kalibrierung" wird die Qualität der Beurteilung der
Kalibrierungsaufgaben bewertet.

Im Workflow-Schritt "Peer Review" werden 2 Aspekte bewertet:

  1.  **Qualität des Peer Review**:  
Wie akkurat ist die gegebene Beurteilung der Arbeit? Wie gut haben die Peers
die Arbeit beurteilt, d.h., ist die Beurteilung von allen Reviewers mehr oder
weniger ähnlich oder gibt es grosse Diskrepanzen?

  2.  **Qualität der Arbeit**:  
Wie gut wurde die abgegebene Arbeit von den Peers bewertet?

Im Workflow-Schritt "Expert Review" wird die abgegebene Arbeit durch den Experten bewertet.

Jeder aktivierte Workflow-Schritt ist Teil der Bewertung.  
Diese Anteile werden in der Bewertungsstrategie der Reviews (Tab "Bewertung"
im Kurseditor) definiert.

### Bewertung der Kalibrierung

Hier wird die Qualität der Beurteilung der Kalibrierungsaufgaben bewertet.

#### → _Auf Ebene der Kalibrierungsaufgabe_

Weil jede korrekte Antwort einen Punkt ergibt, stimmt die Anzahl Fragen in den
Review Kriterien überein mit der **maximalen Punktzahl**.  
Pro korrekte Antwort wird ein Punkt verdient. So können wir pro
Kalibrierungsaufgabe die **erreichte Punktzahl** berechnen.

In der Konfiguration der Kalibrierung (Tab "Kalibrierung" im Kurseditor) wurde
definiert, wie viele korrekte Antworten pro Kalibrierungsaufgabe erreicht
werden müssen. Hat der Teilnehmende ebenso viele oder mehr korrekte Antworten
erzielt, handelt es sich um eine **erlaubte Abweichung und er bekommt
Punkte**. Ist die **Abweichung nicht erlaubt**, d.h., hat der Teilnehmende
weniger korrekte Antworten als die konfigurierte Zahl, **dann bekommt er keine
Punkte**.

Der **RSI (Reviewer Skill Index)** ist eine Gewichtung und definiert die
Qualität des Beurteilungsvermögens der Teilnehmenden. Diese Gewichtung wird
benötigt, um die nachfolgenden Reviews bzw. die Qualität der Arbeit bewerten
zu können.  
Der RSI ist die erreichte Punktzahl in Prozent geteilt durch 100. So bekommen
wir immer eine Zahl zwischen 0 und 1.

Jedes Mal, wenn der Teilnehmende eine **Wiederholung** braucht, um die
Kalibrierung zu erledigen, kann ein **Abzug in Prozent** konfiguriert werden.

 **Um die endgültige Punktzahl zu berechnen, brauchen wir den Reviewer Skill
Index (RSI) und die Anzahl Wiederholungen mit dem entsprechenden Abzug.**  
Nur wenn die Abweichung erlaubt ist, bekommt man Punkte. Bei einer nicht-
erlaubten Abweichung ist die endgültige Punktzahl immer 0.  
Die endgültige Punktzahl bei einer erlaubten Abweichung ist die erreichte
Punktzahl, vermindert um den Abzug der Wiederholung.

Zum Schluss können wir noch die **Beurteilung** definieren. Hierfür
brauchen wir die endgültige Punktzahl und die minimale Punktzahl, die nötig
ist, um die Kalibrierung endgültig zu bestehen (siehe Tab "Bewertung",
Abschnitt "Kalibrierung" im Kurseditor).

#### → _Endgültiger RSI, Gesamtpunktzahl und -beurteilung_

Nun können wir die gesamte Kalibrierung bewerten.

  * Der **endgültige RSI** (Reviewer Skill Index) ist der Durchschnitt der erhaltenen einzelnen RSI-Werte.
  * Die **Gesamtpunktzahl** ist die Summe der einzelnen endgültigen Punktzahlen.
  * Und die **Gesamtbeurteilung** basiert auf den Werten der einzelnen endgültigen Beurteilungen.

### Bewertung des Peer Review (Bewertung der erledigten Reviews)

Hier wird die Qualität des Peer Review bewertet.

#### → _Auf der Ebene des Peer Review_

In den Review Kriterien hat jede Antwortmöglichkeit einer Review Frage eine
Punktzahl. Bei jedem durchgeführten Peer Review wird die Summe der
Antwortmöglichkeiten errechnet. Das ist die **erteilte Punktzahl**.  
Die **maximale Punktzahl** ist die Summe der maximalen Punktzahl pro Review-
Frage.

Die **durchschnittliche Punktzahl** ist der Durchschnitt aller individuellen
Resultate dieser Arbeit, d.h., die von den anderen Reviewers erteilte
Punktzahl für die gleiche Arbeit.

Entscheidend für diese Berechnung ist die **Abweichung**. Die Abweichung ist
der Unterschied zwischen der erteilten Punktzahl und der durchschnittlichen
Punktzahl.  
Für den Umgang mit der Abweichung gibt es nun 3 Möglichkeiten, diese wurden
vorher definiert (Tab "Bewertung" Abschnitt "Qualität des Peer Reviews" im
Kurseditor):

  * erlaubte Abweichung (voller Kredit): bei einer erlaubten Abweichung (voller Kredit) bekommt der Reviewer die ganze Punktzahl pro Peer Review (wie vorher definiert)
  * erlaubte Abweichung (partieller Kredit): bei einer erlaubten Abweichung (partieller Kredit) bekommt der Reviewer die ganze Punktzahl pro Peer Review, vermindert um einen Abzug (wie vorher definiert)
  * nicht-erlaubte Abweichung: wenn die Abweichung zu gross ist, bekommt der Reviewer keine Punkte

Die **endgültige Punktzahl** basiert auf der Abweichung.

Zum Schluss können wir noch die **Gesamtbeurteilung** definieren. Hierfür
brauchen wir die endgültige Punktzahl und die minimale Punktzahl, die nötig
ist, um das Peer Review endgültig zu bestehen (siehe Tab "Bewertung" Abschnitt
"Qualität des Peer Reviews" im Kurseditor).

#### → _Gesamtpunktzahl und -beurteilung_

Nun können wir die Gesamtqualität des Peer Review bewerten.

  * Die **Gesamtpunktzahl** ist die Summe der einzelnen endgültigen Punktzahlen.
  * Und die **Gesamtbeurteilung** basiert auf den Werten der einzelnen endgültigen Beurteilungen.

### Bewertung der Arbeit (Bewertung durch die erhaltenen Reviews)

Hier wird die Qualität der abgegebenen Arbeit bewertet.

#### → _Auf der Ebene des erhaltenen Review_

Die **erhaltene Punktzahl** ist die Punktzahl, die ich für meine abgegebene
Arbeit von meinem Peer bekommen habe.  
Die **maximale Punktzahl** ist die Summe der maximalen Punktzahl pro Review-
Frage.

Falls die Kalibrierung aktiviert wurde, ist der **RSI der Reviewer** auch
sichtbar und wichtig für die Berechnung der endgültigen Punktzahl.

#### → _Gesamtpunktzahl und -beurteilung_

Hier machen wir für die **Gesamtpunktzahl** einen Unterschied zwischen den
Berechnungen mit und ohne Kalibrierung.

  *  **Ohne Kalibrierung**:
      * Der RSI ist nicht vorhanden.
      * Die Gesamtpunktzahl ist der Durchschnitt aller einzelnen erhaltenen Punktzahlen.
  *  **Mit Kalibrierung**:
      * Der RSI ist vorhanden und wird in den Berechnungen berücksichtigt.
      * Ein Review von einem Reviewer mit einem hohen RSI hat mehr Wert als ein Review von einem Reviewer mit einem tiefen RSI.

Zum Schluss können wir noch die **Gesamtbeurteilung** definieren. Hierfür
brauchen wir die Gesamtpunktzahl und die minimale Punktzahl, die nötig ist, um
mit der Arbeit endgültig zu bestehen (siehe Tab "Bewertung" Abschnitt
"Qualität der Arbeit" im Kurseditor).

### Bewertung des Expert Reviews

Hier wird die abgegebene Arbeit durch den Experten bewertet.

#### → _Auf der Ebene des Expert Reviews_

In den Review Kriterien hat jede Antwortmöglichkeit einer Review Frage eine Punktzahl. Bei jedem durchgeführten Peer Review wird die Summe der Antwortmöglichkeiten errechnet. Das ist die erteilte Punktzahl.
Die maximale Punktzahl ist die Summe der maximalen Punktzahl pro Review  Frage.
Zum Schluss können wir noch die Beurteilung definieren. Hierfür brauchen wir die endgültige Punktzahl und die minimale Punktzahl, die nötig ist, um das Expert Review endgültig zu bestehen (siehe Tab "Bewertung" Abschnitt "Expert Review" im Kurseditor).

#### → _Gesamtpunktzahl und –beurteilung_

Nun können wir die Gesamtqualität des Expert Review bewerten.
  
  * Die Gesamtpunktzahl ist die Summe der einzelnen endgültigen Punktzahlen.
  * Und die Gesamtbeurteilung basiert auf den Werten der einzelnen endgültigen Beurteilungen.

### Gesamtbewertung

In der **Bewertungsstrategie** wird definiert, wie gross den **Anteil** jedes
aktivierten Schritts ist.  
Die Gesamtpunktzahlen der einzelnen aktivierten Schritte werden umgerechnet
und die Summe ist die **Endnote**.
