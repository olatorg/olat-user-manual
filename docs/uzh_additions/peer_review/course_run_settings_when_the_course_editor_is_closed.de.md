# Kursrun: Einstellungen bei geschlossenem Editor

Nachdem Sie alles konfiguriert haben, können Sie im Kursrun den ganzen Prozess
verfolgen.

## Tab "Alle Teilnehmer"

### Übersicht über den Status der Teilnehmer

Im Kursun haben Sie als Kursbesitzer immer eine Übersicht über den Status der
einzelnen Kursteilnehmer. Falls die Bewertung aktiviert ist, können Sie auch
ein paar wichtige Bewertungsdaten überprüfen.
Verwenden Sie das Zahnrad-Icon um bestimmte Spalten anzuzeigen oder zu verbergen.

![](attachments/course_run_settings_when_the_course_editor_is_closed_de/DE_Uebersicht.png)

### Detailsicht eines Kursteilnehmers

In der Übersichtstabelle können Sie als Kursbesitzer oder -betreuer auf
"Details" klicken. So können Sie die Details zu einem einzelnen Kursteilnehmer
einsehen und den entsprechenden Status überprüfen.  
Falls die Bewertung konfiguriert ist und die Bewertungsdaten schon vorhanden
sind, können hier auch die Details der Bewertung eingesehen werden.

![](attachments/course_run_settings_when_the_course_editor_is_closed_de/Peer_Review_-_Detail_Teilnehmer.png)

## Tab "Expert Review"
Falls der Workflow-Schritt "Expert Review" aktiviert ist, wird dieser Tab sichtbar. Hier können die Experten die Expert Reviews für die ausgewählten Arbeiten durchführen oder den Status ihrer Expert Reviews überprüfen.

## Tab "Verwaltung"

### Abschnitt "Kursbaustein 'Aufgabe'"

Weil der Kursbaustein "Peer Review" immer einen Kursbaustein "Aufgabe" mit
einer absoluten Abgabefrist braucht, können Sie hier diese Infos überprüfen.

### Abschnitt "Kalibrierung"

Falls die Kalibrierung aktiviert ist, müssen Kalibrierungsaufgaben hinzugefügt
werden. Im Tab "Verwaltung" können Sie zuerst die konfigurierte Anzahl
Kalibrierungsaufgaben einstellen.  
Beachten Sie, dass Sie vorher schon die Review Kriterien hinzugefügt haben,
weil die Kalibrierungsaufgaben auch die Review Kriterien verwenden.

![](attachments/course_run_settings_when_the_course_editor_is_closed_de/Peer_Review_-_Kalibrierungsaufgaben_-_DE.png)

Sie können Dateien erstellen oder bestehende Dateien hochladen.  
Pro Kalibrierungsaufgabe werden die Review Fragen mit den entsprechenden
Antwortmöglichkeiten angezeigt.  
Wählen Sie nun pro Review Frage die korrekte Antwort aus.

![](attachments/course_run_settings_when_the_course_editor_is_closed_de/Peer_Review_-_Detail_Kalibrierungsaufgabe_-_DE.png)

Sobald alle Kalibrierungsaufgaben erstellt sind, können diese endgültig
bestätigt werden.  
Ab diesem Zeitpunkt können diese nicht mehr geändert werden, und falls es kein
Startdatum für die Kalibrierung gibt, können die Kursteilnehmer auch mit der
Kalibrierung anfangen.

!!! warning ""
    Beachten Sie, dass die Dateinamen der Kalibrierungsaufgaben für die
    Teilnehmende sichtbar sind.
  

### Abschnitt "Zuweisung des Peer Reviews"

Nach einer erfolgreichen Abgabe der Arbeiten können Sie im Tab "Zuweisung" die
zufällige und anonyme Zuweisung für die Peer Reviews durchführen.  
Hierfür brauchen Sie eine absolute Abgabefrist, die in der Vergangenheit
liegt, und im Minimum die Anzahl konfigurierte Peer Reviews plus eins.

![](attachments/course_run_settings_when_the_course_editor_is_closed_de/Peer_Review_-_Zuweisung_-_DE.png)

Klicken Sie auf den Knopf "Peer Review zuweisen", um die Zuweisung
durchzuführen.  
Nach einer erfolgreichen Zuweisung wird eine Zuweisungstabelle angezeigt.
Diese ist nur für die Kursbesitzer des Kurses sichtbar.

![](attachments/course_run_settings_when_the_course_editor_is_closed_de/Peer_Review_-_Zuweisung_durchgefuhrt_-_DE.png)

## Tab "Bewertung"

Sobald die Fristen abgelaufen sind und die Bewertung aktiviert ist, kann man
die **Bewertung abschliessen**.  

Hier haben Sie 2 Optionen:

1. **Bewertung abschliessen und freigeben**: Die Bewertung wird abgeschlossen und sofort für die Teilnehmer sichtbar.
2. **Bewertung ohne Freigabe abschliessen**: Die Bewertung wird abgeschlossen, aber noch nicht für die Teilnehmer freigegeben. Mit dem Knopf "Bewertung freigeben" können Sie die Bewertung zu einem späteren Zeitpunkt freigeben.

Nach dem Knopfdruck wird alles berechnet, und Änderungen können nicht länger
vorgenommen werden.

In der Detailsicht des Kursteilnehmers können Sie die Bewertung überprüfen.
