# Didactical concept

**By means of the course element "Peer Review", participants can learn to assess a essay firstly (workflow step "Calibration"). Then, their own submitted work will be reviewed by one or more peers (workflow step "Peer Review"). Optionally, they can also assess their own submitted work (workflow step "Self Review"), likewise experts have the possibility of reviewing the participants' work (workflow step "Expert Review"). At the end, a grade for the reviews can be calculated (grading).**

The purpose of **Calibration** is that participants learn how to assess a essay (and thus are enabled to write better essays).  
They are given one or more sample essays (calibration tasks). Applying one or more review questions (review criteria), they undertake the assessment.  
When the calibration is finished, they receive a feedback. If configured, the calibration tasks can be repeated up to two times.

In the step "**Peer Review**", the course participants no longer assess sample essays, but three essays written by their peers.  
The peers' essays are assigned randomly and anonymously, and the same review questions (review criteria) are used to assess these essays.  
Additionally, there is a setting option that requires the reviewers to provide an explanation for their review answers.

In the step **"Self Review "**, the participants now assess their own work using the same review questions. In this way, they learn to reflect on their work critically.

Finally, the work can also be assessed by one or more experts (step **"Expert Review "**). Again, the same review questions are used for the assessment, if desired with the option that the reviewers have to provide an explanation for their review answers.

After completing the peer reviews, participants can have a look at the **feedback on their own essay**.  
If configured, a **final grade** is calculated for the whole peer review.

!!! info "More information on the topic feedback:"
    [Teaching Tools](https://teachingtools.uzh.ch/en/tools/feedback-geben){:target="\_blank"}

!!! note "Application example for the course element Peer Review:"
    [Democourse with Peer Review on lms.uzh.ch (in german)](https://lms.uzh.ch/url/RepositoryEntry/17425989659){:target="\_blank"}
    To get access to the configuration of the Peer Review course element in the demo course, OLAT users with author rights can request course membership at the [OLAT Support](../https://www.zi.uzh.ch/en/support/e-learning-and-examination/staff/olat.html/).

