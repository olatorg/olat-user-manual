# Didaktisches Konzept

 **Mit dem Kursbaustein "Peer Review" können die Teilnehmenden zuerst lernen, eine Arbeit zu beurteilen (Workflow-Schritt "Kalibrierung"). Dann wird ihre eigene abgegebene Arbeit durch mehrere Peers beurteilt (Workflow- Schritt "Peer Review"). Optional können sie ihre eigene abgegebene Arbeit auch selbst beurteilen (Workflow-Schritt "Self Review"), ebenso haben auch Experten die Möglichkeit, Arbeiten zu beurteilen (Workflow-Schritt "Expert Review"). Am Ende kann eine Note für die Reviews berechnet werden (Bewertung).**

Das Ziel der **Kalibrierung** ist, dass Teilnehmende lernen, eine Arbeit zu
beurteilen (und so auch eine bessere Arbeit verfassen können).  
Sie bekommen eine oder mehrere Beispielsarbeiten (Kalibrierungsaufgaben) und
anhand einer oder mehrerer Review Fragen (Review Kriterien) machen sie die
Beurteilung.  
Am Ende der Kalibrierung bekommen sie ein Feedback. Falls konfiguriert, können
die Kalibrierungsaufgaben bis zu zweimal wiederholt werden.

Im Schritt "**Peer Review**" beurteilen die Kursteilnehmende keine
Beispielsarbeiten mehr, sondern 3 Arbeiten ihrer Peers.  
Die Zuweisung der Arbeiten der Peers ist zufällig und anonym, und anhand der
gleichen Review Fragen (Review Kriterien) wird die Arbeit der Peers beurteilt.  
Zusätzlich gibt es eine Einstellungsmöglichkeit, dass der Reviewer seine
Beurteilung begründen muss.

Nach der Erledigung der Peer Reviews kann man das **Feedback zur eigenen
Arbeit einsehen**.  

Im Schritt **"Self Review"** beurteilen die Teilnehmenden anhand der gleichen Review Fragen nun ihre eigene Arbeit. So lernen sie, kritisch zu reflektieren.
Zum Schluss kann die Arbeit auch durch einen oder mehrere Experten beurteilt werden (Schritt **"Expert Review"**). Auch hier werden für die Beurteilung die gleichen Review Fragen verwendet, auf Wunsch mit der Option, dass der Reviewer seine Beurteilung begründen muss.

Falls konfiguriert, wird eine **Endnote** für das Peer Review berechnet.

!!! info "Weitere Informationen zum Thema Feedback:"
    [Teaching Tools](https://teachingtools.uzh.ch/de/tools/feedback-geben){:target="\_blank"}

!!! note "Anwendungsbeispiel zum Kursbaustein Peer Review:"
    [Demokurs mit Peer Review auf lms.uzh.ch](https://lms.uzh.ch/url/RepositoryEntry/17425989659){:target="\_blank"}
    Um Zugang zur Konfiguration des Peer Review Kursbausteins im Demokurs zu erhalten, können OLAT-Benutzer mit Autorenrechten die Kursmitgliedschaft beim [OLAT-Support](https://www.zi.uzh.ch/de/support/e-learning-and-examination/staff/olat.html) beantragen.
