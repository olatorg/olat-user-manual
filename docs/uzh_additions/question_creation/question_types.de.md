# Fragetypen

## Fragetyp “Single Choice"

### Beschreibung

Bei einer Single Choice Frage gibt es genau eine korrekte Antwort. Die Teilnehmer*innen wählen aus einer Liste von Antwortmöglichkeiten diejenige aus, die ihrer Meinung nach korrekt ist.

### Worauf Sie achten müssen

* Formulieren Sie die Frage klar und eindeutig.
* Stellen Sie sicher, dass nur eine Antwortmöglichkeit eindeutig korrekt ist.
* Vermeiden Sie irreführende oder zu ähnliche Antwortmöglichkeiten.

### Beispiel

* Frage: Was ist die Hauptstadt der Schweiz?
* Antwortmöglichkeiten:
    - Basel
    - Bern (korrekte Antwort)
    - Genf
    - Zürich

## Fragetyp “Multiple Choice"

### Beschreibung

Bei einer Multiple Choice Frage gibt es mehrere korrekte Antworten. Die Teilnehmer*innen können eine oder mehrere Antwortmöglichkeiten auswählen.

### Worauf Sie achten müssen

* Geben Sie in der Fragestellung deutlich an, dass mehrere Antworten korrekt sein können.
* Gestalten Sie die Antwortmöglichkeiten so, dass sie weder zu offensichtlich noch verwirrend sind.
* Überprüfen Sie, dass alle korrekten Antworten gleichwertig und logisch nachvollziehbar sind.

### Beispiel

* Frage: Welche dieser Sprachen sind Amtssprachen der Schweiz?
* Antwortmöglichkeiten:
    - Deutsch (richtig)
    - Italienisch (richtig)
    - Englisch
    - Französisch (richtig)

## Fragetyp “KPRIM”

### Beschreibung

Bei einer KPRIM Frage (richtig/falsch-Prinzip) werden mehrere Aussagen vorgegeben, die jeweils als “richtig” oder “falsch” bewertet werden müssen.

### Worauf Sie achten müssen

* Formulieren Sie eindeutige und kurze Aussagen.
* Um Missverständnisse zu vermeiden, sollte jede Aussage eindeutig entweder als "richtig” oder “falsch” erkennbar sein.

### Beispiel 

* Frage: Beurteilen Sie die folgenden Aussagen zur Schweiz.
* Aussagen:
    - Die Schweiz hat vier Amtssprachen. (richtig)
    - Der höchste Berg der Schweiz ist das Matterhorn. (falsch)
    - Zürich ist die bevölkerungsreichste Stadt der Schweiz. (richtig)
    - Der Rhein fliesst durch die Schweiz. (richtig)

## Fragetyp “Freitext"

### Beschreibung

Bei einer Freitextfrage wird den Teilnehmenden eine offene Frage gestellt, auf die sie eine selbst formulierte Antwort geben müssen. Diese Antworten sind nicht vorgegeben und können in Länge und Inhalt variieren.

### Worauf Sie achten müssen

* Formulieren Sie die Frage klar und präzise, damit die erwartete Antwortrichtung verständlich ist.
* Vermeiden Sie mehrdeutige oder zu allgemeine Fragen, um präzisere Antworten zu erhalten.
* Erstellen Sie eine Musterlösung, die als Referenz für die Bewertung der Antworten dient. Die Musterlösung sollte eine vollständige und präzise Antwort enthalten, die die wichtigsten Aspekte der Frage abdeckt.

### Beispiel

* Frage: Welche Bedeutung hat die direkte Demokratie in der Schweiz?
* Mögliche Antwort: Die direkte Demokratie ermöglicht es den Bürgerinnen und Bürgern, durch Volksabstimmungen und Initiativen direkt über wichtige politische Fragen zu entscheiden.
* Musterlösung: Die direkte Demokratie in der Schweiz gibt den Bürgerinnen und Bürgern eine zentrale Rolle in der politischen Entscheidungsfindung. Durch Volksinitiativen können sie Änderungen der Verfassung vorschlagen, und durch Referenden können sie über bereits beschlossene Gesetze abstimmen. Dieses System fördert die politische Mitbestimmung und sorgt dafür, dass wichtige Entscheidungen breit abgestützt sind.