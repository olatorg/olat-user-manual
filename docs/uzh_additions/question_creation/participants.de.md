# Im Kursrun (für Kursteilnehmer\*innen)

Im Kursrun haben die Kursteilnehmer\*innen die Möglichkeit, die Aufgabe zu erledigen (d.h. eine Frage zu erfassen) und diese gegenseitig in einem Peer Review zu beurteilen. Nach dem Peer Review kann das Feedback zur eigenen Frage eingesehen werden.

## Schritt “Aufgabe"

Wenn das Startdatum der Aufgabe (falls konfiguriert) verstrichen ist, kann die Aufgabe hier eingesehen werden. Die verfügbaren Fragetypen sind unter der Beschreibung der Aufgabe aufgelistet.

<figure><img src="../attachments/FE_Kursrun_Aufgabe.png"/><figcaption>
Aufgabe</figcaption></figure>

## Schritt “Abgabe"

Im Schritt “Abgabe” können die Kursteilnehmer\*innen eine Frage erfassen.

### Schritt 1: Fragetyp auswählen, Frage erstellen und Antwortmöglichkeiten hinzufügen

Zuerst überlegen die Kursteilnehmer\*innen, welchen Fragetyp sie verwenden möchten. Aktuell stehen 4 Fragetypen zur Verfügung:

* [Single Choice](../question_types/#fragetyp-single-choice)
* [Multiple Choice](../question_types/#fragetyp-multiple-choice)
* [KPRIM](../question_types/#fragetyp-kprim)
* [Freitext](../question_types/#fragetyp-freitext)

<figure><img src="../attachments/FE_Kursrun_Abgabe.png"/><figcaption>
Abgabe</figcaption></figure>

Nachdem Sie den gewünschten Fragetyp ausgewählt haben, klicken Sie auf “Zu erledigen". Nun können Sie die Frage erfassen und die Antwortmöglichkeiten hinzufügen.
Wenn Sie den Fragetyp ändern möchten, nachdem Sie mit der Erstellung einer Frage begonnen haben, können Sie die bereits erfasste Frage zurücksetzen. Die Frage wird so komplett gelöscht und Sie können von vorne beginnen.

### Schritt 2: Die Abgabe endgültig bestätigen

Sobald Sie die Frage komplett erfasst haben, können Sie die Abgabe der Frage endgültig bestätigen.

<figure><img src="../attachments/FE_Kursrun_PeerReview.png"/><figcaption>
Peer Review</figcaption></figure>

## Schritt “Peer Review”

Im Schritt “Peer Review” führen die Kursteilnehmer*innen das Peer Review durch.

### Schritt 1 : Peer Review durchführen

Hier werden die durchzuführenden Peer Reviews mit ihrem jeweiligen Status aufgelistet. Klicken Sie auf “Zu erledigen”, um das Peer Review einzusehen und durchzuführen.

1. __Frage beantworten__: Zuerst lesen und beantworten Sie die Frage. Nach dem Speichern der Antwort wird angezeigt welche Antworten die Fragensteller\*in als richtig definiert hat.
2. __Peer Review durchführen__: Nach dem Beantworten der Frage kann die Frage beurteilt werden. Die Likert-Skala ermöglicht es Ihnen, Ihre Meinung in abgestuften Kategorien von “stimme überhaupt nicht zu” bis “stimme völlig zu” auszudrücken. Begründen Sie Ihre Beurteilung im Freitextfeld.

<figure><img src="../attachments/FE_Kursrun_PeerReviewDurchfuehren.png"/><figcaption>
Peer Review durchführen</figcaption></figure>

### Schritt 2: Die Abgabe endgültig bestätigen

Sobald Sie alle Peer Reviews abgeschlossen haben, können Sie die Abgabe der Peer Reviews endgültig bestätigen.

## Schritt “Feedback"

Sobald Sie die Peer Reviews durchgeführt und abgeschlossen haben, können Sie das bereits vorhandene Feedback zu der von Ihnen erstellten Frage einsehen.

<figure><img src="../attachments/FE_Kursrun_Feedback.png"/><figcaption>
Feedback</figcaption></figure>