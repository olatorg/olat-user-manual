# In the course editor (for course owners)

In the course editor, the tabs can be used for the central settings of the module.

## Tab “Title and description”

Here you can enter a title and description for the course element.
The description you add will be visible to students during the course.

!!! info "For more information, see the chapter "[Title and description](../../../manual_user/learningresources/General_Configuration_of_Course_Elements/#desc)"."

## Tab “Layout"

Here you can change the layout options.

!!! info "For more information, see the chapter "[Layout](../../../manual_user/learningresources/General_Configuration_of_Course_Elements/#tab-layout)"."

## Tab “Visibility" (only for conventional courses)

Changing the visibility of a course element is one of three ways to restrict access to course elements.
If you restrict the visibility of a course element, it will no longer appear in the course navigation.

!!! info "For more information, see the chapter "[Visibility](../../../manual_user/learningresources/General_Configuration_of_Course_Elements/#access)"."

## Tab “Access” (only for conventional courses)

In the tab “Access” of the course element you can define who has access to this element.

!!! info "For more information, see the chapter "[Access](../../../manual_user/learningresources/General_Configuration_of_Course_Elements/#access)"."

## Tab “Learning path” (only for learning path courses)

In case of a [learning path course](../../../manual_user/learningresources/Learning_path_course_Course_editor/), you can define in the tab “Learning path” whether the execution of the course element is obligatory or voluntary. Furthermore, you can define a time period during which the course element can be processed.

Each course element has individual completion criteria, an overview of which can be found in the "[Learning path](../../../manual_user/learningresources/Learning_path_course/)" section.

!!! info "For more information, see the chapter "[Learning path](../../../manual_user/learningresources/Learning_path_course/)"."

!!! warning "Conventional courses do not have the "Learning path" Tab and instead have the "[Visibility](../../../manual_user/learningresources/General_Configuration_of_Course_Elements/#access)" und "[Access](../../../manual_user/learningresources/General_Configuration_of_Course_Elements/#access)" tabs including [export mode](../access_restrictions_in_expert_mode/)."

## Tab “Creation of the question”
<figure><img src="../attachments/FE_courseEditor_CreationOfTheQuestion.png" /><figcaption>Tab "Creation of the question"</figcaption></figure>


### Section “Task definition”

As the course participants can enter a question, an assignment must be added by the course owner.

* __Title__: Add a title to the assignment. This title will be used as the question title by default (but can be changed later).
* __Description__: Explain the course participants what the assignment is about.
* __Start date__: This is an optional start date. The assignment will only be visible to the course participants after this date.
* __Submission deadline__: Enter the deadline for submitting the assignment (creation of the question).
* __QTI question types__: You can select one or more of the available QTI question types. Course participants can use one of the selected QTI selection types for their question.

!!! info "More information about [the different types of QTI questions](question_types.md) can be found in the corresponding chapter."

## Tab "Peer review of the question"

<figure><img src="../attachments/FE_courseEditor_PeerReviewOfTheQuestion.png" /><figcaption>
Tab "Peer review of the question"</figcaption></figure>

Once a question has been created it can be peer reviewed by other course participants. Therefore this step has to be configured by the course owner.

### Section “Deadline”

* __Peer review deadline__: Enter the deadline for completion of the peer review.

### Section “Review questions”

In the “Peer Review” step, peers evaluate a created question. They do this anonymously using the provided review questions. Each question uses the Likert scale with 5 possible answers.

Add the necessary review question as course owner.

!!! info "__Tips for clear, focused and balanced review questions__
* __Clear wording__: Questions should be understandable, precise and specific to avoid misunderstandings. Use language that students are familiar with and make sure the question is unambiguous.
* __Focused__: Questions must support the learning objectives and focus on relevant aspects of the task being assessed.
* __Balanced__: Cover different dimensions of the task (e.g. content, structure, creativity) without overwhelming peers with too many questions."

### Section “Assignment of the peer reviews"

* __Number of peer reviews__: Specify how many peer reviews each course participant should conduct.