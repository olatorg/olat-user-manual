# In the course run (for course participants)

In the course run, course participants have the opportunity to complete the task (in other words to create a question) and to evaluate it in a peer review. After the peer review they can view the feedback on their own question.

## Step “Task”

If the start date for the task (if configured) has passed, the task can be viewed here. The available question types are listed below the task description.

<figure><img src="../attachments/FE_courseRun_task.png"/><figcaption>
Task</figcaption></figure>

## Step “Submission”

In the “Submission” step, course participants can enter a question.

### Step 1: Select question type, create question and add answer options

The first step for course participants is to decide which question type they would like to use. There are currently 4 question types available:

* [Single choice](../question_types/#question-type-single-choice)
* [Multiple choice](../question_types/#question-type-multiple-choice)
* [Kprim](../question_types/#question-type-kprim)
* [Essay](../question_types/#question-type-essay)

<figure><img src="../attachments/FE_courseRun_submission.png"/><figcaption>
Task</figcaption></figure>

Once you have selected the question type, click on “To complete". You can now enter the question and add the answer options.
If you want to change the question type after you have started creating a question, you can reset the question you have already created. The question will be completely deleted and you can start again.

### Step 2: Final confirmation of the submission

Once you have entered the question completely, you can final confirm the submission of the question.

## Step “Peer review”

In the “Peer review” step, course participants conduct the peer review.

<figure><img src="../attachments/FE_courseRun_PeerReview.png"/><figcaption>
Peer Review</figcaption></figure>

### Step 1: Complete the peer review

The pending peer reviews with their status are listed here. Click on “To complete” to view and complete the peer review.

1. __Answer the question__: Read and answer the question first. Once you have submitted your answer, you will see which answer(s) the questioner has defined as correct.
2. __Conduct the peer review__: After answering the question you can evaluate it. The Likert scale allows you to express your opinion in graded categories from “strongly disagree” to “strongly agree". Use the text field to explain and justify your answer.

<figure><img src="../attachments/FE_courseRun_conductPeerReview.png"/><figcaption>
Conduct the peer review</figcaption></figure>

### Step 2: Final confirmation of the submission

Once you have completed all the peer reviews, you can final confirm the submission of the peer reviews.

### Step “Feedback”

Once you have conducted and completed the peer reviews, you can view the existing feedback for the question you have created.

<figure><img src="../attachments/FE_courseRun_feedback.png"/><figcaption>
Feedback</figcaption></figure>