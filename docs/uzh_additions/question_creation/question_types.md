# Question types

## Question type “Single choice"

### Description

A single choice question type has exactly __one correct answer__. Participants select the answer they think is correct from a list of possible answers.

### What to consider

* Make the question clear and unambiguous.
* Make sure that only one answer is clearly correct.
* Avoid misleading or similar answer options.

### An example

* Question: What is the capital of Switzerland?
* Possible answers:
    - Basel
    - Bern (correct)
    - Geneva
    - Zurich

## Question type “Multiple choice"

### Description

A multiple choice question has __more than one correct answer__. Participants can choose one or more of the possible answers.

### What to consider

* Make it clear in the question that more than one answer can be correct.
* Design the possible answers so that they are neither too obvious nor confusing.
* Check that all correct answers are equally valid and logical.

### An example

* Question: Which of these languages are official languages of Switzerland?
* Possible answers:
    - German (correct)
    - Italian (correct)
    - English
    - French (correct)

## Question type "KPRIM”

### Description

In a KPRIM question (true/false principle), a number of statements are given. Each statement must be rated as __“true” or “false"__.

### What to consider

* Formulate clear and concise statements.
* To avoid misunderstandings, each statement should be clearly identifiable as either “true” or “false".

### An example

* Question: Evaluate the following statements about Switzerland.
* Statements:
    - Switzerland has four official languages. (true)
    - The highest mountain in Switzerland is the Matterhorn. (false)
    - Zurich ist the most populated city in Switzerland. (true)
    - The Rhine flows through Switzerland. (true)

## Question type “Essay"

### Description

In an open-ended question, participants are presented with an open-ended question to which they must formulate their own answer. These answers are not predetermined and can vary in length and content.

### What to consider

* Formulate the question clearly and precisely so that the expected direction of the answer is clear.
* Avoid ambiguous or overly general questions to get more precise answers.
* Prepare a sample solution to use as a reference for evaluating the answers. This should include a complete and accurate answer that covers the most important aspects of the question.

### An example:

* Question: What is the importance of direct democracy in Switzerland?
* Possible answer: Direct democracy allows citizens to decide on important political issues through referendums and initiatives.
* Sample solution: Direct democracy in Switzerland gives citizens a central role in political decision-making. Through popular initiatives they can propose changes to the constitution, and through referendums they can vote on laws that have already been passed. This system encourages political participation and ensures that important decisions are widely supported.