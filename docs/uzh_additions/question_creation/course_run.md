# In the course run (for course owners and coaches)

In the course run, you can see which step each course participant is in.

## Tab “Participants”

The participants are listed here with their corresponding step in the process.
Course owners and coaches with author rights can export created questions to the question bank.

!!! info "Only questions that have been approved for use by the creator can be exported to the question bank."

Click on a student’s name to see their details.

<figure><img src="../attachments/FE_courseRun_participants.png"/><figcaption>
Tab "Participants"</figcaption></figure>

## Tab “Administration”

### Section “Task"

This displays the pre-defined task description with the available QTI question types.

### Section “Assignment of the peer reviews”

Before the “Peer review” step can be started the course owner must assign the peer reviews.
The assignment is random and only applies to assignments submitted on time. After the assignment has been made course participants can start the peer reviews.

The assignment of peer reviews can only take place if:

1. the deadline for submitting assignments has passed, and
2. there are enough submitted assignments (= the number of configured peer reviews + 1).

Once these conditions are met, the course owner can assign the peer reviews. This assignment cannot be changed afterwards.