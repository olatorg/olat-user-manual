# Im Kursrun (für Kursbesitzer\*innen und -betreuer\*innen)

Im Kursrun können Sie überprüfen, in welchem Schritt sich die einzelnen Teilnehmer*innen befinden.

## Tab "Teilnehmer\*innen”

Hier werden die Teilnehmer\*innen mit ihrem entsprechenden Schritt im Ablauf aufgelistet.
Kursbesitzer\*innen und -betreuer\*innen mit Autorenrechte haben die Möglichkeit, erfasste Fragen in den Fragenpool zu exportieren.

!!! info "Nur Fragen, die die Erlaubnis der Erfasser\*innen zur Verwendung erhalten haben, können in den Fragenpool exportiert werden."

Klicken Sie auf den Namen einer Teilnehmer\*in um die Detailansicht zu überprüfen.

<figure><img src="../attachments/FE_Kursrun_Teilnehmerinnen.png"/><figcaption>
Tab "Teilnehmer\*innen"</figcaption></figure>

## Tab “Verwaltung”

### Abschnitt “Aufgabe"

Hier wird die schon definierte Beschreibung der Aufgabe mit den verfügbaren QTI-Fragetypen angezeigt.

### Abschnitt “Zuweisung der Peer Reviews”

Bevor der Schritt “Peer Review” gestartet werden kann, muss die Kursbesitzer\*in die Zuweisung der Peer Reviews durchführen.
Die Zuweisung ist zufällig und gilt nur für die rechtzeitig abgegebenen Abgaben der Aufgabe. Nach der Zuweisung können die Kursteilnehmer\*innen mit den Peer Reviews anfangen.

Die Zuweisung der Peer Reviews kann nur stattfinden, wenn:

1. die Abgabefrist (der Erstellung der Frage) abgelaufen ist, und
2. es genug abgegebenen Aufgaben gibt (= die Anzahl konfigurierte Peer Reviews + 1)

Sobald die Bedingungen erfüllt sind, kann die Kursbesitzer\*in die Zuweisung der Peer Reviews vornehmen. Diese Zuweisung kann später nicht mehr geändert werden.