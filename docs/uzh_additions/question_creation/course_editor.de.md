# Im Kurseditor (für Kursbesitzer*innen)

Im Kurseditor können die Tabs für die zentralen Einstellungen des Bausteins verwendet werden.

## Tab “Titel und Beschreibung”

Hier können Sie einen Titel und eine Beschreibung des Kursbausteins eingeben.
Die hinzugefügte Beschreibung ist für die Kursteilnehmer*innen im Kursrun sichtbar.

!!! info "Weitere Informationen finden Sie im Kapitel "[Titel und Beschreibung](../../../manual_user/learningresources/General_Configuration_of_Course_Elements/#desc)".

## Tab “Layout"

Hier können Sie die Layout-Optionen ändern.

!!! info "Weitere Informationen finden Sie im Kapitel "[Layout](../../../manual_user/learningresources/General_Configuration_of_Course_Elements/#tab-layout)".

## Tab “Sichtbarkeit" (nur bei herkömmlichen Kursen)

Die Sichtbarkeit eines Kursbausteins zu ändern, ist eine von drei Arten, um den Zugriff auf Kursbausteine einzuschränken.
Wenn Sie die Sichtbarkeit eines Kursbausteins einschränken, erscheint dieser nicht mehr in der Kursnavigation.

!!! info "Weitere Informationen finden Sie im Kapitel "[Sichtbarkeit und Zugang](../../../manual_user/learningresources/General_Configuration_of_Course_Elements/#access)".

## Tab “Zugang” (nur bei herkömmlichen Kursen)

Im Tab "Zugang" des Kursbausteins können Sie definieren, wer Zugang zu diesem Baustein hat.

!!! info "Weitere Informationen finden Sie im Kapitel "[Sichtbarkeit und Zugang](../../../manual_user/learningresources/General_Configuration_of_Course_Elements/#access)".

## Tab “Lernpfad” (nur bei Lernpfadkursen)

In Ihrem [Lernpfadkurs](../../../manual_user/learningresources/Learning_path_course_Course_editor/) definieren Sie im Tab "Lernpfad", ob die Bearbeitung des Kursbausteins obligatorisch oder freiwillig ist. Ferner können Sie einen Zeitraum festlegen, in dem der Kursbaustein bearbeitet werden kann.

Jeder Kursbaustein verfügt über individuelle Erledigungskriterien, eine Übersicht finden Sie im Kapitel "Lernpfad".

!!! info "Weitere Informationen finden Sie im Kapitel "[Lernpfad](../../../manual_user/learningresources/Learning_path_course/)"."

!!! warning "Herkömmliche Kurse verfügen nicht über den Tab "Lernpfad" und haben stattdessen die Tabs "[Sichtbarkeit](../../../manual_user/learningresources/General_Configuration_of_Course_Elements/#access)" und "[Zugang](../../../manual_user/learningresources/General_Configuration_of_Course_Elements/#access)" inklusive [Expertenmodus](../access_restrictions_in_expert_mode/).

## Tab “Erstellung der Frage”

<figure><img src="../attachments/FE_Kurseditor_ErstellungDerFrage.png"/><figcaption>
Tab "Erstellung der Frage"</figcaption></figure>

### Abschnitt “Aufgabenstellung”

Da die Kursteilnehmer\*innen eine Frage erfassen können, muss eine Aufgabe durch die Kursbesitzer\*innen hinzugefügt werden.

* __Titel__: Fügen Sie einen Titel der Aufgabe hinzu. Dieser Titel wird standardmässig als Titel der Frage übernommen (kann jedoch später noch geändert werden).
* __Beschreibung__: Erklären Sie wie die Aufgabe für die Kursteilnehmer\*innen aussieht.
* __Startdatum__: Optional kann ein Startdatum eingegeben werden. Erst nach dem Startdatum ist die Aufgabe für die Kursteilnehmer\*innen sichtbar.
* __Abgabe bis …__: Hier geben Sie die Frist für die Abgabe der Aufgabe (Erstellung der Frage) ein.
* __QTI-Fragetypen__: Aus den verfügbaren QTI-Fragetypen können Sie einen oder mehrere auswählen. Kursteilnehmer\*innen können einen der ausgewählten QTI-Fragetypen für ihre Frage verwenden.

!!! info "Weitere Infos zu den [QTI-Fragetypen](question_types.de.md) können Sie im entsprechenden Kapitel finden."

## Tab “Peer Review der Frage”

<figure><img src="../attachments/FE_Kurseditor_PeerReviewDerFrage.png"/><figcaption>
Tab "Peer review der Frage"</figcaption></figure>

Da eine erfasste Frage durch die Kursteilnehmer\*innen in einem Peer Review beurteilt werden kann, muss dieser Schritt durch die Kursbesitzer\*innen zuerst konfiguriert werden.

### Abschnitt “Frist”

* __Peer Review bis …__: Hier geben Sie die Frist für das erledigen der Peer Reviews ein.

### Abschnitt “Review Frage”

Während dem Schritt “Peer Review” beurteilen Peers eine erfasste Frage. Das machen sie anonym und anhand von Review Fragen. Jede Review Frage verwendet die Likert-Skala mit 5 Antwortmöglichkeiten.

Fügen Sie als Kursbesitzer\*in die gewünschte Review Fragen hinzu.

!!! info "Hinweise für klare, zielgerichtete und ausgewogene Review Fragen<
* __Klare Formulierung__: Die Fragen sollten verständlich, präzise und konkret sein, um Missverständnisse zu vermeiden. Verwenden Sie eine Sprache, die den Studierenden vertraut ist, und stellen Sie sicher, dass die Fragestellung eindeutig ist.
* __Zielgerichtet__: Die Fragen müssen die Lernziele unterstützen und auf relevante Aspekte der Aufgabe fokussieren, die bewertet werden sollen.
* __Ausgewogenheit__: Decken Sie verschiedene Dimensionen der Aufgabe ab (z. B. Inhalt, Struktur, Kreativität), ohne die Peers mit zu vielen Fragen zu überfordern."

### Abschnitt “Zuweisung der Peer Reviews”

* __Anzahl Peer Reviews__: Definieren Sie hier wie viele Peer Reviews jede Kursteilnehmer\*in durchführen soll.