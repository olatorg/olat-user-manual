# Course element “Question creation”

The course element “Question creation” promotes sustainable learning by focusing on continuous engagement with the learning material and its application.

As part of the course module, students develop their own questions on the subject, which are then reviewed by their peers. These questions are not only used to create individual, randomised exams, but also serve as valuable practice material for students. This collaborative process promotes a deeper understanding of the subject matter and motivates students to actively engage with the learning content.

For lecturers, the course module also provides valuable insights into student learning. It opens up the possibility of adapting teaching material in a targeted way and responding flexibility to learners’ needs.

The question creation process consists of two main steps:

1. __Creation of the question__: After the course owner has formulated the task, students create a question about the learning material. They define possible answers and mark what they consider to be the correct answer(s).
2. __Peer review of the question__: After the course owner has assigned the peer reviews, course participants conduct the peer review. They first answer the question and then rate the question and its answer options using a Likert scale. Once the peer review is complete, students can view the feedback they have received on their own question.
<br /><br />
<hr />

* [In the course editor (for course owners)](course_editor.md)
* [In the course run (for course owners and coaches)](course_run.md)
* [In the course run (for course participants)](participants.md)
* [Question types](question_types.md)