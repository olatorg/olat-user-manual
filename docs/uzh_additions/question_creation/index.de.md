# Kursbaustein "Fragenerstellung"

Der Kursbaustein “Fragenerstellung“ fördert nachhaltiges Lernen, indem er die kontinuierliche Auseinandersetzung mit dem Lernstoff und dessen Anwendung in den Mittelpunkt stellt.

Im Rahmen dieses Kursbausteins entwickeln die Studierenden eigenständig Fragen zum Lernstoff, welche anschliessend von ihren Peers beurteilt werden. Diese Fragen dienen nicht nur zur Erstellung individueller, randomisierter Prüfungen, sondern auch als wertvolles Übungsmaterial für die Studierenden. Dieser kollaborative Prozess fördert ein tieferes Verständnis des Lernstoffs und motiviert die Lernenden, sich aktiv mit dem Lerninhalt auseinanderzusetzen.

Für Dozierende bietet der Kursbaustein zudem wertvolle Einblicke in den Lernfortschritt der Studierenden. Er eröffnet die Möglichkeit, den Lehrstoff gezielt anzupassen und flexibel auf die Bedürfnisse der Lernenden einzugehen.

Der Prozess der Fragenerstellung besteht aus zwei Hauptschritten:

1. __Erstellung der Frage__: Nachdem die Kursbesitzer\*in die Aufgabe formuliert hat, erfassen die Kursteilnehmer\*innen eine Frage zum Lernstoff. Dabei definieren sie mögliche Antwortmöglichkeiten und markieren die ihrer Meinung nach richtige(n) Antwort(en).

2. __Peer Review der Frage__: Nach der Zuweisung der Peer Reviews durch die Kursbesitzer\*in führen die Kursteilnehmer\*innen das Peer Review durch. Dabei beantworten sie zunächst die erstellte Frage und beurteilen anschliessend die Frage und die zugehörigen Antwortmöglichkeiten anhand einer Likert-Skala. Nach Abschluss der Peer Reviews können die Teilnehmer*innen das erhaltene Feedback zu ihrer eigenen Frage einsehen.
<br /><br />
<hr/>

* [Im Kurseditor (für Kursbesitzer\*innen)](course_editor.de.md)
* [Im Kursrun (für Kursbesitzer\*innen und -betreuer\*innen)](course_run.de.md)
* [Im Kursrun (für Kursteilnehmer\*innen)](participants.de.md)
* [Fragetypen](question_types.de.md)