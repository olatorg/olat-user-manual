# Drei Schritte zu Ihrer Aufgabe

Mit der folgenden Anleitung haben Sie in kurzer Zeit Ihrem Kurs eine [Aufgabe](../../manual_user/learningresources/Assessment.de.md#course_element_task) oder [Gruppenaufgabe](../../manual_user/learningresources/Assessment.de.md#course_element_group_tasks) hinzugefügt.

!!! info "Voraussetzungen"

    * Vor dem Hinzufügen der Aufgabe sollten die Aufgabendateien und optional eine Musterlösung in einem gängigen Dateiformat vorliegen (beispielsweise als PDF-Dokumente). Sie können jedoch die Aufgabe und die Musterlösung auch direkt in OLAT erstellen.
    * Wenn Sie noch keinen Kurs erstellt haben, steht im Kapitel "[Kurs erstellen und bearbeiten](../../manual_user/learningresources/index.de.md)", wie Sie vorgehen müssen, bevor Sie mit Hilfe der folgenden Anleitung Ihre Aufgabe erstellen.

  * Schritt 1: Kurseditor öffnen und Aufgabenbaustein einfügen
  * Schritt 2: Aufgabe konfigurieren
  * Schritt 3: Kurs publizieren

## Schritt 1: Kurseditor öffnen und Aufgabenbaustein einfügen

  1. Im Autorenbereich unter "Meine Kurse" den zu bearbeitenden Kurs suchen und öffnen.
  2. Oben im Dropdown-Menü "Administration" auf "Kurseditor" klicken.
  3. Links im Kursmenü das Element, unter dem der Aufgabe-Kursbaustein eingefügt werden soll, durch Klicken auswählen.
  4. Oben die Schaltfläche "Kursbausteine einfügen" anklicken und unter "Wissensüberprüfung" den Kursbaustein "Aufgabe" oder "Gruppenaufgabe" auswählen.
  5. Im Tab "Titel und Beschreibung" kurzen Titel des Kursbausteins eingeben und speichern.

## Schritt 2: Aufgabe konfigurieren

  1. Im Tab **"Workflow"** können die gewünschten Teilschritte ausgewählt werden (Aufgabenstellung, Abgabe, Rückgabe/Feedback, Musterlösung, Bewertung) und gewisse grundlegende Einstellungen bereits gemacht werden (Abgabe bis... Datum). In der Gruppenaufgabe legen Sie hier zusätzlich noch fest für welche Gruppen diese Aufgabe gültig ist. Änderungen Speichern. Je nach ausgewählten Teilschritten sind die entsprechenden Konfigurations-Tabs aktiviert oder inaktiv (grau dargestellt). 

  ??? info "Zuweisung bis..."

    Ist die Option "Aufgabenstellung: Den Teilnehmer\*innen Aufgabenstellungen bereitstellen und zuweisen" ausgewählt, kann ein Datum **"Zuweisung bis ..."** eingetragen werden. 
    Teilnehmer\*innen können nur bis zum hier eingestellten Datum die Aufgabe annehmen (starten). Diese Einschränkung wird nur in seltenen Spezialfällen benötigt und das Datumsfeld kann im Normalfall leer gelassen werden. 
    Ab welchem Zeitpunkt die Aufgabenstellung sichtbar sein soll, kann über die Einstellungen im [Tab "Zugang"](../../manual_user/learningresources/General_Configuration_of_Course_Elements/#access) konfiguriert werden (Option: Datumsabhängig).

  2. a) Im Tab **"Aufgabenstellung“** können eine oder mehrere zuvor erstellte Aufgabendateien hochgeladen oder neu erstellt werden. Sie können festlegen, ob auch Kurs-Betreuer Aufgaben hochladen dürfen, ob Aufgaben automatisch zugewiesen oder von den Lernenden manuell gewählt werden, ob eine Aufgabe nur  einer Einzelperson bzw. einer einzelnen Gruppe zugeteilt wird oder ob dieselbe Aufgabe allen Lernenden/Gruppen zugewiesen wird (Standardfall: "Aufgabe wird mehreren Personen / Gruppe zugewiesen).
  Ferner kann eine Mitteilung für die Benutzer hinterlegt werden.<br />
  b) Bei Gruppenaufgaben müssen hier Gruppen oder Lernbereiche ausgewählt werden, für die diese Aufgabe zugänglich ist.

  3. Im Tab **"Abgabe"** festlegen, ob Dateien hochgeladen und/oder mit dem internen Editor erstellt werden können. Die im minimum verlante und maximal erlaubte Anzahl Dokumente kann dabei vorgegeben werden. Optional Bestätigungstext anpassen und E-Mailversand konfigurieren. Speichern.
  4. Im Tab **"Rückgabe und Feedback"** können Sie einstellen, wieviele Dateien maximal eingereicht werden können.  

  5. Im Tab **"Bewertung"** Bewertungsoptionen wählen. Möglich sind: "Punkte vergeben" (mit Minimal- und Maximalpunktzahl), "Bestanden/Nicht bestanden ausgeben" mit Option für "Art der Ausgabe" (automatisch oder manuell) und "Individueller Kommentar". Speichern.
  6. Im Tab **"Musterlösung"** entweder die zuvor erstellten(n) Musterlösung(en) hochladen oder direkt erstellen.
  7. Sofern bei der **"Bewertung"** Punkte vergeben wurden, steht auch der Tab "Highscore" zur Verfügung.
  Bestimmen Sie hier, ob ein "Gratulationstitel", ein "Siegertreppchen", ein "Histogramm" und/oder die Ranking-Liste angezeigt werden soll. Die Darstellung kann mit Namen oder anonym erfolgen.

## Schritt 3: Änderungen publizieren

  Oben rechts in der Toolbar "Publizieren" wählen (Wizard mit vollständigen Zugangseinstellungen) oder schnellpublizieren durch Anklicken des roten x-Symbols oben rechts ("Kurseditor schliessen") und Auswahl:"Automatisch Publizieren".

Die Aufgabe ist nun im Kurs eingebunden. Je nach Konfiguration des (Gruppen-)Aufgabenbausteins können Kursteilnehmer eine Aufgabe wählen und ihre Lösung abgeben. Kursautor\*innen und -betreuer\*innen können die abgegebenen Aufgaben der Kursteilnehmer\*innen einsehen und die korrigierten Dateien wieder zurückgeben.
Wenn die Überarbeitung aktiviert wurde, können Betreuer‹innen zusätzlich noch Revisionen von den Kursteilnehmenden verlangen. Die Bewertung kann entweder direkt im Kurselement oder über das [Bewertungswerkzeug](../../manual_user/learningresources/Assessment_tool_overview.de.md) erfolgen.

  
!!! tip "Tipp"
    Abonnieren Sie in der Kursansicht Benachrichtigungen über Änderungen im Kurselement. Wenn ein Kursteilnehmer etwas abgegeben hat, erfahren Sie das per E-Mail oder im persönlichen Menü unter "[Abonnements](../../manual_user/personal_menu/Personal_Tools.de.md)“. Die Benachrichtigung wird erst dann ausgelöst, wenn eine Aktion abgeschlossen ist.
    Alternativ können Sie auch die Einreichungen für alle bewertbaren Kursabusteine im Bewertungswerkzeug abonnieren.
