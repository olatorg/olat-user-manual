# SWITCHportfolio im OLAT

Viele Personen an der UZH nutzen mittlerweile SWITCHportfolio, um eine
Übersicht ihrer akademischen Leistungen zu erstellen.

Dank der LTI Schnittstelle ist das Zusammenspiel von SWITCHportfolio und OLAT
nun noch einfacher geworden. Um die Integration nahtlos zu gestalten, braucht
es im Kurs einen LTI Baustein. Dieser muss mit der URL zu SWITCHportfolio und
einem kryptischen Passwort und Schlüssel konfiguriert werden.

Folgende zusätzlichen Einstellungen müssen noch vorgenommen werden, damit die
Integration klappt:

  *  **Vorname/Name übertragen** Ja
  *  **E-Mail-Adresse übertragen** **Ja**
  *  **Anzeige** : Neues Fenster öffnen
  *  **Höhe Anzeigefläche** : Leer
  *  **Breite Anzeigefläche** : Leer

<video width="480" height="270" controls>
  <source src="../attachments/switchportfolio_with_olat_de/OLAT-
SWITCHportfolio-1.mp4" type="video/mp4">
</video>

Danach kann, wenn der Übertragung der Anmeldung und Anmeldedaten zugestimmt
wurde, SWITCHportfolio aus dem Kurs heraus verwendet werden.

 ** _Update 31.10.2019!_ Neu ist die Konfiguration von OLAT auch in der
[Dokumention von
Mahara](https://manual.mahara.org/de/19.10/external/lti_olat.html)
integriert.**

Für Fragen zur Konfiguration, oder um die notwendigen Schlüssel zu bekommen,
wenden Sie sich bitte an den [OLAT
Support.](https://www.zi.uzh.ch/de/support/e-learning-and-examination/staff/olat.html)

Weitere Informationen sind verfügbar unter:

[![](attachments/switchportfolio_with_olat_de/teachingtool.png)](https://teachingtools.uzh.ch/de/tools/lernportfolios-sinnvoll-einsetzen)



