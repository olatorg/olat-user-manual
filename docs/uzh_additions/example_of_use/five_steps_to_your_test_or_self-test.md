# Five steps to your test or self-test

This manual will help you to create a [test](../../manual_user/learningresources/Test.md) or [self-test](../../manual_user/learningresources/Assessment.md#course_element_self_test) for your course and share it with our participants.

## Step 1: Edit your course and insert a new course element "test / "self-test"

  1. Go to "Authoring" \- "My learning resources" and open the course in which you would like to add the test / self-test.
  2. Open the "Course editor" from "Administration" drop-down.
  3. Click to the element after which one you would like to insert the new test / self-test element.
  4. Click "[Insert course element](../../manual_user/learningresources/General_Configuration_of_Course_Elements.md)" and choose "[Test](../../manual_user/learningresources/Test.md)" or "[Self-test](../../manual_user/learningresources/Assessment.md#course_element_self_test)".
  5. Change the title in the tab "Title and description" and add a description if needed. 
  6. Click "Save".

## Step 2: Create a new test

  1. Go to the tab "Test configuration" and click "Choose, create or import file".
  2. Click "Create".
  3. Give a name to your new test and click "Create".

Your new test learning resource was successfully created and contains by
default one single choice question. If you do not use a single choice question
in your taste, you can delete the default question as soon as another question
was added.

## Step 3: Edit your test and add new questions (e.g. of the type "Multiple Choice")

  1. Click on "Edit" to open the test editor.
  2. Click "Add element" and choose a new question type (e.g. [multiple choice](../../manual_user/learningresources/Test_question_types.md#multiple-choice)).
  3. Position the new question by using drag & drop.
  4. In the tab "Choice" you can define the title of your new question.
  5. in the tab "Choice" you can enter the question into the "Question" input field.
  6. Depending on the number of available answers, you can add new answers using the plus sign.
  7. Define the correct answer using the checkbox. 
  8. Optionally you can change the score in the tab "Score".
  9. Optionally you can define feedback text in the tab "Feedback".

Add as many questions as needed to your test using the same procedure. Use
sections to structure the questions and parts. All question types are
explained in the chapter "[Learning resources Test (QTI 2.1)](../../manual_user/learningresources/index.md#test-qti-21)".

## Step 4: Save the test learning resource

  1. Leave the test editor (red cross upper right corner) and save all content that has not been saved yet.
  2. Go back to your course.

## Step 5: Publish your course

  1. Navigate back to your course.
  2. Click "Publish" in the upper right corner.
  3. Check if your new survey element ist listed and click "Next".
  4. Set your [course access ](../../manual_user/learningresources/Access_configuration.md)and [publication status](../../manual_user/learningresources/Access_configuration.md) using the publish-assistance and the following steps. 
  5. Click "Finish" when done.
  6. Leave the course editor. 

Your test / self-test is now published and can be accessed by the course
participants.

The test results can be found under "[Test statistics](../../manual_user/learningresources/Using_Course_Tools.md#test-statistics)", "[Assessment tool](../../manual_user/learningresources/Assessment_tool_overview.md)"
or "[Archive tool](../../manual_user/learningresources/Data_archiving.md)".

The Self-tests results are stored anonymously and will not be available in the
"[Assessment tool](../../manual_user/learningresources/Assessment_tool_overview.md)", only in "[Test
Statistics](../../manual_user/learningresources/Using_Course_Tools.md#test-statistics)" and "[Archive tool](../../manual_user/learningresources/Data_archiving.md)".
