# Examples of use

This chapter provides you with information on OLAT exceeding the mere description of single elements or functionalities, and rather describing a set of steps with a specific goal in mind.  

## Examples of use
  * [Three steps to your task](three_steps_to_your_task.md)
  * [Three steps to topic assignment](three_steps_to_topic_assignment.md)
  * [Automatically activate a questionnaire at the end of the course](automatically_activate_a_questionnaire_at_the_end_of_the_course.md)
  * [Create reminders in the course element "Task"](erinnerung_im_KB_Aufgabe.md)
  * [Flashcards with OLAT and H5P (German only)](flashcards_with_olat_and_h5p.md)
  * [Five steps to your content package](five_steps_to_your_content_package.md)
  * [Five steps to your test or self-test](five_steps_to_your_test_or_self-test.md)
  * [Five steps to your survey](five_steps_to_your_survey.md)
  * [Use MS-Teams in OLAT (German only)](ms_teams_with_olat.md)
  * [OLAT meets gather.town (German only)](gather_town_with_olat.md)
  * [Online exams](online_exams.md)
  * [Save data in OLAT (German only)](save_data_in_olat.md)
  * [SWITCHportfolio in OLAT (German only)](switchportfolio_with_olat.md)
  * [Comparison between the course elements "Page" and "HTML Page"](comparison_course_element_page_HTML_page.md)
  * [Comparison of the course element types "folder" and "task"](comparison_of_the_course_element_types_folder_and_task.md)
  * [Using video](using_video.md)
  * [Four steps to your blog](four_steps_to_your_blog.md)
  * [Four steps to your wiki](four_steps_to_your_wiki.md)

* * *

## Teaching Tools (Hochschuldidaktik - Center for University Teaching and Learning)

If you are interested in methodical and didactic use of OLAT for a certain learning sequence, please visit: [https://teachingtools.uzh.ch](https://teachingtools.uzh.ch/en/){target=_blank}

[![](attachments/example_of_use/teachingtool.png)](https://teachingtools.uzh.ch/en/)

* * *

## OLAT Blog

Our [Blog of the OLAT project team ](https://www.uzh.ch/blog/zi-olat/){target=_blank} contains
some tips and tricks for external link to 3rd party software, e.g.:

  * [gather.town](https://www.uzh.ch/blog/zi-olat/2021/03/05/olat-meets-gather-town/){target=_blank}
  * [MS Teams meetings](https://www.uzh.ch/blog/zi-olat/2020/12/16/ms-teams-in-olat-nutzen/){target=_blank}

Using the LTI page course element it is possible to link external services to OLAT. Two of them have been tried and tested and documented. These are:

  * [SWITCHportfolio](https://www.uzh.ch/blog/zi-olat/2019/07/17/switchportfolio-im-){target=_blank}
  * [H5P](https://www.uzh.ch/blog/zi-olat/2019/10/28/flashcards-mit-olat-und-h5p/){target=_blank}

* * *

## Learning Videos for OLAT

  * [Using OLAT for exams](https://tube.switch.ch/channels/627adf59){target=_blank}
  * [OLAT learning videos](https://tube.switch.ch/channels/049b8055){target=_blank}
  * [learning videos from DLF](https://tube.switch.ch/channels/5833c58a){target=_blank} (digitale Lehre und Forschung)

* * *

## LMS Community

Are you interested to get in contact and share content with other OLAT authors? Join our [Community in MS Teams](https://teams.microsoft.com/l/channel/19%3aa9f6d50a44a64b27ac224603ec65d62a%40thread.tacv2/Allgemein?groupId=bad7b058-eae7-44ec-afb8-0fa0f34392b9&tenantId=c7e438db-e462-4c22-a90a-c358b16980b3){target=_blank}!

* * *

## Demo courses and course templates

The OLAT course catalogue contains [demo courses and course templates](https://lms.uzh.ch/url/CatalogEntry/2877390852?guest=true&lang=de){target=_blank} that you can view and copy.

