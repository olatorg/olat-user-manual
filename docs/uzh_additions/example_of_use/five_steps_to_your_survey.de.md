# Fünf Schritte zu Ihrer Umfrage

Mit dieser Anleitung haben Sie in kurzer Zeit ein einfaches
[Formular](../../manual_user/learningresources/Form.de.md) in Ihren Kurs eingebunden und den Kurs für Ihre
Teilnehmer freigeschaltet.

## Schritt 1: Kurseditor öffnen und Kursbaustein "Umfrage" einfügen

  1. Im Autorenbereich unter "Meine Lernressourcen" Kurs suchen und öffnen.
  2. Oben im Dropdown-Menü "Administration" auf "Kurseditor" klicken.
  3. Kurselement, unter dem die Umfrage eingefügt werden soll, durch Klicken auswählen.
  4. Oben im Pop-Up "[Kursbausteine einfügen](../../manual_user/learningresources/Using_additional_Course_Editor_Tools.de.md#kursbaustein-einfugen)" und dann "[Umfrage](../../manual_user/learningresources/Assessment.de.md#course_element_survey)" wählen.
  5. Im Tab "Titel und Beschreibung" kurzen Titel des Kursbausteins eingeben und speichern.

## Schritt 2: Formular erstellen

  1. Im Tab "Umfrage" auf "Wählen, erstellen oder importieren" klicken.
  2. "Erstellen" klicken.
  3. Titel des Formulars eingeben und auf "Erstellen" klicken.

Die Lernressource "[Formular](../../manual_user/learningresources/Form.de.md)" ist nun erstellt worden.  

## Schritt 3: Formular editieren und neue Frage hinzufügen

  1. "Bearbeiten" klicken, so gelangen Sie zum Formular-Editor.  

  2. Klicken Sie auf "Inhalt hinzufügen" um eine neue Frage hinzuzufügen.
  3. Wählen Sie im Dropdown-Menü das gewünschte Element aus (z.B. [Einzelauswahl](../../manual_user/learningresources/Form_editor_Questionnaire_editor.de.md#einzelauswahl) oder [Mehrfachauswahl](../../manual_user/learningresources/Form_editor_Questionnaire_editor.de.md#mehrfachauswahl)).  

  4. Definieren Sie nun die Darstellung, ob die Elemente untereinander, nebeneinander oder als Auswahlliste (Dropdown) zur Verfügung stehen.
  5. Erfassen Sie Ihre Auswahl im Feld "Eintrag".
  6. Für weitere Einträge wählen Sie "Eintrag hinzufügen".
  7. Die Position des neuen Elements können Sie auf Wunsch mit Drag & Drop ändern.

Fügen Sie nun weitere Fragen / Elemente des gewünschten Typs nach demselben
Prinzip ein. Verwenden Sie das Layout Element "Container" oder "Separator", um
Ihre Fragen zu gliedern.

## Schritt 4: Formular Lernressource speichern

  1. Rechts oben in der Toolbar den Formular-Editor schliessen und allfällige noch nicht gespeicherte Änderungen sichern.
  2. Zum Kurs zurückkehren.

## Schritt 5: Kurs publizieren und freischalten

  1. Zurück zum Kurseditor navigieren.
  2. Oben in der Toolbar "[Publizieren](../../manual_user/learningresources/Using_additional_Course_Editor_Tools.de.md#publizieren)" wählen.
  3. Kursbaustein überprüfen und "Weiter" klicken.
  4. Im Assistenten unter "[Zugang- und Buchungskonfiguration](../../manual_user/learningresources/Access_configuration.de.md)" die entsprechenden Freigabe- und Zugriffsoption auswählen.  

  5. "Fertigstellen" klicken.
  6. Verlassen Sie den Kurseditor.

Das Formular resp. die Umfrage ist nun eingebunden und kann von
Kursteilnehmern ausgefüllt werden.  
Die Resultate werden anonym gespeichert und können über "[Fragebogen
Statistik](../../manual_user/learningresources/Using_Course_Tools.de.md#fragebogen-statistiken)" oder
"[Datenarchivierung](../../manual_user/learningresources/Data_archiving.de.md)" eingesehen werden.  

