# What's up in OLAT: Zugriffs- und Aktivitätsstatistiken

Wenn Sie sich als Kursbesitzer über die Aktivitäten der Kursteilnehmenden informieren möchten, stehen Ihnen in OLAT einige Statistiken zur Verfügung.

## Teststatistiken

Wenn Sie in OLAT einen Test durchführen, finden Sie im Kursbaustein "Test" zahlreiche Informationen zum Prüfungsverlauf.

### Kennzahlen

Anhand der Kennzahlen können Sie sich schnell einen Überblick darüber verschaffen, wie ein Test verlaufen ist. U.a. finden Sie Angaben zur Teilnehmerzahl sowie zur durchschnittlichen Dauer, die die Teilnehmenden für die Bearbeitung des Tests benötigt haben. Nach einer automatischen bzw. manuellen Auswertung sind weitere Angaben verfügbar, z.B. die Anzahl bestandener Tests, die durchschnittlich erreichte Punktzahl sowie die Spannweite der erreichten Punktzahlen.

![](attachments/statistics_in_olat_de/kennzahlen.png)
