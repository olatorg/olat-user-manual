# Online-Prüfungen

Mit OLAT können Sie Leistungkontrollen verschiedener Art durchführen. Neben
den Kurselementen die z.B. der Wissensvermittlung oder der Kollaboration
dienen, gibt es weiterhin eine Reihe an Kurselementen, die zur Kontrolle oder
Bewertung eingesetzt werden können. Darunter sind einerseits jene, die kurs-
oder semesterbegleitend eingesetzt werden und mehr der formativen Bewertung
dienen, wie z.B. die Aufgabe, die Checkliste, und andererseits gibt es die
Tests, die punktuell und vor allem zur summativen Bewertung eingesetzt werden.

## Leistungsbewertung

Eine Reihe an Kursbausteinen kann entweder zur summativen oder formativen
Bewertung genutzt werden, oder dient der Fortschrittskontrolle. Folgende
Kursbausteine können im [Bewertungswerkzeug](../../manual_user/learningresources/Assessment_tool_overview.de.md)
eingesehen und bearbeitet werden:

  * [Aufgabe](../../manual_user/learningresources/Assessment.de.md#course_element_assessment) (manuelle Bewertung)
  * [Checkliste](../../manual_user/learningresources/Course_Element_Checklist.de.md) (manuelle & automatische Bewertung)
  * [Bewertung](../../manual_user/learningresources/Course_Element_Assessment.de.md) (manuelle Bewertung)
  * [LTI](../../manual_user/learningresources/Assessment_of_course_modules.de.md#lti) (automatische Bewertung, wird von LTI-Seite übertragen)
  * [SCORM](../../manual_user/learningresources/Assessment_of_course_modules.de.md#scorm) (automatische Bewertung, wird durch SCORM-Modul übertragen)
  * [Test](../../manual_user/learningresources/Assessing_tests.de.md) (automatische & manuelle Bewertung)

Zur Aufgabe und dem Test finden sich separate Unterkapitel die sich der
Erstellung und Nutzung des Kursbausteins widmen. Das Kapitel
"[Fragenpool](../../manual_user/area_modules/Question_Bank.de.md)" beschäftigt sich mit der Erstellung
von Items für Tests, die im Fragenpool der kollaborativen Nutzung und
Bearbeitung zur Verfügung stehen. Tests können mit Items aus dem Fragenpool
erstellt werden.

## Leistungsnachweis und Zertifikat

Ein Leistungsnachweis ist eine Bestätigung einer erfolgten Leistungskontrolle
und zeigt beispielsweise absolvierte Tests oder abgegebene und bewertete
Aufgaben pro Kurs an. Leistungsnachweise müssen pro Kurs eingeschaltet werden,
und sind abrufbar, wenn es im betroffenen Kurs bewertbare Kursbausteine gibt
und bereits ein solcher Kursbaustein absolviert und bewertet wurde.
Leistungsnachweise sind für Benutzer im persönlichen Menü und im Kurs
abrufbar, während Betreuer und Autoren diese im Coaching-Tool einsehen können.
Druckbare Zertifikate können mittels der Kurseinstellung
"[Leistungsnachweis](../../manual_user/personal_menu/Personal_Tools.de.md#leistungsnachweise)" ausgestellt werden.


