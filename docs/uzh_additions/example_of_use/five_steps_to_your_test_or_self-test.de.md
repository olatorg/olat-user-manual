# Fünf Schritte zu Ihrem Test oder Selbsttest

Mit dieser Anleitung haben Sie in kurzer Zeit einen einfachen
[Test](../../manual_user/learningresources/Test.de.md) oder [Selbsttest](../../manual_user/learningresources/Assessment.de.md#course_element_self_test) in
Ihren Kurs eingebunden und den Kurs für Ihre Teilnehmer freigeschaltet.

## Schritt 1: Kurseditor öffnen und Kursbaustein einfügen

  1. Im Autorenbereich unter "Meine Lernressourcen" Kurs suchen und öffnen.
  2. Oben im Dropdown-Menü "Administration" auf "Kurseditor" klicken.
  3. Kurselement, unter dem der Test oder Selbsttest eingefügt werden soll, durch Klicken auswählen.
  4. Oben im Pop-Up "[Kursbausteine einfügen](../../manual_user/learningresources/Using_additional_Course_Editor_Tools.de.md#kursbaustein-einfugen)" "[Test](../../manual_user/learningresources/Test.de.md)" oder "[Selbsttest](../../manual_user/learningresources/Assessment.de.md#course_element_self_test)" wählen.
  5. Im Tab "Titel und Beschreibung" kurzen Titel des Kursbausteins eingeben und speichern.

## Schritt 2: Testdatei erstellen

  1. Im Tab "Test-/Selbsttest-Konfiguration" auf "Datei wählen, erstellen oder importieren" klicken.
  2. "Erstellen" klicken.
  3. Titel des Tests eingeben und auf "Erstellen" klicken.

Die Test-Lernressource ist nun erstellt worden und besteht standardmässig aus
einer Sektion und einer Single-Choice-Frage. Falls in Ihrem Test keine Single-
Choice Frage vorkommt, können Sie die standardmässig angelegte Single-Choice-
Frage löschen, sobald Sie eine andere Frage hinzugefügt haben.

## Schritt 3: Test editieren und neue Frage hinzufügen (z.B. Vom Typ "Multiple Choice")

  1. "Editieren" klicken.
  2. Oben im Dropdown-Menü "Element hinzufügen" den entsprechenden Fragetypen auswählen (z.B. "[Multiple Choice](../../manual_user/learningresources/Test_question_types.de.md#multiple-choice)").
  3. Die Position der neuen Frage können Sie auf Wunsch mit Drag and Drop ändern.  

  4. Im Tab "Auswahl" bei "Titel" einen Titel für die Frage eingeben (erscheint dann links im Menü).
  5. Im Tab "Auswahl" bei "Frage" die Fragestellung editieren und zusammen mit dem Titel speichern.
  6. Abhängig von der Anzahl Antwortmöglichkeiten mehrmals neue Antworten über das Plus Icon hinzufügen, Antwort editieren und speichern.
  7. Beim Test bei allen korrekten Antwortmöglichkeiten Häkchen setzen und speichern.
  8. Optional im Tab "Punkte" die Einstellungen ändern und speichern.
  9. Optional im Tab "Feedback" eingeben und speichern.

Fügen Sie nun weitere Fragen des gewünschten Typs nach demselben Prinzip ein.
Verwenden Sie Sektionen, um Ihre Fragen zu gliedern. Eine Übersicht aller
Frage-Typen finden Sie im Kapitel "[Lernressourcen QTI](../../manual_user/learningresources/Test_question_types.de.md)".

## Schritt 4: Test Lernressource speichern

  1. Rechts oben in der Toolbar den Test-Editor schliessen und allfällige noch nicht gespeicherte Änderungen sichern.
  2. Zum Kurs zurückkehren.

## Schritt 5: Kurs publizieren und freischalten

  1. Zurück zum Kurseditor navigieren.
  2. Oben in der Toolbar "[Publizieren](../../manual_user/learningresources/Using_additional_Course_Editor_Tools.de.md#publizieren)" wählen.
  3. Kursbaustein überprüfen und "Weiter" klicken.
  4. Im Assistenten unter "[Zugang- und Buchungskonfiguration](../../manual_user/learningresources/Access_configuration.de.md)" die entsprechenden Freigabe- und Zugriffsoption auswählen.  

  5. "Fertigstellen" klicken.
  6. Verlassen Sie den Kurseditor.

Der Test bzw. Selbsttest ist nun eingebunden und kann von Kursteilnehmern
gelöst bzw. ausgefüllt werden.

Die Resultate können unter "[Kurs Statistik](../../manual_user/learningresources/Using_Course_Tools.de.md#fragebogen-statistiken)",
"[Bewertungswerkzeug](../../manual_user/learningresources/Assessment_tool_overview.de.md)" und
"[Datenarchivierung](../../manual_user/learningresources/Data_archiving.de.md)" eingesehen werden.

