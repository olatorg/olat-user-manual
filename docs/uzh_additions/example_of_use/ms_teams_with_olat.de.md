# MS-Teams in OLAT nutzen

Dieser Artikel zeigt, wie Sie ein MS-Teams Meeting in OLAT einbinden können
und damit zwei starke Tools der Online Lehre verknüpfen.

Um ein MS-Teams Meeting in OLAT einbinden zu können, müssen Sie zuerst einen
Link zum Meeting erstellen. Das können Sie auf der Seite:
<https://www.zi.uzh.ch/cl/msteams/meetme.html>

Wenn Sie noch nicht bei Microsoft angemeldet sind, werden Sie dazu
aufgefordert und können danach einen Teams-Besprechung für Ihren Kurs
erstellen:

![](attachments/ms_teams_with_olat_de/Bildschirmfoto-2020-12-16-um-15.26.22.png)

Danach geben Sie den Titel und die Daten des Meetings ein:

![](attachments/ms_teams_with_olat_de/Meeting_erstellen.png)

Damit ist das Meeting auch schon erstellt:

![](attachments/ms_teams_with_olat_de/Bildschirmfoto-2020-12-16-um-15.27.31.png)

Wenn Sie möchten, können Sie noch in den Besprechungsoptionen anpassen, wer
den Wartebereich umgehen kann, wer präsentieren darf und ob es den Teilnehmern
erlaubt ist, die Stummschaltung aufzuheben:

![](attachments/ms_teams_with_olat_de/Bildschirmfoto-2020-12-16-um-15.46.15.png)

!!! danger "Achtung" 
    Wenn Sie die Optionen ändern, öffnet sich ein neuer Tab. Um zum
    Meeting Link zurückzukommen, müssen Sie den Tab wieder wechseln.

Schliesslich können Sie den Link des Meetings kopieren und an einer beliebigen
Stelle in OLAT einfügen. Gut eignet sich der Kalender oder die
Übersichtsseite:

![](attachments/ms_teams_with_olat_de/Bildschirmfoto-2020-12-16-um-15.29.54.png)

Viel Erfolg bei der Integration von MS Teams in OLAT!
