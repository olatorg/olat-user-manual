# Anwendungsbeispiele

In diesem Kapitel finden Sie Informationen zu OLAT die über die Beschreibung einzelner Elemente oder Funktionen hinausgeht, und ein bestimmtes Ziel vor Augen hat.  

## Anwendungsbeispiele
  * [Drei Schritte zu Ihrer Aufgabe](three_steps_to_your_task.de.md)
  * [Drei Schritte zur Erstellung und Verwaltung von Themen](three_steps_to_topic_assignment.de.md)
  * [Eine Umfrage automatisch nach Kursende freischalten](automatically_activate_a_questionnaire_at_the_end_of_the_course.de.md)
  * [Erinnerung im Kursbaustein "Aufgabe" einrichten](erinnerung_im_KB_Aufgabe.de.md)
  * [Flashcards mit OLAT und H5P](flashcards_with_olat_and_h5p.de.md)
  * [Fünf Schritte zu Ihrem Content-Packaging](five_steps_to_your_content_package.de.md)
  * [Fünf Schritte zu Ihrem Test oder Selbsttest](five_steps_to_your_test_or_self-test.de.md)
  * [Fünf Schritte zu Ihrer Umfrage](five_steps_to_your_survey.de.md)
  * [MS-Teams in OLAT nutzen](ms_teams_with_olat.de.md)
  * [OLAT meets gather.town](gather_town_with_olat.de.md)
  * [Online-Prüfungen](online_exams.de.md)
  * [Speichern in OLAT](save_data_in_olat.de.md)
  * [SWITCHportfolio im OLAT](switchportfolio_with_olat.de.md)  
  * [Vergleich der Kurselemente "Seite" und "HTML-Seite"](comparison_course_element_page_HTML_page.de.md)
  * [Vergleich der Kursbausteintypen "Ordner" und "Aufgabe"](comparison_of_the_course_element_types_folder_and_task.de.md)
  * [Video verwenden](using_video.de.md)  
  * [Vier Schritte zu Ihrem Blog](four_steps_to_your_blog.de.md)
  * [Vier Schritte zu Ihrem Wiki](four_steps_to_your_wiki.de.md)

  

* * *

## Teaching Tools der Hochschuldidaktik

Methodisch-didaktische Tipps zur Umsetzung einer Lernsequenz mit OLAT finden Sie unter: <https://teachingtools.uzh.ch>{target=_blank}

## [![](attachments/example_of_use_de/teachingtool.png)](https://teachingtools.uzh.ch)

* * *

## OLAT Lernvideos

  * [OLAT als Prüfungssoftware einsetzten](https://tube.switch.ch/channels/627adf59){target=_blank}
  * [OLAT Lernvideos](https://tube.switch.ch/channels/049b8055){target=_blank}
  * [Lernvideos von DLF](https://tube.switch.ch/channels/5833c58a){target=_blank} (digitale Lehre und Forschung)

* * *

## LMS Community

Möchten Sie sich mit anderen OLAT Autoren austauschen? Treten Sie unserer [Community in MS Teams bei](https://teams.microsoft.com/l/channel/19%3aa9f6d50a44a64b27ac224603ec65d62a%40thread.tacv2/Allgemein?groupId=bad7b058-eae7-44ec-afb8-0fa0f34392b9&tenantId=c7e438db-e462-4c22-a90a-c358b16980b3){target=_blank}!

* * *

## Kursvorlagen und Demokurse

Im OLAT-Kurskatalog finden Sie [Demokurse und Kursvorlagen](https://lms.uzh.ch/url/CatalogEntry/2877390852?guest=true&lang=de){target=_blank}, die Sie betrachten und kopieren können.
