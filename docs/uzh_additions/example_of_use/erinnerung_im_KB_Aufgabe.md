# Automatic reminders in the “Task” course element 

## Table of contents

* [Overview](#overview)
* [Step-by-step guide: Create reminder](#step-by-step-guide-create-reminder)
    * [Step 1: Edit rules](#step-1-edit-rules) 
    * [Step 2: Check rules](#step-2-rules-overview)
    * [Step 3: E-mail notification](#step-3-e-mail-notification)
* [Send reminder manually](# #send-reminder-manually)  

## Overview

!!! info ""

    You can find an overview of all the configuration options for the reminders function in the [OpenOlat documentation](https://docs.openolat.org/manual_user/learningresources/Course_Reminders){ target="_blank" class="lightbox border" }

The course element Task offers the option of setting up conditional reminders for course participants in the “Reminders” tab. This function is often used to draw attention to a submission deadline.

Reminders can be triggered manually at a self-chosen point in time or automatically based on the specified conditions.  

In OLAT of UZH, automatic reminders are sent daily at three times: at 6:00, 14:00 and 22:00. For each course participant who fulfills all conditions, the same reminder is only sent once at the next dispatch time. 

!!! warning "Reminder for a deadline"

    When entering a reminder for a deadline, an **additional condition “Until date”** should always be configured. It makes sense to select  the submission date of the corresponding task as the until date.
    This prevents unintentional reminders to course participants being triggered when the course is reused (e.g. when it is copied for reuse or used a basis for a UZH campus course). 

## Step-by-step guide: Create reminder

In the course view, course owners see four tabs in the “Task” course element:   

* Overview 
* Participants 
* Edit tasks and sample solutions 
* Reminders  

![](attachments/erinnerung_im_KB_Aufgabe/Reminder_01_create.png){ alt="Course element 'Task': Reminders" class="lightbox border" }

Select the “Add reminder” button to create a new reminder.

The configuration of reminders comprises three steps:  

1. edit rules 
2. rules overview 
3. e-mail message  
<br />
![](attachments/erinnerung_im_KB_Aufgabe/Reminder_02_three_steps.png){ alt="Create reminders: three steps" class="lightbox border" }

### Step 1: Edit rules
* Enter a **Description of the reminder** (mandatory field) 

* Select the condition **"Deadline: Task documents submission “** and enter how many days, weeks, ... before the deadline the reminder should be sent.  

* One or more additional conditions can be added using the “Select new rule...” button. All selected rules must be fulfilled for a reminder to be sent (“AND” logical link). <br /><br />

![](attachments/erinnerung_im_KB_Aufgabe/Reminder_02_create.png){ alt="New Reminder: add rules." class="lightbox border" }

* Under “And the following additional conditions are met”, select the additional condition: “Until date” and enter the date of the submission deadline. 

!!! warning "Reminders for selected groups only"

    If you only want to send a reminder to a specific group in your course, a separate additional condition must be entered for all affected groups (rule “Group members” / select corresponding group). This also applies to task elements that are restricted to individual groups in the course using the visibility/access rules.
    Otherwise, all members of the course will receive the reminder.

### Step 2: Rules overview
In the next step, the selected conditions are displayed for review. The course members who already meet the conditions and will therefore receive the reminder message the next time it is sent are listed under “Preview receivers". 

![](attachments/erinnerung_im_KB_Aufgabe/Reminder_03_check-rules.png){ alt="Check rules" class="lightbox border" }

### Step 3: E-mail notification
Now enter the **subject** and the **text of the reminder message.** User variables can be used in the text to personalize the message ($firstName for the first name, $lastName for the last name, $courseUrl for a link to the course).

![](attachments/erinnerung_im_KB_Aufgabe/Reminder_04_enter_message.png){ alt="Enter message text" class="lightbox border" }

Wählen Sie die Benutzergruppen des Kurses aus, welche eine Kopie jeder versandten Erinnerungsnachricht erhalten sollen. Zur Auswahl stehen:

* Course owners 
* Course or group coaches 
* External e-mail addresses (one or more addresses, to be entered manually, comma-separated) 

Select “Finish” to complete the configuration of the reminder. You will then be taken to the overview with all recorded reminders.  

You can use the three-dot button to display a list of the reminders that have already been sent (Show sent reminders).

## Send reminder manually
To trigger a reminder manually, select the three-dot menu of the desired reminder and select “Send reminders now” (no reminders will be sent yet). 

![](attachments/erinnerung_im_KB_Aufgabe/Reminder_05_edit_reminder.png){ alt="show sent reminders" class="lightbox border" }

A list of course members who meet the conditions will now be displayed. 

You can send the reminder message immediately using the “Send for the first time (*number of recipients*)” button. 

The “Send to all” button sends the reminder message again, even to course members who have already received the message. 

![](attachments/erinnerung_im_KB_Aufgabe/Reminder_06_send_manually.png){ alt="Send reminders manually" class="lightbox border" }