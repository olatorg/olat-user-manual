# Flashcards mit OLAT und H5P

Mit dem LTI Kursbaustein können beliebige [H5P
Inhalte](https://h5p.org/content-types-and-applications){:target="\_blank"} in OLAT integriert
werden. Wie das geht zeigt dieser Blogeintrag am Beispiel von Flashcards.

Um H5P nutzen zu können, müssen wir ein paar Konfigurationen vornehmen, damit
die Verbindung klappt. Es beginnt mit dem Einrichten der H5P LTI Provider
Organisation.

## H5P LTI Provider einrichten

Zuerst brauchen wir einen Account bei [h5p.com](https://h5p.com/){:target="\_blank"}. Dieser
Account muss so dimensioniert sein, dass es mindestens einen Administrator und
die Anzahl der Lernenden dort eingetragen sind.

![](attachments/flashcards_with_olat_and_h5p_de/Bildschirmfoto-2019-10-28-um-09.14.51-1024x721.png)

!!! hint "Hinweis!"
    Die Lernenden müssen NICHT manuell angelegt werden. Das
    übernimmt später OLAT automatisch.

Als Nächstes muss eine LTI Verbindung von [h5p.com](https://h5p.com){:target="\_blank"} und OLAT
hergestellt werden. Das wird mit Hilfe der Option „ _Connect LMS_ “ gemacht:

![](attachments/flashcards_with_olat_and_h5p_de/Bildschirmfoto-2019-10-28-um-09.17.58-1024x695.png)

## H5P Inhalt erstellen

Ist der LTI Provider eingerichtet, können wir uns an die Erstellung des
Content machen. Dazu klicken wir den blauen Button “ _\+ Add Content_ “ rechts
oben. Danach suchen wir die gewünschte H5P Komponente. Wir wählen Flaschcards
für unser Beispiel:

![](attachments/flashcards_with_olat_and_h5p_de/Bildschirmfoto-2019-10-28-um-09.00.33.png)

Danach können wir einen Titel und eine Beschreibung für unser Kartenset
vergeben und die Fragen und Antworten für die Flashcards erfassen. Zusätzlich
können für jede Karte Bilder hinterlegt werden.

![](attachments/flashcards_with_olat_and_h5p_de/Bildschirmfoto-2019-10-28-um-09.02.32.png)

Sind wir mit dem Erfassen fertig, können wir die Flashcard Serie testen. Wenn
alles funktioniert, machen wir uns an die Integration in OLAT:

## Integration von H5P in OLAT

Für die Integration in OLAT wählen wir den LTI Kursbaustein.

![](attachments/flashcards_with_olat_and_h5p_de/Bildschirmfoto-2019-10-28-um-09.06.04-1.png)

Im Reiter „ _Page content_ “ wird der LTI Baustein konfiguriert:

![](attachments/flashcards_with_olat_and_h5p_de/Bildschirmfoto-2019-10-28-um-09.35.35-720x1024.png)

  1.  **URL** : Dort steht die URL des H5P LTI Inhalt. Sie kann 1:1 aus dem Browser im [h5p.com](https://h5p.com/){:target="\_blank"} Fenster kopiert werden. Sie ist immer gleich aufgebaut: _[ORGNAME.h5p.com/content/LANGE_ZAHL](https://ORGNAME.h5p.com/content/LANGE_ZAHL){:target="\_blank"}_
  2.  **Key & Passwort**: Diese Werte werden aus der Konfiguration von [h5p.com](https://h5p.com/){:target="\_blank"} übernommen.
  3.  **Transmit firstname/name & Transmit e-mail address**: Möchte man das Bewertungswerkzeug von OLAT nutzen, ist es notwendig, diese Checkboxen zu aktivieren.
  4.  **Author, Coach & Participant**: Hier ist überall „ _Learner_ “ zu wählen, sonst kann es zu Problemen mit der User-Quota bei [h5p.com](https://h5p.com/){:target="\_blank"} kommen.
  5.  **Transfer score** : Möchte man das Bewertungswerkzeug von OLAT nutzen, ist es notwendig, diese Checkboxen zu aktivieren.
  6. Alle anderen Display Optionen können auf der Standardoption „ _Embedded in course (iFrame)_ “ und „ _Automatic_ “ belassen werden.

### Fertig

Nach der Publikation des Kurses steht das Flashcard Set allen Kursteilnehmern
zur Verfügung.

![](attachments/flashcards_with_olat_and_h5p_de/Bildschirmfoto-2019-10-28-um-09.07.54-951x1024.png)

Viel Erfolg und Spass mit Flashcards und H5P!

