# Automatically activate a questionnaire at the end of the course

!!! info "Request"

    **Some** days after the start or end of the course, the participants of a
    course should give feedback on the course by filling in a questionnaire.

!!! hint "Requirement"

    * Add an [execution period](../../manual_user/learningresources/Course_Settings.md#Execution) to the course
    * Include [form](../../manual_user/learningresources/Form_editor_Questionnaire_editor.md) in course

## Procedure

### Define visibility rule

A rule must be defined in the tab [Visibility](../../manual_user/learningresources/General_Configuration_of_Course_Elements.md#access) on
the [survey](../../manual_user/learningresources/Assessment.md#course_element_survey) element to determine when the element should be
visible. In simple configuration mode this is only possible with absolute
data. To work with relative data, you have to switch to the [expert
mode](../../manual_user/learningresources/Access_Restrictions_in_the_Expert_Mode/). This allows you to
control that the questionnaire is only displayed at the end of the course, for
example, with a duration of one week.

#### Step 1: Open expert mode

  1. Navigate to the [survey](../../manual_user/learningresources/Assessment.md#course_element_survey) in the course editor
  2. Open tab "[Visibility](../../manual_user/learningresources/General_Configuration_of_Course_Elements.md#access)"
  3. Click on the button "Show [expert mode](../../manual_user/learningresources/Access_Restrictions_in_the_Expert_Mode/)"

#### Step 2a: Create expert rule: Duration after course start

Add the following expert rule:    
    
    (today <= getCourseBeginDate(0) + 7d)

This expert rule says that the questionnaire should be visible for 7 days
after the start of the course.  
The number of days can be adjusted as desired.

#### Step 2b: Create expert rule: Duration after course end

Add the following expert rule:    
    
    (getCourseEndDate(0) >= today) & (getCourseEndDate(0) + 7d >= today)

This expert rule says that the questionnaire should be visible for 7 days
after the end of the course.  
The number of days can be adjusted as desired.

#### Step 3: Save

Save your expert rules and [publish](../../manual_user/learningresources/Using_additional_Course_Editor_Tools.md#publizieren) your course again.

### Add reminder

The [reminder tool](../../manual_user/learningresources/Course_Reminders.md) allows you to send a reminder,
e.g. 3 days after the start or end of the course, for participants to complete
the questionnaire.

#### Add reminder:

  1. In the drop-down menu "Administration" opens the item "[Reminder](../../manual_user/learningresources/Course_Reminders.md)".
  2. Click the button "Create reminder". A new reminder will open.
  3. Enter a description
  4. Add conditions:
    1. After course start  
![](attachments/automatically_activate_a_questionnaire_at_the_end_of_the_course/Reminder_-_course_start.png)

    2. after course end  
![](attachments/automatically_activate_a_questionnaire_at_the_end_of_the_course/Reminder_-_course_end.png)

  5. Fill in mail subject and text
  6. Save
  