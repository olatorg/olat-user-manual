# Vergleich der Kurselemente "Seite" und "HTML-Seite"

Seit OLAT 6.0 ist neben dem bisherigen Kursbaustein ["HTML-Seite"](../../manual_user/learningresources/Course_Element_HTML_Page.de.md) (früher: "Einzelne Seite") der Kursbaustein ["Seite"](../../manual_user/learningresources/Course_Element_Page.de.md) verfügbar.<br /><br />
Beide Kursbausteine ermöglichen es, eigene Inhalte wie Text, Bilder usw. in OLAT einzufügen. Je nach beabsichtigter Verwendung ist der neue Kursbaustein Seite oder der bisherige Kursbaustein "HTML-Seite" besser geeignet.<br />
Die nachfolgende Tabelle listet die wichtigsten Unterschiede zwischen den beiden Kursbausteinen auf und soll eine Hilfe bei der Entscheidung bieten, welcher der beiden Kursbausteine im Einzelfall eher in Frage kommt.

| Feature     | Subfeature     | Kursbaustein Seite  | HTML-Seite   |
| ----------- | -------------- | ------------------- | ------------ |
| Titel       |                | :material-check:    | :material-check: |
| Paragraph   | Textformatierung | :material-check:  | :material-check: |
|             | Link           | :material-check:    | :material-check: |
|             | Mathematische Formel | :material-check: (auch als eigener Block verfügbar) | :material-check: |
| Tabelle     |                | :material-check: | :material-check: |
| Code (Formatierung) |        | :material-check: | :material-close: |
| Video [Siehe Anwendungsbeispiel "Video verwenden"](using_video.de.md) | lokal          | :material-check: (wird danach im Medien Center gespeichert) | :material-check: |
|             | per URL        | :material-check: (wird danach im Medien Center gespeichert) | :material-close: |
|             | per Embed-Code | :material-close: (keine Quellcode-Ansicht) | :material-check: |
| draw.io     |                | :material-check: | :material-close: |
| Audio       |                | :material-check: | :material-check: |
| Dokument    |                | :material-check: (aus Medien Center oder Upload) | :material-close: |
| Bild        |                | :material-check: (in und aus Medien Center) | :material-check: (in und aus Ablageordner / Ressourcenordner) |
| Zitat       |                | :material-check: (wird danach im Medien Center gespeichert) | :material-close: |
| Aus Medien Center wählen |   | :material-check: | :material-close: |
| Textabschnitt |              | :material-check: | :material-check: |
| Quelltext   |                | :material-close: | :material-check: |

