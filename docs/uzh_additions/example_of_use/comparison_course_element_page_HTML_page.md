# Comparison between the course elements "Page" and "HTML Page"

Since OLAT 6.0, in addition to the previous course element ["HTML page"](../../manual_user/learningresources/Course_Element_HTML_Page.md) (previously named "Single Page"), the course element "Page" is available.<br /><br />
Both course elements allow you to insert your own content such as text, images etc. into OLAT. Depending on the intended use, the new course element "Page" or the previous course element "HTML page" is more suitable.
The following table lists the most important differences between the two course elements and is intended to help you decide which of the two course elements is more suitable in individual cases.

| Feature     | Subfeature     | Course Element  | HTML Page   |
| ----------- | -------------- | ------------------- | ------------ |
| Title       |                | :material-check:    | :material-check: |
| Paragraph   | Text formats | :material-check:  | :material-check: |
|             | Link           | :material-check:    | :material-check: |
|             | Mathematical formula | :material-check: (available as block as well) | :material-check: |
| Table     |                | :material-check: | :material-check: |
| Code (Format type) |        | :material-check: | :material-close: |
| Video [See example of use "Using Video"](using_video.md) | local          | :material-check: (stored in Media Center) | :material-check: |
|             | via URL        | :material-check: (stored in Media Center) | :material-close: |
|             | via embed code | :material-close: (no source code view) | :material-check: |
| draw.io     |                | :material-check: | :material-close: |
| Audio       |                | :material-check: | :material-check: |
| Document    |                | :material-check: (from Media Center or upload) | :material-close: |
| Image        |                | :material-check: (from Medien Center) | :material-check: (from storage folder / ressource folder) |
| Citation       |                | :material-check: (stored in Media Center) | :material-close: |
| Aus Medien Center wählen |   | :material-check: | :material-close: |
| Text section |              | :material-check: | :material-check: |
| Source code   |                | :material-close: | :material-check: |

