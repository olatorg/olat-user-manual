# Video verwenden

Es gibt mehrere Möglichkeiten, ein Video in OLAT einzubinden.

  * [Kursbaustein "Kaltura"](#kursbaustein-kaltura)
  * [Kursbausteine "Video" und "Videoaufgabe"](#kursbausteine-video-und-videoaufgabe)
  * [Videos im Kursbaustein "Seite" einfügen](#videos-im-kursbaustein-seite-einfugen)
  * [Video im Inhalt beim Kursbaustein "HTML-Seite"](#video-im-inhaltseditor-einfugen-kursbaustein-html-seite)
  * [Ein Kaltura-Video via URL in OLAT importieren](#ein-kaltura-video-via-url-in-olat-importieren)

## Kursbaustein "Kaltura"

Die Videoplattform _Kaltura_ wird für Schweizer Hochschulen von SWITCH
angeboten. Die [Videoplattform](https://uzh.mediaspace.cast.switch.ch/){target=_blank} steht allen OLAT-Benutzer*innen offen und
erlaubt es Ihnen, eigene Videos hochzuladen und einem dedizierten Publikum zugänglich zu machen.  

Eigene Videos oder [Aufzeichnungen Ihrer Lehr&shy;ver&shy;an&shy;staltungen](https://www.zi.uzh.ch/de/support/audio-video/event-recording.html){target=\_blank} (UZH-Lehrveranstaltungen) können Sie direkt in OLAT über den Kursbaustein "Kaltura" in Ihre Kurse einbinden. Eine umfassende Dokumentation zu _Kaltura_ finden Sie unter <https://uzh.mediaspace.cast.switch.ch/Help>{target=_blank}.

Die Hilfe-Seite zum Kursbaustein "Kaltura" finden Sie
[hier](../kaltura/index.de.md).

## Kursbausteine "Video" und "Videoaufgabe"

Die Lernressource "Video" von OpenOlat wird auf OLAT-UZH nicht angeboten, da Kaltura als Plattform für die Bereitstellung von Videos vorgesehen ist. Sie können jedoch im [Kursbaustein "Video"](../../manual_user/learningresources/Course_Element_Video.de.md) und im [Kursbaustein "Videoaufgabe"](../../manual_user/learningresources/Course_Element_Video_Task.de.md) Videos von anderen Videoplattformen per URL importieren. Zudem können Sie Videos von Ihrem Gerät als Seiteninhalt in den Kursbausteinen ["HTML-Seite"](../../manual_user/learningresources/Video_in_HTML_Pages.de.md) und ["Seite"](../../manual_user/learningresources/Course_Element_Page.de.md) einbinden (siehe Abschnitt [Video im Inhaltseditor einfügen](#video-im-inhaltseditor-einfugen-kursbaustein-html-seite)). 

## Videos im Kursbaustein "Seite" einfügen

Videos können im Kursbaustein ["Seite"](../../manual_user/learningresources/Course_Element_Page.de.md) hinzugefügt und auch neu aufgenommen werden.
Videos werden hierzu im [OLAT Medien-Center](../../manual_user/personal_menu/Media_Center.de.md#medien-dem-medien-center-hinzufugen) (neu seit OLAT 6 / OpenOlat 18) ausgewählt, hochgeladen oder neu aufgenommen.

## Video im Inhaltseditor einfügen (Kursbaustein "HTML-Seite")

Im Inhaltseditor des Kursbausteins "HTML-Seite" kann über die Schaltfläche "Video" eine Video-Datei vom eigenen Computer hochgeladen werden.<br />
Siehe dazu den Hilfe-Artikel [Videos im Kursbaustein "HTML-Seite"](https://docs.olat.uzh.ch/de/manual_user/learningresources/Video_in_HTML_Pages/)

Im Fall von vielen und/oder längeren Videos empfehlen wir die Einbindung über den Kursbaustein Kaltura.

Das Video-Werkzeug im Inhaltseditor des Kursbausteins "HTML-Seite":

![](attachments/using_video_de/Inhaltseditor_Einzelne_Seite.png)

![](attachments/using_video_de/Inhaltseditor_Video_hochladen.png)

## Ein Kaltura-Video via URL in OLAT importieren

Kaltura stellt keine direkten Links zur eigentlichen Videodatei zur Verfügung. Links zu Videos auf Kaltura führen normalerweise zur Medienseite bzw. zu einer Playlist auf SWITCHcast MediaSpace. Wollen Sie ein Video von SWITCHcast MediaSpace bzw. aus “Meine Medien” z.B. in einem Kursbaustein “Videoaufgabe” verwenden, müssen Sie den Link mit Hilfe einer Erweiterung im Browser [Mozilla Firefox](https://www.mozilla.org/de/firefox/new/){target=_blank} manuell generieren. Führen Sie dazu die folgenden Schritte aus: 

  1. Öffnen Sie Firefox und wählen Sie im Menü “Extras” > “Add-Ons und Themes” aus. Suchen Sie nach dem Add-On [“Video Download Helper”](https://addons.mozilla.org/de/firefox/addon/video-downloadhelper/?utm_source=addons.mozilla.org&utm_medium=referral&utm_content=search){target=_blank} und wählen Sie “Zu Firefox hinzufügen”.  
  Nach der Aktivierung sollte das Icon in der Symbolleiste von Firefox angezeigt werden:<br />
  <br />
    ![](./attachments/using_video_de/Mozilla_video-download-helper_1.png)
  <br />
  <br />
  2. Öffnen Sie “Meine Medien” im persönlichen Menü in OLAT und rufen Sie das Video auf, dessen URL Sie generieren möchten. 
    
  3. Spielen Sie das Video ab und wählen Sie das nun farbig dargestellte Icon des Video Download Helpers an. Wählen Sie im Drei-Punkte-Menü rechts vom blauen “Herunterladen”-Button die Option “URL kopieren” aus.<br />
  <br />
    ![](./attachments/using_video_de/Mozilla_video-download-helper.png)
    <br />
    <br />
  4. Die kopierte URL sieht ungefähr so aus: 
  _https://streaming.cast.switch.ch/hls/p/106/sp/10600/serveFlavor/entryId/0_xu2z4inr/v/2/flavorId/0_o720p621/name/a.mp4/index.m3u8_<br /><br />
  Ersetzen Sie am Anfang der URL _`https://streaming.cast.switch.ch/hls`_ durch _`https://api.cast.switch.ch`_ und löschen Sie am Ende der URL den Teil, welcher nach a.mp4 folgt:<br /><br /><del>`https://streaming.cast.switch.ch/hls`</del>/p/106/sp/10600/serveFlavor/entryId/0\_xu2z4inr/v/2/flavorId/0\_o720p621/name/a.mp4<del>`/index.m3u8`</del><br /><br />Die URL sieht dann so aus:
  <mark>https://api.cast.switch.ch</mark>/p/106/sp/10600/serveFlavor/entryId/0_xu2z4inr/v/2/flavorId/0_o720p621/name/a<mark>.mp4</mark>
  <br /><br />
  Diese URL kann nun verwendet werden, um z.B. im Kursbaustein [“Videoaufgabe”](../../manual_user/learningresources/Course_Element_Video_Task.de.md) per URL das Video hinzuzufügen.