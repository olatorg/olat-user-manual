# OLAT meets gather.town

Der Mensch ist ein soziales Wesen. Physische Treffen jedoch sind nicht immer
möglich. Unterricht wird vermehrt online abgehalten. Wie kommen wir unserem
sozialen Bedürfnis nach?

Mit Videokonferenz-Tools wie MS-Teams oder Zoom hört man seine Gegenüber nicht
nur, sondern sieht sie auch. Aber reicht das? Nein, findet die Online
Plattform [gather.town](https://gather.town/){:target="\_blank"}, die es sich zum Ziel gemacht
hat, menschliche Treffen in die Online Welt zu verlagern und das so lebensecht
wie möglich.

Optisch erinnert die Plattform an ein Computerspiel der Neunziger Jahre und
das Konzept des Treffens, welches räumliche Nähe berücksichtigt, funktioniert
erstaunlich gut. Jeder User hat einen Avatar, der sich in einer virtuellen
Landschaft bewegt. Diese kann einem Büro, einem Seminarraum oder eine
Aussenlandschaft nachempfunden sein, oder komplett selbst gestaltet werden.

Audio und Video sind für die Personen aktiv, die sich in der Nähe aufhalten.
Es können auch interaktive Tools wie Whiteboards verwendet und Videos
eingebunden werden. Das macht es zur idealen Plattform für formelle und
informelle Treffen mit den Teilnehmern eines Seminares oder auch für einen
Apéro.

Hilfe zum Aufbau einer solchen virtuellen Welt stellt die Plattform in ihrem
[Hilfezentrum](https://support.gather.town/help){:target="\_blank"} zur Verfügung. Räume mit bis
zu 25 Usern sind kostenlos.

Die Integration in OLAT ist in wenigen Schritten möglich.

Zuerst brauchen wir einen Kursbaustein "[Externe Seite](../../manual_user/learningresources/Knowledge_Transfer.de.md#external_page)" in
unserem Kurs:

![](attachments/gather_town_with_olat_de/KB-Externe-Seite.png)

Diesen konfigurieren wir wie folgt:

 **URL:** Die URL des gather.town-Raumes aus der Adressleiste des Browsers  
 **Darstellung konfigurieren:**

  * Entweder _„Neues Browser-Fenster (Quelle sichtbar)“_ , falls der Raum in einem neuen Browser Fenster erscheinen soll.
  * oder _„Eingebettet (Quelle sichtbar)“_ , wenn der Raum direkt in OLAT dargestellt werden soll.

![](attachments/gather_town_with_olat_de/Config_gather_town.png)

Danach können wir gather.town in OLAT im Kursrun auch schon nutzen:

![](attachments/gather_town_with_olat_de/Gather_town_run-2048x1087.png)

Achtung! gather.town läuft auf externen Servern. Bei der Verwendung müssen die
Datenschutzbestimmungen berücksichtigt werden.

Viel Freude und Erfolg beim Treffen im virtuellen Raum.
