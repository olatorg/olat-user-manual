# Using video

There are different ways of using videos in OLAT.

  * [Course element "Kaltura"](#course-element-kaltura)
  * [Course elements "Video" and "Video task"](#course-element-kaltura)
  * [Inserting videos in the content element "Page"](#inserting-videos-in-the-course-element-page)
  * [Inserting videos in the content editor in course element "HTML page"](#inserting-video-in-the-content-editor-course-element-html-page)
  * [Importing a Kaltura video via URL in OLAT](#importing-a-kaltura-video-into-olat-via-url)

## Course element "Kaltura"

The new video platform _Kaltura_ is offered to Swiss universities by SWITCH. The
[video platform](https://uzh.mediaspace.cast.switch.ch/){target=_blank} is open to all OLAT users and allows you to upload your own
videos and make them accessible to a dedicated audience.

Self recorded videos or your [lecture recordings](https://www.zi.uzh.ch/en/support/audio-video/event-recording.html){target=\_blank} (UZH lectures), can be
integrated in OLAT directly through the course element "Kaltura".
Comprehensive documentation on _Kaltura_ can be
found at <https://uzh.mediaspace.cast.switch.ch/Help>{target=_blank}.

You can find the online help for the Kaltura course element
[here](../kaltura/index.md).

## Course element "Video" and "Video task"

The learning resource "Video" from OpenOlat is not offered on OLAT-UZH, as Kaltura is the preferred video platform. However, you can import videos from other video platforms via URL in the [Course element "Video"](../../manual_user/learningresources/Course_Element_Video.md) and in the [Course element "Video task"](../../manual_user/learningresources/Course_Element_Video_Task.md).
You can also import videos from your device as page content in the course elements ["HTML page"](../../manual_user/learningresources/Video_in_HTML_Pages.md)".

## Inserting videos in the course element "Page"

You can add and even record videos as well in the course element ["Page"](../../manual_user/learningresources/Course_Element_Page.md).
You can upload to, record into or select videos from the [OLAT Media center](../../manual_user/personal_menu/Media_Center.md#add-media-to-the-media-center) (new since OLAT 6 / OpenOlat 18).

## Inserting video in the content editor (course element "HTML page")

You can upload a video file from your own computer via the "Video" button in the content editor of the course element "HTML page". 
Compare [Videos in course element "HTML page"](https://docs.olat.uzh.ch/manual_user/learningresources/Video_in_HTML_Pages/)

In the case of many and/or longer videos, we recommend integration via the Kaltura course element.

The video tool in the content editor of the course element "HTML page":

![](attachments/using_video_de/Inhaltseditor_Einzelne_Seite_EN.png)

![](attachments/using_video_de/Inhaltseditor_Video_hochladen_EN.png)

## Importing a Kaltura video into OLAT via URL

Kaltura does not provide direct links to video files. Links to videos on Kaltura normally lead to the media page or to a playlist on SWITCHcast MediaSpace. If you want to use a video from SWITCHcast MediaSpace or from "My media", e.g. in a course element "Video assignment", you must generate the link manually with the help of an extension in the browser [Mozilla Firefox](https://www.mozilla.org/de/firefox/new/){target=_blank}. To do this, carry out the following steps: 

  1. Start the Firefox browser and under the menu item "Extras"  choose "Add-Ons and Themes". Search for the Add-On [“Video Download Helper”](https://addons.mozilla.org/de/firefox/addon/video-downloadhelper/?utm_source=addons.mozilla.org&utm_medium=referral&utm_content=search){target=_blank} and choose "Add to Firefox".
  After the Add-On is activated, its icon is displayed in the menu bar in Firfox:<br />
  <br />
    ![](./attachments/using_video_de/Mozilla_video-download-helper_1.png)
  <br />
  <br />
  2. Open "My Media" in the personal menu in OLAT and call up the video whose URL you would like to generate.

  3. Play the video and select the icon of Download Helper that is displayed in color now. Choose "Copy URL" from the three dot menu to the right of the "download" button.<br />
  <br />
    ![](./attachments/using_video_de/Mozilla_video-download-helper.png)
    <br />
    <br />
  4. The URL you copied looks similar to this:
  _https://streaming.cast.switch.ch/hls/p/106/sp/10600/serveFlavor/entryId/0_xu2z4inr/v/2/flavorId/0_o720p621/name/a.mp4/index.m3u8_<br /><br />
  Replace _`https://streaming.cast.switch.ch/hls`_ at the beginning of the URL with _`https://api.cast.switch.ch`_ and delete the part of the URL following _`a.mp4`_:<br /><br /><del>`https://streaming.cast.switch.ch/hls`</del>/p/106/sp/10600/serveFlavor/entryId/0\_xu2z4inr/v/2/flavorId/0\_o720p621/name/a.mp4<del>`/index.m3u8`</del><br /><br />The URL should then look like this:
  <mark>https://api.cast.switch.ch</mark>/p/106/sp/10600/serveFlavor/entryId/0_xu2z4inr/v/2/flavorId/0_o720p621/name/a<mark>.mp4</mark>
  <br /><br />
  You can use this URL to add a video via URL for example in the course element ["Video Task"](../../manual_user/learningresources/Course_Element_Video_Task.md).