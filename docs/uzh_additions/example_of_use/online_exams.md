# Online exams

With OLAT you can carry out performance assessments of various kinds. In
addition to the course elements that serve the purpose of knowledge transfer
or collaboration, there are also a number of course elements that can be used
for control or assessment. On the one hand, there are those that are used
during the course or semester and serve more for formative assessment, such as
the assignment, the checklist, and on the other hand, there are the tests that
are used selectively and primarily for summative assessment.

## Performance evaluation

A number of course elements can be used either for summative or formative
assessment, or for progress monitoring. The following course elements can be
viewed and edited in the [assessment tool](../../manual_user/learningresources/Assessment_tool_overview.md):

  * [Task](../../manual_user/learningresources/Assessment.md#course_element_assessment) (manual assessment)
  * [Checklist](../../manual_user/learningresources/Course_Element_Checklist.md) (manual & automatic assessment)
  * [Assessment](../../manual_user/learningresources/Course_Element_Assessment.md) (manual assessment)
  * [LTI](../../manual_user/learningresources/Assessment_of_course_modules.md#lti) (automatic assessment, transferred from LTI side)
  * [SCORM](../../manual_user/learningresources/Assessment_of_course_modules.md#scorm) (automatic assessment, transferred by SCORM module)
  * [Test](../../manual_user/learningresources/Assessing_tests.md) (automatic & manual assessment)

For the task and the test there are separate subchapters dedicated to the
creation and use of the course element. The chapter "[Question Bank](../../manual_user/area_modules/Question_Bank.md)" deals with the creation of items for tests and surveys
that are available for collaborative use and editing in the question pool.
Tests can be created with items from the question pool.

## Performance record and certificate

A transcript of records is a confirmation of a completed performance
assessment and shows, for example, completed tests or submitted and assessed
tasks per course. Evidence of achievement must be switched on per course and
can be retrieved if there are assessable course elements in the course
concerned and if such an element has already been completed and assessed.
Certificates of achievement are available to users in the personal menu and in
the course, while tutors and authors can view them in the coaching tool.
Printable certificates can be issued by means of the course setting "[Evidence
of achievement](../../manual_user/personal_menu/Personal_Tools.md#evidences-of-achievement)".

