# Speichern in OLAT

Hier finden Sie einen kleinen Überblick über die Möglichkeiten es mit und im Umfeld von OLAT gibt, um Daten zu tauschen und zur Verfügung zu stellen:

![](attachments/save_data_in_olat_de/cloud-computing-cloud-infrastructure-abstract-background-dark-version.jpg)

## Szenario 1: Grosse Dateien verschicken

Sie haben eine sehr grosse Datei (z.B. Bauplan, Software), die Sie Ihren
Studierenen **einmalig** zur Verfügung stellen möchten. Für diesen Fall eignet
sich [SWITCHfilesender](https://filesender.switch.ch/){:target="\_blank"} perfekt. Mit diesem
Service können Datein bis 50 GB verschickt werden. Die Dateien stehen für 3
Wochen zum Download zur Verfügung. Danach werden sie vom Server gelöscht. Die
Studierenden werden per E-Mail informiert, wenn die Datei zur Verfügung steht,
und Sie werden informiert, wenn die Studierenden die Datei herunterladen.

## Szenario 2: Videos

Bevor die Videos gespeichert werden können, müssen sie natürlich erstellt
werden. Hilfe dazu finden Sie auf der Internetseite von MELS für die
[Aufzeichnung von Videos im Home-Office](https://www.zi.uzh.ch/de/teaching-
and-research/event-support/event-recording-home.html){:target="\_blank"}.

Eigene Videos können Sie auf der Streaming-Plattform der UZH, [SWITCHcast MediaSpace](https://uzh.mediaspace.cast.switch.ch/){:target="\_blank"}, oder im OLAT-Kurs Ihrer Veranstaltung im Kursbaustein [Kaltura](../kaltura/index.de.md){:target="\_blank"} hochladen und veröffentlichen.

## Szenario 3: Regelmässig verwendete Dateien

Für alle übrigen Dateien, auf die Sie und Ihre Studierenden regelmässig
Zugriff brauchen, gibt es gleich mehrere Möglichkeiten, diese abzulegen und zu
verteilen.

### SWITCHdrive

Ein Service, der schon länger im Einsatz ist und sich gut zum Austausch von
Daten auch mit anderen Personen der Schweizer Hochschul-Community eignet, ist
[SWITCHdrive](https://drive.switch.ch/){:target="\_blank"}. 

Wie Sie Dateien und Ordner, die auf SWITCHdrive liegen, in OLAT einbinden
können, zeigt dieses kurze Video:

<video width="480" height="270" controls>
  <source src="../attachments/save_data_in_olat_de/OLAT_SWITCHdrive.mp4" type="video/mp4">
</video>

### MS OneDrive

Falls Sie über einen Office365-Account verfügen (bei allen Angehörigen der
Universität Zürich ist dies der Fall), können Sie auch MS OneDrive nutzen, um Daten
abzulegen und via OLAT zur Verfügung zu stellen. Auch dazu gibt es ein kurzes
Video:

<video width="480" height="270" controls>
  <source src="../attachments/save_data_in_olat_de/OLAT_MSOneDrive.mp4" type="video/mp4">
</video>

Dateien, die auf MS OneDrive gespeichert werden, können direkt im Browser via
Office365 bearbeitet werden und auch ein Video Player steht zur Verfügung.

### OLAT-Ablage

Sie können auch Dateien in OLAT direkt hochladen und zur Verfügung stellen.
Das hat den Vorteil, dass Sie sich um Zugriffsbeschränkungen keine Gedanken
machen müssen, allerdings ist der Speicherplatz auf OLAT sehr beschränkt und
die Ablage von Dateien auf einer der beiden oben erwähnten Cloud Storages ist
empfohlen.

Wir hoffen, dass Sie fündig geworden sind und einen Eindruck erhalten haben, 
wie Sie Ihre Dateien mit und um OLAT optimal verteilen können. Falls Sie Fragen haben, 
steht Ihnen der OLAT-Support [lms-support@zi.uzh.ch](mailto:lms-support@zi.uzh.ch) gerne zur Verfügung.

Happy Uploading und Sharing!
