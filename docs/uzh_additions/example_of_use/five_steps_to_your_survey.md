# Five steps to your survey

This manual will help you to create a [survey](../../manual_user/learningresources/Assessment.md#course_element_survey) for your course
and share it with our participants.

## Step 1: Edit your course and insert a new course element "survey"

  1. Go to "Authoring" \- "My learning resources" and open the course in which you would like to add the survey.
  2. Open the "Course editor" from "Administration" drop-down.
  3. Click to the element after which one you would like to insert the new survey element.
  4. Click "[Insert course element](../../manual_user/learningresources/Using_additional_Course_Editor_Tools.md#insert-course-elements)" and choose "[Survey](../../manual_user/learningresources/Assessment.md#course_element_survey)".
  5. Change the title in the tab "Title and description" and add a description if needed. 
  6. Click "Save".

## Step 2: Create a form

  1. Go to the tab "Survey" and click "Choose, create or import".
  2. Click "Create".
  3. Give a name to your new form and click "Create"

Your new form learning resource was successfully created.  

## Step 3: Edit your form and add new questions

  1. Click on "Edit" to switch to the form editor. 
  2. Click "Add content" to add a new question to your (empty) form. 
  3. Choose the type of question from the drop-down (e.g. [Single choice](../../manual_user/learningresources/Form_editor_Questionnaire_editor.md#single-choice) or [multiple choice](../../manual_user/learningresources/Form_editor_Questionnaire_editor.md#multiple-choice)).
  4. The option "Presentation" structures your questions in vertical, horizontal or as a drop-down design.
  5. Enter all values.
  6. To add more values to the same question, click on "Add choice".
  7. To add more questions, click "Add content".
  8. To reposition your questions, arrange them by using drag & drop. 

Add as many questions, titles, text and information as needed according to the
same procedure. Use the element "Container" or "Seperator" to structure the
form.

## Step 4: Save the form learning resource

  1. Leave the form editor (red cross upper right corner) and save all content that has not been saved yet.
  2. Go back to your course.

## Step 5: Publish your course

  1. Navigate back to your course.
  2. Click "[Publish](../../manual_user/learningresources/Using_additional_Course_Editor_Tools.md#publishing)" in the upper right corner.
  3. Check if your new survey element ist listed and click "Next".
  4. Set your [course access](../../manual_user/learningresources/Access_configuration.md) and [publication status](../../manual_user/learningresources/Access_configuration.md) using the publish-assistance and the following steps. 
  5. Click "Finish" when done.
  6. Leave the course editor. 

Your survey with your attached form is now published and can be accessed by
the course participants.

The results will be stored anonymously and can be found under "[Survey
statistics](../../manual_user/learningresources/Using_Course_Tools.md#questionnaire-statistics)" or "[Archive tool](../../manual_user/learningresources/Data_archiving.md)".
