# Automatische Erinnerungen im Kursbaustein “Aufgabe” konfigurieren 

## Inhaltsübersicht

* [Übersicht](#ubersicht)
* [Schritt-für-Schritt Anleitung: Erinnerung erfassen](#schritt-für-schritt-anleitung-erinnerung-erfassen)
    * [Schritt 1: Bedingungen Bearbeiten](#schritt-1-bedingungen-bearbeiten) 
    * [Schritt 2: Bedingungen überprüfen](#schritt-2-bedingungen-überprüfen)
    * [Schritt 3: E-Mail-Benachrichtigung](#schritt-3-e-mail-benachrichtigung)
* [Erinnerung manuell versenden](#erinnerung-manuell-versenden) 

## Übersicht

!!! info ""

    Einen Überblick über alle Konfigurationsmöglichkeiten der Erinnerungsfunktion finden Sie in der [OpenOlat Dokumentation](https://docs.openolat.org/de/manual_user/learningresources/Course_Reminders){target = "_blank" class="lightbox border" }

Der Kursbaustein Aufgabe bietet die Möglichkeit, im Tab "Erinnerungen" bedingte Benachrichtigungen an Kursteilnehmende einzurichten. Häufig wird diese Funktion verwendet, um auf eine Abgabefrist hinzuweisen. 

Erinnerungen können manuell zu einem bestimmten Zeitpunkt oder automatisch aufgrund der festgelegten Bedingungen ausgelöst werden.  

In OLAT der UZH werden automatische Erinnerungen täglich zu drei Zeitpunkten versendet: um 6:00, 14:00 und 22:00 Uhr. Für jede:n Kursteilnehmer:in, welche:r alle Bedingungen erfüllt, wird dieselbe Erinnerung nur einmal beim nächsten Versandzeitpunkt verschickt. 

!!! warning "Erinnerung zu einem Abgabetermin"

    Beim Erfassen einer Erinnerung für Abgabetermine sollte immer auch eine **Zusatzbedingung "Bis Datum"** konfiguriert werden. Als Datum wird sinnvollerweise das Abgabedatum der entsprechen Aufgabe ausgewählt.
    Dadurch wird verhindert, dass unbeabsichtigt Erinnerungen an Kursteilnehmende ausgelöst werden, wenn der Kurs wiederverwendet wird (z.B. als Kopie oder als Vorlage für einen UZH-Campuskurs).  

## Schritt-für-Schritt Anleitung: Erinnerung erfassen

In der Kursansicht sehen Kursbesitzer:innen beim Kursbaustein “Aufgabe” vier Tabs:  

* Übersicht 
* Teilnehmer:innen 
* Verwalten 
* Erinnerungen 

![](attachments/erinnerung_im_KB_Aufgabe/Erinnerung_01_erstellen.png){ alt="Kursbaustein Aufgabe: Tab 'Erinnerungen' " class="lightbox border" }

Wählen Sie die Schaltfläche “Erinnerung erstellen”, um eine neue Erinnerung zu erfassen. 

Die Konfiguration von Erinnerungen umfasst drei Schritte:  

1. Bedingungen bearbeiten 
2. Bedingungen überprüfen 
3. E-Mail-Benachrichtigung verfassen  
<br />
![](attachments/erinnerung_im_KB_Aufgabe/Erinnerung_02_drei_schritte.png){ alt="Erinnerung erstellen, drei Schritte" class="lightbox border" }

### Schritt 1: Bedingungen bearbeiten
* Erfassen Sie eine **Beschreibung der Erinnerung** (Pflichtfeld) 

* Wählen Sie als Bedingung **“Termin: Aufgabe Dokument abgegeben”** und geben Sie ein, wie viele Tage, Wochen, ... vor dem Abgabetermin die Erinnerung erfolgen soll.  

* Über die Schaltfläche “Neue Bedingung wählen...” können eine oder mehrere zusätzliche Bedingungen hinzugefügt werden. Alle ausgewählten Bedingungen müssen erfüllt sein, damit eine Erinnerung verschickt wird ("UND”- Verknüpfung). <br /><br />

![](attachments/erinnerung_im_KB_Aufgabe/Erinnerung_02_erstellen.png){ alt="Neue Erinnerung: Bedingungen erfassen" class="lightbox border" }

* Wählen Sie bei “Und folgende Zusatzbedingungen erfüllt sind” die Zusatzbedingung: “Bis Datum” aus und tragen Sie das Datum des Abgabetermins ein. 

!!! warning "Erinnerungen nur für ausgewählte Gruppen"

    Falls Sie eine Erinnerung nur an eine bestimmte Gruppe in Ihrem Kurs versenden möchten, muss eine separate Zusatzbedingung für alle betroffene Gruppen erfasst werden (“Gruppenmitglieder” / entsprechende Gruppe auswählen). Dies gilt auch für Aufgabenbausteine, die mithilfe der Sichtbarkeits-/Zugangsregeln auf einzelne Gruppen des Kurses eingeschränkt sind.
    Andernfalls erhalten alle Mitglieder des Kurses die Erinnerung. 

### Schritt 2: Bedingungen überprüfen
Im nächsten Schritt werden die gewählten Bedingungen zur Überprüfung angezeigt. Unter “Vorschau Empfänger” werden die Kursmitglieder aufgelistet, welche die Bedingungen bereits erfüllen und damit die Erinnerungsnachricht beim nächsten Versand erhalten werden. 

![](attachments/erinnerung_im_KB_Aufgabe/Erinnerung_03_Bedingung_uberprufen.png){ alt="Erinnerung überprüfen" class="lightbox border" }

### Schritt 3: E-Mail-Benachrichtigung
Erfassen Sie nun den **Betreff** und den **Text der Erinnerungsnachricht.** Im Text können Benutzervariablen verwendet werden, um die Nachricht zu personalisieren ($firstName für den Vornamen, $lastName für den Nachnamen, $courseUrl für einen Link zum Kurs). 

![](attachments/erinnerung_im_KB_Aufgabe/Erinnerung_04_erinnerungstext_erfassen.png){ alt="Nachrichtentext erfassen" class="lightbox border" }

Wählen Sie die Benutzergruppen des Kurses aus, welche eine Kopie jeder versandten Erinnerungsnachricht erhalten sollen. Zur Auswahl stehen:

* Kursbesitzer:innen 
* Kurs- bzw. Gruppenbetreuer:innen 
* Externe E-Mail-Adressen (eine oder mehrere Adressen, kommagetrennt manuell einzugeben) 

Wählen Sie “Fertigstellen”, um die Konfiguration der Erinnerung abzuschliessen. Danach gelangen Sie zur Übersicht mit allen erfassten Erinnerungen.  

Über die Drei-Punkte-Schaltfläche können Sie sich eine Liste der bereits verschickten Erinnerungen anzeigen lassen (Versendete Erinnerungen zeigen).

## Erinnerung manuell versenden
Um eine Erinnerung manuell auszulösen, wählen Sie das Drei-Punkte-Menü der gewünschten Erinnerung und wählen Sie “Erinnerung jetzt senden” (es werden noch keine Erinnerungen verschickt). 

![](attachments/erinnerung_im_KB_Aufgabe/Erinnerung_05_erinnerungen_editieren.png){ alt="Versendete Erinnerungen verwalten" class="lightbox border" }

Nun wird eine Liste der Kursmitglieder angezeigt, welche die Bedingungen erfüllen. 

Über den Button “Erstmalig versenden (*Anzahl Empfänger*)” können Sie die Erinnerungsnachricht sofort verschicken. 

Über den Button “An alle senden” wird die Erinnerungsnachricht zusätzlich erneut an Kursmitglieder verschickt, welche die Nachricht bereits bekommen haben. 

![](attachments/erinnerung_im_KB_Aufgabe/Erinnerung_06_manuell_versenden.png){ alt="Erinnerung manuell versenden" class="lightbox border" }