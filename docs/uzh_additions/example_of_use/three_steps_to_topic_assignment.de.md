# Drei Schritte zur Erstellung und Verwaltung von Themen

Mit der folgenden Anleitung haben Sie in kurzer Zeit gelernt, wie Sie mit dem
Kursbaustein "Themenvergabe" ein Thema anbieten und Kursteilnehmer verwalten
können.

Im folgenden Beispiel hat der Kursautor die Themenvergabe so konfiguriert,
dass die Themenwahl des Kursteilnehmers nicht gleich gilt, sondern zuerst vom
Themenverantwortlichen bestätigt werden muss. Das bedeutet, dass sich
Kursteilnehmer für das Thema bewerben und Sie als Themenverantwortlicher
Kandidaten akzeptieren oder ablehnen können.

!!! info "Voraussetzungen"
    Der Kursautor hat einen Kursbaustein "[Themenvergabe](../../manual_user/learningresources/Other.de.md#topic_assignment)" in den
    Kurs eingebunden und Sie zum Themenverantwortlichen ernannt.

## Schritt 1: Kurs öffnen und Thema erstellen

  1. Im Autorenbereich unter "Meine Einträge" Kurs suchen und öffnen.
  2. Im Kursmenu links zur Themenvergabe navigieren.
  3. Oben im Inhaltsbereich auf "Thema erstellen" klicken.
  4. Im Tab "Beschreibung" Informationen zum Thema eingeben. Neben Titel und Beschreibung können Sie auch die Anzahl Bewerber limitieren, Abmeldungen gestatten oder verbieten, Dateien anhängen und eine E-Mail-Benachrichtigung einrichten.
  5. Optional: Wenn Sie weitere Betreuer für Ihr Thema bestimmen möchten, klicken Sie im Tab "Teilnehmerverwaltung" unter "Verantwortliche des Themas" auf "Benutzer hinzufügen" und wählen die gewünschte Person aus.

Ihr Thema erscheint nun in der Themenübersicht und Kursteilnehmer können sich
dafür bewerben.

Wenn Sie die Checkbox "E-Mail-Benachrichtigung bei Themen Auswahl/Abwahl" in
der Beschreibung des Themas markiert haben, erhalten Sie eine E-Mail, sobald
sich Kursteilnehmer für Ihr Thema bewerben.

## Schritt 2: Teilnehmer verwalten

  1. In der Themenübersicht auf den Titel Ihres Themas klicken und in den Tab "Teilnehmerverwaltung" wechseln.
  2. In der Kandidatenliste diejenigen Personen auswählen, denen das Thema zugeteilt werden soll. Auf "Als Teilnehmer übernehmen" klicken. Bei Bedarf die E-Mail-Benachrichtigung für die akzeptierten Kursteilnehmer anpassen und versenden.
  3. In der Kandidatenliste diejenigen Personen auswählen, denen das Thema nicht zugeteilt werden soll. Auf "Entfernen" klicken. Bei Bedarf die E-Mail-Benachrichtigung für die nicht akzeptierten Kandidaten anpassen und versenden.
  4. Wenn sich keine weiteren Kandidaten bewerben können sollen, im Tab "Beschreibung" auf "Themenstatus auf "Belegt" setzen" klicken.

Akzeptierte Teilnehmer können nun beim gewählten Thema im Tab "Ordner" über
den Abgabeordner Dateien einreichen.

## Schritt 3: Dateien verwalten (optional)

  1. Wenn Dateien eingereicht wurden, auf den Tab "Ordner" in Ihrem Thema klicken und den Ordner des Teilnehmers öffnen.  
Abonnieren Sie den Abgabeordner ihres Themas, um bei neu eingereichten Dateien
benachrichtigt zu werden.

  2. Sie können dem Teilnehmer Dateien über den Rückgabeordner zurückgeben. Im Tab "Ordner" den Ordner des akzeptierten Teilnehmers wählen und auf "Datei hochladen" klicken.
