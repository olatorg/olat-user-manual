# Eine Umfrage automatisch nach Kursende freischalten

!!! info "Anforderung"

    **Einige** Tage nach Kursbeginn oder Kursende sollen die Teilnehmer eines
    Kurses Feedback zum Kurs abgeben, indem sie eine Umfrage ausfüllen.


!!! hint "Voraussetzung"

    * Kurs mit [Durchführungszeitraum](../../manual_user/learningresources/Course_Settings.de.md#tab-durchfuhrung) versehen
    * Formular in Kurs einbinden

## Vorgehensweise

### Sichtbarkeitsregeln definieren

Auf dem [Umfrage-Baustein](../../manual_user/learningresources/Assessment.de.md#course_element_survey) muss im Tab
[Sichtbarkeit](../../manual_user/learningresources/General_Configuration_of_Course_Elements.de.md#access) eine Regel hinterlegt sein, um
festzulegen, wann der Baustein sichtbar sein soll. Dies ist im einfachen
Konfigurationsmodus nur mit absoluten Daten machbar. Um mit relativen Daten zu
arbeiten, muss in den [Expertenmodus](../../manual_user/learningresources/Access_Restrictions_in_the_Expert_Mode/) gewechselt werden. Damit
kann gesteuert werden, dass die Umfrage z.B. erst bei Kursende z.B. für eine
Woche angezeigt wird.

#### Schritt 1: Expertenmodus öffnen

  1. Im Kurseditor zum Baustein "[Umfrage](../../manual_user/learningresources/Assessment.de.md#course_element_survey)" navigeren
  2. Tab "[Sichtbarkeit](../../manual_user/learningresources/General_Configuration_of_Course_Elements.de.md#access)" öffnen
  3. Auf Schaltfläche "[Expertenmodus](../../manual_user/learningresources/Access_Restrictions_in_the_Expert_Mode/) anzeigen" klicken

#### Schritt 2a: Expertenregel anlegen: Dauer nach Kursbeginn

Fügen Sie folgende Expertenregel hinzu:
    
    (today <= getCourseBeginDate(0) + 7d)

Diese Expertenregel besagt, dass die Umfrage nach dem Kursbeginn 7 Tage lang
sichtbar sein soll.  
Die Anzahl Tage kann nach Wunsch angepasst werden.

#### Schritt 2b: Expertenregel anlegen: Dauer nach Kursende

Fügen Sie folgende Expertenregel hinzu:
    
    (getCourseEndDate(0) >= today) & (getCourseEndDate(0) + 7d >= today)

Diese Expertenregel besagt, dass die Umfrage nach dem Kursende 7 Tage lang
sichtbar sein soll.  
Die Anzahl Tage kann nach Wunsch angepasst werden.

#### Schritt 3: Speichern

Speichern Sie Ihre Expertenregeln und [publizieren](../../manual_user/learningresources/Using_additional_Course_Editor_Tools.de.md#publizieren)
Sie Ihren Kurs erneut.

### Erinnerung hinzufügen

Über das [Erinnerungswerkzeug](../../manual_user/learningresources/Course_Reminders.de.md) können Sie z.B. 3
Tage nach Kursbeginn oder Kursende eine Erinnerung versenden, damit die
Teilnehmer die Umfrage ausfüllen.

#### Erinnerung hinzufügen:

  1. Im Drop-Down-Menü "Administration" den Punkt "[Erinnerung](../../manual_user/learningresources/Course_Reminders.de.md)" öffnen
  2. Schaltfläche "Erinnerung erstellen" klicken. Eine neue Erinnerung öffnet sich
  3. Beschreibung eingeben
  4. Bedingungen hinzufügen:  

    1. Nach Kursstart  
![](attachments/automatically_activate_a_questionnaire_at_the_end_of_the_course_de/Erinnerung_-_Kursbeginn.png)

    2. Nach Kursende  
![](attachments/automatically_activate_a_questionnaire_at_the_end_of_the_course_de/Erinnerung_-_Kursende.png)

  5. E-Mail Betreff und Text einfügen
  6. Speichern

  