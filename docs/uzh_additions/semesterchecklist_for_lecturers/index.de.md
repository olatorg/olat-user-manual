# Semesterstart Checkliste für Dozierende der UZH

Angelehnt an den allgemeinen [Kurszyklus](../../../de/manual_user/learningresources/General_Information/){target="_blank"} finden Sie hier einige Tipps und Tricks, die es beim Semesterstart zu beachten gilt:

* [Kurs erstellen](#kurs-erstellen)
* [Kursbesitzer und -Betreuer hinzufügen](#kursbesitzer-und-betreuer-hinzufugen)
* [Kurs einrichten](#kurs-einrichten)
* [Kurszugang für Teilnehmende einrichten](#kurszugang-fur-teilnehmende-einrichten)
* [Kurs veröffentlichen](#kurs-veroffentlichen)
* [Kurs durchführen](#kurs-durchfuhren)
* [Kurs beenden](#kurs-beenden)
* [Kurs löschen ](#kurs-loschen)

## Kurs erstellen

!!! info "Information"

    Die meisten Fakultäten der UZH verwenden [Campuskurse](../campus_course/index.de.md){target=_blank} (mit dem Modulbuchungssystem der UZH verknüpfte OLAT-Kurse). Die Anleitungsschritte mit dem Vermerk `Campuskurse:` sind für diese Lehrveranstaltungen relevant. Die übrigen Schritte gelten für normale OLAT-Kurse, auch für Kurse an anderen Institutionen als der UZH.


`Normale Kurse:`

* [x] Ich sehe den Menüpunkt "[Autorenbereich](../../manual_user/area_modules/Authoring/){:target=\_blank}" in OLAT

??? note "Autorenrechte beantragen"

    Autorenrechte für Ihren OLAT Account können Sie beim [OLAT Support](https://www.zi.uzh.ch/de/support/e-learning-and-examination/staff/olat.html){target="_blank"} beantragen. Sie müssen sich dazu [bei OLAT registriert](../registration/registration_with_a_switch_edu-id.de.md){target="_blank"} haben.

`Campuskurse:`

* [x] Ich sehe den Menüpunkt "Campuskurse"

??? note "Eintrag als Dozent/Dozentin im Vorlesungsverzeichnis der UZH (VVZ)"

    Stellen Sie sicher, dass Sie für die gesuchte Veranstaltung als dozierende Person im [VVZ](https://courses.uzh.ch){target="_blank"} eingetragen sind. Wenden Sie sich im Zweifelsfall an die SAP-Verantwortlichen Ihres Instituts oder Ihrer Fakultät.<br /><br />
    **Dozierende eines Campuskurses, welche als Dozierende des entsprechenden Kurses im VVZ erfasst sind, erhalten automatisch OLAT-Autorenrechte.**
    Um Campuskurse erstellen zu können, müssen Sie ein OLAT-Konto verwenden, welches mit Ihrer UZH-Identität verknüpft ist.<br /><br />
    Informationen und Anleitungen zur Registrierung bei OLAT und zur Verknüpfung ihrer UZH-Identität im Switch edu-ID-Konto finden Sie im Bereich [OLAT-Registrierung](../registration/registration_for_uzh_members.de.md){target="_blank"}.

* [ ] `Optional bei Campuskursen:` **Delegationen für Campuskurse erteilen**<br /><br />Ich teile dem [OLAT Support](https://www.zi.uzh.ch/en/support/e-learning-and-examination/staff/olat.html){target="_blank"} mit, welche OLAT-Benutzer in meinem Namen meine Campuskurse für mich erstellen können sollen ([Delegation](../delegation/)).

??? info "Hintergrundinfo zu Delegationen"

    Beantragen Sie Delegationsrechte für Ihre Assistierenden oder Mitarbeitenden beim [OLAT Support](https://www.zi.uzh.ch/en/support/e-learning-and-examination/staff/olat.html){target="_blank"}  (sofern gewünscht). Alle im VVZ gelisteten Dozierenden Ihrer Veranstaltung und alle in Ihrem OLAT-Profil (im [persönlichen Menü](../../manual_user/personal_menu/) > Profil > "Rollen und Delegationen") als Kursverwalter eingetragenen Personen werden standardmässig als Kursbesitzer im neu erstellten Campuskurs eingetragen.

<hr />

`Normale Kurse:`

* [x] Ich [erstelle den neuen Kurs](../../../de/manual_user/learningresources/){target="_blank"} im "Autorenbereich" > Button: "Erstellen: Kurs" 

`Campuskurse:`

* [x] Ich erstelle den neuen Campuskurs über den Menüpunkt "Campuskurse"<br />Siehe Kapitel [Kurserstellung](../campus_course/#creation){target="_blank"}.

??? info "Campuskurse auf Basis eines bestehenden OLAT-Kurses erstellen (Option 2)"

    Wenn der zu erstellende Campuskurs dieselben Inhalte oder dieselbe Struktur haben soll wie ein früherer OLAT-Kurs, bei welchem Sie bereits Besitzer oder Besitzerin sind, können Sie beim Erstellen des Campuskurses im zweiten Schritt Option 2 ("Basierend auf einem bereits bestehenden Kurs") wählen.
    Die Kursbausteine und die meisten Konfigurationen werden dann aus dem bestehenden Kurs übernommen. Die Teilnehmenden des bestehenden Kurses werden nicht in den neu erstellten Campuskurs übernommen. 
    Falls Sie den Kurs mit den bisherigen Teilnehmenden weiterführen wollen, z.B. bei einem mehrsemestrigen Kurs, verwenden Sie zum Erstellen des Campuskurses Option 3 "Campuskurs des Vorsemesters weiterführen".

<hr />

## Kursbesitzer und -Betreuer hinzufügen

`Normale Kurse:`

* [x] Ich erfasse weitere Kursbesitzer und -Betreuer über "Administration" - "[Mitgliederverwaltung](../../../de/manual_user/learningresources/Members_management){:target=\_blank}"

`Campuskurse:`

??? info "Mitgliederverwaltung bei Campuskursen"

    Bei Campuskursen der UZH werden die im Vorlesungsverzeichnis als Dozierende der Veranstaltung aufgeführte Personen automatisch als Besitzer im entsprechenden OLAT-Campuskurs hinzugefügt.
    Studierende, welche über das Modulbuchungssystem der UZH eine Veranstaltung gebucht haben, werden automatisch beim zugehörigen OLAT-Campuskurs als Kursmitglieder hinzugefügt (Gruppe "\_ UZH Studierende mit Modulbuchung").
    

    Sie können in der "[Mitgliederverwaltung](../../../de/manual_user/learningresources/Members_management){:target=\_blank}" über "Administration" > "Mitgliederverwaltung" > "Mitglieder hinzufügen" weitere Personen als Kursbesitzer:in oder als Kursteilnehmende (mit Gruppenzugehörigkeit "\_ Manuell hinzugefügte Teilnehmende") zum Kurs hinzugügen.

    In Campuskursen, welche mit der Standardvorlage erstellt worden sind ([Campuskurs erstellen mit Option 1](../campus_course/#option-1-standardkurs-mit-materialienordner-und-kommunikationsmoglichkeiten){:target=\_blank}), finden Sie einen Kursbaustein "Einschreibung": 
    Auditorinnen/Gasthörende können sich über diesen Kursbaustein selbst als Kursteilnehmende einschreiben und werden dadurch in die Kursgruppe "\_ Manuell hinzugefügte Teilnehmende" eingetragen.

<hr />

## Kurs einrichten

`Normale Kurse:`

* [x] Ich prüfe die im Kurs bestehenden Bausteine und stelle sicher, dass die für diesen Kurs benötigten Bausteine vorhanden und korrekt konfiguriert sind.
Ich entferne nicht benötigte Kursbausteine.

??? info "Die Standard-Kursvorlage beinhaltet folgende Bausteine:"

    * Informationsseite (Kursbaustein "[Einzelne Seite](../../manual_user/learningresources/Course_Element_Single_Page/)")
    * [Einschreibung](../../manual_user/learningresources/Administration_and_Organisation/#enrolment)
    * [Materialordner](../../manual_user/learningresources/Course_Element_Folder/)
    * [Forum](../../manual_user/learningresources/Communication_and_Collaboration/#forum)
    * [Mail](../../manual_user/learningresources/Administration_and_Organisation/#mail)

* [ ] `Optional` Ich erstelle neue Bausteine und füge diese in meinen Kurs ein und konfiguriere diese nach meinen Wünschen. <br />
[Übersicht aller OLAT Kursbausteine](../../manual_user/learningresources/Course_Elements/)

* [x] Ich prüfe, welche Bausteine ich in der Sichtbarkeit- und Zugangsbeschränkungen einschränken muss.<br />
Bei Lernpfadkursen wird die Sichtbarkeit von Kursbausteinen im Tab "Lernpfad" über ["Ausnahmen"](../../manual_user/learningresources/Learning_path_course_Course_editor/#ausnahmen) eingestellt.

`Campuskurse:`

* [x] Ich prüfe die im Kurs bestehenden Bausteine und stelle sicher, dass die für diesen Kurs benötigten Bausteine vorhanden und korrekt konfiguriert sind.
Ich entferne nicht benötigte Kursbausteine.

??? info "Die Standard-Campuskursvorlage beinhaltet folgende Bausteine:"

    * [Anmeldung](../../de/manual_user/learningresources/Administration_and_Organisation/#enrolment) (Siehe dazu die Infobox unter [Kursbesitzer und -Betreuer hinzufügen](#kursbesitzer-und-betreuer-hinzufugen))
    * [Materialien](../../manual_user/learningresources/Course_Element_Folder/)
    * [Mitteilungen](../../manual_user/learningresources/Administration_and_Organisation/#notification)
    * [Forum]((../../manual_user/learningresources/Communication_and_Collaboration/#forum))
    * [Kaltura](../kaltura/)
    * [Aufgabe](../../manual_user/learningresources/Assessment/#course_element_task)
    * Bereich für Dozierende (nicht sichtbar für Studierende)
        * @Studierende (siehe Infobox "Konfiguration E-Mail Kursbaustein '@Studierende'" und allgemeines zum [E-Mail Kursbaustein](../../manual_user/learningresources/Course_Element_EMail.md))
        * [Teilnehmer*innen-Liste](../../manual_user/learningresources/Course_Element_Participant_List)
        * @OLAT Support (Supportanfrage an den OLAT-Support senden)
        * Empfohlene Bausteine:
            * [Literatur](../literature/) (zur Verfügung stellen einer Literaturliste)
            * [Zoom](../supporting_technologies/zoom_video_conferences.de.md) (Online-Sitzungen über den UZH-Zoom-Account einrichten)

??? note "Konfiguration E-Mail Kursbaustein '@Studierende'"

    Der Baustein "@Studierende" soll an beide Campuskursgruppen "_ UZH Studierende mit Modulbuchung" und "_ Manuell hinzugefügte Teilnehmende" E-Mails versenden (unter "Empfänger > Versand an Teilnehmer\*innen > Alle Teilnehmer\*innen der selektierten Gruppen > Lernbereiche" "Campuslernbereich" auswählen oder unter "Lergruppen > Gruppe auswählen" die oben genannten Gruppen auswählen).
    
* [ ] `Optional` Ich erstelle neue Bausteine und füge diese in meinen Kurs ein und konfiguriere diese nach meinen Wünschen. <br />
[Übersicht aller OLAT Kursbausteine](../../../manual_user/course_elements)

* [x] Ich prüfe, welche Bausteine ich in der Sichtbarkeit- und Zugangsbeschränkungen einschränken muss<br />
Bei Lernpfadkursen wird die Sichtbarkeit von Kursbausteinen im Tab "Lernpfad" über ["Ausnahmen"](../manual_user/learningresources/Learning_path_course_Course_editor.de.md) eingestellt.

* [ ] `Optional` Ich verwende einen [Livestream](https://www.zi.uzh.ch/de/support/audio-video/livestreaming.html){target="_blank"} oder möchte [Vorlesungsaufzeichnungen](https://www.zi.uzh.ch/de/support/audio-video/event-recording.html){target="_blank"} via [Kaltura](../kaltura/) einbinden und füge diesen Baustein meinem Kurs hinzu

* [x] Ich [publiziere](../../manual_user/learningresources/Using_additional_Course_Editor_Tools/#publizieren) meine Änderungen im Kurseditor, um die neuen oder geänderten Inhalte sichtbar zu machen.

!!! info ""

    Der Kurs ist noch nicht für Teilnehmende zugänglich, solange er den Status "Vorbereitung", "Review" oder "Freigabe Betreuer*innen" hat. Siehe dazu den Abschnitt ["Kurs veröffentlichen"](#kurs-veroffentlichen).

<hr />

## Kurszugang für Teilnehmende einrichten

`Campuskurse:`

* [x] Unter "Administration - Einstellungen - Freigabe" ist "Buchbare und offene Angebote" ausgewählt, damit sich Gasthörende und Teilnehmende externer Hochschulen in den Kurs einschreiben können. Siehe [Zugangskonfiguration / Freigabe](../../manual_user/learningresources/Access_configuration/#tab-freigabe)

??? info "Freigabeeinstellung: Privat"

    Mit der Freigabe-Option "Privat" kann der Kurs nicht in der Kurssuche gefunden werden. Dadurch können sich Gasthörende nicht in den Kurs einschreiben, auch wenn ein Kursbaustein "Einschreibung" vorhanden ist. Verwenden Sie die Freigabeeinstellung "Privat" nur dann, wenn im Kurs keine Auditor*innen zugelassen sind. Kursteilnehmende, welche nicht an der UZH immatrikuliert sind, müssen von einer Person mit Kursbesitzerrechten manuell zur Gruppe "_ Manuell hinzugefügte Teilnehmende" hinzugefügt werden.

* [x] Ich wähle die korrekte Organisation für meine Teilnehmenden aus (im Fall von UZH-Campuskursen: OLAT). [Freigabe / Angebote](../../manual_user/learningresources/Access_configuration/#offer)

??? info "Organisationsfreigabe bei Campuskursen"

    Wählen Sie "OLAT", um Angehörige aller Organisationen zuzulassen. Auditoren und Auditorinnen können so den Kurs finden und sich im Einschreibebaustein des Campuskurses in die Gruppen "_ Manuell hinzugefügte Teilnehmende" eintragen. Durch die gruppenabhängige Einschränkung der Kursbausteine im Tab "Sichtbarkeit" und "Zugang" auf den Lernbereich "Campuslernbereich" haben nicht in der Mitgliederverwaltung des Kurses aufgeführte OLAT-Benutzer keinen Zugriff auf die Inhalte.
    <br /><br />
    In den Freigabe-Einstellungen können Sie unter "Angebot" ebenfalls auswählen, für welche Organisation(en) der Kurs freigegeben sein soll. Diese sollten mit der/den unter "Freigabe > Organisation" ausgewählten Organisation(en) übereinstimmen.

<hr />

## Kurs veröffentlichen

* [x] Ich prüfe, ob der [Status](../../manual_user/learningresources/Access_configuration/#status-der-veroffentlichung) korrekt auf "veröffentlicht" gesetzt ist.

* [x] Ich prüfe mit der Schaltfläche "Rolle", ob ich in der Teilnehmer\*innen-Rolle die vorgesehenen Bausteine sehe.
<hr />

## Kurs durchführen

* [x] Ich habe meinen OLAT-Kurs vor dem ersten Veranstaltungstermin bereit und die entsprechenden Materialien in den Materialienordner hochgeladen.
<hr />

## Kurs beenden

* [ ] Ich prüfe nach Ablauf des Semesters, ob ich den Kurs in den Status "beendet" setzen kann.

??? info "Kurstatus 'Beendet'"

    Im Kurstatus "Beendet" ist nur noch Lesezugriff möglich. Bei den Kursmitgliedern erscheint der Kurs nun unter "Beendete Kurse" anstatt unter "Aktiv".

Sollte der Kurs von den Teilnehmenden uneingeschränkt zur Prüfungsvorbereitung genutzt werden können lohnt es sich, diesen bis zum Termin allfälliger Wiederholungsprüfungen im Status "Veröffentlicht" zu belassen.
<hr />

## Kurs löschen

* [x] Ich prüfe, ob ich den Kurs nicht mehr als Duplikat oder als Basis für ein kommendes Semester brauche und werde ihn daher nach Ablauf des Semesters (oder einige Semester später) löschen.
<hr />

Für weitere Fragen steht Ihnen der [OLAT Support](https://www.zi.uzh.ch/de/support/e-learning-and-examination/staff/olat.html) gerne zur Verfügung.
