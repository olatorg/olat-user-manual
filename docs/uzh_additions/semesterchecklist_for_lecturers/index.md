# Semester Checklist for Lecturers at UZH

Based on the general [course cycle](../../manual_user/learningresources/General_Information/){target="_blank"} here are some tips and tricks to keep in mind at the start of the semester:

* [Create a course](#create-a-course)
* [Add course owners and coaches](#add-course-owners-and-coaches)
* [Set up your course](#set-up-your-course)
* [Set up course access for participants](#set-up-course-access-for-participants)
* [Publish your course](#publish-your-course)
* [Run the course](#run-the-course)
* [End your course](#end-your-course)
* [Delete your course](#delete-your-course)

## Create a course

!!! info "Information"

    Most UZH faculties use [campus courses](../campus_course/index.md){target=_blank} (OLAT courses linked to the UZH module booking system). The instruction steps with the note `Campus courses:` are relevant for these courses. The other steps apply to normal OLAT courses, including courses at institutions other than UZH.

`Normal courses:`

* [x] I see the menu item "[Authoring](../../manual_user/area_modules/Authoring/){:target=\_blank}" in OLAT

??? note "Apply for author rights"

    You can request author rights for your OLAT account from [OLAT Support](https://www.zi.uzh.ch/en/support/e-learning-and-examination/staff/olat.html){target="_blank"} . To do so, you must [register with OLAT](../registration/registration_with_a_switch_edu-id.md){target="_blank"} first.

`Campus courses:`

* [x]  I see the menu item "Campus courses"

??? note "Entry as a lecturer in the UZH course catalog (VVZ)"

    Make sure that you are listed as a lecturer for the course you are looking for in the course catalog [VVZ](https://courses.uzh.ch){target="_blank"}.
    In case of doubt, contact the person responsible for SAP (Semesterplanning Database) at your institute or your faculty.<br /><br />
    Lecturers of a campus course who are listed as lecturer of the corresponding course in the VVZ automatically receive OLAT author rights.<br />
    To be able to create campus courses, you must use an [OLAT account that is linked to your UZH identity](../registration/registration_for_uzh_members.md).<br /><br />
    Information and instructions on registering with OLAT and linking your UZH identity in the Switch edu-ID account can be found in the [OLAT registration](../registration/registration_for_uzh_members.md){target="_blank"} area.
    
* [ ] `Optional for Campus courses:` **Granting delegations for campus courses**<br /><br />I inform [OLAT Support](https://www.zi.uzh.ch/en/support/e-learning-and-examination/staff/olat.html){target="_blank"}, which OLAT users should be able to create my campus courses on my behalf ([delegation](../delegation/)).

??? info "Background information on delegations"

    Apply for delegation rights for your assistants or employees at [OLAT Support](https://www.zi.uzh.ch/en/support/e-learning-and-examination/staff/olat.html){target="_blank"}  (if desired). All lecturers of your course listed in the VVZ and all persons entered as course administrators in your OLAT profile (in your [personal menu](../../manual_user/personal_menu/) > profile > "Roles and delegations") will be added as course owners in the newly created campus course by default.

<hr />

`Regular courses:`

* [x]  I [create the new course](../../manual_user/learningresources/){target="_blank"}  in the menu "Authoring" > Button: "Create: Course" 

`Campus courses:`

* [x] I create the new campus course via the menu item "Campus courses"<br />See chapter [Campus courses](../campus_course/#creation){target="_blank"} 

??? info "Create campus courses based on an existing OLAT course (option 2)"

    If the campus course to be created should have the same content or the same structure as a previous OLAT course that you already own, you can select option 2 ("Based on an existing course") in the second step when creating the campus course.
    The course elements and most of the configurations will then be taken from the existing course. The participants of the previous course will not be transferred to the newly created campus course. 
    If you would like to continue the course with the previous participants, e.g. for a multi-semester course, use option 3 "Continue campus course of the previous semester" to create the campus course.

<hr />

## Add course owners and coaches

`Regular courses:`

* [x] I add further course owners and coaches via "Administration" -["Members management"](../../manual_user/learningresources/Members_management){:target=\_blank}"

`Campus courses:`

??? Info "Members management with campus courses"

    For UZH campus courses, the persons listed in the course catalog as lecturers of the course are automatically added as owners in the corresponding OLAT campus course.
    Students who have booked a course via the UZH module booking system are automatically added to the corresponding OLAT campus course as course members (group "_ UZH students with booking modules").

    You can add further persons to the course as course owners or as course participants (with group membership "_ Manually added participants") in "Administration" - "[Members management](../../manual_user/learningresources/Members_management){:target=\_blank}" via "Administration" > "Members management" > "Add members".

    In campus courses that have been created with the standard template ([Create campus courses with option 1](../campus_course/#option-1-standard-course-with-materials-folder-and-communication-tools){:target=\_blank}), you will find a course element "Registration":
    Auditors can use this course element to enrol themselves as course participants and are thus added to the course group "_ Manually added participants".

<hr />

## Set up your course

`Regular courses:`

* [x] I check the existing course elements in the course and make sure that the course elements required for this course are available and correctly configured.
I remove course elements that are not required.

??? info "The standard course template (wizard) contains the following modules:"

    * Information page (Course element "[Single page](../../manual_user/learningresources/Course_Element_Single_Page/)")
    * [Enrollment](../../manual_user/learningresources/Administration_and_Organisation/#enrolment)
    * [Download folder](../../manual_user/learningresources/Course_Element_Folder/)
    * [Forum](../../manual_user/learningresources/Communication_and_Collaboration/#forum)
    * [E-mail](../../manual_user/learningresources/Administration_and_Organisation/#mail)

* [ ] `Optional` I create new course elements and insert them into my course and configure them according to my wishes <br />
[Overview of all OLAT course elements](../../manual_user/learningresources/)

* [x] I check for which course elements I have to set restricted visibility and access. <br />
For learning path courses, the visibility of course elements is set in the "Learning path" tab via ["Exceptions"](../../manual_user/learningresources/Learning_path_course_Course_editor/#exceptions).

`Campus courses:`

* [x] I check the existing course elements in the course and make sure that the course elements required for this course are available and correctly configured.
I remove course elements that are not required.

??? info "The standard campus course template contains the following course elements:"

    * [Registration](../../manual_user/learningresources/Administration_and_Organisation/#enrolment) (See the info box under [Add course owners and coaches](#add-course-owners-and-coaches))
    * [Materials](../../manual_user/learningresources/Course_Element_Folder/)
    * [Notifications](../../manual_user/learningresources/Administration_and_Organisation/#notification)
    * [Forum](../../manual_user/learningresources/Communication_and_Collaboration/#forum)
    * [Kaltura](../kaltura/)
    * [Task](../../manual_user/learningresources/Assessment/#course_element_task)
    * Area for instructors (not visible for participants):
        * @Students (see info box "Configuration e-mail course element" and [e-mail course element](../../manual_user/learningresources/Administration_and_Organisation/#mail))
        * [Participant list](../../manual_user/learningresources/Communication_and_Collaboration/#participant_list)
        * @OLAT Support (send a support request to the OLAT support team)
        * Recommended elements:
            * [Literature](../literature/) (to provide a bibliography) 
            * [Zoom](../supporting_technologies/zoom_video_conferences.md) (for setting up online lectures via your UZH zoom account)

??? note "Configuration e-mail course element '@Students'"

    The "@Students" course element should send e-mails to both campus course groups "_ UZH students with booking module" and "_ Manually added participants" (select "Campus learning area" under "Recipients > Distribution to participants > All participants of the selected groups > Select learning area" or select the above-mentioned groups under "Selected groups > Select group").
    
* [ ] `Optional` I create new course elements and insert them into my course and configure them according to my wishes <br />
[Overview of all OLAT course elements](../../manual_user/course_elements)

* [x] I check for which course elements I have to set restricted visibility and access.<br />
For learning path courses, the visibility of course elements is set in the "Learning path" tab via ["Exceptions"](../../manual_user/learningresources/Learning_path_course_-_Course_editor/#exceptions).

* [ ] `Optional` I use a [livestream](https://www.zi.uzh.ch/en/support/audio-video/livestreaming.html){target="_blank"} or I wish to insert [lecture recordings](https://www.zi.uzh.ch/en/support/audio-video/event-recording.html){target="_blank"} via [Kaltura](../kaltura/) and add this course element to my course.

* [x] I [publish](../../manual_user/learningresources/Using_additional_Course_Editor_Tools/#publishing) my changes in the course editor, to make the new or edited content visible.

!!! info ""

    The course is not yet accessible to participants as long as it has the status "Preparation", "Review" or "Access for Coach". See the section ["Publish your course"](#publish-your-course).

<hr />

## Set up course access for participant

`Campus courses:`

* [x] Under "Administration - Settings - Share", "Bookable and open offers" is selected so that auditors and participants from external universities can enrol in the course. See [Access configuration](../../manual_user/learningresources/Access_configuration/#tab-share)

??? info "Share option: Private"

    With the "Private" share option, the course cannot be found in the course search. This means that guest students cannot enroll in the course, even if an "Enrollment" course element is available. Only use the "Private" share option if no auditors are permitted in the course. Course participants who are not matriculated at UZH must be manually added to the group "_ Manually added participants" by a person with course owner rights.

* [x] I choose the correct organization for my participants. (For campuscourses use the organization 'OLAT'). See [Share / Offers](../../manual_user/learningresources/Access_configuration/#offer)

??? info "Organization share for campus courses"

    Select "OLAT" to admit members of all organizations. This way auditors can find the course in the course search and can register to the course in the enrollment course element of the campus course (group "_ Manually added participants"). The visibility and access configuration of the standard course elements in a campus course is set in a way, that  OLAT users who are not added automatically through synchronization with the module booking system (group "_ Students with booking module") or who did not selfregister as auditor through the enrollment course element do not have access to the course content.
    <br /><br />
    In the share settings of the selected "Offer", you can also select the organization(s) for which the course should be accessible. The selection should match the organization(s) selected under "Share > Organization".

<hr />

## Publish your course

* [x]  I check whether the [Status](../../manual_user/learningresources/Access_configuration/) is correctly set to "published"

* [x] I use the "Role" button (switch from "OWNER" to "Participants view") to check whether I see the intended course elements in the participant role.
<hr />

## Run the course

* [x] I have prepared my OLAT course before the first course date and uploaded the relevant materials to the materials folder.
<hr />

## End your course

* [ ] `Optional` At the end of the semester, I check whether I can set the course to "FINISHED" status.

??? info "Course status 'FINISHED'"

    In the course status "FINISHED" course access is limited to "read only". For course members, the course now appears under "Finished courses" instead of "Active".

If the course should be accessible by participants for exam preparation without restriction, it is suggested to leave it in "PUBLISHED" status until the last repeat exam is over.
<hr />

## Delete your course

* [x] I check whether I no longer need the course as a duplicate or as a basis for an upcoming semester and will therefore delete it at the end of the semester (or a few semesters later).
<hr />

If you have any further questions, please contact [OLAT Support](https://www.zi.uzh.ch/en/support/e-learning-and-examination/staff/olat.html).
