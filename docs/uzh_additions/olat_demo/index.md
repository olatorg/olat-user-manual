# OLAT Demo

To allow you to test the new features of OLAT 6 before this new version has been released you can find here a [demo instance with OLAT 6.0](https://demo.olat.uzh.ch){target=_blank}.

In order to test your OLAT courses (created in earlier OLAT versions) on the demo instance you can export them as described below and import them into the demo instance.

## Instructions: Importing and Exporting Courses for Testing Purposes

To test courses, proceed as follows:

1. Log on to [https://lms.uzh.ch/](https://lms.uzh.ch/){target=_blank}
2. under Authoring open corresponding course
3. under Administration --> Export content (a .zip file will be downloaded)

    ![Export a course](attachments/Demoinstanz_import-export_01.png)

4. log on to [https://demo.olat.uzh.ch](https://demo.olat.uzh.ch){target=_blank}
5. under Authoring --> Import the exported .zip file

    ![Import course](attachments/Demoinstanz_import-export_02.png)

    ![Choose importfile](attachments/Demoinstanz_import-export_03.png)

6. the course with its possibly existing learning resources is now imported and available under the author area. **Important:** the course and its elements are not published and are only accessible to owners in this state.