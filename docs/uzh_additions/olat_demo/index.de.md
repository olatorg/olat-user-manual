# OLAT-Demo

Damit Sie die neuen Funktionen von OLAT 6 bereits vor dem Release testen können, finden Sie hier den Link zur [Demo-Instanz mit OLAT 6.0](https://demo.olat.uzh.ch){target="_blank"}.

Um von Ihnen früher erstellte OLAT-Kurse auf der Demo-Instanz zu testen, können Sie diese wie unten beschrieben exportieren und in die Demo-Instanz importieren.

## Anleitung: Import und Export von Kursen zu Testzwecken

Um Kurse zu testen, gehen Sie wie folgt vor:

1. auf [https://lms.uzh.ch/](https://lms.uzh.ch/){target=_blank} anmelden
2. unter Autorenbereich entsprechenden Kurs öffnen
3. unter Administration --> Inhalt exportieren (es wird eine .zip Datei heruntergeladen)

    ![Kurs-Exportieren](attachments/Demoinstanz_import-export_01.png){class="lightbox"}

4. auf [https://demo.olat.uzh.ch](https://demo.olat.uzh.ch){target=_blank} anmelden
5. unter Autorenbereich --> Importieren der exportierten .zip Datei

    ![Kurs-Importieren](attachments/Demoinstanz_import-export_02.png){class="lightbox"}

    ![Importdatei auswählen](attachments/Demoinstanz_import-export_03.png){class="lightbox"}

6. Der Kurs mit seinen evtl. vorhanden Lernressourcen ist nun importiert und unter dem Autorenbereich vorhanden. **Wichtig:** der Kurs und seine Elemente sind nicht publiziert und sind in diesem Zustand nur für Besitzer zugänglich.