---
ᴴₒᴴₒᴴₒ: true
hide:
  - toc
search:
  exclude: true
---
# OLAT-Dokumentation

Informationen betreffend OLAT, seiner Funktionen und neuesten Änderungen finden Sie hier. Benutzen Sie die Navigationsleiste im Kopfbereich der Seite, um zwischen den Bereichen [OpenOLAT Benutzerhandbuch](manual_user/), [UZH-Erweiterungen](uzh_additions/), [Release Notes](release_notes/) zu wechseln.

[Change to English version](/){ .md-button }

??? success "Registrierung bei OLAT"

    [Registrierung mit Switch edu-ID](uzh_additions/registration/registration_with_a_switch_edu-id/)

***

??? info "Dozierende - Quickstart für Autor\*innen"

    *Registrierte Benutzer\*innen mit der Rolle "Autor"*

    * [An- und Abmeldung](uzh_additions/registration/login_and_logout_in_olat)

    * [Autorenrechte beantragen](https://www.zi.uzh.ch/en/support/e-learning-and-examination/staff/olat.html)
    
    * [Kurse erstellen](manual_user/learningresources/Creating_Course/)

    * [Kurseditor verwenden](manual_user/learningresources/General_Configuration_of_Course_Elements/)

    * [Kurszugriff einstellen und überprüfen](manual_user/learningresources/Access_configuration/)

    * [Aufgaben-Bausteine verwenden](manual_user/learningresources/Course_Element_Task/)
    
    * [Tests, Selbsttests, Umfragen und Formulare](manual_user/learningresources/Test/)

    * [Dateien speichern](manual_user/personal_menu/Personal_folders/)

    * [Video verwenden](uzh_additions/example_of_use/using_video.de.md)

    [//]: # ( * Zoom Videokonferenzen (to do "Unterstützende Technologie") )

??? info "Studierende - Quickstart für registrierte Benutzer\*innen"

    *Registrierte Benutzer\*innen*

    [//]: # ( * Aufgabe abgeben )

    * [An- und Abmeldung](uzh_additions/registration/login_and_logout_in_olat/)

    * [Einen Kurs suchen und einsehen](manual_user/area_modules/Courses/)

    * [Registrierung mit einer Switch edu-ID](uzh_additions/registration/registration_with_a_switch_edu-id.de.md)

    * [Kursbausteine verwenden](manual_user/learningresources/Course_Elements/)

    * [Forum verwenden](manual_user/learningresources/Communication_and_Collaboration/#forum)

    [//]: # ( * Meine Kursliste verwenden )

    * [Eine Gruppe erstellen](manual_user/groups/Create_Groups/)

***

??? tip "Neue Features"

    *Wichtige Themen und neuste Updates*

    * [Kursbaustein "Seite"](manual_user/learningresources/Course_Element_Page.de.md)

    * [To-Dos](manual_user/basic_concepts/To_Dos_Basics.de.md)

    * [Ein Lehrvideo mit Kaltura in OLAT einfügen](uzh_additions/kaltura/index.de.md)

    * [Kurszyklus](manual_user/learningresources/General_Information/)

    * [Lehrvideos, Videokonferenzen und Live-Streaming](https://www.zi.uzh.ch/de/teaching-and-research/event-support.html){target=_blank}

    * [OLAT 6.0.0 (OpenOLAT 18.2) - June 2024](release_notes/OLAT-6.0.0.de.md)

***

!!! help "UZH intern"

    _Campuskurse und Semesterstartinformationen für die Universität Zürich_

    * [Campuskurse und Delegationen](uzh_additions/campus_course/)

    * [Semesterstart Checkliste für UZH-Dozierende](uzh_additions/semesterchecklist_for_lecturers/){target=_blank}<br />&nbsp;<br />

    _Online Prüfungen mit EPIS_
        
    * [Informationen für Dozierende](https://www.zi.uzh.ch/de/support/e-learning-and-examination/staff/online-exam.html){target=_blank}
    * [Informationen für Studierend](https://www.zi.uzh.ch/de/support/e-learning-and-examination/students/online-exams.html){target=_blank}

    [//]: # ( * Zoom Videokonferenzen (nur UZH) (to do Unterstützende Technologie) )

!!! help "Einsatz von OLAT an der UZH"

    * [In welchen Bereichen kann ich die UZH-Lernplattform OLAT in meiner Lehre einsetzen?](https://teachingtools.uzh.ch/de/tools/einsatz-von-olat-in-der-lehre){target=_blank}

***

## Aktuelle Version

Informationen zur aktuellen Version von OLAT finden Sie unter
- [OLAT Release Notes](release_notes/)

***

## OLAT-Support kontaktieren

Bei Login- oder Registrierungsproblemen kontaktieren Sie Bitte den [IT Service Desk](https://www.zi.uzh.ch/de/support.html) der UZH.

Für Beratungsanfragen zur Verwendung von OLAT kontaktieren Sie den [OLAT-Support](https://www.zi.uzh.ch/de/support/e-learning-and-examination/staff/olat.html).
