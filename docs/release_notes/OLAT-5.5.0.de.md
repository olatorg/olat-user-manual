# OLAT 5.5.0

*Released am 6. März 2024*

* Kursbausteine "Aufgabe" und "Gruppenaufgabe": 
    * In den Kursbausteinen "Aufgabe" und "Gruppenaufgabe" können Videoaufgaben erstellt werden. Kursbesitzer*innen können die Abgabe einer Videoaufgabe durch die Kursteilnehmenden konfigurieren.