# OLAT 4.7.0

*Released am 05.04.2023*

* Peer Review

    * Neuer Workflow-Schritt [«Self Review»](../../uzh_additions/peer_review) (formativ und summativ)
    * WYSIWYG-Editor für die Textfelder
    * Anzahl der zu erledigen Peer Reviews ist konfigurierbar 
    * Zuweisung der Experten durch den Kursbesitzer im Workflow-Schritt «Expert Review»
    * Weitere Verbesserungen und Bug fixes

* Literatur:

    * Neuer Kursbaustein [«Literatur»](../../uzh_additions/literature)

!!! info "9 neue Funktionen und Verbesserungen"

    OLAT 4.7.0 weist gegenüber der letzten Version 9 neue Funktionen und Verbesserungen auf.

!!! info "2 Bug Fixes"

    In OLAT 4.7.0 wurden 2 Bugs korrigiert.

