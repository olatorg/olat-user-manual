# OLAT 6.2.0

*Released on September 4, 2024*

* OLAT code update according to [OpenOlat 18.2.13](https://docs.openolat.org/de/release_notes/Release_notes_18.2/)
* Module "Campus courses":
    * Gendering of the labels in the delegations section
* Member search:
    * Authors can additionally see the institution, the matriculation number and the institutional e-mail address in the results list of the members search.
* OnlyOffice:
    * Update to OnlyOffice version 8.1 


!!! info "3 Improvements"

    OLAT 18.2.10-13 provide 3 improvements.

!!! info "24 Bug Fixes"

    In OLAT 18.2.10-13, 24 bugs have been fixed.