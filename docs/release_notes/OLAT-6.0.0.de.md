# OLAT 6.0.0

*Released am 25.6.2024*


**Inhaltsübersicht**

* [Übersicht der neuen Features (Auswahl)](#ubersicht-der-neuen-features-auswahl)
    * [Neuer Kursbaustein Seite](#neuer-kursbaustein-seite)
    * [To-dos](#to-dos)
    * [OpenBadges](#openbadges)
    * [Zusätzliche Dateitypen](#zusatzliche-dateitypen)
    * [Neuer Standard-Kurstyp](#neuer-standard-kurstyp)
    * [Neugestatung von Elementen der Benutzeroberfläche](#neugestaltung-von-elementen-der-benutzeroberflache)
    
* [Neue Struktur der OpenOlat-Dokumentation](#struktur-der-openolat-dokumentation)<br/>
* [Release Notes zu den OpenOlat Versionen 18.0, 18.1 und 18.2](#release-notes-zu-den-openolat-versionen-180-181-und-182)

<br />&nbsp;<br />

## Übersicht der neuen Features (Auswahl):

### Neuer Kursbaustein "Seite"
* Kursinhalte werden mit einem Blockeditor eingefügt, wobei verschiedene Typen von Blöcken zur Verfügung stehen. Medieninhalte können aus dem neuen Medien Center ausgewählt oder neu hochgeladen werden. Der bisherige Kursbaustein "Einzelne Seite" wurde zu "HTML-Seite" umbenannt und steht weiterhin zu Verfügung.<br />
[Handbuchseite: Kursbaustein "Seite"](../../manual_user/learningresources/Course_Element_Page/)

### To-dos
* Kursbesitzer können für einzelne oder alle Kursteilnehmenden Aufgaben als To-dos zuweisen.
* In Lernpfadkursen können To-dos automatisch aus den Teilschritten des Workflows im Kursbaustein Aufgabe erstellt werden.
* OLAT-User können für sich selbst To-dos erstellen und sich ihre To-dos über das persönliche Menü anzeigen lassen.<br />
[Handbuchseite: To-Dos](../../manual_user/basic_concepts/To_Dos_Basics/)

### OpenBadges
* Kursbesitzer können Badges (Abzeichen für erfüllte Aufgaben oder Leistungen) nach selbst festgelegten Kriterien manuell oder automatisch an Kursteilnehmende vergeben.<br />
[Handbuchseite: OpenBadges](../../manual_user/learningresources/OpenBadges/)

### Zusätzliche Dateitypen
* Überall dort, wo neue Dateien erstellt werden können, stehen seit der Aufschaltung von [OnlyOffice](../../uzh_additions/onlyoffice/) in [OLAT 5.6.0](OLAT-5.6.0.de.md) weitere Dateitypen zur Auswahl (.doc, .xlsx, .pptx).

### Neuer Standard-Kurstyp
* Voreingestellter Standard-Kurstyp beim Erstellten von neuen Kursen im Autorenbereich ist der "Kurs mit Lernfortschritt". Beim Erstellen von Campuskursen ist der Standardkurstyp wie bisher der Kurstyp "Herkömmlicher Kurs (Klassisch)".

### Neugestaltung von Elementen der Benutzeroberfläche
* Anstelle von Checkboxen zum Ein- und Ausschalten gewisser Optionen werden neu "Schalter"-Elemente eingesetzt (u.v.m.)

<hr />

## Struktur der OpenOlat-Dokumentation
* Die Struktur des [OpenOlat-Handbuchs](https://docs.openolat.org/de/manual_user/general/) wurde überarbeitet und wird in dieser Dokumentation im Bereich [OpenOLAT](../../manual_user) übernommen. Die Dokumentation der vom OLAT-Projekte-Team der UZH entwickelten Erweiterungen und der UZH-spezifischen Funktionen finden Sie weiterhin im Bereich [OLAT-UZH](../../uzh_additions/)

<hr />


## Release Notes zu den OpenOlat Versionen 18.0, 18.1 und 18.2: 

[OpenOlat 18.0 Release Notes](https://docs.openolat.org/de/release_notes/Release_notes_18.0/)<br />
[OpenOlat 18.1 Release Notes](https://docs.openolat.org/de/release_notes/Release_notes_18.1/)<br />
[OpenOlat 18.2 Release Notes](https://docs.openolat.org/de/release_notes/Release_notes_18.2/)
