# OLAT 4.1.0

*Released am 07.09.2022*

* Upgrade von OpenOlat 16.2.8 auf [OpenOlat 16.2.10](https://docs.openolat.org/de/release_notes/Release_notes_16.2/){target:_blank}

* Peer Review (Version 3.1)
 
    * Aufgabe (falls konfiguriert im Kursbaustein «Aufgabe») ist sichtbar in der Übersichtstabelle der Kursteilnehmer
    * Weitere Verbesserungen und Bug Fixes

* Umwandlung von SWITCHcast-Bausteinen in Kaltura-Bausteine

!!! info "35 neue Funktionen und Verbesserungen"

    OLAT 4.1.0 weist gegenüber der letzten Version 35 neue Funktionen und Verbesserungen auf.

!!! info "12 Bug Fixes"

    In OLAT 4.1.0 wurden 12 Bugs korrigiert.
