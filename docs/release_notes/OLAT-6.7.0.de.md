# OLAT 6.7.0

*Veröffentlicht am 5. Februar 2025*

* Update des OLAT-Codes auf [OpenOlat 18.2.19](https://docs.openolat.org/de/release_notes/Release_notes_18.2){target=_blank}
* Zutrittskontrolle:
    * Neuer Kursbaustein “Zutrittskontrolle”
* Programmieren in ACCESS:
    * Im Tab “Teilnehmer*innen” wird neu auch die Matrikelnummer angezeigt.
<br /><br />

!!! info "1 zusätzliche Verbesserung"

    OLAT 18.2.18-19 enthält 1 zusätzliche Verbesserung.

!!! info "8 Fehlerkorrekturen"

    In OLAT 18.2.18-19 wurden 8 Fehler korrigiert.
