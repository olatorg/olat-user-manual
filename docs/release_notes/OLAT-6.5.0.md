# OLAT 6.5.0

*Released on December 4, 2024*

* OLAT code update according to [OpenOlat 18.2.17](https://docs.openolat.org/release_notes/Release_notes_18.2){target=_blank}
* Campus course delegation (UZH):
    * In addition to the personal delegation for the creation and administration of campus courses, there is now also an organizational delegation. For this purpose, the OLAT role “learning resource manager is assigned to one or more institutes and allows the creation and administration of campus courses for these institutes.
* Mapping of the organizations (UZH):
    * Faculties and institutes of the UZH, which are linked to modules and/or courses according to the UZH course catalog, are now mapped in OLAT.


!!! info "1 additional Improvement"

    OLAT 18.2.16-17 provide 1 additional improvement.

!!! info "8 bug fixes"

    In OLAT 18.2.16-17, 8 bugs habe been fixed.