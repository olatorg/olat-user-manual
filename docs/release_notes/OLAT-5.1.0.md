# OLAT 5.1.0

*Released on Oktober 4, 2023*

* Course element "Literature"
    * Three new formatting options have been added:
        * Chicago Public Library (CPL)
	    * Science
	    * Proceedings of the National Academy of Sciences (PNAS)
    * In addition, there is now the ability to manually select formatting from a list.
	* When duplicating a course or course element, the imported literature lists are adopted.
	* Literature lists can be archived in the course.

* Course element "Peer Review"
    * In the workflow step "Peer Review" there are now 3 possibilities to make the assignment of the peer review:
        * Random assignment
        * Always assign the same task as the own task
        * Always assign the same task but not the own task

* WYSIWYG editor has been enhanced with additional formatting options

!!! info "21 Bug Fixes"

    In OLAT 5.1.0, 21 bugs have been fixed.