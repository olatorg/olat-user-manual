# OLAT 6.3.0

*Released on October 2, 2024*

* OLAT code update according to [OpenOlat 18.2.14](https://docs.openolat.org/release_notes/Release_notes_18.2/){target=_blank}
* Course element "Literature":
    * Accessibility improvements
* Question types "Kprim" and "True/false":
    * Background colors have been removed in both question types.
* Organisation selection:
    * Improvements in the selection of organisations.


!!! info "7 bug fixes"

    In OLAT 18.2.14, 7 bugs have been fixed.
