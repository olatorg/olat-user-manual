# OLAT 4.2.0

*Released on October 5, 2022*

* Upgrade from OpenOlat 16.2.10 to [OpenOlat 16.2.12](https://docs.openolat.org/release_notes/Release_notes_16.2/){target:_blank}

* Campus courses 
    * It is now possible to create multiple campus courses with the same title and the same course abbreviation. This is useful, for example, when multiple separate campus courses are needed to conduct exercises.

* Peer Review (Version 3.2)

    * Individually extend the deadline by which a course participant has to complete the calibration and/or peer review task
    * If the participants have to provide an explanation for their selected answer to a review question, you can define a character limit for this explanation.
    * Excel export of the peer review assignment table
    * A change history is available for the peer review activities of each participant.
    * Make the overall peer review results available to the course participants only at a later date
    * Upon adding the course element "Peer Review", the assessment option will be activated by default.
    * Further improvements and bug fixes

* Course element "Test"
    * The assessment strategy of Kprim tasks changed. If three statements are correctly answered and one is left blank, the Kprim question will be graded with half the points. 

!!! info "15 new Features and Improvements"

    OLAT 4.2.0 provides 15 new features and improvements compared to the last version.

!!! info "7 Bug Fixes"

    In OLAT 4.2.0, 7 bugs have been fixed.
