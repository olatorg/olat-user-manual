# OLAT 4.0.0

*Released am 16.08.2022*

* Usability, Menüführung und Tabellen

    * Tabs, Filtermenü und Suche
    * Massenaktionen
    * Visuelle Verbesserungen

* Neue Features für Kurse

    * Optimierte Kursbaustein-Vorschau
    * Neue Metadatenfelder für Kursbausteine
    * Individuelles Kurslayout
    * Erinnerungen am Kursbaustein
    * Zusätzliche Empfänger
    * Weitere Verbesserungen

* Neue Features im Kurseditor

    * Verbesserungen Toolbar
    * Neue Features im Lernpfad

* Neue Features im Bereich eAssessment

    * Chat Funktion während einer Prüfung
    * Fragetypen neue Optionen
    * Neue Features im Bewertungswerkzeug

* Lebenszyklus für Gruppen und Benutzer
* Support Ende QTI 1.2
* UZH Änderungen:  Campuskurse als Lernpfad-Kurs erstellen

!!! info "113 neue Funktionen und Verbesserungen"

    OLAT 4.0.0 weist gegenüber der letzten Version 113 neue Funktionen und Verbesserungen auf.

!!! info "16 Bug Fixes"

    In OLAT 4.0.0 wurden 16 Bugs korrigiert.
