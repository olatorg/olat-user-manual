# OLAT 4.3.0

*Released am 02.11.2022*

* Upgrade von OpenOlat 16.2.12 auf [OpenOlat 16.2.13](https://docs.openolat.org/de/release_notes/Release_notes_16.2/){target:_blank}

* Login 
    * Umstellung des Login von SWITCHaai auf SWITCH edu-ID

* Peer Review

    * Excel-Export der Peer-Review-Daten
    * Die E-Mail-Adressen der Kursteilnehmer werden auf der Detailseite angezeigt.
    * Weitere Verbesserungen und Bug Fixes

* Kurse
    * Beim Export bzw. Import eines Kurses, der Prüfungsmodus-Konfigurationen enthält, werden die Secure-Exam-Browser-Einstellungen mit berücksichtigt (nur für Kurse, die mit OLAT-Versionen ab 4.3.0 erstellt wurden)

* Sicherheit
    * Update der Text-Bibliothek (Projekt "Apache Commons") 

!!! info "15 neue Funktionen und Verbesserungen"

    OLAT 4.3.0 weist gegenüber der letzten Version 15 neue Funktionen und Verbesserungen auf.

!!! info "2 Bug Fixes"

    In OLAT 4.3.0 wurden 2 Bugs korrigiert.

