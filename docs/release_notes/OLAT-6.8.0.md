# OLAT 6.8.0

*Released on March 12, 2025*

* OLAT code update according to [OpenOlat 18.2.20](https://docs.openolat.org/release_notes/Release_notes_18.2){target=_blank}
* Assessment tool, CE Assessment, CE Test, CE Practice
    * The matriculation number is now also displayed to course owners and coaches in the "Participants" tab. (select the gearwheel symbol and add the column "Matriculation number")
<br /><br />


!!! info "8 bug fixes"

    In OLAT 18.2.20, 3 bugs habe been fixed.
    