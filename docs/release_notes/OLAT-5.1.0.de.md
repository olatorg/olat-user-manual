# OLAT 5.1.0

*Released am 4. Oktober 2023*

* Kursbaustein "Literatur"
    * Es wurden drei neue Formatierungsoptionen hinzugefügt:
        * Chicago Public Library (CPL)
	    * Science
	    * Proceedings of the National Academy of Sciences (PNAS)
    * Zudem gibt es nun die Möglichkeit, manuell eine Formatierung aus einer Liste auszuwählen.
	* Beim Duplizieren eines Kurses oder Kursbausteins werden die importierten Literaturlisten übernommen.
	* Literaturlisten können im Kurs archiviert werden.

* Kursbaustein "Peer Review"
    * Im Workflow-Schritt "Peer Review" gibt es nun 3 Möglichkeiten um die Zuweisung des Peer Reviews zu machen:
        * Zufällige Zuweisung
        * Immer die gleiche Aufgabe wie die eigene Aufgabe zuweisen
        * Immer die gleiche Aufgabe aber nicht die eigene Aufgabe zuweisen 

* Der WYSIWYG-Editor wurde mit zusätzlichen Formatierungsoptionen erweitert

!!! info "21 Bug Fixes"

    In OLAT 5.1.0 wurden 21 Bugs korrigiert.