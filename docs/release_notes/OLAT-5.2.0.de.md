# OLAT 5.2.0

*Released am 1. November 2023*

* Kursbaustein "Literatur"
    * Neu kann eine bestehende Literaturliste umbenannt und editiert werden (Button "Literaturliste editieren").
    * Eine Literaturliste kann nun gedruckt werden, wobei dies auch für einen PDF-Export benutzt werden kann (Button "Drucken").
    * Die Benutzerberechtigungen wurden erweitert, so dass Kursbetreuer:innen und Kursteilnehmer:innen jetzt separat fürs Importieren, Editieren und Löschen berechtigt werden können.
    * Im Kurseditor kann die neue Funktion "Literaturrecherche" aktiviert werden. Diese erlaubt es, eine Suche in einer Bibliothek direkt in OLAT zu starten (standardmässig ist die Bibliothek der Universität Zürich hinterlegt).

!!! info "4 neue Funktionen und Verbesserungen"

    OLAT 5.2.0 weist gegenüber der letzten Version 4 neue Funktionen und Verbesserungen auf.

!!! info "8 Bug Fixes"

    In OLAT 5.2.0 wurden 8 Bugs korrigiert.