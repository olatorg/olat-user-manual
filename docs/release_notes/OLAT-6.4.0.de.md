# OLAT 6.4.0

*Veröffentlicht am 6. November 2024*

* Update des OLAT-Codes auf [OpenOlat 18.2.15](https://docs.openolat.org/de/release_notes/Release_notes_18.2){target=_blank}
* Verbesserung der Lesbarkeit (Accessibility) an diversen Stellen durch:
    * Erhöhung der Kontraste
    * Verwendung von UZH-Farben und UZH-Font als Basis des Designs
* Persönliche Werkzeuge:
    * Der bisherige Eintrag “Meine Medien” wurde in “Kaltura” umbenannt, um ihn besser vom Eintrag “Medien Center” unterscheiden zu können.

!!! info "4 Fehlerkorrekturen"

    In OLAT 18.2.15 wurden 4 Fehler korrigiert.