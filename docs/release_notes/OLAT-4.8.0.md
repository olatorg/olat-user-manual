# OLAT 4.8.0

*Released on May 3, 2023*

* Course element "Video"

    * Inserting external links to video resources (Youtube, Vimeo, etc.)

!!! info "4 new Features and Improvements"

    OLAT 4.8.0 provides 4 new features and improvements compared to the last version.

!!! info "2 Bug Fixes"

    In OLAT 4.8.0, 2 bugs have been fixed.
