# OLAT 4.9.0

*Released am 07.06.2023*

* Kursbaustein "Kaltura"

    * Kaltura-Content wird besser dargestellt
    

!!! info "3 neue Funktionen und Verbesserungen"

    OLAT 4.9.0 weist gegenüber der letzten Version 3 neue Funktionen und Verbesserungen auf.<br />
    - Ausbau des PeerReview Moduls<br />
    - Ausbau des Literatur Moduls

!!! info "1 Bug Fix"

    In OLAT 4.9.0 wurde 1 Bug korrigiert.