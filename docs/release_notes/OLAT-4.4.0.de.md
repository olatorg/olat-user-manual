# OLAT 4.4.0

*Released am 07.12.2022*

* Upgrade von OpenOlat 16.2.13 auf [OpenOlat 16.2.16](https://docs.openolat.org/de/release_notes/Release_notes_16.2/){target:_blank}

* Peer Review

    * Optimierung der Status- und Fortschrittsanzeige in der Übersichtstabelle für Kursbesitzer
    * Weitere Verbesserungen und Bug Fixes

!!! info "15 neue Funktionen und Verbesserungen"

    OLAT 4.4.0 weist gegenüber der letzten Version 15 neue Funktionen und Verbesserungen auf.

!!! info "7 Bug Fixes"

    In OLAT 4.4.0 wurden 2 Bugs korrigiert.

