# OLAT 6.7.0

*Released on February 5, 2025*

* OLAT code update according to [OpenOlat 18.2.19](https://docs.openolat.org/release_notes/Release_notes_18.2){target=_blank}
* Entry control:
    * New Course Element “Entry control”
* Programming in ACCESS:
    * The matriculation number is now also displayed in the "Participants" tab.
<br /><br />

!!! info "1 additional Improvement"

    OLAT 18.2.18-19 provide 1 additional improvement.

!!! info "8 bug fixes"

    In OLAT 18.2.18-19, 8 bugs habe been fixed.