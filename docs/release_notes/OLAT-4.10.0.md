# OLAT 4.10.0

*Released on July 5, 2023*

!!! info "3 Bug Fixes"

    In OLAT 4.10.0, 3 bugs have been fixed.