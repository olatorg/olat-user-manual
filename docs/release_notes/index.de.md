# OLAT Release Notes
[OLAT 6.8.0 Release Notes](OLAT-6.8.0.md)

[OLAT 6.7.0 Release Notes](OLAT-6.7.0.md)

[OLAT 6.6.0 Release Notes](OLAT-6.6.0.md)

[OLAT 6.5.0 Release Notes](OLAT-6.5.0.md)

[OLAT 6.4.0 Release Notes](OLAT-6.4.0.md)

[OLAT 6.3.0 Release Notes](OLAT-6.3.0.md)

[OLAT 6.2.0 Release Notes](OLAT-6.2.0.md)

[OLAT 6.1.0 Release Notes](OLAT-6.1.0.md)

[OLAT 6.0.0 Release Notes](OLAT-6.0.0.md)

[OLAT 5.6.0 Release Notes](OLAT-5.6.0.md)

[OLAT 5.5.0 Release Notes](OLAT-5.5.0.md)

[OLAT 5.4.0 Release Notes](OLAT-5.4.0.md)

[OLAT 5.3.0 Release Notes](OLAT-5.3.0.md)

[OLAT 5.2.0 Release Notes](OLAT-5.2.0.md)

[OLAT 5.1.0 Release Notes](OLAT-5.1.0.md)

[OLAT 5.0.0 Release Notes](OLAT-5.0.0.md)

[OLAT 4.10.0 Release Notes](OLAT-4.10.0.md)

[OLAT 4.9.0 Release Notes](OLAT-4.9.0.md)

[OLAT 4.8.0 Release Notes](OLAT-4.8.0.md)

[OLAT 4.7.0 Release Notes](OLAT-4.7.0.md)

[OLAT 4.6.0 Release Notes](OLAT-4.6.0.md)

[OLAT 4.5.0 Release Notes](OLAT-4.5.0.md)

[OLAT 4.4.0 Release Notes](OLAT-4.4.0.md)

[OLAT 4.3.0 Release Notes](OLAT-4.3.0.md)

[OLAT 4.2.0 Release Notes](OLAT-4.2.0.md)

[OLAT 4.1.0 Release Notes](OLAT-4.1.0.md)

[OLAT 4.0.0 Release Notes](OLAT-4.0.0.md)
