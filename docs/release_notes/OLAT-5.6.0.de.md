# OLAT 5.6.0

*Released am 17. April 2024*

Update des OLAT-Codes auf OpenOlat 17.2.20

* Verwendung der Matrikelnummer (mit und ohne Bindestrich) als Identifikator bei einer Massenbewertung.

* Kursbaustein "Dokument":
    * Neu kann der Kursbaustein "Dokument" verwendet werden. Mit diesem können verschiedene Dokumentformate direkt sichtbar in einen Kurs eingebunden werden. Es können sowohl bereits vorliegende Dateien verwendet als auch neue Dateien hochgeladen oder erstellt werden.

* Dokumenteneditor "OnlyOffice":
    * Neu kann der Dokumenteneditor "OnlyOffice" verwendet werden. Mit diesem Editor können Dokumente einzeln oder gemeinsam bearbeitet werden. Unterstützt werden Dateiformate zur Textverarbeitung, Tabellenkalkulation und Präsentationen. Die Dokumente werden zur Ansicht und zur Bearbeitung in einem separaten Fenster/Tab geöffnet.
    * In folgenden Kursbausteinen kann OnlyOffice verwendet werden:
        * Dokument
        * Ordner
        * Aufgabe
        * Gruppenaufgabe
        * Teilnehmer\*innen Ordner
    * **Wichtige Hinweise:** Nach dem Schliessen des OnlyOffice-Fensters/Tabs dauert es bis zu 10 Sekunden, bis die Änderungen in OLAT verfügbar sind. Bei kollaborativer Bearbeitung des Dokumentes ist die Schliessung des letzten OnlyOffice-Fensters/Tabs relevant.
