# OLAT 5.2.0

*Released on November 1, 2023*

* Course element "Literature"
    * An existing literature list can now be renamed and edited (button "Edit literature list").
    * A literature list can now be printed, whereby this can also be used for a PDF export (button "Print").
    * The user permissions have been extended so that course coaches and course participants can now be authorized separately for printing, editing and deleting.
    * In the course editor the new function "Literature search" can be activated. This allows to start a search in a library directly in OLAT (by default the library of the University of Zurich is stored).

!!! info "4 new features and improvements"

    OLAT 5.2.0 provides 4 new features and improvements compared to the last version.

!!! info "8 Bug Fixes"

    In OLAT 5.2.0 8 bugs have been fixed.