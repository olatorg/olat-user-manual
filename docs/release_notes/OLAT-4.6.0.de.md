# OLAT 4.6.0

*Released am 01.03.2023*

* Peer Review

    * Weitere Verbesserungen und Bug Fixes

!!! info "28 neue Funktionen und Verbesserungen"

    OLAT 4.6.0 weist gegenüber der letzten Version 28 neue Funktionen und Verbesserungen auf.

!!! info "7 Bug Fixes"

    In OLAT 4.6.0 wurden 7 Bugs korrigiert.

