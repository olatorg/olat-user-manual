# OLAT 5.4.0

*Released am 7. Februar 2024*

Update des OLAT-Codes auf OpenOlat 17.2.19

* Kursbaustein "Programmieren in ACCESS": 
    * Die Aufgabenzuteilungen werden anders sortiert, die neueste wird als erste angezeigt.
    * Es werden initial nur die Aufgabenzuteilungen angezeigt. Diese können einzeln auf- und zugeklappt werden, um die einzelnen Aufgaben anzuzeigen resp. auszublenden.
 
* Kursbaustein "Kaltura":
    * Videos im Kursbaustein "Kaltura" sind auch dann noch abrufbar, wenn ein OLAT-Kurs den Status "Beendet" hat.
    * Das Hinzufügen eines Hintergrundbildes im Kursbaustein "Kaltura" ist nun möglich.
 
* Massenbewertung:
    * Die Massenbewertung kann mit der Matrikelnummer (ohne Bindestrich) als Identifikator ausgeführt werden.
 
* Kursbaustein "(Gruppen)Aufgabe":
    * Die Verlängerung der Frist im Kursbaustein "(Gruppen)Aufgabe" wird nun im Änderungsverlauf angezeigt.
 
* Kursbaustein "Struktur":
    * Im Kursbaustein "Struktur" kann mit der neuen Darstellungsoption "Automatische Übersicht ohne Beschreibung der untergeordneten Kursbausteine" die Sichtbarkeit der untergeordneten Kursbausteine besser gesteuert werden, sodass die Kursteilnehmenden eine übersichtlichere Kursübersicht bekommen.

* Prüfungsmodus:
    * Der Reload der Prüfungsmodi-Übersichtsseite wurde optimiert.
    
* Fragetyp "Freitext":
    * Die Autosave-Funktion funktioniert jetzt auch mit der Rich Text Formatierung.


!!! info "1 weiterer Bug korrigiert"

    In OLAT 5.4.0 wurde ein weiterer Bug korrigiert.