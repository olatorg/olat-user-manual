# OLAT 4.8.0

*Released am 03.05.2023*

* Kursbaustein "Video"

    * Einfügen von externen Links zu Videoressourcen (Youtube, Vimeo etc.)

!!! info "4 neue Funktionen und Verbesserungen"

    OLAT 4.8.0 weist gegenüber der letzten Version 4 neue Funktionen und Verbesserungen auf.

!!! info "2 Bug Fixes"

    In OLAT 4.8.0 wurden 2 Bugs korrigiert.

