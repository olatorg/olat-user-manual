# OLAT 6.8.0

*Veröffentlicht am 12. März 2025*

* Update des OLAT-Codes auf [OpenOlat 18.2.20](https://docs.openolat.org/de/release_notes/Release_notes_18.2){target=_blank}
* Bewertungswerkzeug sowie KB Bewertung, KB Test und KB Übung:
    * Im Tab “Teilnehmer\*innen” können sich Kursbesitzer\*innen und Kursbetreuer*innen neu auch die Matrikelnummer anzeigen lassen (über das Zahnradsymbol die Spalte "Matrikelnummer" auswählen).
<br /><br />

!!! info "3 Fehlerkorrekturen"

    In OLAT 18.2.20 wurden 3 Fehler korrigiert.
