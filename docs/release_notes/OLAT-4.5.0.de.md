# OLAT 4.5.0

*Released am 01.02.2023*

* Upgrade von OpenOlat 16.2.16 auf [OpenOlat 16.2.18](https://docs.openolat.org/de/release_notes/Release_notes_16.2/){target:_blank}

* Peer Review

    * Neuer Workflow-Schritt [«Expert Review»](https://docs.olat.uzh.ch/de/uzh_additions/peer_review/) (formativ und summativ)
    * Erweiterungen und Optimierungen in der Übersichtstabelle über die Teilnehmer für die Kursbesitzer und -betreuer
    * Weitere Verbesserungen und Bug Fixes

!!! info "49 neue Funktionen und Verbesserungen"

    OLAT 4.5.0 weist gegenüber der letzten Version 49 neue Funktionen und Verbesserungen auf.

!!! info "12 Bug Fixes"

    In OLAT 4.5.0 wurden 12 Bugs korrigiert.

