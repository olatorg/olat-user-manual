# OLAT 4.3.0

*Released on November 2, 2022*

* Upgrade from OpenOlat 16.2.12 to [OpenOlat 16.2.13](https://docs.openolat.org/release_notes/Release_notes_16.2/){target:_blank}

* Login 
    * The login to OLAT is now using SWITCH edu-ID in place of SWITCHaai.

* Peer Review

    * Excel export of all peer review data
    * The course participants' email addresses are displayed on the details page.
    * Further improvements and bug fixes

* Courses
    * When exporting or importing a course that contains exam mode configurations, the Secure Exam Browser settings will be included as well (only for courses created with OLAT versions 4.3.0 and higher).

* Security
    * Update of the text library (project "Apache Commons") 

!!! info "15 new Features and Improvements"

    OLAT 4.3.0 provides 15 new features and improvements compared to the last version.

!!! info "2 Bug Fixes"

    In OLAT 4.3.0, 2 bugs have been fixed.
