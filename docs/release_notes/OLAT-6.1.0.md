# OLAT 6.1.0

*Released on July 17, 2024*

OLAT code update according to [OpenOlat 18.2.9](https://docs.openolat.org/de/release_notes/Release_notes_18.2/)

!!! info "4 Improvements"

    OLAT 18.2.9 provides 4 improvements.

!!! info "12 Bug Fixes"

    In OLAT 18.2.9, 12 bugs have been fixed.