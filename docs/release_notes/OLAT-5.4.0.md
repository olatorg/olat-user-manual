# OLAT 5.4.0

*Released on February 7, 2024*

OLAT code update according to OpenOlat 17.2.19

* Course element "Programming in ACCESS": 
    * The task assignments are sorted differently, the newest is displayed first.
    * Only the task assignments are initially displayed. These can be expanded and collapsed individually to show or hide the individual tasks.
 
* Course element "Kaltura":
    * Videos in the course element "Kaltura" can still be accessed even if an OLAT course has the status "Finished".
    * It is now possible to add a background image in the layout settings of the course element.
 
* Bulk assessment:
    * Bulk assessment can be performed with the matriculation number (without hyphens) as identifier.
 
* Course element "(Group)Task":
    * The extension of the deadline in the course element "(group)Task" is now displayed in the change history.
 
* Course element "Structure":
    * In the course element "Structure", the new display option "Automatic overview without description of the underlying course elements", can be used to better control the visibility of subordinate course elements so that course participants get a clearer course overview.

* Assessment mode:
    * The reload of the assessment modes overview page has been optimized.
    
* Question type "Essay" 
    * The autosave function now also works with rich text formatting.


!!! info "1 further bug fix"

    In OLAT 5.4.0 1 further bug has been fixed.