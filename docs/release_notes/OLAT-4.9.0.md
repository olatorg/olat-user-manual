# OLAT 4.9.0

*Released on June 7, 2023*

* Course element "Kaltura"

    * Kaltura content is better displayed.

!!! info "3 new Features and Improvements"

    OLAT 4.9.0 provides 4 new features and improvements compared to the last version<br />
    - improvements in the PeerReview Module<br />
    - improvements in the Literature Module 

!!! info "1 Bug Fixe"

    In OLAT 4.9.0, 1 bug has been fixed.