# OLAT 5.5.0

*Released on March 6, 2024*

* "Task" and "Group task" course elements:
    * Video tasks can be created in the "Task" and "Group task" course elements. Course owners can configure the submission of a video task by course participants.