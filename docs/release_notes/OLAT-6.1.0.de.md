# OLAT 6.1.0

*Released am 17. Juli 2024*

Update des OLAT-Codes auf [OpenOlat 18.2.9](https://docs.openolat.org/release_notes/Release_notes_18.2/)

!!! info "4 Verbesserungen"

    OLAT 18.2.9 enthält 4 Verbesserungen.

!!! info "12 Fehlerkorrekturen"

    In OLAT 18.2.9 wurden 12 Fehler korrigiert.