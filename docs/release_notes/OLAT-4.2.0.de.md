# OLAT 4.2.0

*Released am 05.10.2022*

* Upgrade von OpenOlat 16.2.10 auf [OpenOlat 16.2.12](https://docs.openolat.org/de/release_notes/Release_notes_16.2/){target:_blank}

* Campuskurse 
    * Neu können mehrere Campuskurse mit gleichem Titel und gleichem Lehrveranstaltungskürzel erstellt werden. Dies ist z.B. dann nützlich, wenn für Übungen mehrere separate Campuskurse benötigt werden.

* Peer Review (Version 3.2)

    * Frist für die Kalibrierung und/oder das Peer Review der einzelnen Kursteilnehmenden verlängern
    * Zeichenbegrenzung bei der Begründung einer Review-Frage ist möglich
    * Excel-Export der Zuweisungstabelle des Peer Reviews
    * Änderungsverlauf für jeden Teilnehmer
    * Gesamtbewertung erst später für die Kursteilnehmer veröffentlichen
    * Beim Hinzufügen des Kursbausteins «Peer Review» wird die Bewertung standardmässig aktiviert
    * Weitere Verbesserungen und Bug Fixes

* Kursbaustein "Test"
    * Die Bewertung von Kprim-Aufgaben hat sich geändert. Wenn drei Aussagen korrekt beantwortet werden und eine leer gelassen wird, erfolgt die Bewertung der Kprim-Frage mit halber Punktzahl. 

!!! info "15 neue Funktionen und Verbesserungen"

    OLAT 4.2.0 weist gegenüber der letzten Version 15 neue Funktionen und Verbesserungen auf.

!!! info "7 Bug Fixes"

    In OLAT 4.2.0 wurden 7 Bugs korrigiert.
