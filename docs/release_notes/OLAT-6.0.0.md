# OLAT 6.0.0

*Released on June 25, 2024*

**Table of contents**

* [Overview of new features (selection)](#übersicht-der-neuen-features-auswahl)
    * [New course element "Page"](#new-course-element-page)
    * [To-dos](#to-dos)
    * [OpenBadges](#openbadges)
    * [Additional file types](#additional-file-types)
    * [New standard course type](#new-standard-course-type)
    * [Redesign of user interface elements](#redesign-of-user-interface-elements)
    
* [New structure of the OpenOlat documentation](#structure-of-the-openolat-documentation)<br/>
* [Release Notes for OpenOlat versions 18.0, 18.1 and 18.2](#release-notes-for-the-openolat-versions-180-181-and-182)

<br />&nbsp;<br />

## Overview of the new features (selection):

### New course element "Page"
* Course content is inserted using a block editor, with various types of blocks available. Media content can be selected from the new Media Center or uploaded. The previous course element "Single page" has been renamed "HTML page" and is still available.<br />
[Documentation: Course element "Page"](../../manual_user/learningresources/Course_Element_Page/)

### To-dos
* Course owners can assign tasks as to-dos for individual or all course participants.
* In learning path courses, to-dos can be created automatically from the sub-steps of the workflow in the course element "Task".
* OLAT users can create to-dos for themselves and have their to-dos displayed via the personal menu.<br />
[Documentation: To-Dos](../../manual_user/basic_concepts/To_Dos_Basics/)

### OpenBadges
* Course owners can award badges (badges for completed tasks or achievements) to course participants manually or automatically according to criteria they define themselves.
[Documentation: OpenBadges](../../manual_user/learningresources/OpenBadges/)

### Additional file types
* Since the activation of [OnlyOffice](../../uzh_additions/onlyoffice/) in [OLAT 5.6.0](OLAT-5.6.0.md), additional file types (.doc, .xlsx, .pptx) can be selected wherever new files can be created.

### New standard course type
* The default course type when creating new courses in the authoring area is "Course with learning progress". When creating campus courses, the default course type is still the course type "Conventional course (classic)".

### Redesign of user interface elements
* Instead of checkboxes for switching certain options on and off, "switch" elements are now used (and much more)

<hr />

## Structure of the OpenOlat documentation
* The structure of the [OpenOlat-Manual](https://docs.openolat.org/manual_user/general/) has been revised and will be adopted in this documentation in the [OpenOLAT](../../manual_user) section. The documentation of the extensions developed by the UZH OLAT project team and the UZH-specific functions can still be found in the [OLAT-UZH](../../uzh_additions/) section.

<hr />


## Release Notes for the OpenOlat versions 18.0, 18.1 and 18.2: 

[OpenOlat 18.0 Release Notes](https://docs.openolat.org/release_notes/Release_notes_18.0/)<br />
[OpenOlat 18.1 Release Notes](https://docs.openolat.org/release_notes/Release_notes_18.1/)<br />
[OpenOlat 18.2 Release Notes](https://docs.openolat.org/release_notes/Release_notes_18.2/)