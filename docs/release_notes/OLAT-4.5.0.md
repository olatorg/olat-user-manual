# OLAT 4.5.0

*Released on February 1, 2023*

* Upgrade from OpenOlat 16.2.16 to [OpenOlat 16.2.18](https://docs.openolat.org/release_notes/Release_notes_16.2/){target:_blank}

* Peer Review

    * New workflow step ["Expert Review"](https://docs.olat.uzh.ch/uzh_additions/peer_review/) (formative and summative)
    * Additions and optimizations to the overview table of participants available for course owners and coaches
    * Further improvements and bug fixes

!!! info "49 new Features and Improvements"

    OLAT 4.5.0 provides 49 new features and improvements compared to the last version.

!!! info "12 Bug Fixes"

    In OLAT 4.5.0, 12 bugs have been fixed.
