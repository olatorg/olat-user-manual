# OLAT 5.0.0

*Released am 22. August 2023*

* Freigabe von Kursen (Angebote und Buchungen)
    * Mehrere Angebote (Buchungsmethoden) pro Kurs möglich
    * Für jedes Angebot kann die Organisationseinheiten, die Buchungsmethode und ein Zeitraum eingestellt werden. 

* Kurs mit Lernfortschritt
    * Beim Erstellen eines neuen Kurses kann als Kursdesign "Kurs mit Lernfortschritt" gewählt werden (Typ: Lernpfad ohne Reihenfolge).

* Kursübersicht
    * Im neuen Menüpunkt "In Vorbereitung" werden Kurse mit Status "Vorbereitung" aufgelistet.

* Kursbaustein "Übung" (neu)
    * Fragen aus dem Fragepool können im Lernkarteiprinzip geübt werden.

* Kursbausteine (Anpassungen)
    * KB Aufgabe
        * automatische Zuteilung von Betreuenden zu Teilnehmenden
        * Konfigurationsmöglichkeit "Verspätete Abgabe"
    * Formular-Editor: wurde überarbeitet
    * Teilnehmerordner: Es kann eine Ordnerstruktur definiert werden, welche für alle Teilnehmenden gilt.

* Teilnehmeransicht
    * Kursbesitzer können ohne zugewiesene Rolle "Teilnehmer" über die Schaltfläche "Rolle" in die Teilnehmeransicht wechseln

* Test
    * Bestätigung per E-Mail nach Testabschluss
    * Daten zurücksetzen lädt vorher automatisch die vorhandenen Resultate herunter
    * neuer Fragetyp: Lückentext mit Dropdown (Auswahlliste)

* Video Optionen
    * KB Aufgabe mit Video als Aufgabe, Abgabe und Musterlösung
    * KB Video mit Import via URL
    * Video-Aufgabe basierend auf Lernressource Video (Aufgabenerstellung im integrierten Videoeditor)

* UZH-Erweiterungen
    * ACCESS / stats4all

