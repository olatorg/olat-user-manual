# OLAT 6.5.0

*Veröffentlicht am 4. Dezember 2024*

* Update des OLAT-Codes auf [OpenOlat 18.2.17](https://docs.openolat.org/de/release_notes/Release_notes_18.2){target=_blank}
* Campuskurs-Delegation (UZH):
    * Neben der personengebundenen Delegation für die Erstellung und Administrierung von Campuskursen gibt es neu auch eine organisationsgebunde Delegation. Dafür wird die OLAT-Rolle "Lernressourcenverwalter*in" für ein oder mehrere Institute vergeben und erlaubt, die Campuskurse für diese Institute anzulegen und zu administrieren.
* Abbildung der Organisationen (UZH):
    * Fakultäten und Institute der UZH, welche mit Modulen und/oder Kursen gemäss UZH Vorlesungsverzeichnis verknüpft sind, werden neu in OLAT abgebildet.


!!! info "1 zusätzliche Verbesserung"

    OLAT 18.2.16-17 enthält 1 zusätzliche Verbesserung.

!!! info "8 Fehlerkorrekturen"

    In OLAT 18.2.16-17 wurden 8 Fehler korrigiert.
