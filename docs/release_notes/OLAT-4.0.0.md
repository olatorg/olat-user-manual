# OLAT 4.0.0

*Released on August 16, 2022*

* Usability, menu navigation and tables

    * Tabs, filter menu and search
    * Mass actions
    * Visual enhancements

* New features for courses 

    * Optimised course element preview
    * New metadata fields for course elements
    * Individual course layout
    * Reminders for the course element
    * Additional recipients
    * Further improvements
    * Improvements Toolbar
    * New features in the learning path

* New features in eAssessment

    * Chat function during an exam
    * New options question types
    * New features in the assessment tool

* Life cycle for groups and users
* Support End QTI 1.2
* UZH Changes: Create campus courses as learning path courses

!!! info "113 new Features and Improvements"

    OLAT 4.0.0 provides 113 new features and improvements compared to the last version.

!!! info "16 Bug Fixes"

    In OLAT 4.0.0, 16 bugs have been fixed.
