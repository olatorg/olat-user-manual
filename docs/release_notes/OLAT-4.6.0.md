# OLAT 4.6.0

*Released on March 1, 2023*

* Peer Review

    * Further improvements and bug fixes

!!! info "28 new Features and Improvements"

    OLAT 4.6.0 provides 28 new features and improvements compared to the last version.

!!! info "7 Bug Fixes"

    In OLAT 4.6.0, 7 bugs have been fixed.
