# OLAT 4.1.0

*Released on September 7, 2022*

* Upgrade from OpenOlat 16.2.8 to [OpenOlat 16.2.10](https://docs.openolat.org/release_notes/Release_notes_16.2/){target:_blank}

* Peer Review (Version 3.1)
 
    * Task (if configured in the course element "Task") appears in the overview table showing all course participants
    * Further improvements and bug fixes

* Conversion of SWITCHcast course elements into Kaltura course elements

!!! info "35 new Features and Improvements"

    OLAT 4.1.0 provides 35 new features and improvements compared to the last version.

!!! info "12 Bug Fixes"

    In OLAT 4.1.0, 12 bugs have been fixed.
