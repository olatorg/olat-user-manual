# OLAT 5.3.0

*Released am 6. Dezember 2023*

Update des OLAT-Codes auf OpenOlat 17.2.18

* Kursbaustein "Literatur":
    * Beheben eines Redscreens beim Editieren von importierten Literaturlisten mit einer zusätzlichen Formatierung
    * Das Hinzufügen eines Hintergrundbildes im Kursbaustein ist nun möglich

* Kursbaustein "Peer Review":
    * Update-Problem: als Kursteilnehmer wird nach dem Bestätigen des Schrittes "Peer Review" nun sofort der Schritt "Feedback aus Reviews ..." angezeigt
    * Usability: nach dem Erledigen eines Reviews gibt es nun einen Zurück-Knopf
    * Update-Problem: wenn ich als Betreuer einen Expert Review erledigt habe, wird der Status im Tab "Alle Teilnehmer" sofort upgedatet
    * Usability: bei der Anzeige der Review Fragen in Read-only kann zurück zur Übersicht gewechselt werden
    * Beim Excel-Export werden Datenschutzbedingungen hinzugefügt
    * Beim Verlängern der Kalibrierung wird der Status entsprechend upgedatet
    * Korrekte Validierung der Review Fragen
    * Das Kopieren, Duplizieren, Konvertieren, Importieren und Exportieren der Review Kriterien ist nun auch möglich
    * Label- und Textverbesserungen
    * Verbesserung beim Verlängern der Fristen: zusätzliche Meldung, wenn keine Abgabe stattgefunden hat
    * Labels bei der Konfiguration für die Bewertung wurden verbessert
    * Anzeige der Experten für die Kursbesitzer*innen
    * Anzeige der Punktzahl bei den Antwortmöglichkeiten für die Kursbesitzer*innen
    * Terminübersicht: Auflistung der Fristen für die Kursbesitzer*innen
    * Anzeige der Abgabe beim Expert Review
    * Feedback aus Reviews zur abgegebenen Arbeit: passender Status bei verpasster Frist
    * Validierung der Bewertungsstrategie in Prozenten beim nachträglichen Hinzufügen von weiteren Workflow-Schritten
    * Beheben eines Redscreens bei der Defaulteinstellung der Bewertung der Kalibrierung
    * Das Ändern der Bewertungsstrategie kann abgebrochen werden
    * Anzeige des Bewertungsintervalls und Verbesserung der Konfiguration bei der Bewertung der Qualität des Peer Reviews
    * Visueller Statuswechsel im Kursbaustein "Peer Review" bei Abgabe der Arbeit im Kursbaustein "Aufgabe"
    * Das Hinzufügen eines Hintergrundbildes im Kursbaustein ist nun möglich

* Campuskurse:
    * Bei einer grossen Menge an (Campus-)Kursen werden die Wartezeiten bei der Erstellung der Kurse stark reduziert

* Kursbaustein "Programmieren in ACCESS":
    * Gendern der Labels
    * Das Hinzufügen eines Hintergrundbildes im Kursbaustein ist nun möglich
    

!!! info "1 weitere Verbesserung"

    OLAT 5.3.0 weist gegenüber der letzten Version eine weitere Verbesserung auf.

!!! info "7 Bug Fixes"

    In OLAT 5.3.0 wurden 7 Bugs korrigiert.