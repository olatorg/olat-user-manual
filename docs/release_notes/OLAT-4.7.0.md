# OLAT 4.7.0

*Released on April 5, 2023*

* Peer Review

    * New workflow step ["Self Review"](../../uzh_additions/peer_review) (formative and summative)
    * WYSIWYG editor for text fields
    * Number of peer reviews to be completed is configurable 
    * Assignment of experts by the course owner in the workflow step "Expert Review" 
    * Further improvements and bug fixes

* Literature:

    * New course element ["Literature"](../../uzh_additions/literature)


!!! info "9 new Features and Improvements"

    OLAT 4.7.0 provides 9 new features and improvements compared to the last version.

!!! info "2 Bug Fixes"

    In OLAT 4.7.0, 2 bugs have been fixed.
