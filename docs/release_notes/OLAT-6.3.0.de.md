# OLAT 6.3.0

*Veröffentlicht am 2. Oktober 2024*

* Update des OLAT-Codes auf [OpenOlat 18.2.14](https://docs.openolat.org/de/release_notes/Release_notes_18.2/){target=_blank}
* Kursbaustein "Literatur":
    * Verbesserungen der Accessibility
* Fragetypen "Kprim" und "True/false":
    * Die Hintergrundfarben bei den beiden Fragetypen wurden entfernt.
* Auswahl einer Organisation:
    * Verbesserungen bei der Auswahl einer Organisation


!!! info "7 Fehlerkorrekturen"

    In OLAT 18.2.14 wurden 7 Fehler korrigiert.
