# OLAT 5.0.0

*Released on August 22, 2023*

 * Course Settings: Share (offers and bookings)
    * Multiple offers (booking methods) per course possible
    * For each offer, the organizational unit, the booking methods and a time period can be set. 

* Course with learning progress
    * When creating a new course, "Course with learning progress" can be selected as course design (type: learning path without sequence).

* Course overview
    * In the new menu item "In preparation" courses with status "Preparation" are listed.

* Course element "Exercise (new)
    * Questions from the question bank can be practiced using the flashcard principle.

* Course elements (adaptations)
    * CE Task
        * automatic assignment of coaches to participants
        * Configuration option "Late submission"
    * Form editor: has been revised
    * Participant folder: A folder structure can be defined which is valid for all participants.

* Participant view
    * Course owners can switch to the participant view without assigned role "Participant" via the button "Role"

* Test
    * Confirmation by e-mail after the test is completed
    * Reset data automatically downloads existing results beforehand
    * new question type: gap text with dropdown (selection list)

* Video options
    * CE task with video as task, submission and sample solution
    * CE video with import via URL
    * Video task based on learning resource video (task creation in the integrated video editor)

* UZH Additions
    * ACCESS / stats4all
