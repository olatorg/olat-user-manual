# OLAT 5.6.0

*Released on April 17, 2024*

OLAT code update according to OpenOlat 17.2.20

* The matriculation number (with and without hyphen) can be used as an identifier for a bulk assessment.
* Course element "Document":
    * The "Document" course element can now be used. This can be used to display the content of various document formats directly in a course. Existing files can be used and new files can be uploaded or created.
* Document editor "OnlyOffice":
    * The "OnlyOffice" document editor can now be used. This editor can be used to edit documents individually or collectively. File formats for word processing, spreadsheets and presentations are supported. The documents are opened in a separate window/tab for viewing and editing.
    * OnlyOffice can be used in the following course elements:
        * Document
        * Folder
        * Task
        * Group task
        * Participant folder
    * **Please note:** After closing the OnlyOffice window/tab, it takes up to 10 seconds for the changes to be available in OLAT. For collaborative editing of the document, the closing of the last OnlyOffice window/tab is relevant.