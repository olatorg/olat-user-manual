# OLAT 6.2.0

*Released am 4. September 2024*

* Update des OLAT-Code auf [OpenOlat 18.2.13](https://docs.openolat.org/de/release_notes/Release_notes_18.2/)
* Modul "Campuskurse":
    * Gendern der Labels bei den Delegationen
* Mitgliedersuche:
    * Autor*innen sehen neu zusätzlich die Institutionszugehörigkeit, die Matrikelnummer sowie die Institutions-E-Mail-Adresse in der Resultateliste der Mitgliedersuche.
* OnlyOffice:
    * Update von OnlyOffice auf die Version 8.1 


!!! info "3 Verbesserungen"

    OLAT 18.2.10-13 enthält 3 Verbesserungen.

!!! info "24 Fehlerkorrekturen"

    In OLAT 18.2.10-13 wurden 24 Fehler korrigiert.
