# OLAT 5.3.0

*Released on December 6, 2023*

OLAT code update according to OpenOlat 17.2.18

* Course element "Literature"
    * Fixed a red screen when editing imported literature lists with additional formatting
    * Adding a background image in the course element is now possible

* Course element "Peer Review"
    * Update problem: as a course participant, the step "Feedback from reviews ..." is now displayed immediately after confirming the step "Peer review"
    * Usability: after completing a review, there is now a back button
    * Update problem: when I have completed an expert review as a coach, the status in the "All participants" tab is updated immediately
    * Usability: when displaying the review questions in Read Only, you can switch back to the overview
    * Privacy statements are added to the Excel export
    * When extending the calibration, the status is updated accordingly
    * Correct validation of the review questions
    * Copying, duplicating, converting, importing and exporting review criteria is now also possible
    * Label and text improvements
    * Improvement when extending deadlines: additional message if no submission has taken place
    * Labels in the configuration for the evaluation have been improved
    * Display of experts to course owners
    * Display of the number of points for the answer options to course owners
    * Overview of deadlines: listing of deadlines for course owners
    * Display of submission for the Expert Review
    * Feedback from reviews on submitted work: appropriate status if deadline is missed
    * Validation of the assessment strategy in percentages when adding further workflow steps at a later date
    * Fixed a red screen in the default setting of the calibration assessment
    * Changing the evaluation strategy can be canceled
    * Display of the assessment interval and improvement of the configuration in the assessment for the quality of the peer review
    * Visual status change in the course element "Peer Review" when submitting the work in the course element "Assignment"
    * Adding a background image in the course element is now possible

* Campus courses:
    * With a large number of (campus) courses, the waiting time for creating the courses is greatly reduced

* Course element "Programming in ACCESS":
    * Gendering the labels
    * It is now possible to add a background image to the course element

!!! info "1 further improvement"

    OLAT 5.3.0 has one further improvement compared to the last version.

!!! info "7 bug fixes"

    In OLAT 5.3.0 7 bugs have been fixed.