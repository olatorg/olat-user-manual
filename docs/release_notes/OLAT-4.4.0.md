# OLAT 4.4.0

*Released on December 7, 2022*

* Upgrade from OpenOlat 16.2.13 to [OpenOlat 16.2.16](https://docs.openolat.org/release_notes/Release_notes_16.2/){target:_blank}

* Peer Review

    * Optimization of the status and progress notification displayed in the overview table (for course owners)
    * Further improvements and bug fixes

!!! info "15 new Features and Improvements"

    OLAT 4.4.0 provides 15 new features and improvements compared to the last version.

!!! info "7 Bug Fixes"

    In OLAT 4.4.0, 7 bugs have been fixed.
