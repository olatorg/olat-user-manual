---
ᴴₒᴴₒᴴₒ: true
hide:
  - toc
search:
  exclude: true
---
# OLAT Documentation

Find information on OpenOlat, its features and latest changes here. Use the top navigation to switch between [OpenOLAT User manual](manual_user/), [UZH Additions](uzh_additions/) and [Release Notes](release_notes/).

[Zur Deutschen Version wechseln](/de/){ .md-button }

??? success "Registration with OLAT"

    [Registration with Switch edu-ID](uzh_additions/registration/registration_with_a_switch_edu-id/)

***

??? note "Lecturer - Quickstart for authors"

    *System users with the role "Author"*

    * [Login and logout](uzh_additions/registration/login_and_logout_in_olat)

    * [Apply for author rights](https://www.zi.uzh.ch/en/support/e-learning-and-examination/staff/olat.html)
    
    * [Creating courses](manual_user/learningresources/Creating_Course/)

    * [Using the course editor](manual_user/learningresources/General_Configuration_of_Course_Elements/)

    * [Setting and checking the course access](manual_user/learningresources/Access_configuration/)

    * [Using of the task course element](manual_user/learningresources/Course_Element_Task/)
    
    * [Tests, self-tests and surveys](manual_user/learningresources/Test/)

    * [Saving files](manual_user/personal_menu/Personal_folders/)

    * [Using video](uzh_additions/example_of_use/using_video.md)

    [//]: # ( * Zoom video conferences (to do "Unterstützende Technologie") )

??? info "Student - Quickstart for system users"

    *System users*

    [//]: # ( * Submit task )

    * [Login and logout](uzh_additions/registration/login_and_logout_in_olat)

    * [Finding and organizing courses](manual_user/area_modules/Courses/)

    * [Registration with SWITCH edu ID](uzh_additions/registration/registration_with_a_switch_edu-id.md)

    * [Using course elements](manual_user/learningresources/Course_Elements/)

    * [Using the forum](manual_user/learningresources/Communication_and_Collaboration/#forum)

    [//]: # ( * Using the course list )

    * [Creating a group](manual_user/groups/Create_Groups/)

***

??? tip "New Features"

    *Important topics and newest updates*

    * [Course element "Page"](manual_user/learningresources/Course_Element_Page.md)

    * [To-Dos](manual_user/basic_concepts/To_Dos_Basics.md)

    * [Displaying videos with Kaltura in OLAT](uzh_additions/kaltura/index.md)

    * [Course cycle](manual_user/learningresources/General_Information/)

    * [Recordings, video conference and live streaming](https://www.zi.uzh.ch/en/teaching-and-research/event-support.html){target=_blank}

    * [OLAT 6.0.0 (OpenOLAT 18.2) - June 2024](release_notes/OLAT-6.0.0.md)

***

!!! help "UZH internal"

    *Campus courses and semester start information for University of Zürich*

    * [Campus courses and delegations](uzh_additions/campus_course/)

    * [Semester start checklist for UZH lecturers](uzh_additions/semesterchecklist_for_lecturers/){target=_blank}<br />&nbsp;<br />

    _Online exams with EPIS:_

    * [Information for instructors](https://www.zi.uzh.ch/en/support/e-learning-and-examination/staff/online-exams.html){target=_blank}
    * [Information for students](https://www.zi.uzh.ch/en/support/e-learning-and-examination/students/online-exams.html) 

    [//]: # ( * Zoom video conferences (UZH only) (to do Unterstützende Technologie) )

!!! help "Use of OLAT at UZH"

    * [In which areas can I use the UZH learning platform OLAT in my teaching?](
    https://teachingtools.uzh.ch/en/tools/einsatz-von-olat-in-der-lehre){target=_blank}

***

## Current version

You can find information on the actual version of OLAT under
- [OLAT Release Notes](release_notes/)

***

## Contact OLAT Support

In case of problems with login or registraation, please contact the [IT Service Desk of UZH](https://www.zi.uzh.ch/en/support.html)

If you wish a consultation about the use of OLAT, please contact [OLAT Support](https://www.zi.uzh.ch/en/support/e-learning-and-examination/staff/olat.html).
